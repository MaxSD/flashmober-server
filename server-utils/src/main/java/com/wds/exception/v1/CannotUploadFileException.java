package com.wds.exception.v1;

public class CannotUploadFileException extends BaseException {

    public CannotUploadFileException() {
        super();
        this.code = 500;
        this.localCode = -1;
        this.message = "Failed. Cannot upload file!";
    }
}
