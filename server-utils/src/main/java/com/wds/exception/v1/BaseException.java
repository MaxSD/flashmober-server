package com.wds.exception.v1;

@SuppressWarnings("WeakerAccess")
public abstract class BaseException extends Exception {

    protected int code;
    protected int localCode;
    protected String message;

    protected BaseException() {
        super();
    }

    public int getLocalCode() { return localCode; }
    public int getCode() {
        return code;
    }
    public String getMessage() {
        return message;
    }
}
