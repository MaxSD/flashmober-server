package com.wds.exception.v1;

public class PushNotificationException extends BaseException {

    public PushNotificationException() {
        super();
        this.code = 500;
        this.localCode = -2;
        this.message = "Failed sending push notification";
    }
}
