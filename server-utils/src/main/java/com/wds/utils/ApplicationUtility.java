package com.wds.utils;

import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.CannotUploadFileException;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.TimeZone;

public class ApplicationUtility {

    public static final String DATE_FORMAT = "dd.MM.yyyy";

    public static String bcrypt(final String secret) {
        return BCrypt.hashpw(secret, BCrypt.gensalt());
    }
    public static String md5crypt(final String secret) {
        return MD5Util.md5Custom(secret);
    }
    public static long getCurrentTimeStamp() { return System.currentTimeMillis() / 1000; }

    public static long getCurrentTimeStampGMT_0() {
        TimeZone timeZone = TimeZone.getTimeZone("GMT+0");
        Calendar calendar = Calendar.getInstance(timeZone);

        return calendar.getTimeInMillis() / 1000;
    }

    public static long getDateInSeconds() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTimeInMillis() / 1000;
    }

    public static long getDateInSeconds(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTimeInMillis() / 1000;
    }

    public static long getDateInSecondsGMT_0() {
        TimeZone timeZone = TimeZone.getTimeZone("GMT+0");
        Calendar calendar = Calendar.getInstance(timeZone);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTimeInMillis() / 1000;
    }

    public static long getDateInSecondsGMT_0(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        //calendar.set(Calendar.HOUR, 0);
        //calendar.set(Calendar.MINUTE, 0);
        //calendar.set(Calendar.SECOND, 0);
        //calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTimeInMillis() / 1000;
    }

    public static Date getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }

    public static Date getNextDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }

    public static String encodeStringForAjaxResponse(String strUTF8) {
        try {
            return new String(strUTF8.getBytes("UTF-8"), "ISO-8859-1");
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

    public static void writeToFile(InputStream uploadedInputStream, String uploadedFileLocation) throws BaseException {
        try {
            int read;
            byte[] bytes = new byte[1024];

            OutputStream out = new FileOutputStream(new File(uploadedFileLocation));
            while ((read = uploadedInputStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            out.flush();
            out.close();
        } catch (IOException e) {
            throw new CannotUploadFileException();
        }
    }

    public static int generateRandomInt(int min, int max) throws BaseException {
        return (1111 + (int)(Math.random() * ((9999 - 1111) + 1)));
    }

    public static String generateRandomString(int count) throws BaseException {
        return RandomStringUtils.random(count, true, true);
    }

    public static String secondsToFormattedString(long seconds, String format) throws BaseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(seconds * 1000);
    }

    public static String millisecondsToFormattedString(long seconds, String format) throws BaseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(seconds);
    }

    public static Date formattedStringToDate(String dateTime, String format) {
        Date result = null;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
            result = simpleDateFormat.parse(dateTime);
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        return result;
    }

    public static boolean hasRole(String role)  throws BaseException {
        Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>)
                SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        boolean hasRole = false;
        for (GrantedAuthority authority : authorities) {
            hasRole = authority.getAuthority().equals(role);
            if (hasRole) {
                break;
            }
        }
        return hasRole;
    }
}
