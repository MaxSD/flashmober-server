package com.wds.utils.define;

import com.wds.utils.push.apple.ApplePushModeEnum;

public class Defines {

    // Notifications
    public final static String NOTIFICATION_ERROR_PUSH = "pushError";
    public final static String NOTIFICATION_ERROR_SMS = "smsError";
    public final static String NOTIFICATION_ERROR_EMAIL = "emailError";

    public final static int NOTIFICATION_TYPE_COMMENT = 1;
    public final static int NOTIFICATION_TYPE_COMMENT_USERS = 2;
    public final static int NOTIFICATION_TYPE_LIKE = 3;
    public final static int NOTIFICATION_TYPE_FRIENDSHIP = 4;
    public final static int NOTIFICATION_TYPE_NEW_FLASHMOB = 5;

    // SMTP
    public final static String SMTP_HOST = "mail.nic.ru";
    public final static String SMTP_PORT = "587";
    public final static String SMTP_USER = "noreply@flashmober.me";
    public final static String SMTP_PASSWORD = "15FBN99n";
    public final static boolean SMTP_AUTH = true;

    public static boolean DEBUG_MODE = true;
    public static ApplePushModeEnum PUSH_MODE = ApplePushModeEnum.ADHOCK;

    public static void setServerMode(boolean serverMode) {
        DEBUG_MODE = true;
    }

    public static String getPushNotificationPath() {
        if (DEBUG_MODE) {
            if (PUSH_MODE == ApplePushModeEnum.ADHOCK) return DebugDefines.PUSH_ADHOCK_CERTIFICATE_PATH;
            if (PUSH_MODE == ApplePushModeEnum.PRODUCTION) return DebugDefines.PUSH_PRODUCTION_CERTIFICATE_PATH;
        } else {
            if (PUSH_MODE == ApplePushModeEnum.ADHOCK) return ReleaseDefines.PUSH_ADHOCK_CERTIFICATE_PATH;
            if (PUSH_MODE == ApplePushModeEnum.PRODUCTION) return ReleaseDefines.PUSH_PRODUCTION_CERTIFICATE_PATH;
        }
        return DebugDefines.PUSH_PRODUCTION_CERTIFICATE_PATH;
    }

    public static String getPushNotificationPassword() {
        if (DEBUG_MODE) {
            if (PUSH_MODE == ApplePushModeEnum.ADHOCK) return DebugDefines.PUSH_ADHOCK_CERTIFICATE_PASSWORD;
            if (PUSH_MODE == ApplePushModeEnum.PRODUCTION) return DebugDefines.PUSH_PRODUCTION_CERTIFICATE_PASSWORD;
        } else {
            if (PUSH_MODE == ApplePushModeEnum.ADHOCK) return ReleaseDefines.PUSH_ADHOCK_CERTIFICATE_PASSWORD;
            if (PUSH_MODE == ApplePushModeEnum.PRODUCTION) return ReleaseDefines.PUSH_PRODUCTION_CERTIFICATE_PASSWORD;
        }
        return DebugDefines.PUSH_PRODUCTION_CERTIFICATE_PASSWORD;
    }

    public static boolean getDebugServerFlag() {
        return PUSH_MODE != ApplePushModeEnum.ADHOCK;
    }

    public static String getXslPathStorage() {
        return DEBUG_MODE ? DebugDefines.XSL_PATH_STORAGE : ReleaseDefines.XSL_PATH_STORAGE;
    }

    public static String getUsersPhotoPathStorage() {
        return DEBUG_MODE ? DebugDefines.USERS_PHOTO_PATH_STORAGE : ReleaseDefines.USERS_PHOTO_PATH_STORAGE;
    }


}
