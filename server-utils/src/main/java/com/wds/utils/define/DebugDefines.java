package com.wds.utils.define;

public class DebugDefines {
    public static final String  XSL_PATH_STORAGE =                      "/var/www/wd/data/www/flashmober.webdevelopers.su/xsl";
    public static final String  USERS_PHOTO_PATH_STORAGE =              "/var/www/wd/data/www/flashmober.webdevelopers.su/images/";
    //public static final String  USERS_PHOTO_PATH_STORAGE =              "/home/Projects/Flashmober/images/";

    public static final String  PUSH_ADHOCK_CERTIFICATE_PATH     =      "/var/www/wd/data/www/flashmober.webdevelopers.su/certificate/apns-development.jks";
    public static final String  PUSH_ADHOCK_CERTIFICATE_PASSWORD =      "flashmoberPush";

    public static final String  PUSH_PRODUCTION_CERTIFICATE_PATH =      "/var/www/wd/data/www/flashmober.webdevelopers.su/certificate/apns-production.jks";
    //public static final String  PUSH_PRODUCTION_CERTIFICATE_PATH =      "/home/Projects/Flashmober/certificate/apns-production.jks";
    public static final String  PUSH_PRODUCTION_CERTIFICATE_PASSWORD =  "flashmoberPush";
}
