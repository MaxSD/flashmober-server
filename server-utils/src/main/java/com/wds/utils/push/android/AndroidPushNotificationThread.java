package com.wds.utils.push.android;

import java.util.List;

public class AndroidPushNotificationThread extends Thread {

    private List<String> tokens;
    private String messageText;

    public AndroidPushNotificationThread(List<String> tokens, String messageText) {
        this.tokens = tokens;
        this.messageText = messageText;
    }

    public void run() {
        try {
            AndroidPushNotification androidPushNotification = new AndroidPushNotification();
            androidPushNotification.sendNotification(tokens, messageText);
        } catch (Throwable ex) {
            System.out.println("Android push notification exception. " + ex.getMessage());
        }
    }
}
