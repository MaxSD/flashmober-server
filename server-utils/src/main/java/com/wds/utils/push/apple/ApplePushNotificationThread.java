package com.wds.utils.push.apple;

import com.wds.utils.define.Defines;
import javapns.communication.ConnectionToAppleServer;
import javapns.devices.Devices;
import javapns.notification.AppleNotificationServerBasicImpl;
import javapns.notification.PushNotificationManager;
import javapns.notification.PushNotificationPayload;
import org.apache.log4j.BasicConfigurator;

import java.util.List;

public class ApplePushNotificationThread extends Thread {

    private List<String> tokens;
    private String messageText;
    private int notificationType;
    private long userFlashmobId;

    public ApplePushNotificationThread(List<String> tokens, String messageText, int notificationType, long userFlashmobId) {
        this.tokens = tokens;
        this.messageText = messageText;
        this.notificationType = notificationType;
        this.userFlashmobId = userFlashmobId;
    }

    public void run() {
        try {
            BasicConfigurator.configure();
            PushNotificationManager pushManager = new PushNotificationManager();
            pushManager.initializeConnection(new AppleNotificationServerBasicImpl(Defines.getPushNotificationPath(), Defines.getPushNotificationPassword(),
                    ConnectionToAppleServer.KEYSTORE_TYPE_JKS, Defines.getDebugServerFlag()));
            PushNotificationPayload payload = PushNotificationPayload.alert(messageText);
            payload.addSound("default");
            payload.addCustomDictionary("notificationType", notificationType);
            payload.addCustomDictionary("userFlashmobId", userFlashmobId);
            pushManager.sendNotifications(payload, Devices.asDevices(tokens));
        } catch (Throwable ex) {
            System.out.println("Apple push notification exception. " + ex.getMessage());
        }
    }
}
