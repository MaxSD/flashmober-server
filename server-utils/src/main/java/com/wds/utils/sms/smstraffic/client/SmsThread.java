package com.wds.utils.sms.smstraffic.client;

import java.util.ArrayList;
import java.util.List;

public class SmsThread extends Thread {

    List<String> phoneList = new ArrayList<>(0);
    String messageText;
    boolean isRusLanguage = true;

    public SmsThread(List<String> phoneList, String messageText, boolean isRusLanguage) {
        this.phoneList = phoneList;
        this.messageText = messageText;
        this.isRusLanguage = isRusLanguage;
    }

    public void run() {
        try {
            for (String phone : phoneList) {
                if (isRusLanguage)
                    Sms.sendRus(phone, messageText);
                else
                    Sms.sendLat(phone, messageText);
            }
        } catch (Exception ex) {
            System.out.println("SMS notification exception. " + ex.getMessage());
        }
    }
}
