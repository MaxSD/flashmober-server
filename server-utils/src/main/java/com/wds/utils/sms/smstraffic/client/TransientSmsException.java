package com.wds.utils.sms.smstraffic.client;

public class TransientSmsException extends SmsException
{
	public TransientSmsException(String msg) 
	{
		super(msg);
	}
}
