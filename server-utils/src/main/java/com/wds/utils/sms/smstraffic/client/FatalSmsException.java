package com.wds.utils.sms.smstraffic.client;

public class FatalSmsException extends SmsException
{
	public FatalSmsException(String msg) 
	{
		super(msg);
	}
}
