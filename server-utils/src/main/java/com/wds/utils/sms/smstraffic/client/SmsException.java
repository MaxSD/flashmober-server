package com.wds.utils.sms.smstraffic.client;

public class SmsException extends Exception
{
	public SmsException(String msg) 
	{
		super(msg);
	}
}
