package com.wds.scheduleTasks;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class ScheduleTask {

    public static final long PERIOD_DAILY = 1000 * 60 * 60 * 24;

    public static void runTask(TimerTask task, Date startDate, long period) {
        Timer timer = new Timer(true);
        timer.scheduleAtFixedRate(task, startDate, period);
        System.out.println("Timer task '"+task.getClass().getName()+"' will be started at: " + startDate);
    }
}
