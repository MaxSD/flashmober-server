﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<%@include file="/WEB-INF/jspf/checkBrowser.jspf" %>
<html>
<head>
    <%@include file="/WEB-INF/jspf/headSection.jspf" %>
</head>
<body>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<div class="container i-line">
    <div class="row">
        <%@include file="/WEB-INF/jspf/messages.jspf" %>
        <ul>
            <sec:authorize access="hasRole('ROLE_ADMIN')">
                <li></li>
            </sec:authorize>

            <sec:authorize access="hasRole('ROLE_MANAGER')">
            </sec:authorize>

            <sec:authorize access="hasRole('ROLE_USER')">
            </sec:authorize>
        </ul>
    </div>
</div>

<%@include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>
