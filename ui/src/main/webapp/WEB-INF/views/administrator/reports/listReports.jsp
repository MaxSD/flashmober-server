<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<%@include file="/WEB-INF/jspf/checkBrowser.jspf" %>
<html>
<head>
    <%@include file="/WEB-INF/jspf/headSection.jspf" %>
</head>
<body>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<div class="container i-line">
    <div class="row">
        <div id="messages">
            <%@include file="/WEB-INF/jspf/messages.jspf" %>
        </div>

        <div class="col-md-10 col-md-offset-1">
            <h4 style="color: #8b0000"><b><spring:message code="Lbl.Reports"/></b></h4>

            <c:if test="${currentUser.role.id == 1 || currentUser.role.id == 2}">
                <div style="margin: 15px 0;">
                    <button id="btnAllUsersReports" class="btn btn-primary" type="button" onclick="showTab(1);">
                        Все пользователи <span class="badge">${allReportsCount}</span>
                    </button>
                    <button id="btnBusinessUsersReports" class="btn btn-default" type="button" onclick="showTab(2);">
                        Бизнес пользователи <span class="badge">${businessUsersReportsCount}</span>
                    </button>
                </div>
            </c:if>

            <form id="filter" name="folter" method="post"
                  action="${pageContext.request.contextPath}/administrator/reports/exportReportsToFile">

                <input id="activeTab" name="activeTab" type="hidden" value="1"/>

                <fieldset style="text-align: center;">
                    <legend><spring:message code="Lbl.Filter"/></legend>

                    <div class="row">
                        <div class="form-inline">
                            <div class="form-group" style="align-content: center;">
                                <spring:message code="Lbl.User"/>:&nbsp;
                                <input id="userNameFilter" name="userNameFilter" class="form-control"/>
                                &nbsp;&nbsp;&nbsp;
                                <spring:message code="Lbl.Task"/>:&nbsp;
                                <input id="flashmobDescriptionFilter" name="flashmobDescriptionFilter" class="form-control"/>
                                &nbsp;&nbsp;&nbsp;
                                <button id="btnFilter" name="btnFilter" class="btn btn-info" type="button"
                                        onclick="tableReload();">
                                    <spring:message code="Btn.Filter"/>
                                </button>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>

            <div id="all-users-reports">
                <form id="listAllUsersReportsForm" name="listAllUsersReportsForm" method="post">
                    <table id="allUsersReportsTable" class="display" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <!--<th>Photo</th>-->
                            <th><spring:message code="Lbl.ID"/></th>
                            <th><spring:message code="Lbl.Photo"/></th>
                            <th><spring:message code="Lbl.User"/></th>
                            <th><spring:message code="Lbl.Report"/></th>
                            <th><spring:message code="Lbl.Task"/></th>
                            <th><spring:message code="Lbl.Likes"/></th>
                            <th><spring:message code="Lbl.Date"/></th>
                            <th><spring:message code="Lbl.Action"/></th>
                        </tr>
                        </thead>

                        <tfoot>
                        <tr>
                            <th><spring:message code="Lbl.ID"/></th>
                            <th><spring:message code="Lbl.Photo"/></th>
                            <th><spring:message code="Lbl.User"/></th>
                            <th><spring:message code="Lbl.Report"/></th>
                            <th><spring:message code="Lbl.Task"/></th>
                            <th><spring:message code="Lbl.Likes"/></th>
                            <th><spring:message code="Lbl.Date"/></th>
                            <th><spring:message code="Lbl.Action"/></th>
                        </tr>
                        </tfoot>
                    </table>
                </form>
            </div>

            <div id="business-users-reports" style="display: none;">
                <form id="listBusinessUsersReportsForm" name="listBusinessUsersReportsForm" method="post">
                    <table id="businessUsersReportsTable" class="display" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <!--<th>Photo</th>-->
                            <th><spring:message code="Lbl.ID"/></th>
                            <th><spring:message code="Lbl.UserPhoto"/></th>
                            <th><spring:message code="Lbl.User"/></th>
                            <th><spring:message code="Lbl.Report"/></th>
                            <th><spring:message code="Lbl.Task"/></th>
                            <th><spring:message code="Lbl.Likes"/></th>
                            <th><spring:message code="Lbl.Date"/></th>
                            <th><spring:message code="Lbl.Action"/></th>
                        </tr>
                        </thead>

                        <tfoot>
                        <tr>
                            <th><spring:message code="Lbl.ID"/></th>
                            <th><spring:message code="Lbl.UserPhoto"/></th>
                            <th><spring:message code="Lbl.User"/></th>
                            <th><spring:message code="Lbl.Report"/></th>
                            <th><spring:message code="Lbl.Task"/></th>
                            <th><spring:message code="Lbl.Likes"/></th>
                            <th><spring:message code="Lbl.Date"/></th>
                            <th><spring:message code="Lbl.Action"/></th>
                        </tr>
                        </tfoot>
                    </table>
                </form>
            </div>

            <br/>

            <div>

            </div>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf" %>

<script>
    var allUsersReportsTable;
    var businessUsersReportsTable;

    function showTab(activeTab) {
        var tab1 = $("#all-users-reports");
        var tab2 = $("#business-users-reports");

        var btnAllUsersReports = $("#btnAllUsersReports");
        var btnBusinessUsersReports = $("#btnBusinessUsersReports");

        if (activeTab == 1) {
            btnAllUsersReports.attr("class", "btn btn-primary");
            btnBusinessUsersReports.attr("class", "btn btn-default");

            tab2.hide();
            tab1.show();

            $("#activeTab").val('1');
        } else if (activeTab == 2) {
            btnAllUsersReports.attr("class", "btn btn-default");
            btnBusinessUsersReports.attr("class", "btn btn-primary");

            tab1.hide();
            tab2.show();

            $("#activeTab").val('2');
        }
    }

    function tableReload() {
        if ($("#all-users-reports").is(":visible")) {
            allUsersReportsTable.ajax.reload();
        } else if ($("#business-users-reports").is(":visible")) {
            businessUsersReportsTable.ajax.reload();
        }
    }

    $(document).ready(function () {
        allUsersReportsTable = $('#allUsersReportsTable').DataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "bSort": false,
            "ajax": {
                <c:choose>
                <c:when test="${currentUser.role.id == 1 || currentUser.role.id == 2}">
                "url": "${pageContext.request.contextPath}/administrator/reports/get_all_users_reports_JSON",
                </c:when>
                <c:otherwise>
                "url": "${pageContext.request.contextPath}/administrator/reports/${currentUser.id}/get_my_tasks_reports_JSON",
                </c:otherwise>
                </c:choose>
                "type": "POST",
                "data": function (d) {
                    d.userNameFilter = $("#userNameFilter").val();
                    d.flashmobDescriptionFilter = $("#flashmobDescriptionFilter").val();
                }
            },
            "language": {
                "processing": "Загрузка данных...",
                "search": "Поиск:",
                "lengthMenu": "Показать _MENU_ записей",
                "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                "infoEmpty": "Записи с 0 до 0 из 0 записей",
                "infoFiltered": "(отфильтровано из _MAX_ записей)",
                "infoPostFix": "",
                "loadingRecords": "Загрузка записей...",
                "zeroRecords": "Записи отсутствуют.",
                "emptyTable:": "В таблице отсутствуют данные",
                "paginate": {
                    "first": "Первая",
                    "previous": "Предыдущая",
                    "next": "Следующая",
                    "last": "Последняя"
                }
            }
        });

        businessUsersReportsTable = $('#businessUsersReportsTable').DataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "bSort": false,
            "ajax": {
                "url": "${pageContext.request.contextPath}/administrator/reports/get_business_users_reports_JSON",
                "type": "POST",
                "data": function (d) {
                    d.userNameFilter = $("#userNameFilter").val();
                    d.flashmobDescriptionFilter = $("#flashmobDescriptionFilter").val();
                }
            },
            "language": {
                "processing": "Загрузка данных...",
                "search": "Поиск:",
                "lengthMenu": "Показать _MENU_ записей",
                "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                "infoEmpty": "Записи с 0 до 0 из 0 записей",
                "infoFiltered": "(отфильтровано из _MAX_ записей)",
                "infoPostFix": "",
                "loadingRecords": "Загрузка записей...",
                "zeroRecords": "Записи отсутствуют.",
                "emptyTable:": "В таблице отсутствуют данные",
                "paginate": {
                    "first": "Первая",
                    "previous": "Предыдущая",
                    "next": "Следующая",
                    "last": "Последняя"
                }
            }
        });

        $("#btnFilter").click(function () {
            //
        })
    });

    /**
     * Delete flashmob
     */
    var reportId;
    function deleteReportReason(_reportId) {
        reportId = _reportId;

        jQueryAjax.ajaxGET_HTML(jQueryAjax.getServerName() +
                "<c:url value="/administrator/reports/delete_report_reason"/>",
                {}, deleteReasonCallBack, true, null, 'ajaxError');
    }
    function deleteReasonCallBack(data) {
        jQueryAjax.loadPageToDialog("Удаление отчета", data, {
            lockUser: {
                label: "Удалить",
                className: "btn-danger",
                callback: function () {
                    deleteReport();
                    return false;
                }
            }
        });
    }
    function deleteReport() {
        jQueryAjax.ajaxPOST_HTML("${pageContext.request.contextPath}/administrator/reports/" + reportId + "/delete_report_ajax",
                {
                    deleteReason: $("#deleteReason").val(),
                    otherDeleteReason: $("#otherDeleteReason").val()
                },
                deleteCallback, true, "messages", null);
        bootbox.hideAll();
        tableReload();
    }
    function deleteCallback() {
        tableReload();
    }
</script>

</body>
</html>
