<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="row" style="width: 700px; padding: 0 20px;">
  <div style="margin: 15px 0">
    <label for="deleteReason">Выберите причину удаления</label>
    <select id="deleteReason" name="deleteReason" style="width: 560px;">
      <option value="Нарушение пользовательского соглашения.">Нарушение пользовательского соглашения.</option>
      <option value="Многочисленные жалобы пользователей.">Многочисленные жалобы пользователей.</option>
    </select>
  </div>
  <div style="margin: 15px 0">
    <label for="otherDeleteReason">
      Другая причина
      <textarea id="otherDeleteReason" name="otherDeleteReason" style="width: 560px;"></textarea>
    </label>
  </div>
</div>