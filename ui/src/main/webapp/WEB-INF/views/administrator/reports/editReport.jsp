﻿<%--suppress HtmlFormInputWithoutLabel --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<%@include file="/WEB-INF/jspf/checkBrowser.jspf" %>
<html>
<head>
    <%@include file="/WEB-INF/jspf/headSection.jspf" %>
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap-toggle.min.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/fileUpload/fileinput.min.css"/>"/>
</head>
<body>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<div class="container i-line">
    <div class="row">
        <%@include file="/WEB-INF/jspf/messages.jspf" %>

        <div class="col-md-8 col-md-offset-2">
            <form:form id="reportEditForm" name="reportEditForm"
                       action="${pageContext.request.contextPath}/administrator/reports/${reportEditForm.id}/edit_report" method="post"
                       modelAttribute="reportEditForm" autocomplete="true">

                <input id="userFlashmobId" name="userFlashmobId" type="hidden" value="${reportEditForm.id}"/>

                <h4><spring:message code="Lbl.Report"/></h4>

                <table class="b-table table table-striped">
                    <tbody>
                    <tr>
                        <td><spring:message code="Lbl.Photo"/></td>
                        <td style="text-align: left;">
                            <input id="photoFile" name="photoFile" type="file">
                            <input id="photo" name="photo" type="hidden" value="">
                            <input id="photoFileNameForUpload" name="photoFileNameForUpload" type="hidden" value="${photoFileNameForUpload}">
                        </td>
                    </tr>
                    <tr>
                        <td>Ссылка на видео</td>
                        <td>
                            <input id="video_report" name="video_report" type="text" class="form-control"
                                   value="${reportEditForm.video_report}"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Date"/></td>
                        <td>
                            <input type="text" class="form-control" value="${date}" disabled/>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <div style="margin: 15px 0;">
                    <div>
                        <sec:authorize access="hasRole('PERMISSION_EDIT_REPORTS')">
                            <c:if test="${(currentUser.role.id == 1 || currentUser.role.id == 2) || reportEditForm.userId == currentUser.id}">
                                <button class="btn btn-success pull-right" type="submit" name="submit">
                                    <spring:message code="Btn.Save"/>
                                </button>
                            </c:if>
                        </sec:authorize>
                        <a href="<c:url value="/administrator/reports/list_reports"/>">
                            <button class="btn btn-primary pull-right" type="button" name="btnCancel">
                                <spring:message code="Btn.Cancel"/>
                            </button>
                        </a>
                    </div>
                </div>

                <div>
                    <input id="activeTab" name="activeTab" type="hidden" value="1"/>

                    <div style="margin: 15px 0;">
                        <button id="btnReportComments" class="btn btn-primary" type="button" onclick="showTab(1);">
                            Комментарии <span class="badge">${reportCommentsCount}</span>
                        </button>
                        <button id="btnReportLikes" class="btn btn-default" type="button"
                                onclick="showTab(2);">
                            Лайки <span class="badge">${reportLikesCount}</span>
                        </button>
                    </div>

                    <div id="reportComments">
                        <table id="reportCommentsTable" class="display" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th><spring:message code="Lbl.Photo"/></th>
                                <th><spring:message code="Lbl.User"/></th>
                                <th><spring:message code="Lbl.Email"/></th>
                                <th><spring:message code="Lbl.Date"/></th>
                                <th><spring:message code="Lbl.Comment"/></th>
                            </tr>
                            </thead>

                            <tfoot>
                            <tr>
                                <th><spring:message code="Lbl.Photo"/></th>
                                <th><spring:message code="Lbl.User"/></th>
                                <th><spring:message code="Lbl.Email"/></th>
                                <th><spring:message code="Lbl.Date"/></th>
                                <th><spring:message code="Lbl.Comment"/></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>

                    <div id="reportLikes" style="display: none;">
                        <table id="reportLikesTable" class="display" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th><spring:message code="Lbl.Photo"/></th>
                                <th><spring:message code="Lbl.User"/></th>
                                <th><spring:message code="Lbl.Email"/></th>
                                <th><spring:message code="Lbl.Date"/></th>
                            </tr>
                            </thead>

                            <tfoot>
                            <tr>
                                <th><spring:message code="Lbl.Photo"/></th>
                                <th><spring:message code="Lbl.User"/></th>
                                <th><spring:message code="Lbl.Email"/></th>
                                <th><spring:message code="Lbl.Date"/></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </form:form>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf" %>
<script src="<c:url value="/resources/js/vendor/bootstrap-toggle.min.js"/>"></script>
<script src="<c:url value="/resources/js/fileUpload/fileinput.min.js"/>"></script>
<script src="<c:url value="/resources/js/fileUpload/fileinput_locale_ru.js"/>"></script>
<script>
    $("#photoFile").fileinput({
        uploadUrl: "<c:url value="/uploadPhoto/${photoFileNameForUpload}"/>", // server upload action
        uploadAsync: true,
        minFileCount: 1,
        maxFileCount: 1,
        previewFileType: "image",
        language: "ru",
        allowedFileExtensions: ["jpeg", "jpg", "png", "gif"],
        initialPreview: [
            "<img src='${reportEditForm.photo_report}' width='300'>"
        ],
        previewSettings: {
            image: {width: "300px", height: "auto"}
        }
    });

    var reportCommentsTable;
    var reportLikesTable;

    function showTab(activeTab) {
        var tab1 = $("#reportComments");
        var tab2 = $("#reportLikes");

        var btnTab1 = $("#btnReportComments");
        var btnTab2 = $("#btnReportLikes");

        if (activeTab == 1) {
            btnTab1.attr("class", "btn btn-primary");
            btnTab2.attr("class", "btn btn-default");

            tab2.hide();
            tab1.show();

            $("#activeTab").val('1');
        } else if (activeTab == 2) {
            btnTab1.attr("class", "btn btn-default");
            btnTab2.attr("class", "btn btn-primary");

            tab1.hide();
            tab2.show();

            $("#activeTab").val('2');
        } else if (activeTab == 3) {
            btnTab1.attr("class", "btn btn-default");
            btnTab2.attr("class", "btn btn-default");

            tab1.hide();
            tab2.hide();

            $("#activeTab").val('2');
        }
    }

    $(document).ready(function () {
        $("#photoFile").on('fileuploaded', function(event, data, previewId, index) {
            $("#photo").val($("#photoFile").val());
        });

        reportCommentsTable = $('#reportCommentsTable').DataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "bSort": false,
            "ajax": {
                "url": "${pageContext.request.contextPath}/administrator/reports/get_report_comments_JSON",
                "type": "POST",
                "data": function (d) {
                    d.userFlashmobId = ${reportEditForm.id};
                }
            },
            "language": {
                "processing": "Загрузка данных...",
                "search": "Поиск:",
                "lengthMenu": "Показать _MENU_ записей",
                "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                "infoEmpty": "Записи с 0 до 0 из 0 записей",
                "infoFiltered": "(отфильтровано из _MAX_ записей)",
                "infoPostFix": "",
                "loadingRecords": "Загрузка записей...",
                "zeroRecords": "Записи отсутствуют.",
                "emptyTable:": "В таблице отсутствуют данные",
                "paginate": {
                    "first": "Первая",
                    "previous": "Предыдущая",
                    "next": "Следующая",
                    "last": "Последняя"
                }
            }
        });

        reportLikesTable = $('#reportLikesTable').DataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "bSort": false,
            "ajax": {
                "url": "${pageContext.request.contextPath}/administrator/reports/get_report_likes_JSON",
                "type": "POST",
                "data": function (d) {
                    d.userFlashmobId = ${reportEditForm.id};
                }
            },
            "language": {
                "processing": "Загрузка данных...",
                "search": "Поиск:",
                "lengthMenu": "Показать _MENU_ записей",
                "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                "infoEmpty": "Записи с 0 до 0 из 0 записей",
                "infoFiltered": "(отфильтровано из _MAX_ записей)",
                "infoPostFix": "",
                "loadingRecords": "Загрузка записей...",
                "zeroRecords": "Записи отсутствуют.",
                "emptyTable:": "В таблице отсутствуют данные",
                "paginate": {
                    "first": "Первая",
                    "previous": "Предыдущая",
                    "next": "Следующая",
                    "last": "Последняя"
                }
            }
        });
    });

</script>
</body>
</html>
