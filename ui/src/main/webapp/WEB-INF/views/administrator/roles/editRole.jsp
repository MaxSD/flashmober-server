﻿<%--suppress HtmlFormInputWithoutLabel --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="func" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="/WEB-INF/customTags/customTag.tld" prefix="ct" %>

<!DOCTYPE html>
<%@include file="/WEB-INF/jspf/checkBrowser.jspf" %>
<html>
<head>
    <%@include file="/WEB-INF/jspf/headSection.jspf" %>
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap-switch.min.css"/>"/>
</head>
<body>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<div class="container i-line">
    <div class="row">
        <%@include file="/WEB-INF/jspf/messages.jspf" %>

        <div class="col-md-8 col-md-offset-2">
            <form:form action="${pageContext.request.contextPath}/administrator/roles/${roleEditForm.id}/edit_role" method="post"
                       modelAttribute="roleEditForm" autocomplete="true">

                <h4><spring:message code="Lbl.EditRole"/></h4>

                <table class="b-table table table-striped">
                    <tbody>
                    <tr>
                        <td><spring:message code="Lbl.Role.Name.RU"/></td>
                        <td>
                            <input type="text" id="textRU" name="textRU" required="required" class="form-control"
                                    value="${roleEditForm.text.get('ru')}"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Role.Name.EN"/></td>
                        <td>
                            <input type="text" id="textEN" name="textEN" required="required" class="form-control"
                                   value="${roleEditForm.text.get('en')}"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Role.Description.RU"/></td>
                        <td>
                            <input type="text" id="descriptionRU" name="descriptionRU" required="required"
                                   class="form-control" value="${roleEditForm.description.get('ru')}"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Role.Description.EN"/></td>
                        <td>
                            <input type="text" id="descriptionEN" name="descriptionEN" required="required"
                                   class="form-control" value="${roleEditForm.description.get('en')}"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Permissions"/></td>
                        <td>
                            <c:if test="${not empty permissionsList}">
                                <spring:message code="Btn.On" var="dataTextOn"/>
                                <spring:message code="Btn.Off" var="dataTextOff"/>

                                <ct:rolePermissionsListWithChecked path="permissionsList" items="${permissionsList}"
                                                              role="${roleEditForm}" isReadOnly="false"
                                                              dataTextOn="${dataTextOn}"
                                                              dataTextOff="${dataTextOff}"/>
                            </c:if>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <div>
                    <button class="btn btn-success pull-right" type="submit" name="submit">
                        <spring:message code="Btn.Save"/>
                    </button>

                    <a href="<c:url value="/administrator/roles/list_roles"/>">
                        <button class="btn btn-primary pull-left" type="button" name="btnCancel">
                            <spring:message code="Btn.Cancel"/>
                        </button>
                    </a>
                </div>
            </form:form>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf" %>
<script src="<c:url value="/resources/js/vendor/bootstrap-switch.min.js"/>"></script>
<script>
    $('input:checkbox').bootstrapSwitch();
</script>
</body>
</html>
