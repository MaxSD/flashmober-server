<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<%@include file="/WEB-INF/jspf/checkBrowser.jspf" %>
<html>
<head>
    <%@include file="/WEB-INF/jspf/headSection.jspf" %>
</head>
<body>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<div class="container i-line">
    <div class="row">
        <%@include file="/WEB-INF/jspf/messages.jspf" %>

        <div class="col-md-10 col-md-offset-1">
            <h4 style="color: #8b0000;"><b><spring:message code="Lbl.Roles"/></b></h4>

            <form id="listRolesForm" name="listRolesForm" method="post">
                <table class="b-table b-table--search table table-striped">
                    <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>

                    <c:if test="${not empty rolesList}">
                        <c:forEach items="${rolesList}" var="roleData">
                            <tr>
                                <td>${roleData.get('id')}</td>
                                <td>${roleData.get('name')}</td>
                                <td>${roleData.get('text')}</td>
                                <td>${roleData.get('description')}</td>
                                <td style="text-align: right;">
                                    <sec:authorize access="hasAnyRole('PERMISSION_EDIT_ROLE')">
                                        <a href="<c:url value="/administrator/roles/${roleData.get('id')}/edit_role"/>">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </a>
                                    </sec:authorize>
                                    <sec:authorize access="hasAnyRole('PERMISSION_EDIT_ROLE')">
                                    <a href="<c:url value="/administrator/roles/${roleData.get('id')}/view_role"/>">
                                        <span class="glyphicon glyphicon-info-sign"></span>
                                    </a>
                                    </sec:authorize>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:if>
                    </tbody>
                </table>
            </form>

            <div>
                <a href="<c:url value="/administrator/roles/"/>">
                    <button class="btn btn-primary pull-left" type="button" name="btnReturn">
                        <spring:message code="Btn.Return"/>
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf" %>

<script>
    function deleteRole(_roleId, _roleName) {
        bootbox.confirm("<spring:message code="UI.Confirm.DeleteRole"/> (Role name -> " + _roleName + ")", function (result) {
            if (result) {
                $("#listRolesForm").attr("action",
                        "${pageContext.request.contextPath}/administrator/roles/" + _roleId + "/delete_role").submit();
            }
        });
    }
</script>

</body>
</html>
