﻿<%--suppress HtmlFormInputWithoutLabel --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="func" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="/WEB-INF/customTags/customTag.tld" prefix="ct" %>

<!DOCTYPE html>
<%@include file="/WEB-INF/jspf/checkBrowser.jspf" %>
<html>
<head>
    <%@include file="/WEB-INF/jspf/headSection.jspf" %>
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap-toggle.min.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/fileUpload/fileinput.min.css"/>"/>
</head>
<body>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<div class="container i-line">
    <div class="row">
        <%@include file="/WEB-INF/jspf/messages.jspf" %>

        <div class="col-md-8 col-md-offset-2">
            <form:form action="${pageContext.request.contextPath}/administrator/users/${userEditForm.id}/edit_user"
                       method="post"
                       modelAttribute="userEditForm" autocomplete="true">

                <h4 style="color: #8b0000;"><b>Профиль пользователя</b></h4>

                <table class="b-table table table-striped">
                    <tbody>
                    <tr>
                        <td><spring:message code="Lbl.Photo"/></td>
                        <td style="text-align: left;">
                            <input id="photoFileNameForUpload" name="photoFileNameForUpload" type="hidden" value="${photoFileNameForUpload}">
                            <input id="photoFile" name="photoFile" type="file">
                            <input id="photo" name="photo" type="hidden" value="">
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Login"/></td>
                        <td>
                            <form:input type="text" path="name" required="required" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Email"/></td>
                        <td>
                            <form:input type="text" path="email" required="required" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Password"/></td>
                        <td>
                            <form:input type="password" path="password" required="required" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Gender"/></td>
                        <td>
                            <form:select path="gender" class="form-control">
                                <option value="0" <c:if test="${userEditForm.gender == 0}"> selected</c:if>>М</option>
                                <option value="1" <c:if test="${userEditForm.gender == 1}"> selected</c:if>>Ж</option>
                            </form:select>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Age"/></td>
                        <td>
                            <form:input type="text" path="age" required="required" class="form-control" onkeypress="return fieldValidator.checkNumber(event);"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.About"/></td>
                        <td>
                            <form:input type="text" path="about" required="required" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Country"/></td>
                        <td>
                            <form:input type="text" path="country" required="required" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.City"/></td>
                        <td>
                            <form:input type="text" path="city" required="required" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Language"/></td>
                        <td>
                            <select id="language" name="language" class="form-control">
                                <option value="ru" selected>Русский</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Site"/></td>
                        <td>
                            <form:input type="text" path="site" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Categories"/></td>
                        <td style="text-align: center;">
                            <input data-toggle="toggle" name="categoriesList" data-on="Животные" data-off="Животные"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="1" <c:if
                                    test="${categoriesList.contains(1)}"> checked</c:if>>
                            <input data-toggle="toggle" name="categoriesList" data-on="Кино" data-off="Кино"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="2" <c:if
                                    test="${categoriesList.contains(2)}"> checked</c:if>>
                            <input data-toggle="toggle" name="categoriesList" data-on="Искуство" data-off="Искуство"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="3" <c:if
                                    test="${categoriesList.contains(3)}"> checked</c:if>>
                            <input data-toggle="toggle" name="categoriesList" data-on="Еда" data-off="Еда"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="4" <c:if
                                    test="${categoriesList.contains(4)}"> checked</c:if>>
                            <input data-toggle="toggle" name="categoriesList" data-on="Музыка" data-off="Музыка"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="5" <c:if
                                    test="${categoriesList.contains(5)}"> checked</c:if>>
                            <input data-toggle="toggle" name="categoriesList" data-on="Юмор" data-off="Юмор"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="6" <c:if
                                    test="${categoriesList.contains(6)}"> checked</c:if>>
                            <input data-toggle="toggle" name="categoriesList" data-on="Места" data-off="Места"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="7" <c:if
                                    test="${categoriesList.contains(7)}"> checked</c:if>>
                            <input data-toggle="toggle" name="categoriesList" data-on="Спорт" data-off="Спорт"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="9" <c:if
                                    test="${categoriesList.contains(9)}"> checked</c:if>>
                            <input data-toggle="toggle" name="categoriesList" data-on="Мода и стиль"
                                   data-off="Мода и стиль" data-onstyle="success" data-offstyle="default"
                                   type="checkbox" value="10" <c:if test="${categoriesList.contains(10)}">
                                   checked</c:if>>
                            <input data-toggle="toggle" name="categoriesList" data-on="Семейное" data-off="Семейное"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="11" <c:if
                                    test="${categoriesList.contains(11)}"> checked</c:if>>
                            <input data-toggle="toggle" name="categoriesList" data-on="Социальное" data-off="Социальное"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="12" <c:if
                                    test="${categoriesList.contains(12)}"> checked</c:if>>
                            <input data-toggle="toggle" name="categoriesList" data-on="Отдых" data-off="Отдых"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="13" <c:if
                                    test="${categoriesList.contains(13)}"> checked</c:if>>
                            <input data-toggle="toggle" name="categoriesList" data-on="Разное" data-off="Разное"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="14" <c:if
                                    test="${categoriesList.contains(14)}"> checked</c:if>>
                            <input data-toggle="toggle" name="categoriesList" data-on="Наука и технологии"
                                   data-off="Наука и технологии" data-onstyle="success" data-offstyle="default"
                                   type="checkbox" value="8" <c:if test="${categoriesList.contains(8)}"> checked</c:if>>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Roles"/></td>
                        <td>
                            <c:choose>
                                <c:when test="${isAdmin == true}">
                                    <c:if test="${not empty rolesList}">
                                        <form:select path="role.id" id="role" class="form-control"
                                                     onchange="if ($(this).val() == 4) {$('#flashmobUsersCountRow').show();} else {$('#flashmobUsersCountRow').hide();}">
                                            <form:options items="${rolesList}"/>
                                        </form:select>
                                    </c:if>
                                </c:when>
                                <c:otherwise>
                                    ${roleName}
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                    <tr id="flashmobUsersCountRow" <c:if test="${!userEditForm.isBusinessUser}">style="display: none;" </c:if>>
                        <td>Количество пользователей на которое назначается задание</td>
                        <td>
                            <input type="text" id="flashmobUsersCount" name="flashmobUsersCount"
                                   value="${userEditForm.flashmobUsersCount}"
                                   onkeypress="return fieldValidator.checkNumber(event);" class="form-control"/>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <div>
                    <sec:authorize access="hasRole('PERMISSION_EDIT_USERS')">
                        <c:if test="${(currentUser.role.id == 1 || currentUser.role.id == 2) || userEditForm.id == currentUser.id}">
                            <button class="btn btn-success pull-right" type="submit" name="submit">
                                <spring:message code="Btn.Save"/>
                            </button>
                        </c:if>
                    </sec:authorize>

                    <a href="<c:url value="/administrator/users/list_users"/>">
                        <button class="btn btn-primary pull-left" type="button" name="btnCancel">
                            <spring:message code="Btn.Cancel"/>
                        </button>
                    </a>
                </div>
            </form:form>
        </div>
        <div class="col-md-10 col-md-offset-1">
            <div style="margin: 15px 0;">
                <button id="btnFlashmobs" class="btn btn-primary" type="button" onclick="showTab(1);">
                    Задания <span class="badge">${flashmobsCount}</span>
                </button>
                <button id="btnFriends" class="btn btn-default" type="button" onclick="showTab(2);">
                    Друзья <span class="badge">${friendsCount}</span>
                </button>
            </div>

            <div id="users-flashmobs">
                <form id="listUsersFlashmobsForm" name="listUsersFlashmobsForm" method="post">
                    <table id="usersFlashmobsTable" class="display" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <!--<th>Photo</th>-->
                            <th><spring:message code="Lbl.ID"/></th>
                            <th><spring:message code="Lbl.FlashmobPhoto"/></th>
                            <th><spring:message code="Lbl.FlashmobDateCreate"/></th>
                            <th><spring:message code="Lbl.FlashmobDescription"/></th>
                            <th><spring:message code="Lbl.FlashmobPeoplesCount"/></th>
                            <th><spring:message code="Lbl.Action"/></th>
                        </tr>
                        </thead>

                        <tfoot>
                        <tr>
                            <!--<th>Photo</th>-->
                            <th><spring:message code="Lbl.ID"/></th>
                            <th><spring:message code="Lbl.FlashmobPhoto"/></th>
                            <th><spring:message code="Lbl.FlashmobDateCreate"/></th>
                            <th><spring:message code="Lbl.FlashmobDescription"/></th>
                            <th><spring:message code="Lbl.FlashmobPeoplesCount"/></th>
                            <th><spring:message code="Lbl.Action"/></th>
                        </tr>
                        </tfoot>
                    </table>
                </form>
            </div>

            <div id="users-friends" style="display: none;">
                <form id="listUsersFriendsForm" name="listUsersFriendsForm" method="post">
                    <table id="usersFriendsTable" class="display" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th><spring:message code="Lbl.Photo"/></th>
                            <th><spring:message code="Lbl.Login"/></th>
                            <th><spring:message code="Lbl.Email"/></th>
                            <th><spring:message code="Lbl.Country"/></th>
                            <th><spring:message code="Lbl.City"/></th>
                            <th><spring:message code="Lbl.Gender"/></th>
                            <th><spring:message code="Lbl.Age"/></th>
                            <th><spring:message code="Lbl.Action"/></th>
                        </tr>
                        </thead>

                        <tfoot>
                        <tr>
                            <th><spring:message code="Lbl.Photo"/></th>
                            <th><spring:message code="Lbl.Login"/></th>
                            <th><spring:message code="Lbl.Email"/></th>
                            <th><spring:message code="Lbl.Country"/></th>
                            <th><spring:message code="Lbl.City"/></th>
                            <th><spring:message code="Lbl.Gender"/></th>
                            <th><spring:message code="Lbl.Age"/></th>
                            <th><spring:message code="Lbl.Action"/></th>
                        </tr>
                        </tfoot>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf" %>
<script src="<c:url value="/resources/js/vendor/bootstrap-toggle.min.js"/>"></script>
<script src="<c:url value="/resources/js/fileUpload/fileinput.min.js"/>"></script>
<script src="<c:url value="/resources/js/fileUpload/fileinput_locale_ru.js"/>"></script>
<script>
    /* Initialize your widget via javascript as follows */
    $("#photoFile").fileinput({
        uploadUrl: "<c:url value="/uploadPhoto/${photoFileNameForUpload}"/>", // server upload action
        uploadAsync: true,
        minFileCount: 1,
        maxFileCount: 1,
        previewFileType: "image",
        language: "ru",
        allowedFileExtensions: ["jpeg", "jpg", "png", "gif"],
        initialPreview: [
            "<img src='${userEditForm.photo}' width='128'>"
        ],
        previewSettings: {
            image: {width: "128px", height: "auto"}
        }
    });

    function showTab(number) {
        var usersFlashmobs = $("#users-flashmobs");
        var usersFriends = $("#users-friends");

        var btnFlashmobs = $("#btnFlashmobs");
        var btnFriends = $("#btnFriends");

        if (number == 1) {
            btnFlashmobs.attr("class", "btn btn-primary");
            btnFriends.attr("class", "btn btn-default");

            usersFriends.hide();
            usersFlashmobs.show();
        } else if (number == 2) {
            btnFlashmobs.attr("class", "btn btn-default");
            btnFriends.attr("class", "btn btn-primary");

            usersFlashmobs.hide();
            usersFriends.show();
        }
    }

    var usersFlashmobsTable;
    var usersFriendsTable;

    /**
     * Delete flashmob
     */
    var flashmobId;
    function deleteFlashmobReason(_flashmobId) {
        flashmobId = _flashmobId;

        jQueryAjax.ajaxGET_HTML(jQueryAjax.getServerName() +
                "<c:url value="/administrator/flashmobs/delete_flashmob_reason"/>",
                {}, deleteReasonCallBack, true, null, 'ajaxError');
    }
    function deleteReasonCallBack(data) {
        jQueryAjax.loadPageToDialog("Удаление задания", data, {
            lockUser: {
                label: "Удалить",
                className: "btn-danger",
                callback: function () {
                    deleteFlashmob();
                    return false;
                }
            }
        });
    }
    function deleteFlashmob() {
        jQueryAjax.ajaxPOST_HTML("${pageContext.request.contextPath}/administrator/flashmobs/" + flashmobId + "/delete_flashmob_ajax",
                {
                    deleteReason: $("#deleteReason").val(),
                    otherDeleteReason: $("#otherDeleteReason").val()
                },
                deleteCallback, true, "messages", null);
        bootbox.hideAll();
        usersFlashmobsTable.ajax.reload();
    }
    function deleteCallback() {
        usersFlashmobsTable.ajax.reload();
    }

    $(document).ready(function () {
        $("#photoFile").on('fileuploaded', function (event, data, previewId, index) {
            $("#photo").val($("#photoFile").val());
        });

        usersFlashmobsTable = $('#usersFlashmobsTable').DataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "bSort": false,
            "ajax": {
                "url": "${pageContext.request.contextPath}/administrator/flashmobs/get_users_flashmobs_JSON",
                "type": "POST",
                "data": function (d) {
                    d.uid = ${userEditForm.id};
                }
            },
            "language": {
                "processing": "Загрузка данных...",
                "search": "Поиск:",
                "lengthMenu": "Показать _MENU_ записей",
                "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                "infoEmpty": "Записи с 0 до 0 из 0 записей",
                "infoFiltered": "(отфильтровано из _MAX_ записей)",
                "infoPostFix": "",
                "loadingRecords": "Загрузка записей...",
                "zeroRecords": "Записи отсутствуют.",
                "emptyTable:": "В таблице отсутствуют данные",
                "paginate": {
                    "first": "Первая",
                    "previous": "Предыдущая",
                    "next": "Следующая",
                    "last": "Последняя"
                }
            }
        });

        usersFriendsTable = $('#usersFriendsTable').DataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "bSort": false,
            "ajax": {
                "url": "${pageContext.request.contextPath}/administrator/friends/get_users_friends_JSON",
                "type": "POST",
                "data": function (d) {
                    d.uid = ${userEditForm.id};
                }
            },
            "language": {
                "processing": "Загрузка данных...",
                "search": "Поиск:",
                "lengthMenu": "Показать _MENU_ записей",
                "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                "infoEmpty": "Записи с 0 до 0 из 0 записей",
                "infoFiltered": "(отфильтровано из _MAX_ записей)",
                "infoPostFix": "",
                "loadingRecords": "Загрузка записей...",
                "zeroRecords": "Записи отсутствуют.",
                "emptyTable:": "В таблице отсутствуют данные",
                "paginate": {
                    "first": "Первая",
                    "previous": "Предыдущая",
                    "next": "Следующая",
                    "last": "Последняя"
                }
            }
        });

        $("#btnFilter").click(function () {
            //
        })
    });
</script>
</body>
</html>
