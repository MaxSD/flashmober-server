﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="func" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="/WEB-INF/customTags/customTag.tld" prefix="ct" %>

<!DOCTYPE html>
<%@include file="/WEB-INF/jspf/checkBrowser.jspf" %>
<html>
<head>
    <%@include file="/WEB-INF/jspf/headSection.jspf" %>
</head>
<body>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<div class="container i-line">
    <div class="row">
        <%@include file="/WEB-INF/jspf/messages.jspf" %>

        <div class="col-md-8 col-md-offset-2">
            <h4><spring:message code="Lbl.EditUser"/></h4>

            <table class="b-table table table-striped">
                <tbody>
                <tr>
                    <td><spring:message code="Lbl.Login"/></td>
                    <td>
                        ${userViewForm.name}
                    </td>
                </tr>
                <tr>
                    <td><spring:message code="Lbl.Email"/></td>
                    <td>
                        ${userViewForm.email}
                    </td>
                </tr>
                <tr>
                    <td><spring:message code="Lbl.Gender"/></td>
                    <td>
                        ${userViewForm.gender}
                    </td>
                </tr>
                <tr>
                    <td><spring:message code="Lbl.About"/></td>
                    <td>
                        ${userViewForm.about}
                    </td>
                </tr>
                <tr>
                    <td><spring:message code="Lbl.Country"/></td>
                    <td>
                        ${userViewForm.country}
                    </td>
                </tr>
                <tr>
                    <td><spring:message code="Lbl.City"/></td>
                    <td>
                        ${userViewForm.city}
                    </td>
                </tr>
                <tr>
                    <td><spring:message code="Lbl.Language"/></td>
                    <td>

                    </td>
                </tr>
                <tr>
                    <td><spring:message code="Lbl.City"/></td>
                    <td>
                        ${userViewForm.city}
                    </td>
                </tr>
                <tr>
                    <td><spring:message code="Lbl.Roles"/></td>
                    <td>
                        ${roleName}
                    </td>
                </tr>
                </tbody>
            </table>

            <div>
                <a href="<c:url value="/administrator/users/list_users"/>">
                    <button class="btn btn-primary pull-left" type="button" name="btnCancel">
                        <spring:message code="Btn.Cancel"/>
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>
