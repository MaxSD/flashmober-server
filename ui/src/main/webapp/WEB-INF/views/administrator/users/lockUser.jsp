<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="row" style="width: 700px; padding: 0 20px;">
  <div style="margin: 15px 0">
    <label for="lockReason">Выберите причину блокирорвки</label>
    <select id="lockReason" name="lockReason" style="width: 560px;">
      <option value="Нарушение пользовательского соглашения.">Нарушение пользовательского соглашения.</option>
      <option value="Многочисленные жалобы пользователей.">Многочисленные жалобы пользователей.</option>
    </select>
  </div>
  <div style="margin: 15px 0">
    <label for="otherLockReason">
      Другая причина
      <textarea id="otherLockReason" name="otherLockReason" style="width: 560px;"></textarea>
    </label>
  </div>
</div>