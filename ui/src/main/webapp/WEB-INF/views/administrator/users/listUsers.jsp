<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<%@include file="/WEB-INF/jspf/checkBrowser.jspf" %>
<html>
<head>
    <%@include file="/WEB-INF/jspf/headSection.jspf" %>
</head>
<body>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<div class="container i-line" style="width: 80%;">
    <div class="row">
        <div id="messages">
            <%@include file="/WEB-INF/jspf/messages.jspf" %>
        </div>

        <div>
            <h4 style="color: #8b0000"><b><spring:message code="Lbl.Users"/></b></h4>

            <div style="margin: 15px 0;">
                <button id="btnAllUsers" class="btn btn-primary" type="button" onclick="showUsers(1);">
                    Все пользователи <span class="badge">${usersCount}</span>
                </button>
                <button id="btnBusinessUsers" class="btn btn-default" type="button" onclick="showUsers(2);">
                    Бизнес пользователи <span class="badge">${businessUsersCount}</span>
                </button>
                <button id="btnModerators" class="btn btn-default" type="button" onclick="showUsers(3);">
                    Модераторы <span class="badge">${moderatorsCount}</span>
                </button>
                <sec:authorize access="hasAnyRole('PERMISSION_CREATE_USERS')">
                    <a href="<c:url value="/administrator/users/new_user"/>">
                        <button class="btn btn-success pull-right" type="button" name="btnCreateNewUser">
                            <spring:message code="Btn.Create"/>
                        </button>
                    </a>
                </sec:authorize>
            </div>

            <form id="filter" name="folter" method="post"
                  action="${pageContext.request.contextPath}/administrator/users/exportUsersToFile">

                <input id="usersType" name="usersType" type="hidden" value="1"/>

                <fieldset style="text-align: center;">
                    <legend><spring:message code="Lbl.Filter"/></legend>

                    <div class="row">
                        <div class="form-inline">
                            <div class="form-group" style="align-content: center;">
                                <spring:message code="Lbl.Login"/>:&nbsp;
                                <input id="nameFilter" name="nameFilter" class="form-control"/>
                                &nbsp;&nbsp;&nbsp;
                                <spring:message code="Lbl.Email"/>:&nbsp;
                                <input id="emailFilter" name="emailFilter" class="form-control"/>
                                <br/><br/>
                                <spring:message code="Lbl.Country"/>:&nbsp;
                                <select id="countryFilter" name="countryFilter" class="form-control">
                                    <option value="">Все</option>
                                    <c:if test="${not empty userCountries}">
                                        <c:forEach var="country" items="${userCountries}">
                                            <c:if test="${not empty country}">
                                                <option value="${country}">${country}</option>
                                            </c:if>
                                        </c:forEach>
                                    </c:if>
                                </select>
                                &nbsp;&nbsp;&nbsp;
                                <spring:message code="Lbl.City"/>:&nbsp;
                                <select id="cityFilter" name="cityFilter" class="form-control">
                                    <option value="">Все</option>
                                    <c:if test="${not empty userCities}">
                                        <c:forEach var="city" items="${userCities}">
                                            <c:if test="${not empty city}">
                                                <option value="${city}">${city}</option>
                                            </c:if>
                                        </c:forEach>
                                    </c:if>
                                </select>
                                &nbsp;&nbsp;&nbsp;
                                <spring:message code="Lbl.Gender"/>:&nbsp;
                                <select id="genderFilter" name="genderFilter" class="form-control">
                                    <option value="">Все</option>
                                    <option value="0">Ж</option>
                                    <option value="1">М</option>
                                </select>
                                &nbsp;&nbsp;&nbsp;
                                <spring:message code="Lbl.Age"/>:&nbsp;от&nbsp;
                                <input id="ageFromFilter" name="ageFromFilter" class="form-control"
                                       maxlength="2" style="width: 50px"/>
                                &nbsp;до&nbsp;
                                <input id="ageToFilter" name="ageToFilter" class="form-control"
                                       maxlength="2" style="width: 50px"/>

                                &nbsp;&nbsp;&nbsp;
                                <button id="btnFilter" name="btnFilter" class="btn btn-info" type="button"
                                        onclick="tableReload();">
                                    <spring:message code="Btn.Filter"/>
                                </button>
                                <sec:authorize access="hasAnyRole('PERMISSION_VIEW_USERS')">
                                    <button class="btn btn-default" type="submit" name="submit">
                                        <spring:message code="Btn.ExportToExcel"/>
                                    </button>
                                </sec:authorize>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>


            <div id="all-users">
                <form id="listAllUsersForm" name="listAllUsersForm" method="post">
                    <table id="allUsersTable" class="display" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th><spring:message code="Lbl.Photo"/></th>
                            <th><spring:message code="Lbl.Login"/></th>
                            <th><spring:message code="Lbl.Email"/></th>
                            <th><spring:message code="Lbl.Country"/></th>
                            <th><spring:message code="Lbl.City"/></th>
                            <th><spring:message code="Lbl.Gender"/></th>
                            <th><spring:message code="Lbl.Age"/></th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>

                        <tfoot>
                        <tr>
                            <th><spring:message code="Lbl.Photo"/></th>
                            <th><spring:message code="Lbl.Login"/></th>
                            <th><spring:message code="Lbl.Email"/></th>
                            <th><spring:message code="Lbl.Country"/></th>
                            <th><spring:message code="Lbl.City"/></th>
                            <th><spring:message code="Lbl.Gender"/></th>
                            <th><spring:message code="Lbl.Age"/></th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                        </tfoot>
                    </table>
                </form>
            </div>

            <div id="business-users" style="display: none;">
                <form id="listBusinessUsersForm" name="listBusinessUsersForm" method="post">
                    <table id="businessUsersTable" class="display" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th><spring:message code="Lbl.Photo"/></th>
                            <th><spring:message code="Lbl.Login"/></th>
                            <th><spring:message code="Lbl.Email"/></th>
                            <th><spring:message code="Lbl.Country"/></th>
                            <th><spring:message code="Lbl.City"/></th>
                            <th><spring:message code="Lbl.Gender"/></th>
                            <th><spring:message code="Lbl.Age"/></th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>

                        <tfoot>
                        <tr>
                            <th><spring:message code="Lbl.Photo"/></th>
                            <th><spring:message code="Lbl.Login"/></th>
                            <th><spring:message code="Lbl.Email"/></th>
                            <th><spring:message code="Lbl.Country"/></th>
                            <th><spring:message code="Lbl.City"/></th>
                            <th><spring:message code="Lbl.Gender"/></th>
                            <th><spring:message code="Lbl.Age"/></th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                        </tfoot>
                    </table>
                </form>
            </div>

            <div id="moderators" style="display: none;">
                <form id="listModeratorsForm" name="listModeratorsForm" method="post">
                    <table id="moderatorsTable" class="display" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th><spring:message code="Lbl.Photo"/></th>
                            <th><spring:message code="Lbl.Login"/></th>
                            <th><spring:message code="Lbl.Email"/></th>
                            <th><spring:message code="Lbl.Country"/></th>
                            <th><spring:message code="Lbl.City"/></th>
                            <th><spring:message code="Lbl.Gender"/></th>
                            <th><spring:message code="Lbl.Age"/></th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>

                        <tfoot>
                        <tr>
                            <th><spring:message code="Lbl.Photo"/></th>
                            <th><spring:message code="Lbl.Login"/></th>
                            <th><spring:message code="Lbl.Email"/></th>
                            <th><spring:message code="Lbl.Country"/></th>
                            <th><spring:message code="Lbl.City"/></th>
                            <th><spring:message code="Lbl.Gender"/></th>
                            <th><spring:message code="Lbl.Age"/></th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                        </tfoot>
                    </table>
                </form>
            </div>

            <br/>

            <div>
                <sec:authorize access="hasAnyRole('PERMISSION_CREATE_USERS')">
                    <a href="<c:url value="/administrator/users/new_user"/>">
                        <button class="btn btn-success pull-right" type="button" name="btnCreateNewUser">
                            <spring:message code="Btn.Create"/>
                        </button>
                    </a>
                </sec:authorize>
                <a href="<c:url value="/administrator/users/"/>">
                    <button class="btn btn-primary pull-left" type="button" name="btnReturn">
                        <spring:message code="Btn.Return"/>
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf" %>

<script>
    var allUsersTable;
    var businessUsersTable;
    var moderatorsTable;

    function showUsers(type) {
        var allUsers = $("#all-users");
        var businessUsers = $("#business-users");
        var moderators = $("#moderators");

        var btnAllUsers = $("#btnAllUsers");
        var btnBusinessUsers = $("#btnBusinessUsers");
        var btnModerators = $("#btnModerators");

        if (type == 1) {
            btnAllUsers.attr("class", "btn btn-primary");
            btnBusinessUsers.attr("class", "btn btn-default");
            btnModerators.attr("class", "btn btn-default");

            moderators.hide();
            businessUsers.hide();
            allUsers.show();

            $("#usersType").val('1');
        } else if (type == 2) {
            btnAllUsers.attr("class", "btn btn-default");
            btnBusinessUsers.attr("class", "btn btn-primary");
            btnModerators.attr("class", "btn btn-default");

            moderators.hide();
            allUsers.hide();
            businessUsers.show();

            $("#usersType").val('2');
        } else if (type == 3) {
            btnAllUsers.attr("class", "btn btn-default");
            btnBusinessUsers.attr("class", "btn btn-default");
            btnModerators.attr("class", "btn btn-primary");

            allUsers.hide();
            businessUsers.hide();
            moderators.show();

            $("#usersType").val('3');
        }
    }

    function tableReload() {
        if ($("#all-users").is(":visible")) {
            allUsersTable.ajax.reload();
        } else if ($("#business-users").is(":visible")) {
            businessUsersTable.ajax.reload();
        } else if ($("#moderators").is(":visible")) {
            moderatorsTable.ajax.reload();
        }
    }

    $(document).ready(function () {
        allUsersTable = $('#allUsersTable').DataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "bSort": false,
            "ajax": {
                "url": "${pageContext.request.contextPath}/administrator/users/get_all_users_JSON",
                "type": "POST",
                "data": function (d) {
                    d.nameFilter = $("#nameFilter").val();
                    d.emailFilter = $("#emailFilter").val();
                    d.countryFilter = $("#countryFilter").val();
                    d.cityFilter = $("#cityFilter").val();
                    d.genderFilter = $("#genderFilter").val();
                    d.ageFromFilter = $("#ageFromFilter").val();
                    d.ageToFilter = $("#ageToFilter").val();
                }
            },
            "language": {
                "processing": "Загрузка данных...",
                "search": "Поиск:",
                "lengthMenu": "Показать _MENU_ записей",
                "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                "infoEmpty": "Записи с 0 до 0 из 0 записей",
                "infoFiltered": "(отфильтровано из _MAX_ записей)",
                "infoPostFix": "",
                "loadingRecords": "Загрузка записей...",
                "zeroRecords": "Записи отсутствуют.",
                "emptyTable:": "В таблице отсутствуют данные",
                "paginate": {
                    "first": "Первая",
                    "previous": "Предыдущая",
                    "next": "Следующая",
                    "last": "Последняя"
                }
            }
        });

        businessUsersTable = $('#businessUsersTable').DataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "bSort": false,
            "ajax": {
                "url": "${pageContext.request.contextPath}/administrator/users/get_business_users_JSON",
                "type": "POST",
                "data": function (d) {
                    d.nameFilter = $("#nameFilter").val();
                    d.emailFilter = $("#emailFilter").val();
                    d.countryFilter = $("#countryFilter").val();
                    d.cityFilter = $("#cityFilter").val();
                    d.genderFilter = $("#genderFilter").val();
                    d.ageFromFilter = $("#ageFromFilter").val();
                    d.ageToFilter = $("#ageToFilter").val();
                }
            },
            "language": {
                "processing": "Загрузка данных...",
                "search": "Поиск:",
                "lengthMenu": "Показать _MENU_ записей",
                "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                "infoEmpty": "Записи с 0 до 0 из 0 записей",
                "infoFiltered": "(отфильтровано из _MAX_ записей)",
                "infoPostFix": "",
                "loadingRecords": "Загрузка записей...",
                "zeroRecords": "Записи отсутствуют.",
                "emptyTable:": "В таблице отсутствуют данные",
                "paginate": {
                    "first": "Первая",
                    "previous": "Предыдущая",
                    "next": "Следующая",
                    "last": "Последняя"
                }
            }
        });

        moderatorsTable = $('#moderatorsTable').DataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "bSort": false,
            "ajax": {
                "url": "${pageContext.request.contextPath}/administrator/users/get_moderators_JSON",
                "type": "POST",
                "data": function (d) {
                    d.nameFilter = $("#nameFilter").val();
                    d.emailFilter = $("#emailFilter").val();
                    d.countryFilter = $("#countryFilter").val();
                    d.cityFilter = $("#cityFilter").val();
                    d.genderFilter = $("#genderFilter").val();
                    d.ageFromFilter = $("#ageFromFilter").val();
                    d.ageToFilter = $("#ageToFilter").val();
                }
            },
            "language": {
                "processing": "Загрузка данных...",
                "search": "Поиск:",
                "lengthMenu": "Показать _MENU_ записей",
                "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                "infoEmpty": "Записи с 0 до 0 из 0 записей",
                "infoFiltered": "(отфильтровано из _MAX_ записей)",
                "infoPostFix": "",
                "loadingRecords": "Загрузка записей...",
                "zeroRecords": "Записи отсутствуют.",
                "emptyTable:": "В таблице отсутствуют данные",
                "paginate": {
                    "first": "Первая",
                    "previous": "Предыдущая",
                    "next": "Следующая",
                    "last": "Последняя"
                }
            }
        });

        $("#btnFilter").click(function () {
            //
        })
    });

    function deleteUser(_userId, _userLogin) {
        bootbox.confirm("<spring:message code="UI.Confirm.DeleteUser"/> (Login -> " + _userLogin + ")", function (result) {
            if (result) {
                jQueryAjax.ajaxPOST_HTML("${pageContext.request.contextPath}/administrator/users/" + _userId + "/delete_user_ajax", {}, null, true, "messages", null);
                tableReload();
            }
        });
    }

    /**
     * Lock user account
     */
    var lockUserId, lockUserLogin;
    function lockUserReason(_userId, _userLogin) {
        lockUserId = _userId;
        lockUserLogin = _userLogin;

        jQueryAjax.ajaxGET_HTML(jQueryAjax.getServerName() +
                "<c:url value="/administrator/users/lock_user_reason"/>",
                {}, lockUserReasonCallBack, true, null, 'ajaxError');
    }
    function lockUserReasonCallBack(data) {
        jQueryAjax.loadPageToDialog("Блокировка аккаунта пользователя. (Login -> "+lockUserLogin+")", data, {
            lockUser: {
                label: "Заблокировать",
                className: "btn-danger",
                callback: function () {
                    lockUser();
                    return false;
                }
            }
        });
    }
    function lockUser() {
        jQueryAjax.ajaxPOST_HTML("${pageContext.request.contextPath}/administrator/users/" + lockUserId + "/lock_user_ajax",
                {
                    lockReason: $("#lockReason").val(),
                    otherLockReason: $("#otherLockReason").val()
                },
                lockUserCallback, true, "messages", null);
        bootbox.hideAll();
        tableReload();
    }
    function lockUserCallback() {
        tableReload();
    }

    function unlockUser(_userId, _userLogin) {
        bootbox.confirm("<spring:message code="UI.Confirm.UnlockUser"/> (Login -> " + _userLogin + ")", function (result) {
            if (result) {
                jQueryAjax.ajaxPOST_HTML("${pageContext.request.contextPath}/administrator/users/" + _userId + "/unlock_user_ajax", {}, unlockUserCallback, true, "messages", null);
            }
        });
    }
    function unlockUserCallback() {
        tableReload();
    }
</script>

</body>
</html>
