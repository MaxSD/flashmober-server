﻿<%--suppress HtmlFormInputWithoutLabel --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<%@include file="/WEB-INF/jspf/checkBrowser.jspf" %>
<html>
<head>
    <%@include file="/WEB-INF/jspf/headSection.jspf" %>
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap-toggle.min.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/fileUpload/fileinput.min.css"/>"/>
</head>
<body>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<div class="container i-line">
    <div class="row">
        <%@include file="/WEB-INF/jspf/messages.jspf" %>

        <div class="col-md-8 col-md-offset-2">
            <form:form action="${pageContext.request.contextPath}/administrator/users/new_user" method="post"
                       modelAttribute="userCreateForm" autocomplete="true">

                <h4><spring:message code="Lbl.NewUser"/></h4>

                <table class="b-table table table-striped">
                    <tbody>
                    <tr>
                        <td><spring:message code="Lbl.Photo"/></td>
                        <td style="text-align: left;">
                            <input id="photoFile" name="photoFile" type="file">
                            <input id="photo" name="photo" type="hidden" value="">
                            <input id="photoFileNameForUpload" name="photoFileNameForUpload" type="hidden" value="${photoFileNameForUpload}">
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Login"/></td>
                        <td>
                            <form:input type="text" path="name" required="required" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Password"/></td>
                        <td>
                            <form:input type="password" path="password" required="required" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Email"/></td>
                        <td>
                            <form:input type="text" path="email" required="required" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Gender"/></td>
                        <td>
                            <select id="gender" name="gender" class="form-control">
                                <option value="1">Ж</option>
                                <option value="0">М</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Age"/></td>
                        <td>
                            <form:input type="text" path="age" required="required" class="form-control" onkeypress="return fieldValidator.checkNumber(event);"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.About"/></td>
                        <td>
                            <form:input type="text" path="about" required="required" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Country"/></td>
                        <td>
                            <form:input type="text" path="country" required="required" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.City"/></td>
                        <td>
                            <form:input type="text" path="city" required="required" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Language"/></td>
                        <td>
                            <select id="language" name="language" class="form-control">
                                <option value="ru" selected>Русский</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Site"/></td>
                        <td>
                            <form:input type="text" path="site" required="required" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Categories"/></td>
                        <td style="text-align: center;">
                            <input data-toggle="toggle" name="categoriesList" data-on="Животные" data-off="Животные" data-onstyle="success" data-offstyle="default" type="checkbox" value="1">
                            <input data-toggle="toggle" name="categoriesList" data-on="Кино" data-off="Кино" data-onstyle="success" data-offstyle="default" type="checkbox" value="2">
                            <input data-toggle="toggle" name="categoriesList" data-on="Искуство" data-off="Искуство" data-onstyle="success" data-offstyle="default" type="checkbox" value="3">
                            <input data-toggle="toggle" name="categoriesList" data-on="Еда" data-off="Еда" data-onstyle="success" data-offstyle="default" type="checkbox" value="4">
                            <input data-toggle="toggle" name="categoriesList" data-on="Музыка" data-off="Музыка" data-onstyle="success" data-offstyle="default" type="checkbox" value="5">
                            <input data-toggle="toggle" name="categoriesList" data-on="Юмор" data-off="Юмор" data-onstyle="success" data-offstyle="default" type="checkbox" value="6">
                            <input data-toggle="toggle" name="categoriesList" data-on="Места" data-off="Места" data-onstyle="success" data-offstyle="default" type="checkbox" value="7">
                            <input data-toggle="toggle" name="categoriesList" data-on="Спорт" data-off="Спорт" data-onstyle="success" data-offstyle="default" type="checkbox" value="9">
                            <input data-toggle="toggle" name="categoriesList" data-on="Мода и стиль" data-off="Мода и стиль" data-onstyle="success" data-offstyle="default" type="checkbox" value="10">
                            <input data-toggle="toggle" name="categoriesList" data-on="Семейное" data-off="Семейное" data-onstyle="success" data-offstyle="default" type="checkbox" value="11">
                            <input data-toggle="toggle" name="categoriesList" data-on="Социальное" data-off="Социальное" data-onstyle="success" data-offstyle="default" type="checkbox" value="12">
                            <input data-toggle="toggle" name="categoriesList" data-on="Отдых" data-off="Отдых" data-onstyle="success" data-offstyle="default" type="checkbox" value="13">
                            <input data-toggle="toggle" name="categoriesList" data-on="Разное" data-off="Разное" data-onstyle="success" data-offstyle="default" type="checkbox" value="14">
                            <input data-toggle="toggle" name="categoriesList" data-on="Наука и технологии" data-off="Наука и технологии" data-onstyle="success" data-offstyle="default" type="checkbox" value="8">
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Roles"/></td>
                        <td>
                            <c:if test="${not empty rolesList}">
                                <form:select path="role" class="form-control" onchange="if ($(this).val() == 4) {$('#flashmobUsersCountRow').show();} else {$('#flashmobUsersCountRow').hide();}">
                                    <form:options items="${rolesList}"/>
                                </form:select>
                            </c:if>
                        </td>
                    </tr>
                    <tr id="flashmobUsersCountRow" style="display: none;">
                        <td>Количество пользователей на которое назначается задание</td>
                        <td>
                            <input type="text" id="flashmobUsersCount" name="flashmobUsersCount" value="1000"
                                   onkeypress="return fieldValidator.checkNumber(event);" class="form-control"/>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <div>
                    <button class="btn btn-success pull-right" type="submit" name="submit">
                        <spring:message code="Btn.Save"/>
                    </button>

                    <a href="<c:url value="/administrator/users/list_users"/>">
                        <button class="btn btn-primary pull-left" type="button" name="btnCancel">
                            <spring:message code="Btn.Cancel"/>
                        </button>
                    </a>
                </div>
            </form:form>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf" %>
<script src="<c:url value="/resources/js/vendor/bootstrap-toggle.min.js"/>"></script>
<script src="<c:url value="/resources/js/fileUpload/fileinput.min.js"/>"></script>
<script src="<c:url value="/resources/js/fileUpload/fileinput_locale_ru.js"/>"></script>
<script>
    /* Initialize your widget via javascript as follows */
    $("#photoFile").fileinput({
        uploadUrl: "<c:url value="/uploadPhoto/${photoFileNameForUpload}"/>", // server upload action
        uploadAsync: true,
        minFileCount: 1,
        maxFileCount: 1,
        previewFileType: "image",
        language: "ru",
        allowedFileExtensions: ["jpeg", "jpg", "png", "gif"]
    });

    $(document).ready(function () {
        $("#photoFile").on('fileuploaded', function (event, data, previewId, index) {
            $("#photo").val($("#photoFile").val());
        });
    });
</script>
</body>
</html>
