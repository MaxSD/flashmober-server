<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<%@include file="/WEB-INF/jspf/checkBrowser.jspf" %>
<html>
<head>
    <%@include file="/WEB-INF/jspf/headSection.jspf" %>
</head>
<body>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<div class="container i-line">
    <div class="row">
        <div id="messages">
            <%@include file="/WEB-INF/jspf/messages.jspf" %>
        </div>

        <div class="col-md-10 col-md-offset-1">
            <h4><spring:message code="Lbl.Complaints"/></h4>

            <form id="filter" name="folter" method="post"
                  action="${pageContext.request.contextPath}/administrator/comments/exportReportsToFile">

                <input id="activeTab" name="activeTab" type="hidden" value="1"/>

                <fieldset style="text-align: center;">
                    <legend><spring:message code="Lbl.Filter"/></legend>

                    <div class="row">
                        <div class="form-inline">
                            <div class="form-group" style="align-content: center;">
                                <spring:message code="Lbl.User"/>:&nbsp;
                                <input id="userNameFilter" name="userNameFilter" class="form-control"/>
                                &nbsp;&nbsp;&nbsp;
                                <spring:message code="Lbl.Task"/>:&nbsp;
                                <input id="flashmobDescriptionFilter" name="flashmobDescriptionFilter" class="form-control"/>
                                <br/><br/>
                                <button id="btnFilter" name="btnFilter" class="btn btn-info" type="button"
                                        onclick="tableReload();">
                                    <spring:message code="Btn.Filter"/>
                                </button>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>

            <div id="all-comments">
                <form id="allCommentsForm" name="allCommentsForm" method="post">
                    <table id="allCommentsTable" class="display" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <!--<th>Photo</th>-->
                            <th><spring:message code="Lbl.ID"/></th>
                            <th><spring:message code="Lbl.UserPhoto"/></th>
                            <th><spring:message code="Lbl.UserName"/></th>
                            <th><spring:message code="Lbl.Task"/></th>
                            <th><spring:message code="Lbl.Report"/></th>
                            <th><spring:message code="Lbl.Comment"/></th>
                            <th><spring:message code="Lbl.Date"/></th>
                            <th><spring:message code="Lbl.Action"/></th>
                        </tr>
                        </thead>

                        <tfoot>
                        <tr>
                            <th><spring:message code="Lbl.ID"/></th>
                            <th><spring:message code="Lbl.UserPhoto"/></th>
                            <th><spring:message code="Lbl.UserName"/></th>
                            <th><spring:message code="Lbl.Task"/></th>
                            <th><spring:message code="Lbl.Report"/></th>
                            <th><spring:message code="Lbl.Comment"/></th>
                            <th><spring:message code="Lbl.Date"/></th>
                            <th><spring:message code="Lbl.Action"/></th>
                        </tr>
                        </tfoot>
                    </table>
                </form>
            </div>
            <br/>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf" %>

<script>
    var allCommentsTable;

    function tableReload() {
        if ($("#all-comments").is(":visible")) {
            allCommentsTable.ajax.reload();
        }
    }

    $(document).ready(function () {
        allCommentsTable = $('#allCommentsTable').DataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "bSort": false,
            "ajax": {
                "url": "${pageContext.request.contextPath}/administrator/comments/get_all_comments_JSON",
                "type": "POST",
                "data": function (d) {
                    d.userNameFilter = $("#userNameFilter").val();
                    d.flashmobDescriptionFilter = $("#flashmobDescriptionFilter").val();
                }
            },
            "language": {
                "processing": "Загрузка данных...",
                "search": "Поиск:",
                "lengthMenu": "Показать _MENU_ записей",
                "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                "infoEmpty": "Записи с 0 до 0 из 0 записей",
                "infoFiltered": "(отфильтровано из _MAX_ записей)",
                "infoPostFix": "",
                "loadingRecords": "Загрузка записей...",
                "zeroRecords": "Записи отсутствуют.",
                "emptyTable:": "В таблице отсутствуют данные",
                "paginate": {
                    "first": "Первая",
                    "previous": "Предыдущая",
                    "next": "Следующая",
                    "last": "Последняя"
                }
            }
        });
    });

    function deleteComment(_commentId) {
        bootbox.confirm("<spring:message code="UI.Confirm.DeleteComment"/>", function (result) {
            if (result) {
                jQueryAjax.ajaxPOST_HTML("${pageContext.request.contextPath}/administrator/comments/" + _commentId + "/delete_comment_ajax", {}, null, true, "messages", null);
                tableReload();
            }
        });
    }
</script>

</body>
</html>
