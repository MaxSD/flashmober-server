<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<%@include file="/WEB-INF/jspf/checkBrowser.jspf" %>
<html>
<head>
    <%@include file="/WEB-INF/jspf/headSection.jspf" %>
</head>
<body>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<div class="container i-line">
    <div class="row">
        <div id="messages">
            <%@include file="/WEB-INF/jspf/messages.jspf" %>
        </div>

        <div class="col-md-10 col-md-offset-1">
            <h4 style="color: #8b0000"><b><spring:message code="Lbl.Tasks"/></b></h4>

            <div style="margin: 15px 0;">
                <button id="btnAllFlashmobs" class="btn btn-primary" type="button" onclick="showTab(1);">
                    Все задания <span class="badge">${flashmobsCount}</span>
                </button>
                <c:if test="${currentUser.role.id == 1 || currentUser.role.id == 2}">
                    <button id="btnFlashmobsOnModeration" class="btn btn-default" type="button" onclick="showTab(2);">
                        На модерации <span class="badge">${flashmobsOnModerationCount}</span>
                    </button>
                </c:if>
                <sec:authorize access="hasAnyRole('PERMISSION_CREATE_TASKS')">
                    <a href="<c:url value="/administrator/flashmobs/new_flashmob"/>">
                        <button class="btn btn-success pull-right" type="button" name="btnCreateNewFlashmob">
                            <spring:message code="Btn.Create"/>
                        </button>
                    </a>
                </sec:authorize>
            </div>

            <form id="filter" name="folter" method="post"
                  action="${pageContext.request.contextPath}/administrator/flashmobs/exportToFile">

                <input id="usersType" name="usersType" type="hidden" value="1"/>

                <fieldset style="text-align: center;">
                    <legend><spring:message code="Lbl.Filter"/></legend>

                    <div class="row">
                        <div class="form-inline">
                            <div class="form-group" style="align-content: center;">
                                <spring:message code="Lbl.Description"/>:&nbsp;
                                <input id="descriptionFilter" name="descriptionFilter" class="form-control"/>
                                &nbsp;&nbsp;&nbsp;
                                <button id="btnFilter" name="btnFilter" class="btn btn-info" type="button"
                                        onclick="tableReload();">
                                    <spring:message code="Btn.Filter"/>
                                </button>
                                <sec:authorize access="hasAnyRole('PERMISSION_VIEW_USERS')">
                                    <button class="btn btn-default" type="submit" name="submit">
                                        <spring:message code="Btn.ExportToExcel"/>
                                    </button>
                                </sec:authorize>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>

            <div id="all-flashmobs">
                <form id="listAllFlashmobsForm" name="listAllFlashmobsForm" method="post">
                    <table id="allFlashmobsTable" class="display" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <!--<th>Photo</th>-->
                            <th><spring:message code="Lbl.Photo"/></th>
                            <th><spring:message code="Lbl.FlashmobDateCreate"/></th>
                            <th><spring:message code="Lbl.Description"/></th>
                            <c:if test="${currentUser.role.id == 1 || currentUser.role.id == 2}">
                            <th><spring:message code="Lbl.Owner"/></th>
                            </c:if>
                            <th><spring:message code="Lbl.Action"/></th>
                        </tr>
                        </thead>

                        <tfoot>
                        <tr>
                            <th><spring:message code="Lbl.Photo"/></th>
                            <th><spring:message code="Lbl.FlashmobDateCreate"/></th>
                            <th><spring:message code="Lbl.Description"/></th>
                            <c:if test="${currentUser.role.id == 1 || currentUser.role.id == 2}">
                                <th><spring:message code="Lbl.Owner"/></th>
                            </c:if>
                            <th><spring:message code="Lbl.Action"/></th>
                        </tr>
                        </tfoot>
                    </table>
                </form>
            </div>

            <c:if test="${currentUser.role.id == 1 || currentUser.role.id == 2}">
                <div id="flashmobs-on-moderation" style="display: none;">
                    <form id="flashmobsOnModerationForm" name="flashmobsOnModerationForm" method="post">
                        <table id="flashmobsOnModerationTable" class="display" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <!--<th>Photo</th>-->
                                <th><spring:message code="Lbl.Photo"/></th>
                                <th><spring:message code="Lbl.FlashmobDateCreate"/></th>
                                <th><spring:message code="Lbl.Description"/></th>
                                <th><spring:message code="Lbl.Owner"/></th>
                                <th><spring:message code="Lbl.Action"/></th>
                            </tr>
                            </thead>

                            <tfoot>
                            <tr>
                                <th><spring:message code="Lbl.Photo"/></th>
                                <th><spring:message code="Lbl.FlashmobDateCreate"/></th>
                                <th><spring:message code="Lbl.Description"/></th>
                                <th><spring:message code="Lbl.Owner"/></th>
                                <th><spring:message code="Lbl.Action"/></th>
                            </tr>
                            </tfoot>
                        </table>
                    </form>
                </div>
            </c:if>

            <br/>

            <div>
                <sec:authorize access="hasAnyRole('PERMISSION_CREATE_TASKS')">
                    <a href="<c:url value="/administrator/flashmobs/new_flashmob"/>">
                        <button class="btn btn-success pull-right" type="button" name="btnCreateNewFlashmob">
                            <spring:message code="Btn.Create"/>
                        </button>
                    </a>
                </sec:authorize>
            </div>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf" %>

<script>
    var btnAllFlashmobs = $("#btnAllFlashmobs");
    var btnFlashmobsOnModeration = $("#btnFlashmobsOnModeration");

    var tab1 = $("#all-flashmobs");
    var tab2 = $("#flashmobs-on-moderation");

    var allFlashmobsTable;
    var flashmobsOnModerationTable;

    function showTab(activeTab) {
        if (activeTab == 1) {
            btnAllFlashmobs.attr("class", "btn btn-primary");
            btnFlashmobsOnModeration.attr("class", "btn btn-default");

            tab2.hide();
            tab1.show();

            $("#activeTab").val('1');
        } else if (activeTab == 2) {
            btnAllFlashmobs.attr("class", "btn btn-default");
            btnFlashmobsOnModeration.attr("class", "btn btn-primary");

            tab1.hide();
            tab2.show();

            $("#activeTab").val('2');
        }
    }

    function tableReload() {
        if (tab1.is(":visible")) {
            allFlashmobsTable.ajax.reload();
        } else if (tab2.is(":visible")) {
            flashmobsOnModerationTable.ajax.reload();
        }
    }

    $(document).ready(function () {
        allFlashmobsTable = $('#allFlashmobsTable').DataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "bSort": false,
            "ajax": {
                <c:choose>
                <c:when test="${currentUser.role.id == 1 || currentUser.role.id == 2}">
                "url": "${pageContext.request.contextPath}/administrator/flashmobs/get_all_flashmobs_JSON",
                </c:when>
                <c:otherwise>
                "url": "${pageContext.request.contextPath}/administrator/flashmobs/${currentUser.id}/get_my_flashmobs_JSON",
                </c:otherwise>
                </c:choose>
                "type": "POST",
                "data": function (d) {
                    d.descriptionFilter = $("#descriptionFilter").val();
                }
            },
            "language": {
                "processing": "Загрузка данных...",
                "search": "Поиск:",
                "lengthMenu": "Показать _MENU_ записей",
                "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                "infoEmpty": "Записи с 0 до 0 из 0 записей",
                "infoFiltered": "(отфильтровано из _MAX_ записей)",
                "infoPostFix": "",
                "loadingRecords": "Загрузка записей...",
                "zeroRecords": "Записи отсутствуют.",
                "emptyTable:": "В таблице отсутствуют данные",
                "paginate": {
                    "first": "Первая",
                    "previous": "Предыдущая",
                    "next": "Следующая",
                    "last": "Последняя"
                }
            }
        });
    });

    <c:if test="${currentUser.role.id == 1 || currentUser.role.id == 2}">
    $(document).ready(function () {
        flashmobsOnModerationTable = $('#flashmobsOnModerationTable').DataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "bSort": false,
            "ajax": {
                "url": "${pageContext.request.contextPath}/administrator/flashmobs/get_flashmobs_on_moderation_JSON",
                "type": "POST",
                "data": function (d) {
                    d.descriptionFilter = $("#descriptionFilter").val();
                }
            },
            "language": {
                "processing": "Загрузка данных...",
                "search": "Поиск:",
                "lengthMenu": "Показать _MENU_ записей",
                "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                "infoEmpty": "Записи с 0 до 0 из 0 записей",
                "infoFiltered": "(отфильтровано из _MAX_ записей)",
                "infoPostFix": "",
                "loadingRecords": "Загрузка записей...",
                "zeroRecords": "Записи отсутствуют.",
                "emptyTable:": "В таблице отсутствуют данные",
                "paginate": {
                    "first": "Первая",
                    "previous": "Предыдущая",
                    "next": "Следующая",
                    "last": "Последняя"
                }
            }
        });
    });
    </c:if>

    /**
     * Delete flashmob
     */
    var flashmobId;
    function deleteFlashmobReason(_flashmobId) {
        flashmobId = _flashmobId;

        jQueryAjax.ajaxGET_HTML(jQueryAjax.getServerName() +
                "<c:url value="/administrator/flashmobs/delete_flashmob_reason"/>",
                {}, deleteReasonCallBack, true, null, 'ajaxError');
    }
    function deleteReasonCallBack(data) {
        jQueryAjax.loadPageToDialog("Удаление задания", data, {
            lockUser: {
                label: "Удалить",
                className: "btn-danger",
                callback: function () {
                    deleteFlashmob();
                    return false;
                }
            }
        });
    }
    function deleteFlashmob() {
        jQueryAjax.ajaxPOST_HTML("${pageContext.request.contextPath}/administrator/flashmobs/" + flashmobId + "/delete_flashmob_ajax",
                {
                    deleteReason: $("#deleteReason").val(),
                    otherDeleteReason: $("#otherDeleteReason").val()
                },
                deleteCallback, true, "messages", null);
        bootbox.hideAll();
        tableReload();
    }
    function deleteCallback() {
        tableReload();
    }
</script>

</body>
</html>
