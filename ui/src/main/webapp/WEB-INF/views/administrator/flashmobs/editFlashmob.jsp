﻿<%--suppress HtmlFormInputWithoutLabel --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<%@include file="/WEB-INF/jspf/checkBrowser.jspf" %>
<html>
<head>
    <%@include file="/WEB-INF/jspf/headSection.jspf" %>
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap-toggle.min.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/fileUpload/fileinput.min.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/jquery-ui/jquery-bootstrap-datepicker.css"/>"/>
</head>
<body>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<div class="container i-line">
    <div class="row">
        <div id="messages">
            <%@include file="/WEB-INF/jspf/messages.jspf" %>
        </div>

        <div class="col-md-8 col-md-offset-2">
            <form:form id="flashmobEditForm" name="flashmobEditForm"
                       action="${pageContext.request.contextPath}/administrator/flashmobs/${flashmobEditForm.id}/edit_flashmob"
                       method="post"
                       modelAttribute="flashmobEditForm" autocomplete="true">

                <input id="flashmobId" name="flashmobId" type="hidden" value="${flashmobEditForm.id}"/>

                <h4><spring:message code="Lbl.EditTask"/></h4>

                <table class="b-table table table-striped">
                    <tbody>
                    <tr>
                        <td><spring:message code="Lbl.Photo"/></td>
                        <td style="text-align: left;">
                            <input id="photoFile" name="photoFile" type="file">
                            <input id="photo" name="photo" type="hidden" value="">
                            <input id="photoFileNameForUpload" name="photoFileNameForUpload" type="hidden" value="${photoFileNameForUpload}">
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Description"/></td>
                        <td>
                            <form:input type="text" path="description" required="required" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td>Ссылка на видео</td>
                        <td>
                            <input id="video" name="video" type="text" class="form-control" value="${flashmobEditForm.video}"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.HashTags"/></td>
                        <td>
                            <input type="text" id="tags" name="tags" required="required" class="form-control"
                                   value="${tagsList}"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.FlashmobDateCreate"/></td>
                        <td>
                            <input type="text" class="form-control" value="${dateCreate}" disabled/>
                        </td>
                    </tr>
                    <tr>
                        <td>Дата публикации</td>
                        <td>
                            <div class="form-inline">
                                <div class="form-group">
                                    <input type="text" id="datePublication" name="datePublication" required="required"
                                           class="form-control" style="width: 100px;" readonly/>
                                    <c:if test="${isPublished == true}">
                                        <button class="btn btn-default" type="button" id="btnChangeDatePublication"
                                                name="btnChangeDatePublication">
                                            Выбрать дату
                                        </button>
                                    </c:if>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Categories"/></td>
                        <td style="text-align: center;">
                            <input data-toggle="toggle" name="categories_ids" data-on="Животные" data-off="Животные"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="1" <c:if
                                    test="${categoriesList.contains(1)}"> checked</c:if>>
                            <input data-toggle="toggle" name="categories_ids" data-on="Кино" data-off="Кино"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="2" <c:if
                                    test="${categoriesList.contains(2)}"> checked</c:if>>
                            <input data-toggle="toggle" name="categories_ids" data-on="Искуство" data-off="Искуство"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="3" <c:if
                                    test="${categoriesList.contains(3)}"> checked</c:if>>
                            <input data-toggle="toggle" name="categories_ids" data-on="Еда" data-off="Еда"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="4" <c:if
                                    test="${categoriesList.contains(4)}"> checked</c:if>>
                            <input data-toggle="toggle" name="categories_ids" data-on="Музыка" data-off="Музыка"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="5" <c:if
                                    test="${categoriesList.contains(5)}"> checked</c:if>>
                            <input data-toggle="toggle" name="categories_ids" data-on="Юмор" data-off="Юмор"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="6" <c:if
                                    test="${categoriesList.contains(6)}"> checked</c:if>>
                            <input data-toggle="toggle" name="categories_ids" data-on="Места" data-off="Места"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="7" <c:if
                                    test="${categoriesList.contains(7)}"> checked</c:if>>
                            <input data-toggle="toggle" name="categories_ids" data-on="Спорт" data-off="Спорт"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="9" <c:if
                                    test="${categoriesList.contains(9)}"> checked</c:if>>
                            <input data-toggle="toggle" name="categories_ids" data-on="Мода и стиль"
                                   data-off="Мода и стиль" data-onstyle="success" data-offstyle="default"
                                   type="checkbox" value="10" <c:if test="${categoriesList.contains(10)}">
                                   checked</c:if>>
                            <input data-toggle="toggle" name="categories_ids" data-on="Семейное" data-off="Семейное"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="11" <c:if
                                    test="${categoriesList.contains(11)}"> checked</c:if>>
                            <input data-toggle="toggle" name="categories_ids" data-on="Социальное" data-off="Социальное"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="12" <c:if
                                    test="${categoriesList.contains(12)}"> checked</c:if>>
                            <input data-toggle="toggle" name="categories_ids" data-on="Отдых" data-off="Отдых"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="13" <c:if
                                    test="${categoriesList.contains(13)}"> checked</c:if>>
                            <input data-toggle="toggle" name="categories_ids" data-on="Разное" data-off="Разное"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="14" <c:if
                                    test="${categoriesList.contains(14)}"> checked</c:if>>
                            <input data-toggle="toggle" name="categories_ids" data-on="Наука и технологии"
                                   data-off="Наука и технологии" data-onstyle="success" data-offstyle="default"
                                   type="checkbox" value="8" <c:if test="${categoriesList.contains(8)}"> checked</c:if>>
                        </td>
                    </tr>
                    <c:choose>
                        <c:when test="${currentUser.isBusinessUser}">
                            <input type="hidden" id="is_default" name="is_default" value="false"/>
                        </c:when>
                        <c:otherwise>
                            <tr>
                                <td>Дефаултное задание</td>
                                <td style="text-align: left;">
                                    <input data-toggle="toggle" id="is_default" name="is_default" data-on="Да"
                                           data-off="Нет"
                                           data-onstyle="success" data-offstyle="default"
                                           value="true"
                                           type="checkbox"
                                    <c:if test="${flashmobEditForm.isDefault}"> checked</c:if>
                                    >
                                </td>
                            </tr>
                        </c:otherwise>
                    </c:choose>
                    </tbody>
                </table>

                <div style="padding: 15px 0;">
                    <c:if test="${isUserCanDoModeration && flashmobEditForm.moderation && !flashmobEditForm.isAcceptedBbyAdmin}">
                            <h4>Модерация</h4>
                            <hr/>
                            <textarea id="moderatorComment" name="moderatorComment"
                                      class="form-control">${flashmobEditForm.comment}</textarea>
                            <button class="btn btn-warning pull-right" type="button" id="btnAccept" name="btnAccept"
                                    onclick="acceptFlashmob();">
                                Одобрить
                            </button>
                            <br/><br/>
                    </c:if>
                </div>

                <div>
                    <sec:authorize access="hasAnyRole('PERMISSION_EDIT_TASKS')">
                        <button class="btn btn-success pull-right" type="button" id="btnSave" name="btnSave"
                                onclick="$('#flashmobEditForm').submit();">
                            <spring:message code="Btn.Save"/>
                        </button>
                    </sec:authorize>
                </div>

                <div>
                    <input id="activeTab" name="activeTab" type="hidden" value="1"/>

                    <div style="margin: 15px 0;">
                        <button id="btnFlashmobReports" class="btn btn-primary" type="button" onclick="showTab(1);">
                            Отчёты <span class="badge">${flashmobReportsCount}</span>
                        </button>
                        <button id="btnFlashmobAcceptUsers" class="btn btn-default" type="button"
                                onclick="showTab(2);">
                            Приняли <span class="badge">${flashmobAcceptUsersCount}</span>
                        </button>
                        <button id="btnFlashmobRejectUsers" class="btn btn-default" type="button"
                                onclick="showTab(3);">
                            Отказались <span class="badge">${flashmobRejectUsersCount}</span>
                        </button>
                    </div>

                    <div id="flashmobReports">
                        <table id="flashmobReportsTable" class="display" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th><spring:message code="Lbl.Photo"/></th>
                                <th><spring:message code="Lbl.User"/></th>
                                <th><spring:message code="Lbl.Date"/></th>
                            </tr>
                            </thead>

                            <tfoot>
                            <tr>
                                <th><spring:message code="Lbl.Photo"/></th>
                                <th><spring:message code="Lbl.User"/></th>
                                <th><spring:message code="Lbl.Date"/></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>

                    <div id="flashmobAcceptUsers" style="display: none;">
                        <table id="flashmobAcceptUsersTable" class="display" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th><spring:message code="Lbl.Photo"/></th>
                                <th><spring:message code="Lbl.User"/></th>
                                <th><spring:message code="Lbl.Date"/></th>
                            </tr>
                            </thead>

                            <tfoot>
                            <tr>
                                <th><spring:message code="Lbl.Photo"/></th>
                                <th><spring:message code="Lbl.User"/></th>
                                <th><spring:message code="Lbl.Date"/></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>

                    <div id="flashmobRejectUsers" style="display: none;">
                        <table id="flashmobRejectUsersTable" class="display" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th><spring:message code="Lbl.Photo"/></th>
                                <th><spring:message code="Lbl.User"/></th>
                                <th><spring:message code="Lbl.Date"/></th>
                            </tr>
                            </thead>

                            <tfoot>
                            <tr>
                                <th><spring:message code="Lbl.Photo"/></th>
                                <th><spring:message code="Lbl.User"/></th>
                                <th><spring:message code="Lbl.Date"/></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </form:form>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf" %>
<script src="<c:url value="/resources/js/jQuery/jquery-1.11.2.ui.min.js"/>"></script>
<script src="<c:url value="/resources/js/vendor/bootstrap-toggle.min.js"/>"></script>
<script src="<c:url value="/resources/js/fileUpload/fileinput.min.js"/>"></script>
<script src="<c:url value="/resources/js/fileUpload/fileinput_locale_ru.js"/>"></script>
<script src="<c:url value="/resources/js/jQuery/i18n/datepicker-ru.js"/>"></script>
<script>
    $("#photoFile").fileinput({
        uploadUrl: "<c:url value="/uploadPhoto/${photoFileNameForUpload}"/>", // server upload action
        uploadAsync: true,
        minFileCount: 1,
        maxFileCount: 1,
        previewFileType: "image",
        language: "ru",
        allowedFileExtensions: ["jpeg", "jpg", "png", "gif"],
        initialPreview: [
            "<img src='${flashmobEditForm.photo}' width='300'>"
        ],
        previewSettings: {
            image: {width: "300px", height: "auto"}
        }
    });

    var flashmobReportsTable;
    var flashmobAcceptUsersTable;
    var flashmobRejectUsersTable;

    function showTab(activeTab) {
        var tab1 = $("#flashmobReports");
        var tab2 = $("#flashmobAcceptUsers");
        var tab3 = $("#flashmobRejectUsers");

        var btnTab1 = $("#btnFlashmobReports");
        var btnTab2 = $("#btnFlashmobAcceptUsers");
        var btnTab3 = $("#btnFlashmobRejectUsers");

        if (activeTab == 1) {
            btnTab1.attr("class", "btn btn-primary");
            btnTab2.attr("class", "btn btn-default");
            btnTab3.attr("class", "btn btn-default");

            tab3.hide();
            tab2.hide();
            tab1.show();

            $("#activeTab").val('1');
        } else if (activeTab == 2) {
            btnTab1.attr("class", "btn btn-default");
            btnTab3.attr("class", "btn btn-default");
            btnTab2.attr("class", "btn btn-primary");

            tab1.hide();
            tab3.hide();
            tab2.show();

            $("#activeTab").val('2');
        } else if (activeTab == 3) {
            btnTab1.attr("class", "btn btn-default");
            btnTab2.attr("class", "btn btn-default");
            btnTab3.attr("class", "btn btn-primary");

            tab1.hide();
            tab2.hide();
            tab3.show();

            $("#activeTab").val('3');
        }
    }

    $(document).ready(function () {
        $("#photoFile").on('fileuploaded', function(event, data, previewId, index) {
            $("#photo").val($("#photoFile").val());
        });

        var datePublication = $("#datePublication").datepicker({
            dateFormat: "dd/mm/yy"
        });

        datePublication.datepicker("setDate", "${datePublication}");

        $("#btnChangeDatePublication").click(function () {
            datePublication.datepicker('show');
        });

        flashmobReportsTable = $('#flashmobReportsTable').DataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "bSort": false,
            "ajax": {
                "url": "${pageContext.request.contextPath}/administrator/reports/get_flashmobReports_JSON",
                "type": "POST",
                "data": function (d) {
                    d.flashmobId = ${flashmobEditForm.id};
                }
            },
            "language": {
                "processing": "Загрузка данных...",
                "search": "Поиск:",
                "lengthMenu": "Показать _MENU_ записей",
                "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                "infoEmpty": "Записи с 0 до 0 из 0 записей",
                "infoFiltered": "(отфильтровано из _MAX_ записей)",
                "infoPostFix": "",
                "loadingRecords": "Загрузка записей...",
                "zeroRecords": "Записи отсутствуют.",
                "emptyTable:": "В таблице отсутствуют данные",
                "paginate": {
                    "first": "Первая",
                    "previous": "Предыдущая",
                    "next": "Следующая",
                    "last": "Последняя"
                }
            }
        });

        flashmobAcceptUsersTable = $('#flashmobAcceptUsersTable').DataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "bSort": false,
            "ajax": {
                "url": "${pageContext.request.contextPath}/administrator/flashmobs/get_users_accepted_flashmob_JSON",
                "type": "POST",
                "data": function (d) {
                    d.flashmobId = ${flashmobEditForm.id};
                }
            },
            "language": {
                "processing": "Загрузка данных...",
                "search": "Поиск:",
                "lengthMenu": "Показать _MENU_ записей",
                "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                "infoEmpty": "Записи с 0 до 0 из 0 записей",
                "infoFiltered": "(отфильтровано из _MAX_ записей)",
                "infoPostFix": "",
                "loadingRecords": "Загрузка записей...",
                "zeroRecords": "Записи отсутствуют.",
                "emptyTable:": "В таблице отсутствуют данные",
                "paginate": {
                    "first": "Первая",
                    "previous": "Предыдущая",
                    "next": "Следующая",
                    "last": "Последняя"
                }
            }
        });

        flashmobRejectUsersTable = $('#flashmobRejectUsersTable').DataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "bSort": false,
            "ajax": {
                "url": "${pageContext.request.contextPath}/administrator/flashmobs/get_users_rejected_flashmob_JSON",
                "type": "POST",
                "data": function (d) {
                    d.flashmobId = ${flashmobEditForm.id};
                }
            },
            "language": {
                "processing": "Загрузка данных...",
                "search": "Поиск:",
                "lengthMenu": "Показать _MENU_ записей",
                "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                "infoEmpty": "Записи с 0 до 0 из 0 записей",
                "infoFiltered": "(отфильтровано из _MAX_ записей)",
                "infoPostFix": "",
                "loadingRecords": "Загрузка записей...",
                "zeroRecords": "Записи отсутствуют.",
                "emptyTable:": "В таблице отсутствуют данные",
                "paginate": {
                    "first": "Первая",
                    "previous": "Предыдущая",
                    "next": "Следующая",
                    "last": "Последняя"
                }
            }
        });
    });

    function acceptFlashmob() {
        $("#flashmobEditForm").attr("action", "${pageContext.request.contextPath}/administrator/flashmobs/${flashmobEditForm.id}/accept_flashmob_by_moderator");
        $("#flashmobEditForm").submit();

        /*
        jQueryAjax.ajaxPOST_HTML("${pageContext.request.contextPath}/administrator/flashmobs/${flashmobEditForm.id}/accept_flashmob_by_moderator_ajax",
                {
                    "moderatorComment": $("#moderatorComment").val()
                },
                null, true, "messages", null);
                */
    }
</script>
</body>
</html>
