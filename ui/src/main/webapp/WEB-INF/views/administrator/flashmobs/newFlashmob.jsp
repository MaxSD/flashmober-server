﻿﻿<%--suppress HtmlFormInputWithoutLabel --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<%@include file="/WEB-INF/jspf/checkBrowser.jspf" %>
<html>
<head>
    <%@include file="/WEB-INF/jspf/headSection.jspf" %>
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap-toggle.min.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/fileUpload/fileinput.min.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/jquery-ui/jquery-bootstrap-datepicker.css"/>"/>
</head>
<body>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<div class="container i-line">
    <div class="row">
        <%@include file="/WEB-INF/jspf/messages.jspf" %>

        <div class="col-md-8 col-md-offset-2">
            <form:form id="flashmobCreateForm" name="flashmobCreateForm"
                       action="${pageContext.request.contextPath}/administrator/flashmobs/new_flashmob" method="post"
                       modelAttribute="flashmobCreateForm" autocomplete="true">

                <h4><spring:message code="Lbl.NewTask"/></h4>

                <input type="hidden" id="is_from_admin_area" name="is_from_admin_area" value="true"/>
                <table class="b-table table table-striped">
                    <tbody>
                    <tr>
                        <td><spring:message code="Lbl.Photo"/></td>
                        <td style="text-align: left;">
                            <input id="photo" name="photo" type="file">
                            <input id="photoFileName" name="photoFileName" type="hidden" value="">
                            <input id="photoFileNameForUpload" name="photoFileNameForUpload" type="hidden" value="${photoFileNameForUpload}">
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Description"/></td>
                        <td>
                            <form:input type="text" path="description" required="required" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td>Ссылка на видео</td>
                        <td>
                            <input id="video" name="video" type="text" class="form-control" value=""/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.HashTags"/></td>
                        <td>
                            <input type="text" id="tags" name="tags" required="required" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td>Дата публикации</td>
                        <td>
                            <div class="form-inline">
                                <div class="form-group">
                                    <input type="text" id="datePublication" name="datePublication" required="required"
                                           class="form-control" style="width: 100px;" readonly/>
                                    <button class="btn btn-default" type="button" id="btnChangeDatePublication"
                                            name="btnChangeDatePublication">
                                        Выбрать дату
                                    </button>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Categories"/></td>
                        <td style="text-align: center;">
                            <input data-toggle="toggle" name="categories_ids" data-on="Животные" data-off="Животные"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="1">
                            <input data-toggle="toggle" name="categories_ids" data-on="Кино" data-off="Кино"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="2">
                            <input data-toggle="toggle" name="categories_ids" data-on="Искуство" data-off="Искуство"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="3">
                            <input data-toggle="toggle" name="categories_ids" data-on="Еда" data-off="Еда"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="4">
                            <input data-toggle="toggle" name="categories_ids" data-on="Музыка" data-off="Музыка"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="5">
                            <input data-toggle="toggle" name="categories_ids" data-on="Юмор" data-off="Юмор"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="6">
                            <input data-toggle="toggle" name="categories_ids" data-on="Места" data-off="Места"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="7">
                            <input data-toggle="toggle" name="categories_ids" data-on="Спорт" data-off="Спорт"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="9">
                            <input data-toggle="toggle" name="categories_ids" data-on="Мода и стиль"
                                   data-off="Мода и стиль" data-onstyle="success" data-offstyle="default"
                                   type="checkbox" value="10">
                            <input data-toggle="toggle" name="categories_ids" data-on="Семейное" data-off="Семейное"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="11">
                            <input data-toggle="toggle" name="categories_ids" data-on="Социальное" data-off="Социальное"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="12">
                            <input data-toggle="toggle" name="categories_ids" data-on="Отдых" data-off="Отдых"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="13">
                            <input data-toggle="toggle" name="categories_ids" data-on="Разное" data-off="Разное"
                                   data-onstyle="success" data-offstyle="default" type="checkbox" value="14">
                            <input data-toggle="toggle" name="categories_ids" data-on="Наука и технологии"
                                   data-off="Наука и технологии" data-onstyle="success" data-offstyle="default"
                                   type="checkbox" value="8">
                        </td>
                    </tr>
                    <tr id="sendIntoAbyss">
                        <td>Отправить в бездну</td>
                        <td style="text-align: left;">
                            <input data-toggle="toggle" id="send_into_abyss" name="send_into_abyss" data-on="Да"
                                   data-off="Нет"
                                   data-onstyle="success" data-offstyle="default"
                                   value="true"
                                   type="checkbox">
                        </td>
                    </tr>
                    <c:choose>
                        <c:when test="${currentUser.isBusinessUser}">
                            <input type="hidden" id="is_default" name="is_default" value="false"/>
                        </c:when>
                        <c:otherwise>
                            <tr>
                                <td>Дефаултное задание</td>
                                <td style="text-align: left;">
                                    <input data-toggle="toggle" id="is_default" name="is_default" data-on="Да"
                                           data-off="Нет"
                                           data-onstyle="success" data-offstyle="default"
                                           value="true"
                                           type="checkbox">
                                </td>
                            </tr>
                        </c:otherwise>
                    </c:choose>
                    </tbody>
                </table>

                <div style="margin: 15px 0;">
                    <input id="uid" name="uid" type="hidden" value="${flashmobCreateForm.owner_id}"/>

                    <div id="choseUser">
                        <div>
                            <div style="font-size: 20px; text-align: center;">
                                <b>Пользователи, на которых нужно назначить задание</b>
                            </div>

                            <fieldset style="text-align: center;">
                                <legend><spring:message code="Lbl.Filter"/></legend>

                                <div class="row">
                                    <div class="form-inline">
                                        <div class="form-group" style="align-content: center;">
                                            <spring:message code="Lbl.Login"/>:&nbsp;
                                            <input id="nameFilterUsers" name="nameFilterUsers" class="form-control"/>
                                            &nbsp;&nbsp;&nbsp;
                                            <spring:message code="Lbl.Email"/>:&nbsp;
                                            <input id="emailFilterUsers" name="emailFilterUsers" class="form-control"/>
                                            <br/><br/>
                                            <spring:message code="Lbl.Country"/>:&nbsp;
                                            <select id="countryFilter" name="countryFilter" class="form-control">
                                                <option value="">Все</option>
                                                <c:if test="${not empty userCountries}">
                                                    <c:forEach var="country" items="${userCountries}">
                                                        <c:if test="${not empty country}">
                                                            <option value="${country}">${country}</option>
                                                        </c:if>
                                                    </c:forEach>
                                                </c:if>
                                            </select>
                                            &nbsp;&nbsp;&nbsp;
                                            <spring:message code="Lbl.City"/>:&nbsp;
                                            <select id="cityFilter" name="cityFilter" class="form-control">
                                                <option value="">Все</option>
                                                <c:if test="${not empty userCities}">
                                                    <c:forEach var="city" items="${userCities}">
                                                        <c:if test="${not empty city}">
                                                            <option value="${city}">${city}</option>
                                                        </c:if>
                                                    </c:forEach>
                                                </c:if>
                                            </select>
                                            &nbsp;&nbsp;&nbsp;
                                            <spring:message code="Lbl.Gender"/>:&nbsp;
                                            <select id="genderFilter" name="genderFilter" class="form-control">
                                                <option value="">Все</option>
                                                <option value="1">Ж</option>
                                                <option value="0">М</option>
                                            </select>
                                            &nbsp;&nbsp;&nbsp;
                                            <spring:message code="Lbl.Age"/>:&nbsp;от&nbsp;
                                            <input id="ageFromFilter" name="ageFromFilter" class="form-control"
                                                   maxlength="2" style="width: 50px"/>
                                            &nbsp;до&nbsp;
                                            <input id="ageToFilter" name="ageToFilter" class="form-control"
                                                   maxlength="2" style="width: 50px"/>

                                            &nbsp;&nbsp;&nbsp;
                                            <button id="btnFilter" name="btnFilter" class="btn btn-info" type="button"
                                                    onclick="usersTable.ajax.reload();">
                                                <spring:message code="Btn.Filter"/>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <br/>

                        <div>
                            <input data-toggle="toggle" id="checkAllUsers" name="checkAllUsers"
                                   data-on="Выбрать всех" data-off="Выбрать всех"
                                   data-onstyle="success" data-offstyle="default"
                                   type="checkbox" style="width: 70px;">
                        </div>
                        <table id="usersTable" class="display" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <td></td>
                                <th><spring:message code="Lbl.Login"/></th>
                                <th><spring:message code="Lbl.Email"/></th>
                            </tr>
                            </thead>

                            <tfoot>
                            <tr>
                                <td></td>
                                <th><spring:message code="Lbl.Login"/></th>
                                <th><spring:message code="Lbl.Email"/></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

                <br/><br/>

                <div>
                    <sec:authorize access="hasAnyRole('PERMISSION_CREATE_TASKS')">
                        <button class="btn btn-success pull-right" type="submit" id="btnSave" name="btnSave">
                            <spring:message code="Btn.Create"/>
                        </button>
                    </sec:authorize>

                    <a href="<c:url value="/administrator/flashmobs/list_flashmobs"/>">
                        <button class="btn btn-primary pull-left" type="button" name="btnCancel">
                            <spring:message code="Btn.Cancel"/>
                        </button>
                    </a>
                </div>
            </form:form>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf" %>
<script src="<c:url value="/resources/js/jQuery/jquery-1.11.2.ui.min.js"/>"></script>
<script src="<c:url value="/resources/js/vendor/bootstrap-toggle.min.js"/>"></script>
<script src="<c:url value="/resources/js/fileUpload/fileinput.min.js"/>"></script>
<script src="<c:url value="/resources/js/fileUpload/fileinput_locale_ru.js"/>"></script>
<script src="<c:url value="/resources/js/jQuery/i18n/datepicker-ru.js"/>"></script>
<script>
    /* Initialize your widget via javascript as follows */
    $("#photo").fileinput({
        uploadUrl: "<c:url value="/uploadPhotoFlashmobCreating/${photoFileNameForUpload}"/>", // server upload action
        uploadAsync: true,
        maxFileCount: 1,
        previewFileType: "image",
        language: "ru",
        allowedFileExtensions: ["jpeg", "jpg", "png", "gif"]
    });

    var choseUser = $("#choseUser");
    var sendIntoAbyss = $("#sendIntoAbyss");
    var usersTable = $("#usersTable");

    function selectAllUsers() {
        $("#friends_ids").setAttr("checked", "checked");
    }

    function createJSON() {
        var jsonObj = [];
        $("input[class=email]").each(function () {

            var id = $(this).attr("title");
            var email = $(this).val();

            var item = {};
            item ["title"] = id;
            item ["email"] = email;

            jsonObj.push(item);
        });
    }

    function createJSON_1() {
        var o = {};
        var a = $("#flashmobCreateForm").serializeArray();
        $.each(a, function () {
            if (this.name == "uid" ||
                    this.name == "photo" ||
                    this.name == "description" ||
                    this.name == "tags" ||
                    this.name == "friends_ids" ||
                    this.name == "categories_ids" ||
                    this.name == "send_into_abyss") {

                if (o[this.name] !== undefined) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            }
        });

        alert(JSON.stringify(o));
    }

    function findAndRemoveJSONElement(array, property, value) {
        $.each(array, function (index, result) {
            if (result[property] == value) {
                //Remove from array
                array.splice(index, 1);
            }
        });
    }

    function findAndRemoveNotNeededJSONElements(array, property) {
        $.each(array, function (index, result) {
            if (result[property] != "uid" &&
                    result[property] != "photo" &&
                    result[property] != "description" &&
                    result[property] != "tags" &&
                    result[property] != "friends_ids" &&
                    result[property] != "categories_ids" &&
                    result[property] != "send_into_abyss") {
                //Remove from array
                array.splice(index, 1);
            }
        });
    }

    $("form#flashmobCreateForm").submit(function (event) {

        // check, is photo present
        if (!$('#photoFileName').val()) {
            bootbox.dialog({
                title: "Создание задания",
                message: "<span style='color: #8b0000;'>Ошибка: Необходимо выбрать и загрузить фотографию.</span>"
            })
            return false;
        }

        //disable the default form submission
        event.preventDefault();

        //grab all form data
        var formData = new FormData($(this)[0]);

        $.ajax({
            url: '${pageContext.request.contextPath}/rest/v1/flashmob/create',
            type: 'POST',
            data: formData,
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                setTimeout(jQueryAjax.showPleaseWait, 1);
            },
            complete: function (jqXHR, textStatus) {
                setTimeout(jQueryAjax.hidePleaseWait, 1);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (errorThrown == 'CUSTOM')
                    jQueryAjax.showErrorDialog('Error!', textStatus + ': ' + errorThrown + "<br/>" + jqXHR.responseText);
                else
                    jQueryAjax.showErrorDialog('Error!', textStatus + ': ' + errorThrown);
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    bootbox.dialog({
                        title: "Создание задания",
                        message: "<span style='color: #006400;'>Успешно создано.</span>"
                    });
                    window.location = "<c:url value="/administrator/flashmobs/list_flashmobs"/>";
                } else {
                    bootbox.dialog({
                        title: "Создание задания",
                        message: "<span style='color: #8b0000;'>Ошибка: " + JSON.stringify(data) + "</span>"
                    })
                }
            }
        });

        return false;
    });

    function tableReload() {
        usersTable.ajax.reload();
    }

    $(document).ready(function () {
        $("#photo").on('fileuploaded', function (event, data, previewId, index) {
            $("#photoFileName").val($("#photo").val());
        });

        $("#send_into_abyss").change(function () {
            if ($(this).prop('checked')) {
                choseUser.hide();
                sendIntoAbyss.hide();
            } else {
                if ($(this).prop('checked')) {
                    choseUser.hide();
                } else {
                    choseUser.show();
                }
                sendIntoAbyss.show();
            }
        });

        var datePublication = $("#datePublication").datepicker({
            dateFormat: "dd/mm/yy"
        });

        datePublication.datepicker("setDate", "${datePublication}");

        $("#btnChangeDatePublication").click(function () {
            datePublication.datepicker('show');
        });

        $("#is_default").change(function () {
            if ($(this).prop('checked')) {
                choseUser.hide();
            } else {
                choseUser.show();
            }
        });

        $("#checkAllUsers").change(function () {
            if ($(this).prop('checked')) {
                $("#usersTable input:checkbox").prop("checked", true);
            } else {
                $("#usersTable input:checkbox").prop("checked", false);
            }
        });

        usersTable = $('#usersTable').DataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "bSort": false,

            "bPaginate": false,

            "ajax": {
                "url": "${pageContext.request.contextPath}/administrator/flashmobs/get_users_for_chose_JSON",
                "type": "POST",
                "data": function (d) {
                    d.nameFilter = $("#nameFilterUsers").val();
                    d.emailFilter = $("#emailFilterUsers").val();
                    d.countryFilter = $("#countryFilter").val();
                    d.cityFilter = $("#cityFilter").val();
                    d.genderFilter = $("#genderFilter").val();
                    d.ageFromFilter = $("#ageFromFilter").val();
                    d.ageToFilter = $("#ageToFilter").val();
                }
            },
            "language": {
                "processing": "Загрузка данных...",
                "search": "Поиск:",
                "lengthMenu": "Показать _MENU_ записей",
                "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                "infoEmpty": "Записи с 0 до 0 из 0 записей",
                "infoFiltered": "(отфильтровано из _MAX_ записей)",
                "infoPostFix": "",
                "loadingRecords": "Загрузка записей...",
                "zeroRecords": "Записи отсутствуют.",
                "emptyTable:": "В таблице отсутствуют данные",
                "paginate": {
                    "first": "Первая",
                    "previous": "Предыдущая",
                    "next": "Следующая",
                    "last": "Последняя"
                }
            }
        });
    });

</script>
</body>
</html>
