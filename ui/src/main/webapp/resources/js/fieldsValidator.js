﻿var fieldValidator = {};
var timePattern = /^(?:\d|[01]\d|2[0-3]):[0-5]\d$/;

fieldValidator.checkNumber = function (evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    return !(charCode > 31 && (charCode < 35 || charCode > 57));
};

fieldValidator.checkSpecialSymbols = function (evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    return !(charCode == 34 || charCode == 39);
};

fieldValidator.checkTime = function (value) {
    return timePattern.test(value);
};

fieldValidator.checkTime = function (inputObj, errorClassName) {
    if (!timePattern.test(inputObj.val())) {
        inputObj.addClass(errorClassName);
        return false;
    } else {
        inputObj.removeClass(errorClassName);
        return true;
    }
};