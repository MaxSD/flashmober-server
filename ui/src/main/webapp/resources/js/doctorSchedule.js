var DoctorSchedule = {};

DoctorSchedule.chooseGraphicType = function () {
    if ($("#graphicType").val() == 2) {
        $("#workTime").show();
    } else {
        $("#workTime").hide();
        $("#customWorkTime").hide();
        this.clearCustomWorkTime();
    }
};

DoctorSchedule.chooseWorkTime = function () {
    if ($("#workType_5").is(":checked")) {
        $("#customWorkTime").show();
    } else {
        $("#customWorkTime").hide();
        this.clearCustomWorkTime();
    }
};

DoctorSchedule.clearCustomWorkTime = function () {
    $("#timeStart1").val('');
    $("#timeStart2").val('');
    $("#timeStart3").val('');
    $("#timeEnd1").val('');
    $("#timeEnd2").val('');
    $("#timeEnd3").val('');
};

DoctorSchedule.loadScheduleData = function (_page, _date, _doctorId) {
    jQueryAjax.loadPageToElement(jQueryAjax.getServerName() +
        _page,
        {
            doctorId: _doctorId,
            scheduleDate: _date
        }, 'GET', true, 'scheduleData', 'ajaxError');
};