package com.wds.controller;

import com.wds.dao.FmFriendsDao;
import com.wds.dao.FmUsersDao;
import com.wds.entity.FmUser;
import com.wds.utils.ApplicationUtility;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping(value = "/administrator/friends")
public class FriendsController {

    @Autowired
    private MessageSource messageSource;
    @Autowired
    private FmUsersDao fmUusersDao;
    @Autowired
    private FmFriendsDao friendsDao;

    @ModelAttribute("currentUser")
    public FmUser getLoggedUser() {
        try {
            final org.springframework.security.core.userdetails.User principal = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return fmUusersDao.findByEmail(principal.getUsername());
        } catch (Exception ex) {
            return null;
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Principal principal) {
        if (principal == null) return "login";
        return "dashboard";
    }

    @RequestMapping(value = "/get_users_friends_JSON", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String getUsersFriends(
            @RequestParam("draw") int draw,
            @RequestParam("start") int start,
            @RequestParam("length") int length,
            @RequestParam(value = "uid", required = true) int uid,
            Principal principal,
            HttpServletRequest request,
            HttpServletResponse response) {

        String result;

        try {
            if (principal == null) throw new Exception("Please, login first!");

            List<FmUser> friendsList = friendsDao.getFriendsByUid(uid, start, length);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("draw", draw);
            jsonObject.put("recordsTotal", friendsDao.getFriendsCountByUid(uid));
            jsonObject.put("recordsFiltered", friendsDao.getFriendsCountByUid(uid));

            JSONArray jsonUsersArray = new JSONArray();
            JSONArray jsonUserArray;
            for (FmUser user : friendsList) {
                jsonUserArray = new JSONArray();
                jsonUserArray.put("<img src=\""+user.getPhoto()+"\" height=\"80\"/>");
                jsonUserArray.put(user.getName());
                jsonUserArray.put(user.getEmail());
                jsonUserArray.put(user.getCountry());
                jsonUserArray.put(user.getCity());
                if (user.getGender() == 0)
                    jsonUserArray.put("Ж");
                else if (user.getGender() == 1)
                    jsonUserArray.put("М");
                jsonUserArray.put(user.getAge());
                jsonUserArray.put("<a href=\"" + request.getContextPath() + "/administrator/users/" + user.getId() + "/edit_user\"><span class=\"glyphicon glyphicon-edit\"></span></a>");
                jsonUsersArray.put(jsonUserArray);
            }

            jsonObject.put("data", jsonUsersArray);

            result = ApplicationUtility.encodeStringForAjaxResponse(jsonObject.toString());
        } catch (Exception ex) {
            response.setStatus(1001);
            return ex.getMessage();
        }

        return result;
    }
}
