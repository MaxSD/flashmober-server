package com.wds.controller;

import com.wds.dao.FmUsersDao;
import com.wds.entity.FmUser;
import com.wds.utils.ApplicationUtility;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.security.Principal;
import java.util.Locale;

@Controller
@RequestMapping(value = "/*")
public class RootController {

    @Autowired
    private FmUsersDao fmUusersDao;

    @ModelAttribute("currentUser")
    public FmUser getLoggedUser() {
        try {
            final org.springframework.security.core.userdetails.User principal = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return fmUusersDao.findByEmail(principal.getUsername());
        } catch (Exception ex) {
            return null;
        }
    }

    @RequestMapping(value = "/")
    public String home(Principal principal) {
        if(principal==null) return "login";
        return "redirect:/administrator/reports/list_reports";
    }

    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    @RequestMapping(value = "/j_spring_security_exit_user", method = RequestMethod.GET)
    public String switchUserBack(Principal principal) {
        if(principal==null) return "login";
        return "dashboard";
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "/uploadPhoto", method = RequestMethod.POST)
    @ResponseBody
    public String uploadPhoto(@RequestParam("photoFile") MultipartFile file,
                              Principal principal,
                              Locale locale,
                              RedirectAttributes redirectAttributes) {
        try {
            ApplicationUtility.writeToFile(file.getInputStream(),
                    com.wds.utils.define.Defines.getUsersPhotoPathStorage() + File.separator + file.getOriginalFilename());

            return "{}";
        } catch (Exception ex) {
            return "{\"error\":\"" + ex.getMessage() + "\"}";
        }
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "/uploadPhoto/{photoFileNameForUpload}", method = RequestMethod.POST)
    @ResponseBody
    public String uploadPhoto(
            @PathVariable("photoFileNameForUpload") String photoFileNameForUpload,
            @RequestParam("photoFile") MultipartFile file,
                              Principal principal,
                              Locale locale,
                              RedirectAttributes redirectAttributes) {
        try {
            ApplicationUtility.writeToFile(file.getInputStream(),
                    com.wds.utils.define.Defines.getUsersPhotoPathStorage() + File.separator +
                            photoFileNameForUpload + "." + StringUtils.substringAfterLast(file.getOriginalFilename(), "."));

            return "{}";
        } catch (Exception ex) {
            return "{\"error\":\"" + ex.getMessage() + "\"}";
        }
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "/uploadPhotoFlashmobCreating/{photoFileNameForUpload}", method = RequestMethod.POST)
    @ResponseBody
    public String uploadPhotoFlashmobCreating(
            @PathVariable("photoFileNameForUpload") String photoFileNameForUpload,
            @RequestParam("photo") MultipartFile file,
                              Principal principal,
                              Locale locale,
                              RedirectAttributes redirectAttributes) {
        try {
            ApplicationUtility.writeToFile(file.getInputStream(),
                    com.wds.utils.define.Defines.getUsersPhotoPathStorage() + File.separator +
                            photoFileNameForUpload + "." + StringUtils.substringAfterLast(file.getOriginalFilename(), "."));

            return "{}";
        } catch (Exception ex) {
            return "{\"error\":\"" + ex.getMessage() + "\"}";
        }
    }

    @RequestMapping("/403")
    public String _403() {
        return "403";
    }

    @RequestMapping("/404")
    public String _404() {
        return "404";
    }
}
