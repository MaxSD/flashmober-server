package com.wds.controller;

import com.wds.dao.*;
import com.wds.dataObjects.*;
import com.wds.database.Defines;
import com.wds.entity.*;
import com.wds.utils.ApplicationUtility;
import com.wds.utils.push.apple.ApplePushNotificationThread;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping(value = "/administrator/flashmobs")
public class FlashmobController {

    @Autowired
    private MessageSource messageSource;
    @Autowired
    private FmUsersDao fmUusersDao;
    @Autowired
    private FmFriendsDao friendsDao;
    @Autowired
    private FmFlashmobsDao flashmobsDao;

    @ModelAttribute("currentUser")
    public FmUser getLoggedUser() {
        try {
            final org.springframework.security.core.userdetails.User principal = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return fmUusersDao.findByEmail(principal.getUsername());
        } catch (Exception ex) {
            return null;
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Principal principal) {
        if (principal == null) return "login";
        return "dashboard";
    }

    @RequestMapping(value = "/list_flashmobs", method = RequestMethod.GET)
    public String listFlashmobs(Model model, Principal principal, RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        try {
            /**
             * Check permissions
             */
            FmUser loggedUser = getLoggedUser();
            if (loggedUser.getRole().getId() == 1 || loggedUser.getRole().getId() == 2) {
                model.addAttribute("flashmobsCount", flashmobsDao.getFlashmobsCount(""));
                model.addAttribute("flashmobsOnModerationCount", flashmobsDao.getFlashmobsOnModerationCount(""));
            } else {
                model.addAttribute("flashmobsCount", flashmobsDao.getFlashmobsCountByOwnerId(loggedUser.getId(), ""));
            }
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute(com.wds.define.Defines.ERROR_MESSAGE, ex.getMessage());
            return "redirect:/administrator/";
        }
        return "administrator/flashmobs/listFlashmobs";
    }

    @RequestMapping(value = "/exportToFile", method = RequestMethod.POST)
    public void exportToFile(
            @RequestParam(value = "descriptionFilter", defaultValue = "", required = false) String descriptionFilter,
            HttpServletRequest request, HttpServletResponse response) {

        FileOutputStream fileOut = null;
        try {

            List<Object> list = flashmobsDao.getAll(0, 0, descriptionFilter, new GetAllFleshmobList());

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
            String fileName = com.wds.utils.define.Defines.getXslPathStorage() + simpleDateFormat.format(new Date());
            Workbook wb = new XSSFWorkbook();

            Sheet sheetUsers = wb.createSheet("Users");

            int i = 1;
            for (Object rowList : list) {
                GetAllFleshmobList rowData = (GetAllFleshmobList) rowList;

                Row row = sheetUsers.createRow(i);

                row.createCell(0).setCellValue(StringUtils.trimToEmpty(rowData.getPhoto()));
                row.createCell(1).setCellValue(StringUtils.trimToEmpty(ApplicationUtility.secondsToFormattedString(rowData.getDate_create(), "dd/MM/yyyy HH:mm")));
                row.createCell(2).setCellValue(StringUtils.trimToEmpty(rowData.getDescription()));

                i++;
            }

            /**
             * Save result to the file
             */
            fileOut = new FileOutputStream(fileName + ".xlsx");
            wb.write(fileOut);
            fileOut.close();

            ServletContext ctx = request.getServletContext();
            File file = new File(fileName + ".xlsx");
            InputStream fis = new FileInputStream(file);
            String mimeType = ctx.getMimeType(file.getAbsolutePath());
            response.setContentType(mimeType != null ? mimeType : "application/octet-stream");
            response.setContentLength((int) file.length());
            response.setHeader("Content-Disposition", "attachment; filename=\"flashmobsList.xlsx\"");

            ServletOutputStream os = response.getOutputStream();
            byte[] bufferData = new byte[1024];
            int read = 0;
            while ((read = fis.read(bufferData)) != -1) {
                os.write(bufferData, 0, read);
            }

            os.flush();
            os.close();
            fis.close();
        } catch (Exception ex) {
            request.setAttribute("message", "File Upload Failed due to " + ex);
        } finally {
            if (fileOut != null) {
                try {
                    fileOut.close();
                } catch (Exception ex) {
                    //
                }
            }
        }
    }

    @RequestMapping(value = "/get_all_flashmobs_JSON", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String getAllFlashmobs(
            @RequestParam("draw") int draw,
            @RequestParam("start") int start,
            @RequestParam("length") int length,
            @RequestParam(value = "descriptionFilter", defaultValue = "", required = false) String descriptionFilter,
            Principal principal,
            HttpServletRequest request,
            HttpServletResponse response) {

        String result;

        try {
            if (principal == null) throw new Exception("Please, login first!");

            List<Object> flashmobsList = flashmobsDao.getAll(start, length, descriptionFilter, new GetAllFleshmobList());

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("draw", draw);
            jsonObject.put("recordsTotal", flashmobsDao.getFlashmobsCount(descriptionFilter));
            jsonObject.put("recordsFiltered", flashmobsDao.getFlashmobsCount(descriptionFilter));

            JSONArray jsonArray = new JSONArray();
            JSONArray jsonDataArray;
            for (Object row : flashmobsList) {
                GetAllFleshmobList rowData = (GetAllFleshmobList) row;

                jsonDataArray = new JSONArray();
                jsonDataArray.put("<img src=\"" + rowData.getPhoto() + "\" height=\"96\"/>");
                jsonDataArray.put(ApplicationUtility.secondsToFormattedString(rowData.getDate_create(), "dd/MM/yyyy HH:mm"));
                jsonDataArray.put(rowData.getDescription());
                jsonDataArray.put("<a href=\"" + request.getContextPath() + "/administrator/users/"+rowData.getOwner_id()+"/edit_user\">"+rowData.getOwner_name()+"</a>");
                jsonDataArray.put("<a href=\"" + request.getContextPath() + "/administrator/flashmobs/" + rowData.getId() + "/edit_flashmob\"><span class=\"glyphicon glyphicon-edit\"></span></a>" +
                        "&nbsp;&nbsp;<a href=\"#\" onclick=\"deleteFlashmobReason('" + rowData.getId() + "');\"><span class=\"glyphicon glyphicon-remove-sign\"></span></a>");
                jsonArray.put(jsonDataArray);
            }

            jsonObject.put("data", jsonArray);

            result = ApplicationUtility.encodeStringForAjaxResponse(jsonObject.toString());
        } catch (Exception ex) {
            response.setStatus(1001);
            return ex.getMessage();
        }

        return result;
    }

    @RequestMapping(value = "/{userId}/get_my_flashmobs_JSON", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String getMyFlashmobs(
            @PathVariable("userId") long userId,
            @RequestParam("draw") int draw,
            @RequestParam("start") int start,
            @RequestParam("length") int length,
            @RequestParam(value = "descriptionFilter", defaultValue = "", required = false) String descriptionFilter,
            Principal principal,
            HttpServletRequest request,
            HttpServletResponse response) {

        String result;

        try {
            if (principal == null) throw new Exception("Please, login first!");

            List<FmFlashmobs> flashmobsList = flashmobsDao.getAllByOwnerId(userId, start, length, descriptionFilter);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("draw", draw);
            jsonObject.put("recordsTotal", flashmobsDao.getFlashmobsCountByOwnerId(userId, descriptionFilter));
            jsonObject.put("recordsFiltered", flashmobsDao.getFlashmobsCountByOwnerId(userId, descriptionFilter));

            JSONArray jsonArray = new JSONArray();
            JSONArray jsonDataArray;
            for (FmFlashmobs row : flashmobsList) {
                jsonDataArray = new JSONArray();
                jsonDataArray.put("<img src=\"" + row.getPhoto() + "\" height=\"96\"/>");
                jsonDataArray.put(ApplicationUtility.secondsToFormattedString(row.getDate_create(), "dd/MM/yyyy HH:mm"));
                jsonDataArray.put(row.getDescription());
                jsonDataArray.put("<a href=\"" + request.getContextPath() + "/administrator/flashmobs/" + row.getId() + "/edit_flashmob\"><span class=\"glyphicon glyphicon-edit\"></span></a>" +
                        "&nbsp;&nbsp;<a href=\"#\" onclick=\"deleteFlashmobReason('" + row.getId() + "');\"><span class=\"glyphicon glyphicon-remove-sign\"></span></a>");
                jsonArray.put(jsonDataArray);
            }

            jsonObject.put("data", jsonArray);

            result = ApplicationUtility.encodeStringForAjaxResponse(jsonObject.toString());
        } catch (Exception ex) {
            response.setStatus(1001);
            return ex.getMessage();
        }

        return result;
    }

    @RequestMapping(value = "/get_flashmobs_on_moderation_JSON", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String getFlashmobsOnModeration(
            @RequestParam("draw") int draw,
            @RequestParam("start") int start,
            @RequestParam("length") int length,
            @RequestParam(value = "descriptionFilter", defaultValue = "", required = false) String descriptionFilter,
            Principal principal,
            HttpServletRequest request,
            HttpServletResponse response) {

        String result;

        try {
            if (principal == null) throw new Exception("Please, login first!");

            List<Object> flashmobsList = flashmobsDao.getAllOnModeration(start, length, descriptionFilter, new GetAllFleshmobList());

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("draw", draw);
            jsonObject.put("recordsTotal", flashmobsDao.getFlashmobsOnModerationCount(descriptionFilter));
            jsonObject.put("recordsFiltered", flashmobsDao.getFlashmobsOnModerationCount(descriptionFilter));

            JSONArray jsonArray = new JSONArray();
            JSONArray jsonDataArray;
            for (Object row : flashmobsList) {
                GetAllFleshmobList rowData = (GetAllFleshmobList) row;

                jsonDataArray = new JSONArray();
                jsonDataArray.put("<img src=\"" + rowData.getPhoto() + "\" height=\"96\"/>");
                jsonDataArray.put(ApplicationUtility.secondsToFormattedString(rowData.getDate_create(), "dd/MM/yyyy HH:mm"));
                jsonDataArray.put(rowData.getDescription());
                jsonDataArray.put("<a href=\"" + request.getContextPath() + "/administrator/users/"+rowData.getOwner_id()+"/edit_user\">"+rowData.getOwner_name()+"</a>");
                jsonDataArray.put("<a href=\"" + request.getContextPath() + "/administrator/flashmobs/" + rowData.getId() + "/edit_flashmob\"><span class=\"glyphicon glyphicon-edit\"></span></a>" +
                        "&nbsp;&nbsp;<a href=\"#\" onclick=\"deleteFlashmobReason('" + rowData.getId() + "');\"><span class=\"glyphicon glyphicon-remove-sign\"></span></a>");
                jsonArray.put(jsonDataArray);
            }

            jsonObject.put("data", jsonArray);

            result = ApplicationUtility.encodeStringForAjaxResponse(jsonObject.toString());
        } catch (Exception ex) {
            response.setStatus(1001);
            return ex.getMessage();
        }

        return result;
    }

    @RequestMapping(value = "/get_users_flashmobs_JSON", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String getUsersFlashmobs(
            @RequestParam("draw") int draw,
            @RequestParam("start") int start,
            @RequestParam("length") int length,
            @RequestParam(value = "uid", required = true) int uid,
            Principal principal,
            HttpServletRequest request,
            HttpServletResponse response) {

        String result;

        try {
            if (principal == null) throw new Exception("Please, login first!");

            FmUser user = getLoggedUser();

            List<Object> usersFlashmobsList = flashmobsDao.getFinishedFlashmobsListByOwnerIdWithPaging(
                    uid, start, length, new GetFleshmobListByOwnerId());

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("draw", draw);
            jsonObject.put("recordsTotal", flashmobsDao.getFinishedFlashmobsCountByOwnerId(uid));
            jsonObject.put("recordsFiltered", flashmobsDao.getFinishedFlashmobsCountByOwnerId(uid));

            JSONArray jsonUsersArray = new JSONArray();
            JSONArray jsonUserArray;
            for (Object row : usersFlashmobsList) {
                GetFleshmobListByOwnerId rowData = (GetFleshmobListByOwnerId) row;

                jsonUserArray = new JSONArray();
                jsonUserArray.put(rowData.getId());
                jsonUserArray.put("<img src=\"" + rowData.getPhoto() + "\" height=\"96\"/>");
                jsonUserArray.put(ApplicationUtility.secondsToFormattedString(rowData.getDate_create(), "dd/MM/yyyy HH:mm"));
                jsonUserArray.put(rowData.getDescription());
                jsonUserArray.put(rowData.getPeople_count());

                /**
                 * Actions
                 */
                StringBuilder sbActions = new StringBuilder("");
                sbActions.append("<a href=\"" + request.getContextPath() + "/administrator/flashmobs/" + rowData.getId() + "/edit_flashmob\"><span class=\"glyphicon glyphicon-edit\"></span></a>");

                if (user.getRole().getId() == 1 || user.getRole().getId() == 2 || user.getId() == uid) {
                    sbActions.append("&nbsp;&nbsp;<a href=\"#\" onclick=\"deleteFlashmobReason('"+rowData.getId()+"');\"><span class=\"glyphicon glyphicon-remove-sign\"></span></a>");
                }
                jsonUserArray.put(sbActions.toString());

                jsonUsersArray.put(jsonUserArray);
            }

            jsonObject.put("data", jsonUsersArray);

            result = ApplicationUtility.encodeStringForAjaxResponse(jsonObject.toString());
        } catch (Exception ex) {
            response.setStatus(1001);
            return ex.getMessage();
        }

        return result;
    }

    @RequestMapping(value = "/get_users_for_chose_JSON", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String getUsersForChose(
            @RequestParam("draw") int draw,
            @RequestParam("start") int start,
            @RequestParam("length") int length,
            @RequestParam(value = "search[value]", defaultValue = "", required = false) String search,
            @RequestParam(value = "nameFilter", defaultValue = "", required = false) String nameFilter,
            @RequestParam(value = "emailFilter", defaultValue = "", required = false) String emailFilter,
            @RequestParam(value = "countryFilter", defaultValue = "", required = false) String countryFilter,
            @RequestParam(value = "cityFilter", defaultValue = "", required = false) String cityFilter,
            @RequestParam(value = "genderFilter", defaultValue = "", required = false) String genderFilter,
            @RequestParam(value = "ageFromFilter", defaultValue = "", required = false) String ageFromFilter,
            @RequestParam(value = "ageToFilter", defaultValue = "", required = false) String ageToFilter,
            Principal principal,
            HttpServletRequest request,
            HttpServletResponse response) {

        String result;

        try {
            if (principal == null) throw new Exception("Please, login first!");

            List<FmUser> userList = fmUusersDao.getAll(start, length, search, nameFilter, emailFilter,
                    countryFilter, cityFilter, genderFilter, ageFromFilter, ageToFilter);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("draw", draw);
            jsonObject.put("recordsTotal", fmUusersDao.getUsersCount());
            jsonObject.put("recordsFiltered", fmUusersDao.getUsersCount(nameFilter, emailFilter,
                    countryFilter, cityFilter, genderFilter, ageFromFilter, ageToFilter));

            JSONArray jsonUsersArray = new JSONArray();
            JSONArray jsonUserArray;
            for (FmUser user : userList) {
                jsonUserArray = new JSONArray();
                jsonUserArray.put("<input type=\"checkbox\" id=\"friends_ids\" name=\"friends_ids\" value=\"" + user.getId() + "\"/>");
                jsonUserArray.put(user.getName());
                jsonUserArray.put(user.getEmail());

                jsonUsersArray.put(jsonUserArray);
            }

            jsonObject.put("data", jsonUsersArray);

            result = ApplicationUtility.encodeStringForAjaxResponse(jsonObject.toString());
        } catch (Exception ex) {
            response.setStatus(1001);
            return ex.getMessage();
        }

        return result;
    }

    @RequestMapping(value = "/get_users_friends_for_chose_JSON", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String getUsersFriendsForChose(
            @RequestParam("draw") int draw,
            @RequestParam("start") int start,
            @RequestParam("length") int length,
            @RequestParam(value = "uid", required = true) Long uid,
            @RequestParam(value = "nameFilter", defaultValue = "", required = false) String nameFilter,
            @RequestParam(value = "emailFilter", defaultValue = "", required = false) String emailFilter,
            Principal principal,
            HttpServletRequest request,
            HttpServletResponse response) {

        String result;

        try {
            if (principal == null) throw new Exception("Please, login first!");

            /**
             * Show ALL FRIENDS !!!
             */
            List<FmUser> userList = friendsDao.getFriendsByUid(uid, 0, 0, nameFilter, emailFilter);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("draw", draw);
            jsonObject.put("recordsTotal", friendsDao.getFriendsCountByUid(uid));
            jsonObject.put("recordsFiltered", friendsDao.getFriendsCountByUid(uid, nameFilter, emailFilter));

            JSONArray jsonUsersArray = new JSONArray();
            JSONArray jsonUserArray;
            for (FmUser user : userList) {
                jsonUserArray = new JSONArray();
                jsonUserArray.put("<input type=\"checkbox\" id=\"friends_ids\" name=\"friends_ids\" value=\"" + user.getId() + "\"/>");
                jsonUserArray.put(user.getName());
                jsonUserArray.put(user.getEmail());

                jsonUsersArray.put(jsonUserArray);
            }

            jsonObject.put("data", jsonUsersArray);

            result = ApplicationUtility.encodeStringForAjaxResponse(jsonObject.toString());
        } catch (Exception ex) {
            response.setStatus(1001);
            return ex.getMessage();
        }

        return result;
    }

    @RequestMapping(value = "/new_flashmob", method = RequestMethod.GET)
    public String newFlashmob(Model model, Principal principal, Locale locale) {
        if (principal == null) return "login";

        try {
            FmFlashmobs flashmobCreateForm = new FmFlashmobs();
            flashmobCreateForm.setOwner_id(getLoggedUser().getId());
            model.addAttribute("flashmobCreateForm", flashmobCreateForm);

            model.addAttribute("datePublication", ApplicationUtility.secondsToFormattedString(ApplicationUtility.getDateInSeconds(),
                    "dd/MM/yyyy"));

            model.addAttribute("userCountries", fmUusersDao.getUsersCountries());
            model.addAttribute("userCities", fmUusersDao.getUsersCities());
            model.addAttribute("photoFileNameForUpload", String.valueOf(System.currentTimeMillis()) + String.valueOf(getLoggedUser().getId()));
        } catch (Exception ex) {
            model.addAttribute(com.wds.define.Defines.ERROR_MESSAGE, ex.getMessage());
        }
        return "administrator/flashmobs/newFlashmob";
    }

    @RequestMapping(value = "{flashmobId}/edit_flashmob", method = RequestMethod.GET)
    public String editFlashmob(@PathVariable long flashmobId,
                               Model model, Principal principal, Locale locale) {
        if (principal == null) return "login";
        try {
            FmUser user = getLoggedUser();

            FmFlashmobs flashmob = flashmobsDao.findById(flashmobId);
            FmFlashmobsCategoriesDao flashmobsCategoriesDao = new FmFlashmobsCategoriesDao();
            FmFlashmobsTagsDao flashmobsTagsDao = new FmFlashmobsTagsDao();

            model.addAttribute("flashmobEditForm", flashmob);
            model.addAttribute("categoriesList", flashmobsCategoriesDao.getCategoriesIdsListByFlashmobId(flashmobId));
            model.addAttribute("tagsList", StringUtils.join(flashmobsTagsDao.getTagsListByFlashmobId(flashmobId), ","));
            model.addAttribute("dateCreate", ApplicationUtility.secondsToFormattedString(flashmob.getDate_create(),
                    "dd/MM/yyyy HH:mm"));

            if (flashmob.getDate_publication() == null || flashmob.getDate_publication() == 0) {
                model.addAttribute("datePublication", ApplicationUtility.secondsToFormattedString(flashmob.getDate_create(),
                        "dd/MM/yyyy"));
            } else {
                model.addAttribute("datePublication", ApplicationUtility.secondsToFormattedString(flashmob.getDate_publication(),
                        "dd/MM/yyyy"));
            }

            boolean isPublished = false;
            if (System.currentTimeMillis() / 1000 >= flashmob.getDate_publication()) {
                isPublished = true;
            }
            model.addAttribute("isPublished", isPublished);

            if (user.getRole().getId() == 1 || user.getRole().getId() == 2) {
                /**
                 * If user is Administrator or Moderator
                 */

                model.addAttribute("isUserCanDoModeration", true);
            } else {
                model.addAttribute("isUserCanDoModeration", false);
            }

            model.addAttribute("flashmobReportsCount", flashmobsDao.getFlashmobReportsCount(flashmobId));
            model.addAttribute("flashmobAcceptUsersCount", flashmobsDao.getFlashmobAcceptedUsersCount(flashmobId));
            model.addAttribute("flashmobRejectUsersCount", flashmobsDao.getFlashmobRejectedUsersCount(flashmobId));
            model.addAttribute("photoFileNameForUpload", String.valueOf(System.currentTimeMillis()) + String.valueOf(getLoggedUser().getId()));
        } catch (Exception ex) {
            model.addAttribute(com.wds.define.Defines.ERROR_MESSAGE, ex.getMessage());
        }
        return "administrator/flashmobs/editFlashmob";
    }

    @RequestMapping(value = "{flashmobId}/edit_flashmob", method = RequestMethod.POST)
    public String editFlashmob(@ModelAttribute FmFlashmobs flashmobEditForm,
                               @PathVariable long flashmobId,
                               @RequestParam("photo") String photo,
                               @RequestParam("photoFileNameForUpload") String photoFileNameForUpload,
                               @RequestParam("tags") String tags,
                               @RequestParam("datePublication") String datePublication,
                               @RequestParam(value = "is_default", defaultValue = "false", required = false) Boolean isDefault,
                               RedirectAttributes redirectAttributes,
                               Model model, Principal principal, Locale locale) {
        if (principal == null) return "login";
        try {
            /**
             * Check permissions
             */
            if (!ApplicationUtility.hasRole("PERMISSION_EDIT_TASKS")) {
                return "403";
            }

            FmFlashmobs flashmob = flashmobsDao.findById(flashmobId);

            if (flashmob == null)
                throw new Exception("Задание не найдено");

            /**
             * Create new flashmob's tags
             */
            FmFlashmobsTagsDao flashmobsTagsDao = new FmFlashmobsTagsDao();
            if (!StringUtils.isBlank(tags)) {

                flashmobsTagsDao.delete(flashmobId);

                for (String tag : StringUtils.splitByWholeSeparator(tags, ",")) {
                    FmFlashmobTags flashmobTags = new FmFlashmobTags();
                    flashmobTags.setFlashmobId(flashmobId);
                    flashmobTags.setTag(tag);
                    flashmobsTagsDao.create(flashmobTags);
                }
            }

            flashmob.setDescription(flashmobEditForm.getDescription());
            flashmob.setVideo(flashmobEditForm.getVideo());
            if (!StringUtils.isBlank(photo)) {
                flashmob.setPhoto(com.wds.define.Defines.getHostImages() + photoFileNameForUpload + "." + StringUtils.substringAfterLast(photo, "."));
            }

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            flashmob.setDate_publication(ApplicationUtility.getDateInSeconds(sdf.parse(datePublication)));
            flashmob.setIsDefault(isDefault);

            flashmobsDao.update(flashmob);
            redirectAttributes.addFlashAttribute(com.wds.define.Defines.SUCCESS_MESSAGE, messageSource.getMessage("UI.Messages.Flashmob.UpdatedSuccess", null, locale));
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute(com.wds.define.Defines.ERROR_MESSAGE, ex.getMessage());
            return "redirect:/administrator/flashmobs/"+flashmobId+"/edit_flashmob";
        }
        return "redirect:/administrator/flashmobs/list_flashmobs";
    }

    @RequestMapping(value = "/get_users_accepted_flashmob_JSON", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String getUsersAcceptedFlashmob_JSON(
            @RequestParam("draw") int draw,
            @RequestParam("start") int start,
            @RequestParam("length") int length,
            @RequestParam(value = "flashmobId", defaultValue = "", required = false) Long flashmobId,
            Principal principal,
            HttpServletRequest request,
            HttpServletResponse response) {

        String result;

        try {
            if (principal == null) throw new Exception("Please, login first!");

            List<Object> resultList =
                    flashmobsDao.getFlashmobUsersAccepted(
                            start, length, flashmobId, "", "", new GetUsersAcceptedFlashmob());

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("draw", draw);
            jsonObject.put("recordsTotal", flashmobsDao.getFlashmobAcceptedUsersCount(flashmobId));
            jsonObject.put("recordsFiltered", flashmobsDao.getFlashmobAcceptedUsersCount(flashmobId, "", ""));

            JSONArray jsonUsersArray = new JSONArray();
            JSONArray jsonUserArray;
            for (Object row : resultList) {
                GetUsersAcceptedFlashmob rowData = (GetUsersAcceptedFlashmob) row;

                jsonUserArray = new JSONArray();
                jsonUserArray.put("<img src=\"" + rowData.getUser_photo() + "\" width=\"64\" height=\"64\"/>");
                jsonUserArray.put(rowData.getName());
                jsonUserArray.put(ApplicationUtility.secondsToFormattedString(rowData.getDate_start(), "dd/MM/yyyy HH:mm"));

                /**
                 * Actions
                 */
            /*
                StringBuilder sbActions = new StringBuilder("");
                if (ApplicationUtility.hasRole("PERMISSION_EDIT_USERS")) {
                    sbActions.append("<a href=\"" + request.getContextPath() + "/administrator/reports/" + rowData.getId() + "/edit_report\"><span class=\"glyphicon glyphicon-edit\"></span></a>");
                }
                if (ApplicationUtility.hasRole("PERMISSION_DELETE_USERS")) {
                    sbActions.append("&nbsp;&nbsp;<a href=\"#\" onclick=\"deleteReports('" + rowData.getId() + "');\"><span class=\"glyphicon glyphicon-remove-sign\"></span></a>");
                }
                jsonUserArray.put(sbActions.toString());
            */
                jsonUsersArray.put(jsonUserArray);
            }

            jsonObject.put("data", jsonUsersArray);

            result = ApplicationUtility.encodeStringForAjaxResponse(jsonObject.toString());
        } catch (Exception ex) {
            response.setStatus(1001);
            return ex.getMessage();
        }

        return result;
    }

    @RequestMapping(value = "/get_users_rejected_flashmob_JSON", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String getUsersRejectedFlashmob_JSON(
            @RequestParam("draw") int draw,
            @RequestParam("start") int start,
            @RequestParam("length") int length,
            @RequestParam(value = "flashmobId", defaultValue = "", required = false) Long flashmobId,
            Principal principal,
            HttpServletRequest request,
            HttpServletResponse response) {

        String result;

        try {
            if (principal == null) throw new Exception("Please, login first!");

            List<Object> resultList =
                    flashmobsDao.getFlashmobUsersRejected(
                            start, length, flashmobId, "", "", new GetUsersRejectedFlashmob());

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("draw", draw);
            jsonObject.put("recordsTotal", flashmobsDao.getFlashmobRejectedUsersCount(flashmobId));
            jsonObject.put("recordsFiltered", flashmobsDao.getFlashmobRejectedUsersCount(flashmobId, "", ""));

            JSONArray jsonUsersArray = new JSONArray();
            JSONArray jsonUserArray;
            for (Object row : resultList) {
                GetUsersRejectedFlashmob rowData = (GetUsersRejectedFlashmob) row;

                jsonUserArray = new JSONArray();
                jsonUserArray.put("<img src=\"" + rowData.getUser_photo() + "\" width=\"64\" height=\"64\"/>");
                jsonUserArray.put(rowData.getName());
                jsonUserArray.put("");

                /**
                 * Actions
                 */
            /*
                StringBuilder sbActions = new StringBuilder("");
                if (ApplicationUtility.hasRole("PERMISSION_EDIT_USERS")) {
                    sbActions.append("<a href=\"" + request.getContextPath() + "/administrator/reports/" + rowData.getId() + "/edit_report\"><span class=\"glyphicon glyphicon-edit\"></span></a>");
                }
                if (ApplicationUtility.hasRole("PERMISSION_DELETE_USERS")) {
                    sbActions.append("&nbsp;&nbsp;<a href=\"#\" onclick=\"deleteReports('" + rowData.getId() + "');\"><span class=\"glyphicon glyphicon-remove-sign\"></span></a>");
                }
                jsonUserArray.put(sbActions.toString());
            */
                jsonUsersArray.put(jsonUserArray);
            }

            jsonObject.put("data", jsonUsersArray);

            result = ApplicationUtility.encodeStringForAjaxResponse(jsonObject.toString());
        } catch (Exception ex) {
            response.setStatus(1001);
            return ex.getMessage();
        }

        return result;
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "{flashmobId}/accept_flashmob_by_moderator", method = RequestMethod.POST)
    public String acceptFlashmobByModerator(@PathVariable long flashmobId,
                                                 @RequestParam("moderatorComment") String moderatorComment,
                                                 @RequestParam("datePublication") String datePublication,
                                    Model model,
                                    Principal principal,
                                    Locale locale,
                                    RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        try {
            FmFlashmobs flashmob = flashmobsDao.findById(flashmobId);
            if (flashmob != null) {
                flashmob.setComment(moderatorComment);

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                flashmob.setDate_publication(ApplicationUtility.getDateInSeconds(sdf.parse(datePublication)));

                flashmob.setIsAcceptedBbyAdmin(true);
                flashmobsDao.update(flashmob);

                /**
                 * Send push notification, if publication date is now.
                 */
                if (flashmob.getDate_publication() == ApplicationUtility.getDateInSeconds()) {
                    List<FlashmobsUsersAssignedOn> flashmobsUsersAssignedOnList =
                            flashmobsDao.getFlashmobUsersAssignedOn(flashmobId);

                    if (flashmobsUsersAssignedOnList != null && flashmobsUsersAssignedOnList.size() > 0) {
                        FmUserTokenDao userTokenDao = new FmUserTokenDao();
                        for (FlashmobsUsersAssignedOn flashmobsUsersAssignedOn : flashmobsUsersAssignedOnList) {
                            FmUserToken userToken = userTokenDao.findByUserId(flashmobsUsersAssignedOn.getId());

                            if (userToken != null) {
                                List<String> appleTokenList = new ArrayList<>();
                                appleTokenList.add(StringUtils.trimToEmpty(userToken.getToken()));

                                ApplePushNotificationThread applePushNotificationThread =
                                        new ApplePushNotificationThread(appleTokenList, "Пользователь " + flashmobsUsersAssignedOn.getName() + " предлагает Вам флешмоб",
                                                com.wds.utils.define.Defines.NOTIFICATION_TYPE_NEW_FLASHMOB,
                                                flashmobsUsersAssignedOn.getUser_flashmob_id());
                                applePushNotificationThread.start();
                            }
                        }
                    }
                }
            }
            redirectAttributes.addFlashAttribute(com.wds.define.Defines.SUCCESS_MESSAGE, messageSource.getMessage("UI.Messages.Flashmob.AcceptedByModeratorSuccess", null, locale));
            redirectAttributes.addFlashAttribute("tabNumber", 2);
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute(com.wds.define.Defines.ERROR_MESSAGE, ex.getMessage());
            return "redirect:/administrator/flashmobs/"+flashmobId+"/edit_flashmob";
        }
        return "redirect:/administrator/flashmobs/list_flashmobs";
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "{flashmobId}/accept_flashmob_by_moderator_ajax", method = RequestMethod.POST)
    @ResponseBody
    public String acceptFlashmobByModerator_Ajax(@PathVariable long flashmobId,
                                                 @RequestParam("moderatorComment") String moderatorComment,
                                                 Model model,
                                                 Principal principal,
                                                 Locale locale,
                                                 RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        try {
            FmFlashmobs flashmob = flashmobsDao.findById(flashmobId);
            if (flashmob != null) {
                flashmob.setComment(moderatorComment);
                flashmob.setIsAcceptedBbyAdmin(true);
                flashmobsDao.update(flashmob);

                /**
                 * Send push notification, if publication date is now.
                 */
                if (flashmob.getDate_publication() <= ApplicationUtility.getDateInSeconds()) {
                    List<FlashmobsUsersAssignedOn> flashmobsUsersAssignedOnList =
                            flashmobsDao.getFlashmobUsersAssignedOn(flashmobId);

                    if (flashmobsUsersAssignedOnList != null && flashmobsUsersAssignedOnList.size() > 0) {
                        FmUserTokenDao userTokenDao = new FmUserTokenDao();
                        for (FlashmobsUsersAssignedOn flashmobsUsersAssignedOn : flashmobsUsersAssignedOnList) {
                            FmUserToken userToken = userTokenDao.findByUserId(flashmobsUsersAssignedOn.getId());

                            if (userToken != null) {
                                List<String> appleTokenList = new ArrayList<>();
                                appleTokenList.add(StringUtils.trimToEmpty(userToken.getToken()));

                                ApplePushNotificationThread applePushNotificationThread =
                                        new ApplePushNotificationThread(appleTokenList, "Пользователь " + flashmobsUsersAssignedOn.getName() + " предлагает Вам флешмоб",
                                                com.wds.utils.define.Defines.NOTIFICATION_TYPE_NEW_FLASHMOB,
                                                flashmobsUsersAssignedOn.getUser_flashmob_id());
                                applePushNotificationThread.start();
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            return "<p class=\"alert alert-danger alert-dismissable\">" + ApplicationUtility.encodeStringForAjaxResponse(ex.getMessage()) + "</p>";
        }
        return "<p class=\"alert alert-success alert-dismissable\">" + ApplicationUtility.encodeStringForAjaxResponse(messageSource.getMessage("UI.Messages.Flashmob.AcceptedByModeratorSuccess", null, locale)) + "</p>";
    }

    @RequestMapping(value = "/delete_flashmob_reason", method = RequestMethod.GET)
    public String lockUserReason(Principal principal, Locale locale) {
        if (principal == null) return "login";
        return "administrator/flashmobs/deleteFlashmob";
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "{flashmobId}/delete_flashmob_ajax", method = RequestMethod.POST)
    @ResponseBody
    public String deleteFlashmobAjax(@PathVariable long flashmobId,
                                     @RequestParam("deleteReason") String deleteReason,
                                     @RequestParam("otherDeleteReason") String otherDeleteReason,
                                    Model model,
                                    Principal principal,
                                    Locale locale,
                                    RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        try {
            FmFlashmobs flashmob = flashmobsDao.findById(flashmobId);
            if (flashmob != null) {
                flashmobsDao.delete(flashmob);

                if (getLoggedUser().getRole().getId() == Defines.USER_TYPE_ADMIN ||
                        getLoggedUser().getRole().getId() == Defines.USER_TYPE_MODERATOR) {

                    FmUser user = fmUusersDao.findById(flashmob.getOwner_id());
                    if (user != null) {
                        if (!user.isIsFlashmobsDeletedByModerator()) {
                            if (StringUtils.isBlank(otherDeleteReason)) {
                                user.setFlashmobsDeletedReason(StringUtils.trimToEmpty(deleteReason));
                            } else {
                                user.setFlashmobsDeletedReason(StringUtils.trimToEmpty(otherDeleteReason));
                            }

                            user.setIsIsFlashmobsDeletedByModerator(true);
                            fmUusersDao.update(user);
                        }
                    }
                }
            }

        } catch (Exception ex) {
            return "<p class=\"alert alert-danger alert-dismissable\">" + ApplicationUtility.encodeStringForAjaxResponse(ex.getMessage()) + "</p>";
        }
        return "<p class=\"alert alert-success alert-dismissable\">" + ApplicationUtility.encodeStringForAjaxResponse(messageSource.getMessage("UI.Messages.Flashmob.DeletedSuccess", null, locale)) + "</p>";
    }
}
