package com.wds.controller;

import com.wds.dao.FmComplaintsDao;
import com.wds.dao.FmUsersDao;
import com.wds.database.Defines;
import com.wds.entity.FmUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;

@Controller
@RequestMapping(value = "/administrator/complaints")
public class ComplaintController {

    @Autowired
    private MessageSource messageSource;
    @Autowired
    private FmUsersDao fmUusersDao;
    @Autowired
    private FmComplaintsDao complaintsDao;

    @ModelAttribute("currentUser")
    public FmUser getLoggedUser() {
        try {
            final org.springframework.security.core.userdetails.User principal = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return fmUusersDao.findByEmail(principal.getUsername());
        } catch (Exception ex) {
            return null;
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Principal principal) {
        if (principal == null) return "login";
        return "dashboard";
    }

    @RequestMapping(value = "/list_complaints", method = RequestMethod.GET)
    public String listAdmins(Model model, Principal principal, RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        try {
            //model.addAttribute("allReportsCount", flashmobsDao.getAllUsersReportsCount());
            //model.addAttribute("businessUsersReportsCount", flashmobsDao.getBusinessUsersReportsCount());
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute(com.wds.define.Defines.ERROR_MESSAGE, ex.getMessage());
            return "redirect:/administrator/";
        }
        return "administrator/reports/listReports";
    }
}
