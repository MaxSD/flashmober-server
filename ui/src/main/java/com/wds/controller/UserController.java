package com.wds.controller;

import com.wds.dao.*;
import com.wds.database.Defines;
import com.wds.entity.FmUser;
import com.wds.entity.FmUserToken;
import com.wds.entity.Role;
import com.wds.service.UserDetailsServiceImpl;
import com.wds.service.UserRoleConverter;
import com.wds.utils.ApplicationUtility;
import com.wds.utils.Transliterator;
import com.wds.utils.push.apple.ApplePushNotificationThread;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping(value = "/administrator/users")
public class UserController {

    @Autowired
    private MessageSource messageSource;
    @Autowired
    private FmUsersDao fmUusersDao;
    @Autowired
    private FmFlashmobsDao flashmobsDao;
    @Autowired
    private FmFriendsDao friendsDao;
    @Autowired
    private UsersDao usersDao;
    @Autowired
    private RolesDao rolesDao;
    @Autowired
    private UserRoleConverter userRoleConverter;
    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Role.class, this.userRoleConverter);
    }

    @ModelAttribute("currentUser")
    public FmUser getLoggedUser() {
        try {
            final org.springframework.security.core.userdetails.User principal = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return fmUusersDao.findByEmail(principal.getUsername());
        } catch (Exception ex) {
            return null;
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Principal principal) {
        if (principal == null) return "login";
        return "dashboard";
    }

    @RequestMapping(value = "/list_users", method = RequestMethod.GET)
    public String listAdmins(Model model, Principal principal, RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        try {
            model.addAttribute("usersList", usersDao.getAll());
            model.addAttribute("usersCount", fmUusersDao.getUsersCount());
            model.addAttribute("businessUsersCount", fmUusersDao.getBusinessUsersCount());
            model.addAttribute("moderatorsCount", fmUusersDao.getModeratorsCount());
            model.addAttribute("userCountries", fmUusersDao.getUsersCountries());
            model.addAttribute("userCities", fmUusersDao.getUsersCities());
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute(com.wds.define.Defines.ERROR_MESSAGE, ex.getMessage());
            return "redirect:/administrator/";
        }
        return "administrator/users/listUsers";
    }

    @RequestMapping(value = "/exportUsersToFile", method = RequestMethod.POST)
    public void exportUsersToFile(
            @RequestParam(value = "usersType", defaultValue = "", required = false) Integer usersType,
            @RequestParam(value = "nameFilter", defaultValue = "", required = false) String nameFilter,
            @RequestParam(value = "emailFilter", defaultValue = "", required = false) String emailFilter,
            @RequestParam(value = "countryFilter", defaultValue = "", required = false) String countryFilter,
            @RequestParam(value = "cityFilter", defaultValue = "", required = false) String cityFilter,
            @RequestParam(value = "genderFilter", defaultValue = "", required = false) String genderFilter,
            @RequestParam(value = "ageFromFilter", defaultValue = "", required = false) String ageFromFilter,
            @RequestParam(value = "ageToFilter", defaultValue = "", required = false) String ageToFilter,
            HttpServletRequest request, HttpServletResponse response) {

        FileOutputStream fileOut = null;
        try {

            List<FmUser> userList = new ArrayList<>(0);
            if (usersType == 1) {
                userList =
                        fmUusersDao.getAll(0, 0, "", nameFilter, emailFilter,
                                countryFilter, cityFilter, genderFilter, ageFromFilter, ageToFilter);
            } else if (usersType == 2) {
                userList =
                        fmUusersDao.getBusinessUsers(0, 0, "", nameFilter, emailFilter,
                                countryFilter, cityFilter, genderFilter, ageFromFilter, ageToFilter);
            } else if (usersType == 3) {
                userList =
                        fmUusersDao.getModerators(0, 0, "", nameFilter, emailFilter,
                                countryFilter, cityFilter, genderFilter, ageFromFilter, ageToFilter);
            }

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
            String fileName = com.wds.utils.define.Defines.getXslPathStorage() + simpleDateFormat.format(new Date());
            Workbook wb = new XSSFWorkbook();

            Sheet sheetUsers = wb.createSheet("Users");

            int i = 1;
            for (FmUser user : userList) {
                Row row = sheetUsers.createRow(i);

                row.createCell(0).setCellValue(StringUtils.trimToEmpty(user.getName()));
                row.createCell(1).setCellValue(StringUtils.trimToEmpty(user.getEmail()));
                row.createCell(2).setCellValue(StringUtils.trimToEmpty(user.getAbout()));
                row.createCell(3).setCellValue(StringUtils.trimToEmpty(user.getCountry()));
                row.createCell(4).setCellValue(StringUtils.trimToEmpty(user.getCity()));
                if (user.getGender() == 1) {
                    row.createCell(5).setCellValue("Ж");
                } else if (user.getGender() == 0) {
                    row.createCell(5).setCellValue("М");
                }
                row.createCell(6).setCellValue(user.getAge());
                row.createCell(7).setCellValue(StringUtils.trimToEmpty(user.getSite()));

                i++;
            }

            /**
             * Save result to the file
             */
            fileOut = new FileOutputStream(fileName + ".xlsx");
            wb.write(fileOut);
            fileOut.close();

            ServletContext ctx = request.getServletContext();
            File file = new File(fileName + ".xlsx");
            InputStream fis = new FileInputStream(file);
            String mimeType = ctx.getMimeType(file.getAbsolutePath());
            response.setContentType(mimeType != null ? mimeType : "application/octet-stream");
            response.setContentLength((int) file.length());
            response.setHeader("Content-Disposition", "attachment; filename=\"usersList.xlsx\"");

            ServletOutputStream os = response.getOutputStream();
            byte[] bufferData = new byte[1024];
            int read = 0;
            while ((read = fis.read(bufferData)) != -1) {
                os.write(bufferData, 0, read);
            }

            os.flush();
            os.close();
            fis.close();
        } catch (Exception ex) {
            request.setAttribute("message", "File Upload Failed due to " + ex);
        } finally {
            if (fileOut != null) {
                try {
                    fileOut.close();
                } catch (Exception ex) {
                    //
                }
            }
        }
    }

    @RequestMapping(value = "/get_all_users_JSON", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String getAllUsers(
            @RequestParam("draw") int draw,
            @RequestParam("start") int start,
            @RequestParam("length") int length,
            @RequestParam(value = "search[value]", defaultValue = "", required = false) String search,
            @RequestParam(value = "nameFilter", defaultValue = "", required = false) String nameFilter,
            @RequestParam(value = "emailFilter", defaultValue = "", required = false) String emailFilter,
            @RequestParam(value = "countryFilter", defaultValue = "", required = false) String countryFilter,
            @RequestParam(value = "cityFilter", defaultValue = "", required = false) String cityFilter,
            @RequestParam(value = "genderFilter", defaultValue = "", required = false) String genderFilter,
            @RequestParam(value = "ageFromFilter", defaultValue = "", required = false) String ageFromFilter,
            @RequestParam(value = "ageToFilter", defaultValue = "", required = false) String ageToFilter,
            Principal principal,
            HttpServletRequest request,
            HttpServletResponse response) {

        String result;

        try {
            if (principal == null) throw new Exception("Please, login first!");

            List<FmUser> userList =
                    fmUusersDao.getAll(start, length, search, nameFilter, emailFilter,
                            countryFilter, cityFilter, genderFilter, ageFromFilter, ageToFilter);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("draw", draw);
            jsonObject.put("recordsTotal", fmUusersDao.getUsersCount(nameFilter, emailFilter,
                    countryFilter, cityFilter, genderFilter, ageFromFilter, ageToFilter));
            if (!StringUtils.isBlank(search)) {
                jsonObject.put("recordsFiltered", fmUusersDao.getUsersCount(nameFilter, emailFilter,
                        countryFilter, cityFilter, genderFilter, ageFromFilter, ageToFilter));
            } else {
                jsonObject.put("recordsFiltered", fmUusersDao.getUsersCount(nameFilter, emailFilter,
                        countryFilter, cityFilter, genderFilter, ageFromFilter, ageToFilter));
            }


            JSONArray jsonUsersArray = new JSONArray();
            JSONArray jsonUserArray;
            for (FmUser user : userList) {
                jsonUserArray = new JSONArray();
                jsonUserArray.put("<img src=\"" + user.getPhoto() + "\" width=\"64\" height=\"64\"/>");
                jsonUserArray.put(user.getName());
                jsonUserArray.put(user.getEmail());
                jsonUserArray.put(user.getCountry());
                jsonUserArray.put(user.getCity());
                if (user.getGender() == 1)
                    jsonUserArray.put("Ж");
                else if (user.getGender() == 0)
                    jsonUserArray.put("М");
                jsonUserArray.put(user.getAge());
                if (user.isIsLocked())
                    jsonUserArray.put("<div style=\"color: #8b0000;\"><b>ЗАБЛОКИРОВАН.</b</br>Причина:&nbsp;" + user.getLockComment() + "</div>");
                else
                    jsonUserArray.put("");

                /**
                 * Actions
                 */
                StringBuilder sbActions = new StringBuilder("");
                if (ApplicationUtility.hasRole("PERMISSION_EDIT_USERS") || ApplicationUtility.hasRole("PERMISSION_VIEW_USERS")) {
                    sbActions.append("<a href=\"" + request.getContextPath() + "/administrator/users/" + user.getId() + "/edit_user\"><button class=\"btn btn-xs btn-info pull-right\" type=\"button\">Перейти</button></a>");
                }
                if (user.getRole().getId() != Defines.USER_TYPE_ADMIN) {
                    if (user.isIsLocked()) {
                        if (ApplicationUtility.hasRole("PERMISSION_DELETE_USERS")) {
                            sbActions.append("</br><a href=\"#\" onclick=\"unlockUser('" + user.getId() + "', '" + user.getName() + "');\"><button class=\"btn btn-xs btn-success pull-right\" type=\"button\">Разблокировать</button></span></a>");
                        }
                    } else {
                        if (ApplicationUtility.hasRole("PERMISSION_DELETE_USERS")) {
                            sbActions.append("</br><a href=\"#\" onclick=\"lockUserReason('" + user.getId() + "', '" + user.getName() + "');\"><button class=\"btn btn-xs btn-warning pull-right\" type=\"button\">Заблокировать</button></span></a>");
                        }
                    }
                    if (ApplicationUtility.hasRole("PERMISSION_DELETE_USERS")) {
                        sbActions.append("</br><a href=\"#\" onclick=\"deleteUser('" + user.getId() + "', '" + user.getName() + "');\"><button class=\"btn btn-xs btn-danger pull-right\" type=\"button\">Удалить</button></span></a>");
                    }
                }
                jsonUserArray.put(sbActions.toString());

                jsonUsersArray.put(jsonUserArray);
            }

            jsonObject.put("data", jsonUsersArray);

            result = ApplicationUtility.encodeStringForAjaxResponse(jsonObject.toString());
        } catch (Exception ex) {
            response.setStatus(1001);
            return ex.getMessage();
        }

        return result;
    }

    @RequestMapping(value = "/get_business_users_JSON", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String getBusinessUsers(
            @RequestParam("draw") int draw,
            @RequestParam("start") int start,
            @RequestParam("length") int length,
            @RequestParam(value = "search[value]", defaultValue = "", required = false) String search,
            @RequestParam(value = "nameFilter", defaultValue = "", required = false) String nameFilter,
            @RequestParam(value = "emailFilter", defaultValue = "", required = false) String emailFilter,
            @RequestParam(value = "countryFilter", defaultValue = "", required = false) String countryFilter,
            @RequestParam(value = "cityFilter", defaultValue = "", required = false) String cityFilter,
            @RequestParam(value = "genderFilter", defaultValue = "", required = false) String genderFilter,
            @RequestParam(value = "ageFromFilter", defaultValue = "", required = false) String ageFromFilter,
            @RequestParam(value = "ageToFilter", defaultValue = "", required = false) String ageToFilter,
            Principal principal,
            HttpServletRequest request,
            HttpServletResponse response) {

        String result;

        try {
            if (principal == null) throw new Exception("Please, login first!");

            List<FmUser> userList = fmUusersDao.getBusinessUsers(0, 0, "", nameFilter, emailFilter,
                    countryFilter, cityFilter, genderFilter, ageFromFilter, ageToFilter);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("draw", draw);
            jsonObject.put("recordsTotal", fmUusersDao.getBusinessUsersCount(nameFilter, emailFilter,
                    countryFilter, cityFilter, genderFilter, ageFromFilter, ageToFilter));
            if (!StringUtils.isBlank(search)) {
                jsonObject.put("recordsFiltered", fmUusersDao.getBusinessUsersCount(nameFilter, emailFilter,
                        countryFilter, cityFilter, genderFilter, ageFromFilter, ageToFilter));
            } else {
                jsonObject.put("recordsFiltered", fmUusersDao.getBusinessUsersCount(nameFilter, emailFilter,
                        countryFilter, cityFilter, genderFilter, ageFromFilter, ageToFilter));
            }


            JSONArray jsonUsersArray = new JSONArray();
            JSONArray jsonUserArray;
            for (FmUser user : userList) {
                jsonUserArray = new JSONArray();
                jsonUserArray.put("<img src=\"" + user.getPhoto() + "\" width=\"64\" height=\"64\"/>");
                jsonUserArray.put(user.getName());
                jsonUserArray.put(user.getEmail());
                jsonUserArray.put(user.getCountry());
                jsonUserArray.put(user.getCity());
                if (user.getGender() == 1)
                    jsonUserArray.put("Ж");
                else if (user.getGender() == 0)
                    jsonUserArray.put("М");
                jsonUserArray.put(user.getAge());
                if (user.isIsLocked())
                    jsonUserArray.put("<div style=\"color: #8b0000;\"><b>ЗАБЛОКИРОВАН.</b</br>Причина:&nbsp;" + user.getLockComment() + "</div>");
                else
                    jsonUserArray.put("");

                /**
                 * Actions
                 */
                StringBuilder sbActions = new StringBuilder("");
                if (ApplicationUtility.hasRole("PERMISSION_EDIT_USERS") || ApplicationUtility.hasRole("PERMISSION_VIEW_USERS")) {
                    sbActions.append("<a href=\"" + request.getContextPath() + "/administrator/users/" + user.getId() + "/edit_user\"><button class=\"btn btn-xs btn-info pull-right\" type=\"button\">Перейти</button></a>");
                }
                if (user.getRole().getId() != Defines.USER_TYPE_ADMIN) {
                    if (user.isIsLocked()) {
                        if (ApplicationUtility.hasRole("PERMISSION_DELETE_USERS")) {
                            sbActions.append("</br><a href=\"#\" onclick=\"unlockUser('" + user.getId() + "', '" + user.getName() + "');\"><button class=\"btn btn-xs btn-success pull-right\" type=\"button\">Разблокировать</button></span></a>");
                        }
                    } else {
                        if (ApplicationUtility.hasRole("PERMISSION_DELETE_USERS")) {
                            sbActions.append("</br><a href=\"#\" onclick=\"lockUserReason('" + user.getId() + "', '" + user.getName() + "');\"><button class=\"btn btn-xs btn-warning pull-right\" type=\"button\">Заблокировать</button></span></a>");
                        }
                    }
                    if (ApplicationUtility.hasRole("PERMISSION_DELETE_USERS")) {
                        sbActions.append("</br><a href=\"#\" onclick=\"deleteUser('" + user.getId() + "', '" + user.getName() + "');\"><button class=\"btn btn-xs btn-danger pull-right\" type=\"button\">Удалить</button></span></a>");
                    }
                }
                jsonUserArray.put(sbActions.toString());

                jsonUsersArray.put(jsonUserArray);
            }

            jsonObject.put("data", jsonUsersArray);

            result = ApplicationUtility.encodeStringForAjaxResponse(jsonObject.toString());
        } catch (Exception ex) {
            response.setStatus(1001);
            return ex.getMessage();
        }

        return result;
    }

    @RequestMapping(value = "/get_moderators_JSON", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String getModerators(
            @RequestParam("draw") int draw,
            @RequestParam("start") int start,
            @RequestParam("length") int length,
            @RequestParam(value = "search[value]", defaultValue = "", required = false) String search,
            @RequestParam(value = "nameFilter", defaultValue = "", required = false) String nameFilter,
            @RequestParam(value = "emailFilter", defaultValue = "", required = false) String emailFilter,
            @RequestParam(value = "countryFilter", defaultValue = "", required = false) String countryFilter,
            @RequestParam(value = "cityFilter", defaultValue = "", required = false) String cityFilter,
            @RequestParam(value = "genderFilter", defaultValue = "", required = false) String genderFilter,
            @RequestParam(value = "ageFromFilter", defaultValue = "", required = false) String ageFromFilter,
            @RequestParam(value = "ageToFilter", defaultValue = "", required = false) String ageToFilter,
            Principal principal,
            HttpServletRequest request,
            HttpServletResponse response) {

        String result;

        try {
            if (principal == null) throw new Exception("Please, login first!");

            List<FmUser> userList = fmUusersDao.getModerators(0, 0, "", nameFilter, emailFilter,
                    countryFilter, cityFilter, genderFilter, ageFromFilter, ageToFilter);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("draw", draw);
            jsonObject.put("recordsTotal", fmUusersDao.getModeratorsCount(nameFilter, emailFilter,
                    countryFilter, cityFilter, genderFilter, ageFromFilter, ageToFilter));
            if (!StringUtils.isBlank(search)) {
                jsonObject.put("recordsFiltered", fmUusersDao.getModeratorsCount(nameFilter, emailFilter,
                        countryFilter, cityFilter, genderFilter, ageFromFilter, ageToFilter));
            } else {
                jsonObject.put("recordsFiltered", fmUusersDao.getModeratorsCount(nameFilter, emailFilter,
                        countryFilter, cityFilter, genderFilter, ageFromFilter, ageToFilter));
            }

            JSONArray jsonUsersArray = new JSONArray();
            JSONArray jsonUserArray;
            for (FmUser user : userList) {
                jsonUserArray = new JSONArray();
                jsonUserArray.put("<img src=\"" + user.getPhoto() + "\" width=\"64\" height=\"64\"/>");
                jsonUserArray.put(user.getName());
                jsonUserArray.put(user.getEmail());
                jsonUserArray.put(user.getCountry());
                jsonUserArray.put(user.getCity());
                if (user.getGender() == 1)
                    jsonUserArray.put("Ж");
                else if (user.getGender() == 0)
                    jsonUserArray.put("М");
                jsonUserArray.put(user.getAge());
                if (user.isIsLocked())
                    jsonUserArray.put("<div style=\"color: #8b0000;\"><b>ЗАБЛОКИРОВАН.</b<br/><br/>Причина:&nbsp;" + user.getLockComment() + "</div>");
                else
                    jsonUserArray.put("");

                /**
                 * Actions
                 */
                StringBuilder sbActions = new StringBuilder("");
                if (ApplicationUtility.hasRole("PERMISSION_EDIT_USERS") || ApplicationUtility.hasRole("PERMISSION_VIEW_USERS")) {
                    sbActions.append("<a href=\"" + request.getContextPath() + "/administrator/users/" + user.getId() + "/edit_user\"><button class=\"btn btn-xs btn-info pull-right\" type=\"button\">Перейти</button></a>");
                }
                if (user.getRole().getId() != Defines.USER_TYPE_ADMIN) {
                    if (user.isIsLocked()) {
                        if (ApplicationUtility.hasRole("PERMISSION_DELETE_USERS")) {
                            sbActions.append("</br><a href=\"#\" onclick=\"unlockUser('" + user.getId() + "', '" + user.getName() + "');\"><button class=\"btn btn-xs btn-success pull-right\" type=\"button\">Разблокировать</button></span></a>");
                        }
                    } else {
                        if (ApplicationUtility.hasRole("PERMISSION_DELETE_USERS")) {
                            sbActions.append("</br><a href=\"#\" onclick=\"lockUserReason('" + user.getId() + "', '" + user.getName() + "');\"><button class=\"btn btn-xs btn-warning pull-right\" type=\"button\">Заблокировать</button></span></a>");
                        }
                    }
                    if (ApplicationUtility.hasRole("PERMISSION_DELETE_USERS")) {
                        sbActions.append("</br><a href=\"#\" onclick=\"deleteUser('" + user.getId() + "', '" + user.getName() + "');\"><button class=\"btn btn-xs btn-danger pull-right\" type=\"button\">Удалить</button></span></a>");
                    }
                }
                jsonUserArray.put(sbActions.toString());

                jsonUsersArray.put(jsonUserArray);
            }

            jsonObject.put("data", jsonUsersArray);

            result = ApplicationUtility.encodeStringForAjaxResponse(jsonObject.toString());
        } catch (Exception ex) {
            response.setStatus(1001);
            return ex.getMessage();
        }

        return result;
    }

    @RequestMapping(value = "/new_user", method = RequestMethod.GET)
    public String newUser(Model model, Principal principal, Locale locale) {
        if (principal == null) return "login";
        Map<Long, String> rolesList = null;
        try {
            rolesList = rolesDao.getAll(locale.getLanguage());
            FmUser userCreateForm = new FmUser();
            model.addAttribute("userCreateForm", userCreateForm);
            model.addAttribute("rolesList", rolesList);
            model.addAttribute("photoFileNameForUpload", String.valueOf(System.currentTimeMillis()) + ApplicationUtility.generateRandomString(6));
        } catch (Exception ex) {
            model.addAttribute(com.wds.define.Defines.ERROR_MESSAGE, ex.getMessage());
            model.addAttribute("rolesList", rolesList);
        }
        return "administrator/users/newUser";
    }

    @RequestMapping(value = "/new_user", method = RequestMethod.POST)
    public String newUser(@ModelAttribute FmUser user,
                          @RequestParam("photo") String photo,
                          @RequestParam("photoFileNameForUpload") String photoFileNameForUpload,
                          @RequestParam("gender") int gender,
                          @RequestParam("language") String language,
                          @RequestParam("categoriesList") List<Long> categoriesList,
                          @RequestParam("flashmobUsersCount") int flashmobUsersCount,
                          BindingResult bindingResult,
                          Model model,
                          Locale locale,
                          Principal principal,
                          RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        Map<Long, String> rolesList = null;
        try {
            rolesList = rolesDao.getAll(locale.getLanguage());

            if (bindingResult.hasErrors())
                throw new Exception(bindingResult.getFieldError().getField());

            if (fmUusersDao.findByEmail(user.getEmail().trim().toLowerCase()) != null)
                throw new Exception("Пользователь с таким email'ом уже существует.");

            if (fmUusersDao.findByName(user.getName().trim().toLowerCase()) != null)
                throw new Exception("Пользователь с таким именем уже существует.");

            user.setPhoto(com.wds.define.Defines.getHostImages() + photoFileNameForUpload + "." + StringUtils.substringAfterLast(photo, "."));
            user.setGender(gender);
            user.setAge(0);
            user.setVkId("");
            user.setFbId("");
            user.setTwId("");

            if (user.getRole().getId() == Defines.USER_TYPE_BUSINESS_USER)
                user.setIsBusinessUser(true);
            else
                user.setIsBusinessUser(false);

            user.setIsLocked(false);
            user.setLockComment("");
            user.setLockExpireDate(0l);
            user.setPassword(ApplicationUtility.md5crypt(user.getPassword()));

            if (user.isIsBusinessUser()) {
                user.setFlashmobUsersCount(flashmobUsersCount);
            }

            fmUusersDao.create(user);
            List<String> languagesList = new ArrayList<>();
            languagesList.add(language);
            fmUusersDao.updateSettings(user, categoriesList, languagesList);

            redirectAttributes.addFlashAttribute(com.wds.define.Defines.SUCCESS_MESSAGE, messageSource.getMessage("UI.Messages.User.CreatedSuccess", null, locale));
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute(com.wds.define.Defines.ERROR_MESSAGE, ex.getMessage());
            redirectAttributes.addFlashAttribute("userCreateForm", user);
            redirectAttributes.addFlashAttribute("userRolesList", rolesList);
            return "redirect:/administrator/users/new_user";
        }
        return "redirect:/administrator/users/list_users";
    }

    @RequestMapping(value = "{userId}/edit_user", method = RequestMethod.GET)
    public String editUser(@PathVariable long userId,
                           Model model, Principal principal, Locale locale) {
        if (principal == null) return "login";
        Map<Long, String> rolesList = null;
        try {
            rolesList = rolesDao.getAll(locale.getLanguage());
            FmUser user = fmUusersDao.findById(userId);
            FmUsersCategoriesDao usersCategoriesDao = new FmUsersCategoriesDao();

            model.addAttribute("userEditForm", user);
            model.addAttribute("rolesList", rolesList);
            model.addAttribute("flashmobsCount", flashmobsDao.getFinishedFlashmobsCountByOwnerId(userId));
            model.addAttribute("friendsCount", friendsDao.getFriendsCountByUid(userId));
            model.addAttribute("categoriesList", usersCategoriesDao.getCategoriesIdsListByUserId(userId));
            model.addAttribute("roleName", user.getRole().getText().get(locale.getLanguage()));
            model.addAttribute("photoFileNameForUpload", String.valueOf(System.currentTimeMillis()) + userId);

            boolean isAdmin = false;
            if (getLoggedUser().getRole().getId() == Defines.USER_TYPE_ADMIN) {
                isAdmin = true;
            }
            model.addAttribute("isAdmin", isAdmin);

            boolean isBusinessUser = false;
            if (getLoggedUser().getRole().getId() == Defines.USER_TYPE_BUSINESS_USER) {
                isBusinessUser = true;
            }
            model.addAttribute("isBusinessUser", isBusinessUser);

        } catch (Exception ex) {
            model.addAttribute(com.wds.define.Defines.ERROR_MESSAGE, ex.getMessage());
            model.addAttribute("rolesList", rolesList);
        }
        return "administrator/users/editUser";
    }

    @RequestMapping(value = "{userId}/edit_user", method = RequestMethod.POST)
    public String editUser(@ModelAttribute FmUser userEditForm,
                           @PathVariable long userId,
                           @RequestParam("photo") String photo,
                           @RequestParam("photoFileNameForUpload") String photoFileNameForUpload,
                           @RequestParam("language") String language,
                           @RequestParam("categoriesList") List<Long> categoriesList,
                           @RequestParam("flashmobUsersCount") int flashmobUsersCount,
                           HttpServletRequest request,
                           BindingResult bindingResult,
                           Locale locale,
                           Principal principal,
                           RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        try {
            if (bindingResult.hasErrors())
                throw new Exception(bindingResult.getFieldError().getField());

            FmUser user = fmUusersDao.findById(userId);

            if (user == null)
                throw new Exception(messageSource.getMessage("UI.Error.UserNotFound", null, locale));

            /**
             * Check permissions
             */
            if (!ApplicationUtility.hasRole("PERMISSION_EDIT_USERS")) {
                return "403";
            }

            if (!StringUtils.isBlank(photo)) {
                user.setPhoto(com.wds.define.Defines.getHostImages() + photoFileNameForUpload + "." + StringUtils.substringAfterLast(photo, "."));
            }
            user.setName(userEditForm.getName());
            user.setEmail(userEditForm.getEmail());

            if (!userEditForm.getPassword().equals(user.getPassword())) {
                user.setPassword(ApplicationUtility.md5crypt(userEditForm.getPassword()));
            }

            user.setGender(userEditForm.getGender());
            user.setAge(userEditForm.getAge());
            user.setAbout(userEditForm.getAbout());
            user.setCountry(userEditForm.getCountry());
            user.setCity(userEditForm.getCity());
            user.setSite(userEditForm.getSite());

            if (userEditForm.getRole() == null) {
                if (user.getRole().getId() == Defines.USER_TYPE_BUSINESS_USER) {
                    user.setIsBusinessUser(true);
                }
            } else {
                user.setRole(userEditForm.getRole());
                if (userEditForm.getRole().getId() == Defines.USER_TYPE_BUSINESS_USER) {
                    user.setIsBusinessUser(true);
                } else {
                    user.setIsBusinessUser(false);
                }
            }

            List<String> languagesList = new ArrayList<>();
            languagesList.add(language);

            if (user.isIsBusinessUser()) {
                user.setFlashmobUsersCount(flashmobUsersCount);
            }

            fmUusersDao.updateSettings(user, categoriesList, languagesList);

            /**
             * Update logged user's data
             */
            if (getLoggedUser().getId() == userId) {
                // Authenticate the user
                UserDetails userDetails = userDetailsService.loadUserByUsername(user.getEmail());

                Authentication authentication = new UsernamePasswordAuthenticationToken(
                        userDetails, null, userDetails.getAuthorities());

                SecurityContextHolder.getContext().setAuthentication(authentication);

                // Create a new session and add the security context.
                //request.getSession().setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, securityContext);
            }

            redirectAttributes.addFlashAttribute(com.wds.define.Defines.SUCCESS_MESSAGE, messageSource.getMessage("UI.Messages.User.UpdatedSuccess", null, locale));

            /**
             * Send push
             */
            FmUserTokenDao userTokenDao = new FmUserTokenDao();
            FmUserToken userToken = userTokenDao.findByUserId(user.getId());

            if (userToken != null) {
                List<String> appleTokenList = new ArrayList<>();
                appleTokenList.add(StringUtils.trimToEmpty(userToken.getToken()));

                ApplePushNotificationThread applePushNotificationThread =
                        new ApplePushNotificationThread(appleTokenList,
                                "Вашы данные узменены.",
                                0,
                                0);
                applePushNotificationThread.start();
            }
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute(com.wds.define.Defines.ERROR_MESSAGE, ex.toString());
            return "redirect:/administrator/users/" + userId + "/edit_user";
        }
        return "redirect:/administrator/users/list_users";
    }

    @RequestMapping(value = "{userId}/view_user", method = RequestMethod.GET)
    public String viewUser(@PathVariable long userId,
                           Model model, Principal principal, Locale locale,
                           RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        try {
            FmUser user = fmUusersDao.findById(userId);
            if (user == null)
                throw new Exception(messageSource.getMessage("UI.Error.UserNotFound", null, locale));

            model.addAttribute("userViewForm", user);
            model.addAttribute("roleName", user.getRole().getText().get(locale.getLanguage()));
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute(com.wds.define.Defines.ERROR_MESSAGE, ex.getMessage());
            return "redirect:/administrator/users/list_users";
        }
        return "administrator/users/viewUser";
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "{userId}/delete_user", method = RequestMethod.POST)
    public String deleteUser(@PathVariable long userId,
                             Model model,
                             Principal principal,
                             Locale locale,
                             RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        try {
            FmUser user = fmUusersDao.findById(userId);
            if (user == null)
                throw new Exception(messageSource.getMessage("UI.Error.UserNotFound", null, locale));
            if (user.getId() == 1)
                throw new Exception(messageSource.getMessage("UI.Warning.YouCannotDeleteUser", null, locale));
            fmUusersDao.delete(user);
            redirectAttributes.addFlashAttribute(com.wds.define.Defines.SUCCESS_MESSAGE, messageSource.getMessage("UI.Messages.User.DeletedSuccess", null, locale));
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute(com.wds.define.Defines.ERROR_MESSAGE, ex.getMessage());
        }
        return "redirect:/administrator/users/list_users";
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "{userId}/delete_user_ajax", method = RequestMethod.POST)
    @ResponseBody
    public String deleteUserAjax(@PathVariable long userId,
                                 Model model,
                                 Principal principal,
                                 Locale locale,
                                 RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        try {
            FmUser user = fmUusersDao.findById(userId);
            if (user == null)
                throw new Exception(messageSource.getMessage("UI.Error.UserNotFound", null, locale));
            if (user.getId() == 1)
                throw new Exception(messageSource.getMessage("UI.Warning.YouCannotDeleteUser", null, locale));
            fmUusersDao.delete(user);
        } catch (Exception ex) {
            return "<p class=\"alert alert-danger alert-dismissable\">" + ApplicationUtility.encodeStringForAjaxResponse(ex.getMessage()) + "</p>";
        }
        return "<p class=\"alert alert-success alert-dismissable\">" + ApplicationUtility.encodeStringForAjaxResponse(messageSource.getMessage("UI.Messages.User.DeletedSuccess", null, locale)) + "</p>";
    }

    @RequestMapping(value = "/lock_user_reason", method = RequestMethod.GET)
    public String lockUserReason(Principal principal, Locale locale) {
        if (principal == null) return "login";
        return "administrator/users/lockUser";
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "{userId}/lock_user_ajax", method = RequestMethod.POST)
    @ResponseBody
    public String lockUserAjax(@PathVariable long userId,
                               @RequestParam("lockReason") String lockReason,
                               @RequestParam("otherLockReason") String otherLockReason,
                               Model model,
                               Principal principal,
                               Locale locale,
                               RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        try {
            FmUser user = fmUusersDao.findById(userId);
            if (user == null)
                throw new Exception(messageSource.getMessage("UI.Error.UserNotFound", null, locale));
            if (user.getRole().getId() == Defines.USER_TYPE_ADMIN)
                throw new Exception(messageSource.getMessage("UI.Warning.YouCannotLockUser", null, locale));

            String strLockReason = "";

            if (StringUtils.isBlank(otherLockReason)) {
                strLockReason += StringUtils.trimToEmpty(lockReason);
            } else {
                strLockReason += StringUtils.trimToEmpty(otherLockReason);
            }

            user.setIsLocked(true);
            user.setLockComment(strLockReason);
            fmUusersDao.update(user);

            /**
             * Send push notification to the user
             */
        /*
            FmUserTokenDao userTokenDao = new FmUserTokenDao();
            FmUserToken userToken = userTokenDao.findByUserId(user.getId());
            if (userToken != null) {
                List<String> appleTokenList = new ArrayList<>();
                appleTokenList.add(StringUtils.trimToEmpty(userToken.getToken()));

                ApplePushNotificationThread applePushNotificationThread =
                        new ApplePushNotificationThread(appleTokenList,
                                Transliterator.transliterate("Ваш аккаунт заблокирован. Причина: " + strLockReason),
                                0, 0);
                applePushNotificationThread.start();
            }
            */
        } catch (Exception ex) {
            return "<p class=\"alert alert-danger alert-dismissable\">" + ApplicationUtility.encodeStringForAjaxResponse(ex.getMessage()) + "</p>";
        }
        return "<p class=\"alert alert-success alert-dismissable\">" + ApplicationUtility.encodeStringForAjaxResponse(messageSource.getMessage("UI.Messages.User.LockedSuccess", null, locale)) + "</p>";
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "{userId}/unlock_user_ajax", method = RequestMethod.POST)
    @ResponseBody
    public String unlockUserAjax(@PathVariable long userId,
                                 Model model,
                                 Principal principal,
                                 Locale locale,
                                 RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        try {
            FmUser user = fmUusersDao.findById(userId);
            if (user == null)
                throw new Exception(messageSource.getMessage("UI.Error.UserNotFound", null, locale));
            user.setIsLocked(false);
            fmUusersDao.update(user);
        } catch (Exception ex) {
            return "<p class=\"alert alert-danger alert-dismissable\">" + ApplicationUtility.encodeStringForAjaxResponse(ex.getMessage()) + "</p>";
        }
        return "<p class=\"alert alert-success alert-dismissable\">" + ApplicationUtility.encodeStringForAjaxResponse(messageSource.getMessage("UI.Messages.User.UnlockedSuccess", null, locale)) + "</p>";
    }
}
