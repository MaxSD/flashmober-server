package com.wds.controller;

import com.wds.dao.*;
import com.wds.dataObjects.GetAllUsersReports;
import com.wds.dataObjects.GetFlashmobReports;
import com.wds.dataObjects.GetReportComments;
import com.wds.dataObjects.GetReportLikes;
import com.wds.database.Defines;
import com.wds.entity.FmFlashmobs;
import com.wds.entity.FmUser;
import com.wds.entity.FmUsersFlashmobs;
import com.wds.utils.ApplicationUtility;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.List;
import java.util.Locale;

@Controller
@RequestMapping(value = "/administrator/reports")
public class ReportController {

    @Autowired
    private MessageSource messageSource;
    @Autowired
    private FmUsersDao fmUusersDao;
    @Autowired
    private FmUsersFlashmobsDao fmUsersFlashmobsDao;
    @Autowired
    private FmFlashmobsDao flashmobsDao;
    @Autowired
    private FmCommentsDao commentsDao;
    @Autowired
    private FmLikesDao likesDao;

    @ModelAttribute("currentUser")
    public FmUser getLoggedUser() {
        try {
            final org.springframework.security.core.userdetails.User principal = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return fmUusersDao.findByEmail(principal.getUsername());
        } catch (Exception ex) {
            return null;
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Principal principal) {
        if (principal == null) return "login";
        return "dashboard";
    }

    @RequestMapping(value = "/list_reports", method = RequestMethod.GET)
    public String listAdmins(Model model, Principal principal, RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        try {
            model.addAttribute("allReportsCount", flashmobsDao.getAllUsersReportsCount());
            model.addAttribute("businessUsersReportsCount", flashmobsDao.getBusinessUsersReportsCount());
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute(com.wds.define.Defines.ERROR_MESSAGE, ex.getMessage());
            return "redirect:/administrator/";
        }
        return "administrator/reports/listReports";
    }

    @RequestMapping(value = "{userFlashmobId}/edit_report", method = RequestMethod.GET)
    public String editReport(@PathVariable long userFlashmobId,
                             Model model, Principal principal, Locale locale) {
        if (principal == null) return "login";
        try {
            FmUsersFlashmobs usersFlashmob = fmUsersFlashmobsDao.findById(userFlashmobId);

            model.addAttribute("reportEditForm", usersFlashmob);
            model.addAttribute("date", ApplicationUtility.secondsToFormattedString(usersFlashmob.getDate_end(),
                    "dd/MM/yyyy HH:mm"));

            model.addAttribute("reportCommentsCount", commentsDao.getReportCommentsCount(userFlashmobId));
            model.addAttribute("reportLikesCount", likesDao.getReportLikesCount(userFlashmobId));
            model.addAttribute("photoFileNameForUpload", String.valueOf(System.currentTimeMillis()) + String.valueOf(getLoggedUser().getId()));
        } catch (Exception ex) {
            model.addAttribute(com.wds.define.Defines.ERROR_MESSAGE, ex.getMessage());
        }
        return "administrator/reports/editReport";
    }

    @RequestMapping(value = "/get_all_users_reports_JSON", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String getAllUsersReports(
            @RequestParam("draw") int draw,
            @RequestParam("start") int start,
            @RequestParam("length") int length,
            @RequestParam(value = "userNameFilter", defaultValue = "", required = false) String userNameFilter,
            @RequestParam(value = "flashmobDescriptionFilter", defaultValue = "", required = false) String flashmobDescriptionFilter,
            Principal principal,
            HttpServletRequest request,
            HttpServletResponse response) {

        String result;

        try {
            if (principal == null) throw new Exception("Please, login first!");

            List<Object> resultList =
                    flashmobsDao.getAllUsersReports(
                            start, length, userNameFilter, flashmobDescriptionFilter, new GetAllUsersReports());

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("draw", draw);
            jsonObject.put("recordsTotal", flashmobsDao.getAllUsersReportsCount());
            jsonObject.put("recordsFiltered", flashmobsDao.getAllUsersReportsCount(userNameFilter, flashmobDescriptionFilter));

            JSONArray jsonUsersArray = new JSONArray();
            JSONArray jsonUserArray;
            for (Object row : resultList) {
                GetAllUsersReports rowData = (GetAllUsersReports) row;

                jsonUserArray = new JSONArray();
                //jsonUserArray.put("<img src=\""+user.getPhotoUrl()+"\" width=\"64\" height=\"64\"/>");
                jsonUserArray.put(rowData.getId());
                jsonUserArray.put("<a href=\"" + request.getContextPath() + "/administrator/users/" + rowData.getOwner_id() + "/edit_user\"><img src=\"" + rowData.getUser_photo() + "\" width=\"64\" height=\"64\"/></a>");
                jsonUserArray.put(rowData.getName());
                jsonUserArray.put("<a href=\"" + request.getContextPath() + "/administrator/reports/" + rowData.getId() + "/edit_report\"><img src=\"" + rowData.getPhoto_report() + "\" width=\"64\" height=\"64\"/></a>");
                jsonUserArray.put(rowData.getDescription());
                jsonUserArray.put(rowData.getLike_count());
                jsonUserArray.put(ApplicationUtility.secondsToFormattedString(rowData.getDate_end(), "dd/MM/yyyy HH:mm"));

                /**
                 * Actions
                 */
                StringBuilder sbActions = new StringBuilder("");
                if (ApplicationUtility.hasRole("PERMISSION_EDIT_USERS")) {
                    sbActions.append("<a href=\"" + request.getContextPath() + "/administrator/reports/" + rowData.getId() + "/edit_report\"><span class=\"glyphicon glyphicon-edit\"></span></a>");
                }
                if (ApplicationUtility.hasRole("PERMISSION_DELETE_USERS")) {
                    sbActions.append("&nbsp;&nbsp;<a href=\"#\" onclick=\"deleteReportReason('" + rowData.getId() + "');\"><span class=\"glyphicon glyphicon-remove-sign\"></span></a>");
                }
                jsonUserArray.put(sbActions.toString());

                jsonUsersArray.put(jsonUserArray);
            }

            jsonObject.put("data", jsonUsersArray);

            result = ApplicationUtility.encodeStringForAjaxResponse(jsonObject.toString());
        } catch (Exception ex) {
            response.setStatus(1001);
            return ex.getMessage();
        }

        return result;
    }

    @RequestMapping(value = "/get_business_users_reports_JSON", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String getBusinessUsersReports(
            @RequestParam("draw") int draw,
            @RequestParam("start") int start,
            @RequestParam("length") int length,
            @RequestParam(value = "userNameFilter", defaultValue = "", required = false) String userNameFilter,
            @RequestParam(value = "flashmobDescriptionFilter", defaultValue = "", required = false) String flashmobDescriptionFilter,
            Principal principal,
            HttpServletRequest request,
            HttpServletResponse response) {

        String result;

        try {
            if (principal == null) throw new Exception("Please, login first!");

            List<Object> resultList =
                    flashmobsDao.getBusinessUsersReports(
                            start, length, userNameFilter, flashmobDescriptionFilter, new GetAllUsersReports());

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("draw", draw);
            jsonObject.put("recordsTotal", flashmobsDao.getBusinessUsersReportsCount());
            jsonObject.put("recordsFiltered", flashmobsDao.getBusinessUsersReportsCount(userNameFilter, flashmobDescriptionFilter));

            JSONArray jsonUsersArray = new JSONArray();
            JSONArray jsonUserArray;
            for (Object row : resultList) {
                GetAllUsersReports rowData = (GetAllUsersReports) row;

                jsonUserArray = new JSONArray();
                //jsonUserArray.put("<img src=\""+user.getPhotoUrl()+"\" width=\"64\" height=\"64\"/>");
                jsonUserArray.put(rowData.getId());
                jsonUserArray.put("<a href=\"" + request.getContextPath() + "/administrator/users/" + rowData.getOwner_id() + "/edit_user\"><img src=\"" + rowData.getUser_photo() + "\" width=\"64\" height=\"64\"/></a>");
                jsonUserArray.put(rowData.getName());
                jsonUserArray.put("<a href=\"" + request.getContextPath() + "/administrator/reports/" + rowData.getId() + "/edit_report\"><img src=\"" + rowData.getPhoto_report() + "\" width=\"64\" height=\"64\"/></a>");
                jsonUserArray.put(rowData.getDescription());
                jsonUserArray.put(rowData.getLike_count());
                jsonUserArray.put(ApplicationUtility.secondsToFormattedString(rowData.getDate_end(), "dd/MM/yyyy HH:mm"));

                /**
                 * Actions
                 */
                StringBuilder sbActions = new StringBuilder("");
                if (ApplicationUtility.hasRole("PERMISSION_EDIT_REPORTS")) {
                    sbActions.append("<a href=\"" + request.getContextPath() + "/administrator/reports/" + rowData.getId() + "/edit_report\"><span class=\"glyphicon glyphicon-edit\"></span></a>");
                }
                if (ApplicationUtility.hasRole("PERMISSION_DELETE_REPORTS")) {
                    sbActions.append("&nbsp;&nbsp;<a href=\"#\" onclick=\"deleteReportReason('" + rowData.getId() + "');\"><span class=\"glyphicon glyphicon-remove-sign\"></span></a>");
                }
                jsonUserArray.put(sbActions.toString());

                jsonUsersArray.put(jsonUserArray);
            }

            jsonObject.put("data", jsonUsersArray);

            result = ApplicationUtility.encodeStringForAjaxResponse(jsonObject.toString());
        } catch (Exception ex) {
            response.setStatus(1001);
            return ex.getMessage();
        }

        return result;
    }

    @RequestMapping(value = "{uid}/get_my_tasks_reports_JSON", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String getMyTasksReports(
            @PathVariable("uid") long uid,
            @RequestParam("draw") int draw,
            @RequestParam("start") int start,
            @RequestParam("length") int length,
            @RequestParam(value = "userNameFilter", defaultValue = "", required = false) String userNameFilter,
            @RequestParam(value = "flashmobDescriptionFilter", defaultValue = "", required = false) String flashmobDescriptionFilter,
            Principal principal,
            HttpServletRequest request,
            HttpServletResponse response) {

        String result;

        try {
            if (principal == null) throw new Exception("Please, login first!");

            List<Object> resultList =
                    flashmobsDao.getMyTasksReports(
                            uid, start, length, userNameFilter, flashmobDescriptionFilter, new GetAllUsersReports());

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("draw", draw);
            jsonObject.put("recordsTotal", flashmobsDao.getBusinessUsersReportsCount());
            jsonObject.put("recordsFiltered", flashmobsDao.getBusinessUsersReportsCount(userNameFilter, flashmobDescriptionFilter));

            JSONArray jsonUsersArray = new JSONArray();
            JSONArray jsonUserArray;
            for (Object row : resultList) {
                GetAllUsersReports rowData = (GetAllUsersReports) row;

                jsonUserArray = new JSONArray();
                //jsonUserArray.put("<img src=\""+user.getPhotoUrl()+"\" width=\"64\" height=\"64\"/>");
                jsonUserArray.put(rowData.getId());
                jsonUserArray.put("<a href=\"" + request.getContextPath() + "/administrator/users/" + rowData.getOwner_id() + "/edit_user\"><img src=\"" + rowData.getUser_photo() + "\" width=\"64\" height=\"64\"/></a>");
                jsonUserArray.put(rowData.getName());
                jsonUserArray.put("<a href=\"" + request.getContextPath() + "/administrator/reports/" + rowData.getId() + "/edit_report\"><img src=\"" + rowData.getPhoto_report() + "\" width=\"64\" height=\"64\"/></a>");
                jsonUserArray.put(rowData.getDescription());
                jsonUserArray.put(rowData.getLike_count());
                jsonUserArray.put(ApplicationUtility.secondsToFormattedString(rowData.getDate_end(), "dd/MM/yyyy HH:mm"));

                /**
                 * Actions
                 */
                StringBuilder sbActions = new StringBuilder("");
                if (ApplicationUtility.hasRole("PERMISSION_EDIT_REPORTS")) {
                    sbActions.append("<a href=\"" + request.getContextPath() + "/administrator/reports/" + rowData.getId() + "/edit_report\"><span class=\"glyphicon glyphicon-edit\"></span></a>");
                }
                if (ApplicationUtility.hasRole("PERMISSION_DELETE_REPORTS")) {
                    sbActions.append("&nbsp;&nbsp;<a href=\"#\" onclick=\"deleteReportReason('" + rowData.getId() + "');\"><span class=\"glyphicon glyphicon-remove-sign\"></span></a>");
                }
                jsonUserArray.put(sbActions.toString());

                jsonUsersArray.put(jsonUserArray);
            }

            jsonObject.put("data", jsonUsersArray);

            result = ApplicationUtility.encodeStringForAjaxResponse(jsonObject.toString());
        } catch (Exception ex) {
            response.setStatus(1001);
            return ex.getMessage();
        }

        return result;
    }

    @RequestMapping(value = "/get_flashmobReports_JSON", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String getFlashmobReports(
            @RequestParam("draw") int draw,
            @RequestParam("start") int start,
            @RequestParam("length") int length,
            @RequestParam(value = "flashmobId", defaultValue = "", required = false) Long flashmobId,
            Principal principal,
            HttpServletRequest request,
            HttpServletResponse response) {

        String result;

        try {
            if (principal == null) throw new Exception("Please, login first!");

            List<Object> resultList =
                    flashmobsDao.getFlashmobReports(
                            start, length, flashmobId, "", "", new GetFlashmobReports());

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("draw", draw);
            jsonObject.put("recordsTotal", flashmobsDao.getFlashmobReportsCount(flashmobId));
            jsonObject.put("recordsFiltered", flashmobsDao.getFlashmobReportsCount(flashmobId, "", ""));

            JSONArray jsonUsersArray = new JSONArray();
            JSONArray jsonUserArray;
            for (Object row : resultList) {
                GetFlashmobReports rowData = (GetFlashmobReports) row;

                jsonUserArray = new JSONArray();
                jsonUserArray.put("<img src=\"" + rowData.getUser_photo() + "\" width=\"64\" height=\"64\"/>");
                jsonUserArray.put(rowData.getName());
                jsonUserArray.put(ApplicationUtility.secondsToFormattedString(rowData.getDate_end(), "dd/MM/yyyy HH:mm"));

                /**
                 * Actions
                 */
            /*
                StringBuilder sbActions = new StringBuilder("");
                if (ApplicationUtility.hasRole("PERMISSION_EDIT_USERS")) {
                    sbActions.append("<a href=\"" + request.getContextPath() + "/administrator/reports/" + rowData.getId() + "/edit_report\"><span class=\"glyphicon glyphicon-edit\"></span></a>");
                }
                if (ApplicationUtility.hasRole("PERMISSION_DELETE_USERS")) {
                    sbActions.append("&nbsp;&nbsp;<a href=\"#\" onclick=\"deleteReports('" + rowData.getId() + "');\"><span class=\"glyphicon glyphicon-remove-sign\"></span></a>");
                }
                jsonUserArray.put(sbActions.toString());
            */
                jsonUsersArray.put(jsonUserArray);
            }

            jsonObject.put("data", jsonUsersArray);

            result = ApplicationUtility.encodeStringForAjaxResponse(jsonObject.toString());
        } catch (Exception ex) {
            response.setStatus(1001);
            return ex.getMessage();
        }

        return result;
    }

    @RequestMapping(value = "/get_report_comments_JSON", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String getReportComments_JSON(
            @RequestParam("draw") int draw,
            @RequestParam("start") int start,
            @RequestParam("length") int length,
            @RequestParam(value = "userFlashmobId", defaultValue = "", required = false) Long userFlashmobId,
            Principal principal,
            HttpServletRequest request,
            HttpServletResponse response) {

        String result;

        try {
            if (principal == null) throw new Exception("Please, login first!");

            List<Object> resultList =
                    commentsDao.getCommentsList(start, length, userFlashmobId, new GetReportComments());

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("draw", draw);
            jsonObject.put("recordsTotal", commentsDao.getReportCommentsCount(userFlashmobId));
            jsonObject.put("recordsFiltered", commentsDao.getReportCommentsCount(userFlashmobId));

            JSONArray jsonUsersArray = new JSONArray();
            JSONArray jsonUserArray;
            for (Object row : resultList) {
                GetReportComments rowData = (GetReportComments) row;

                jsonUserArray = new JSONArray();
                jsonUserArray.put("<img src=\"" + rowData.getUser_photo() + "\" width=\"64\" height=\"64\"/>");
                jsonUserArray.put(rowData.getName());
                jsonUserArray.put(rowData.getEmail());
                jsonUserArray.put(ApplicationUtility.secondsToFormattedString(rowData.getDate(), "dd/MM/yyyy HH:mm"));
                jsonUserArray.put(rowData.getComment());

                /**
                 * Actions
                 */
            /*
                StringBuilder sbActions = new StringBuilder("");
                if (ApplicationUtility.hasRole("PERMISSION_EDIT_USERS")) {
                    sbActions.append("<a href=\"" + request.getContextPath() + "/administrator/reports/" + rowData.getId() + "/edit_report\"><span class=\"glyphicon glyphicon-edit\"></span></a>");
                }
                if (ApplicationUtility.hasRole("PERMISSION_DELETE_USERS")) {
                    sbActions.append("&nbsp;&nbsp;<a href=\"#\" onclick=\"deleteReports('" + rowData.getId() + "');\"><span class=\"glyphicon glyphicon-remove-sign\"></span></a>");
                }
                jsonUserArray.put(sbActions.toString());
            */
                jsonUsersArray.put(jsonUserArray);
            }

            jsonObject.put("data", jsonUsersArray);

            result = ApplicationUtility.encodeStringForAjaxResponse(jsonObject.toString());
        } catch (Exception ex) {
            response.setStatus(1001);
            return ex.getMessage();
        }

        return result;
    }

    @RequestMapping(value = "/get_report_likes_JSON", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String getReportLikes_JSON(
            @RequestParam("draw") int draw,
            @RequestParam("start") int start,
            @RequestParam("length") int length,
            @RequestParam(value = "userFlashmobId", defaultValue = "", required = false) Long userFlashmobId,
            Principal principal,
            HttpServletRequest request,
            HttpServletResponse response) {

        String result;

        try {
            if (principal == null) throw new Exception("Please, login first!");

            List<Object> resultList =
                    likesDao.getLikesList(start, length, userFlashmobId, new GetReportLikes());

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("draw", draw);
            jsonObject.put("recordsTotal", likesDao.getReportLikesCount(userFlashmobId));
            jsonObject.put("recordsFiltered", likesDao.getReportLikesCount(userFlashmobId));

            JSONArray jsonUsersArray = new JSONArray();
            JSONArray jsonUserArray;
            for (Object row : resultList) {
                GetReportLikes rowData = (GetReportLikes) row;

                jsonUserArray = new JSONArray();
                jsonUserArray.put("<img src=\"" + rowData.getUser_photo() + "\" width=\"64\" height=\"64\"/>");
                jsonUserArray.put(rowData.getName());
                jsonUserArray.put(rowData.getEmail());
                jsonUserArray.put(ApplicationUtility.secondsToFormattedString(rowData.getDate(), "dd/MM/yyyy HH:mm"));

                /**
                 * Actions
                 */
            /*
                StringBuilder sbActions = new StringBuilder("");
                if (ApplicationUtility.hasRole("PERMISSION_EDIT_USERS")) {
                    sbActions.append("<a href=\"" + request.getContextPath() + "/administrator/reports/" + rowData.getId() + "/edit_report\"><span class=\"glyphicon glyphicon-edit\"></span></a>");
                }
                if (ApplicationUtility.hasRole("PERMISSION_DELETE_USERS")) {
                    sbActions.append("&nbsp;&nbsp;<a href=\"#\" onclick=\"deleteReports('" + rowData.getId() + "');\"><span class=\"glyphicon glyphicon-remove-sign\"></span></a>");
                }
                jsonUserArray.put(sbActions.toString());
            */
                jsonUsersArray.put(jsonUserArray);
            }

            jsonObject.put("data", jsonUsersArray);

            result = ApplicationUtility.encodeStringForAjaxResponse(jsonObject.toString());
        } catch (Exception ex) {
            response.setStatus(1001);
            return ex.getMessage();
        }

        return result;
    }

    @RequestMapping(value = "/delete_report_reason", method = RequestMethod.GET)
    public String lockUserReason(Principal principal, Locale locale) {
        if (principal == null) return "login";
        return "administrator/reports/deleteReport";
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "{reportId}/delete_report_ajax", method = RequestMethod.POST)
    @ResponseBody
    public String deleteReportAjax(@PathVariable long reportId,
                                   @RequestParam("deleteReason") String deleteReason,
                                   @RequestParam("otherDeleteReason") String otherDeleteReason,
                                   Model model,
                                   Principal principal,
                                   Locale locale,
                                   RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        try {
            FmUsersFlashmobs usersFlashmobs = fmUsersFlashmobsDao.findById(reportId);
            if (usersFlashmobs != null) {
                if (getLoggedUser().getRole().getId() == Defines.USER_TYPE_ADMIN ||
                        getLoggedUser().getRole().getId() == Defines.USER_TYPE_MODERATOR) {

                    FmUser user = fmUusersDao.findById(usersFlashmobs.getUserId());
                    if (user != null) {
                        if (!user.isIsReportsDeletedByModerator()) {
                            if (StringUtils.isBlank(otherDeleteReason)) {
                                user.setReportsDeletedReason(StringUtils.trimToEmpty(deleteReason));
                            } else {
                                user.setReportsDeletedReason(StringUtils.trimToEmpty(otherDeleteReason));
                            }

                            user.setIsIsReportsDeletedByModerator(true);
                            fmUusersDao.update(user);
                        }
                    }
                }

                fmUsersFlashmobsDao.delete(usersFlashmobs);
            }
        } catch (Exception ex) {
            return "<p class=\"alert alert-danger alert-dismissable\">" + ApplicationUtility.encodeStringForAjaxResponse(ex.getMessage()) + "</p>";
        }
        return "<p class=\"alert alert-success alert-dismissable\">" + ApplicationUtility.encodeStringForAjaxResponse(messageSource.getMessage("UI.Messages.Report.DeletedSuccess", null, locale)) + "</p>";
    }

    @RequestMapping(value = "{userFlashmobId}/edit_report", method = RequestMethod.POST)
    public String editReport(@ModelAttribute FmUsersFlashmobs reportEditForm,
                             @PathVariable long userFlashmobId,
                             @RequestParam("photo") String photo,
                             @RequestParam("photoFileNameForUpload") String photoFileNameForUpload,
                             @RequestParam("video_report") String videoReport,
                             BindingResult bindingResult,
                             Locale locale,
                             Principal principal,
                             RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        try {
            if (bindingResult.hasErrors())
                throw new Exception(bindingResult.getFieldError().getField());

            FmUsersFlashmobs usersFlashmob = fmUsersFlashmobsDao.findById(userFlashmobId);

            if (usersFlashmob == null)
                throw new Exception("Отчёт не найден.");

            /**
             * Check permissions
             */
            if (!ApplicationUtility.hasRole("PERMISSION_EDIT_REPORTS")) {
                return "403";
            }

            if (!StringUtils.isBlank(photo)) {
                usersFlashmob.setPhoto_report(com.wds.define.Defines.getHostImages() + photoFileNameForUpload + "." + StringUtils.substringAfterLast(photo, "."));
            }
            usersFlashmob.setVideo_report(videoReport);
            fmUsersFlashmobsDao.update(usersFlashmob);

            redirectAttributes.addFlashAttribute(com.wds.define.Defines.SUCCESS_MESSAGE, messageSource.getMessage("UI.Messages.Report.UpdatedSuccess", null, locale));
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute(com.wds.define.Defines.ERROR_MESSAGE, ex.getMessage());
            return "redirect:/administrator/reports/" + reportEditForm + "/edit_report";
        }
        return "redirect:/administrator/reports/list_reports";
    }
}
