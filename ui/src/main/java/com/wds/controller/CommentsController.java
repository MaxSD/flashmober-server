package com.wds.controller;

import com.wds.dao.FmCommentsDao;
import com.wds.dao.FmUsersDao;
import com.wds.dataObjects.GetAllComments;
import com.wds.database.Defines;
import com.wds.entity.FmComment;
import com.wds.entity.FmUser;
import com.wds.utils.ApplicationUtility;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.List;
import java.util.Locale;

@Controller
@RequestMapping(value = "/administrator/comments")
public class CommentsController {

    @Autowired
    private MessageSource messageSource;
    @Autowired
    private FmUsersDao fmUusersDao;
    @Autowired
    private FmCommentsDao commentsDao;

    @ModelAttribute("currentUser")
    public FmUser getLoggedUser() {
        try {
            final org.springframework.security.core.userdetails.User principal = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return fmUusersDao.findByEmail(principal.getUsername());
        } catch (Exception ex) {
            return null;
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Principal principal) {
        if (principal == null) return "login";
        return "dashboard";
    }

    @RequestMapping(value = "/list_comments", method = RequestMethod.GET)
    public String listComments(Model model, Principal principal, RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        try {
            //model.addAttribute("allReportsCount", flashmobsDao.getAllUsersReportsCount());
            //model.addAttribute("businessUsersReportsCount", flashmobsDao.getBusinessUsersReportsCount());
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute(com.wds.define.Defines.ERROR_MESSAGE, ex.getMessage());
            return "redirect:/administrator/";
        }
        return "administrator/comments/listComments";
    }

    @RequestMapping(value = "/get_all_comments_JSON", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String getAllComments_JSON(
            @RequestParam("draw") int draw,
            @RequestParam("start") int start,
            @RequestParam("length") int length,
            @RequestParam(value = "userNameFilter", defaultValue = "", required = false) String userNameFilter,
            @RequestParam(value = "flashmobDescriptionFilter", defaultValue = "", required = false) String flashmobDescriptionFilter,
            Principal principal,
            HttpServletRequest request,
            HttpServletResponse response) {

        String result;

        try {
            if (principal == null) throw new Exception("Please, login first!");

            List<Object> resultList =
                    commentsDao.getAllCommentsList(
                            start, length, userNameFilter, flashmobDescriptionFilter, new GetAllComments());

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("draw", draw);
            jsonObject.put("recordsTotal", commentsDao.getCommentsCount(null, null));
            jsonObject.put("recordsFiltered", commentsDao.getCommentsCount(userNameFilter, flashmobDescriptionFilter));

            JSONArray jsonUsersArray = new JSONArray();
            JSONArray jsonUserArray;
            for (Object row : resultList) {
                GetAllComments rowData = (GetAllComments) row;

                jsonUserArray = new JSONArray();
                //jsonUserArray.put("<img src=\""+user.getPhotoUrl()+"\" width=\"64\" height=\"64\"/>");
                jsonUserArray.put(rowData.getId());
                jsonUserArray.put("<a href=\""+request.getContextPath()+"/administrator/users/"+rowData.getUid()+"/edit_user\"><img src=\""+rowData.getUser_photo()+ "\" width=\"64\" height=\"64\"/></a>");
                jsonUserArray.put(rowData.getName());
                jsonUserArray.put(rowData.getDescription());
                jsonUserArray.put("<a href=\""+request.getContextPath()+"/administrator/reports/"+rowData.getUser_flashmob_id()+"/edit_report\"><img src=\""+rowData.getPhoto_report()+"\" width=\"64\" height=\"64\"/></a>");
                jsonUserArray.put(rowData.getComment());
                jsonUserArray.put(ApplicationUtility.secondsToFormattedString(rowData.getDate(), "dd/MM/yyyy HH:mm"));

                /**
                 * Actions
                 */
                StringBuilder sbActions = new StringBuilder("");
                if (ApplicationUtility.hasRole("PERMISSION_DELETE_USERS")) {
                    sbActions.append("&nbsp;&nbsp;<a href=\"#\" onclick=\"deleteComment('" + rowData.getId() + "');\"><span class=\"glyphicon glyphicon-remove-sign\"></span></a>");
                }
                jsonUserArray.put(sbActions.toString());

                jsonUsersArray.put(jsonUserArray);
            }

            jsonObject.put("data", jsonUsersArray);

            result = ApplicationUtility.encodeStringForAjaxResponse(jsonObject.toString());
        } catch (Exception ex) {
            response.setStatus(1001);
            return ex.getMessage();
        }

        return result;
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "{commentId}/delete_comment_ajax", method = RequestMethod.POST)
    @ResponseBody
    public String deleteCommentAjax(@PathVariable long commentId,
                                    Model model,
                                    Principal principal,
                                    Locale locale,
                                    RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        try {
            FmComment comment = commentsDao.findById(commentId);
            if (comment != null) {
                commentsDao.delete(comment);
            }
        } catch (Exception ex) {
            return "<p class=\"alert alert-danger alert-dismissable\">" + ApplicationUtility.encodeStringForAjaxResponse(ex.getMessage()) + "</p>";
        }
        return "<p class=\"alert alert-success alert-dismissable\">" + ApplicationUtility.encodeStringForAjaxResponse(messageSource.getMessage("UI.Messages.Comment.DeletedSuccess", null, locale)) + "</p>";
    }
}