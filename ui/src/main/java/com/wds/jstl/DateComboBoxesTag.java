package com.wds.jstl;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings({"UnusedDeclaration", "StringConcatenationInsideStringBufferAppend"})
public class DateComboBoxesTag extends SimpleTagSupport {

    private String elementDayId = "day";
    private String elementMonthId = "month";
    private String elementYearId = "year";
    private Long date = 0l;
    private String selectedDay = "00";
    private String selectedMonth = "00";
    private String selectedYear = "00";

    public void doTag() throws JspException {
        try {
            if (date != null && date != 0) {
                parseDate(date);
            }

            JspWriter out = getJspContext().getOut();

            StringBuilder sb = new StringBuilder();
            sb.append("<div class=\"form-inline\">");
            sb.append("<div class=\"form-group\">");
            // Day
            sb.append("День:  ");
            sb.append("<select id=\"" + elementDayId + "\" name=\"" + elementDayId + "\" class=\"form-control \" style=\"width: 70px;\">");
            for (int day = 1; day <= 31; day++) {
                sb.append("<option " +
                        "value=\"" + StringUtils.leftPad(Integer.toString(day), 2, "0") + "\" "+selected(StringUtils.leftPad(Integer.toString(day), 2, "0"), selectedDay)+">" +
                        StringUtils.leftPad(Integer.toString(day), 2, "0") + "</option>");
            }
            sb.append("</select>");

            // Month
            sb.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Месяц:  ");
            sb.append("<select id=\"" + elementMonthId + "\" name=\"" + elementMonthId + "\" class=\"form-control\" style=\"width: 130px;\">");
            sb.append("<option value=\"01\" "+selected("01", selectedMonth)+">Январь</option>");
            sb.append("<option value=\"02\" "+selected("02", selectedMonth)+">Февраль</option>");
            sb.append("<option value=\"03\" "+selected("03", selectedMonth)+">Март</option>");
            sb.append("<option value=\"04\" "+selected("04", selectedMonth)+">Апрель</option>");
            sb.append("<option value=\"05\" "+selected("05", selectedMonth)+">Май</option>");
            sb.append("<option value=\"06\" "+selected("06", selectedMonth)+">Июнь</option>");
            sb.append("<option value=\"07\" "+selected("07", selectedMonth)+">Июль</option>");
            sb.append("<option value=\"08\" "+selected("08", selectedMonth)+">Август</option>");
            sb.append("<option value=\"09\" "+selected("09", selectedMonth)+">Сентябрь</option>");
            sb.append("<option value=\"10\" "+selected("10", selectedMonth)+">Октябрь</option>");
            sb.append("<option value=\"11\" "+selected("11", selectedMonth)+">Ноябрь</option>");
            sb.append("<option value=\"12\" "+selected("12", selectedMonth)+">Декабрь</option>");
            sb.append("</select>");

            // Year
            sb.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Год:  ");
            sb.append("<select id=\"" + elementYearId + "\" name=\"" + elementYearId + "\" class=\"form-control\" style=\"width: 80px;\">");
            for (int year = 1950; year <= Integer.parseInt(new SimpleDateFormat("yyyy").format(new Date())) -18; year++) {
                sb.append("<option value=\"" + year + "\" "+selected(String.valueOf(year), selectedYear)+">" + year + "</option>");
            }
            sb.append("</select>");
            sb.append("</div></div>");

            out.print(sb.toString());

        } catch (IOException ioe) {
            throw new JspException("Error: " + ioe.getMessage());
        }
    }

    private String selected(String optionValue, String currentValue) {
        return optionValue.equals(currentValue) ? "selected=\"selected\"" : "";
    }

    private void parseDate(long date) {
        String[] parsedDate = StringUtils.split(new SimpleDateFormat("dd.MM.yyyy").format(new Date(date)), ".");
        setSelectedDay(parsedDate[0]);
        setSelectedMonth(parsedDate[1]);
        setSelectedYear(parsedDate[2]);
    }

    public String getElementDayId() {
        return elementDayId;
    }

    public void setElementDayId(String elementDayId) {
        this.elementDayId = elementDayId;
    }

    public String getElementMonthId() {
        return elementMonthId;
    }

    public void setElementMonthId(String elementMonthId) {
        this.elementMonthId = elementMonthId;
    }

    public String getElementYearId() {
        return elementYearId;
    }

    public void setElementYearId(String elementYearId) {
        this.elementYearId = elementYearId;
    }

    public long getDate() { return date; }
    public void setDate(long date) { this.date = date; }

    public String getSelectedDay() { return selectedDay; }
    public void setSelectedDay(String selectedDay) { this.selectedDay = selectedDay; }

    public String getSelectedMonth() { return selectedMonth; }
    public void setSelectedMonth(String selectedMonth) { this.selectedMonth = selectedMonth; }

    public String getSelectedYear() { return selectedYear; }
    public void setSelectedYear(String selectedYear) { this.selectedYear = selectedYear; }
}
