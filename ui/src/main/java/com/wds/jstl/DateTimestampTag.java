package com.wds.jstl;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class DateTimestampTag extends SimpleTagSupport {

    private Long date = 0l;

    public void doTag() throws JspException {
        try {
            if (date != null && date != 0) {
                String[] parsedDate = StringUtils.split(new SimpleDateFormat("dd.MM.yyyy").format(new Date(date)), ".");
                JspWriter out = getJspContext().getOut();
                out.print(Arrays.toString(parsedDate));
            }
        } catch (IOException ioe) {
            throw new JspException("Error: " + ioe.getMessage());
        }
    }

    public Long getDate() {
        return date;
    }
    public void setDate(Long date) {
        this.date = date;
    }
}
