package com.wds.service;

import com.wds.dao.RolesDao;
import com.wds.entity.Role;
import com.wds.exception.v1.SelectDatabaseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.beans.PropertyEditorSupport;

@Component("userRoleConverter")
public class UserRoleConverter extends PropertyEditorSupport {

    @Autowired
    private RolesDao rolesDao;

    @Override
    public void setAsText(String id) {
        try {
            Role c = this.rolesDao.findById(Long.valueOf(id));
            this.setValue(c);
        } catch (SelectDatabaseException ce) {
            throw new RuntimeException(ce.getMessage());
        }
    }
}
