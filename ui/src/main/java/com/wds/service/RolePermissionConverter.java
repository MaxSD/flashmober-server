package com.wds.service;

import com.wds.dao.PermissionsDao;
import com.wds.entity.Permission;
import com.wds.exception.v1.SelectDatabaseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.beans.PropertyEditorSupport;

@Component("rolePermissionConverter")
public class RolePermissionConverter extends PropertyEditorSupport {

    @Autowired
    private PermissionsDao permissionsDao;

    @Override
    public void setAsText(String id) {
        try {
            Permission c = this.permissionsDao.findById(Long.valueOf(id));
            this.setValue(c);
        } catch (SelectDatabaseException ce) {
            throw new RuntimeException(ce.getMessage());
        }
    }
}
