package com.wds.dataObjects;

import java.math.BigInteger;

public class GetFleshmobListByOwnerId {

    private Long id;
    private Long user_flashmob_id;
    private String photo;
    private Long date_create;
    private String description;
    private BigInteger people_count;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUser_flashmob_id() {
        return user_flashmob_id;
    }

    public void setUser_flashmob_id(Long user_flashmob_id) {
        this.user_flashmob_id = user_flashmob_id;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Long getDate_create() {
        return date_create;
    }

    public void setDate_create(Long date_create) {
        this.date_create = date_create;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigInteger getPeople_count() {
        return people_count;
    }

    public void setPeople_count(BigInteger people_count) {
        this.people_count = people_count;
    }
}
