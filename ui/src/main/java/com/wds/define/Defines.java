package com.wds.define;

public class Defines {
    public static boolean       DEBUG_MODE = true;
    // MESSAGE
    public final static String  ERROR_MESSAGE =             "errorMessage";
    public final static String  SUCCESS_MESSAGE =           "successMessage";

    // SMTP
    public final static String  SMTP_HOST =                 "smtp.gmail.com";
    public final static String  SMTP_PORT =                 "587";
    public final static String  SMTP_USER =                 "test@gmail.com";
    public final static String  SMTP_PASSWORD =             "test";
    public final static boolean SMTP_AUTH =                 true;

    public static String getDirectoryPdf() { return DEBUG_MODE ? DebugDefines.DIRECTORY_TO_PDF : ReleaseDefines.DIRECTORY_TO_PDF; }
    public static String getHostPdf() {
        return DEBUG_MODE ? DebugDefines.HOST_TO_PDF : ReleaseDefines.HOST_TO_PDF;
    }

    public static String getDirectoryImages() { return DEBUG_MODE ? DebugDefines.DIRECTORY_TO_IMAGES : ReleaseDefines.DIRECTORY_TO_IMAGES; }
    public static String getHostImages() { return DEBUG_MODE ? DebugDefines.HOST_TO_IMAGES : ReleaseDefines.HOST_TO_IMAGES; }
}
