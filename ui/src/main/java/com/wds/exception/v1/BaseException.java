package com.wds.exception.v1;

@SuppressWarnings("WeakerAccess")
public abstract class BaseException extends Exception {

    protected int code;
    protected String message;

    protected BaseException() {
        super();
    }

    public int getCode() {
        return code;
    }
    public String getMessage() {
        return message;
    }
}
