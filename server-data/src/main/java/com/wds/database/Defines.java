package com.wds.database;

public class Defines {

    public final static int ACTION_TYPE_LIKE = 1;
    public final static int ACTION_TYPE_COMMENT = 2;
    public final static int ACTION_TYPE_FRIEND_ADD = 3;
    public final static int ACTION_TYPE_FRIEND_ACCEPT = 4;

    public final static int FLASHMOB_STATUS_CREATED = 1;
    public final static int FLASHMOB_STATUS_ACCEPTED = 2;
    public final static int FLASHMOB_STATUS_REJECTED = 3;
    public final static int FLASHMOB_STATUS_FINISHED = 4;

    public final static long USER_TYPE_ADMIN = 1;
    public final static long USER_TYPE_MODERATOR = 2;
    public final static long USER_TYPE_USER = 3;
    public final static long USER_TYPE_BUSINESS_USER = 4;
}
