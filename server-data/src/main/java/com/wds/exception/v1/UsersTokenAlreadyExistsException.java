package com.wds.exception.v1;

public class UsersTokenAlreadyExistsException extends BaseException {

    public UsersTokenAlreadyExistsException() {
        super();
        this.code = 500;
        this.localCode = 7;
        this.message = "Failed. User's token already exists!";
    }
}