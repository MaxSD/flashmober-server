package com.wds.exception.v1;

public class CommentFromUserAlreadyExistsException extends BaseException {

    public CommentFromUserAlreadyExistsException() {
        super();
        this.code = 500;
        this.localCode = 6;
        this.message = "Failed. Comment from the user already exists!";
    }
}