package com.wds.exception.v1;

public class RequestToChangeEmailIsNotFoundException extends BaseException {

    public RequestToChangeEmailIsNotFoundException() {
        super();
        this.code = 404;
        this.localCode = 4;
        this.message = "Failed. Request to change user's email is not found!";
    }
}