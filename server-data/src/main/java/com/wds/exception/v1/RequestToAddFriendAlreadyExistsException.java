package com.wds.exception.v1;

public class RequestToAddFriendAlreadyExistsException extends BaseException {

    public RequestToAddFriendAlreadyExistsException() {
        super();
        this.code = 500;
        this.localCode = 4;
        this.message = "Failed. Request to add the user already exists!";
    }
}