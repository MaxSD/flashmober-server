package com.wds.exception.v1;

public class UserAlreadyExistsException extends BaseException {

    public UserAlreadyExistsException() {
        super();
        this.code = 500;
        this.localCode = 3;
        this.message = "Failed. User already exists!";
    }
}