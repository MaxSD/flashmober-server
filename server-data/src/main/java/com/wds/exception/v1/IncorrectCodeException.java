package com.wds.exception.v1;

public class IncorrectCodeException extends BaseException {

    public IncorrectCodeException() {
        super();
        this.code = 400;
        this.localCode = 15;
        this.message = "Failed. Incorrect code!";
    }

}
