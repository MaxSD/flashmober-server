package com.wds.exception.v1;

public class WrongDateFormatException extends BaseException {

    public WrongDateFormatException() {
        super();
        this.code = 400;
        this.message = "Failed. Wrong date format!";
    }

}
