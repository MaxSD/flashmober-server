package com.wds.exception.v1;

public class IncorrectPasswordException extends BaseException {

    public IncorrectPasswordException() {
        super();
        this.code = 400;
        this.localCode = 13;
        this.message = "Failed. Incorrect user's password";
    }
}