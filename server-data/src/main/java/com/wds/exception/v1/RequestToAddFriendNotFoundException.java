package com.wds.exception.v1;

public class RequestToAddFriendNotFoundException extends BaseException {

    public RequestToAddFriendNotFoundException() {
        super();
        this.code = 400;
        this.localCode = 2;
        this.message = "Failed. Request to add the user not found!";
    }
}