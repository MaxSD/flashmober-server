package com.wds.exception.v1;

public class LikeFromUserAlreadyExistsException extends BaseException {

    public LikeFromUserAlreadyExistsException() {
        super();
        this.code = 500;
        this.localCode = 5;
        this.message = "Failed. Like from the user already exists!";
    }
}