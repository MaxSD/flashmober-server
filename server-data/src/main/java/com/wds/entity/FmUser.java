package com.wds.entity;

import javax.persistence.*;
import java.io.Serializable;

@SuppressWarnings("UnusedDeclaration")
@Entity
@Table(name = "fm_users")
public class FmUser implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "name")
    private String name;

    @Column(name = "photo")
    private String photo;

    @Column(name = "vk_id")
    private String vkId;

    @Column(name = "fb_id")
    private String fbId;

    @Column(name = "tw_id")
    private String twId;

    @Column(name = "gender")
    private int gender;

    @Column(name = "age")
    private int age;

    @Column(name = "about")
    private String about;

    @Column(name = "country")
    private String country;

    @Column(name = "city")
    private String city;

    @Column(name = "site")
    private String site;

    @Column(name = "is_business_user")
    private boolean isIsBusinessUser;

    @Column(name = "is_locked")
    private boolean isIsLocked;

    @Column(name = "lock_comment")
    private String lockComment;

    @Column(name = "lock_expire_date")
    private Long lockExpireDate;

    @ManyToOne
    @JoinColumn(name = "id_role")
    private Role role;

    @Column(name = "flashmob_users_count")
    private int flashmobUsersCount;

    @Column(name = "is_flashmobs_deleted_by_moderator")
    private boolean isIsFlashmobsDeletedByModerator;

    @Column(name = "flashmobs_deleted_reason")
    private String flashmobsDeletedReason;

    @Column(name = "is_reports_deleted_by_moderator")
    private boolean isIsReportsDeletedByModerator;

    @Column(name = "reports_deleted_reason")
    private String reportsDeletedReason;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getVkId() {
        return vkId;
    }

    public void setVkId(String vkId) {
        this.vkId = vkId;
    }

    public String getFbId() {
        return fbId;
    }

    public void setFbId(String fbId) {
        this.fbId = fbId;
    }

    public String getTwId() {
        return twId;
    }

    public void setTwId(String twId) {
        this.twId = twId;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public boolean isIsBusinessUser() {
        return isIsBusinessUser;
    }

    public void setIsBusinessUser(boolean isIsBusinessUser) {
        this.isIsBusinessUser = isIsBusinessUser;
    }

    public boolean isIsLocked() {
        return isIsLocked;
    }

    public void setIsLocked(boolean isIsLocked) {
        this.isIsLocked = isIsLocked;
    }

    public String getLockComment() {
        return lockComment;
    }

    public void setLockComment(String lockComment) {
        this.lockComment = lockComment;
    }

    public Long getLockExpireDate() {
        return lockExpireDate;
    }

    public void setLockExpireDate(Long lockExpireDate) {
        this.lockExpireDate = lockExpireDate;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public int getFlashmobUsersCount() {
        return flashmobUsersCount;
    }

    public void setFlashmobUsersCount(int flashmobUsersCount) {
        this.flashmobUsersCount = flashmobUsersCount;
    }

    public void setIsIsBusinessUser(boolean isIsBusinessUser) {
        this.isIsBusinessUser = isIsBusinessUser;
    }

    public void setIsIsLocked(boolean isIsLocked) {
        this.isIsLocked = isIsLocked;
    }

    public boolean isIsFlashmobsDeletedByModerator() {
        return isIsFlashmobsDeletedByModerator;
    }

    public void setIsIsFlashmobsDeletedByModerator(boolean isIsFlashmobsDeletedByModerator) {
        this.isIsFlashmobsDeletedByModerator = isIsFlashmobsDeletedByModerator;
    }

    public boolean isIsReportsDeletedByModerator() {
        return isIsReportsDeletedByModerator;
    }

    public void setIsIsReportsDeletedByModerator(boolean isIsReportsDeletedByModerator) {
        this.isIsReportsDeletedByModerator = isIsReportsDeletedByModerator;
    }

    public String getFlashmobsDeletedReason() {
        return flashmobsDeletedReason;
    }

    public void setFlashmobsDeletedReason(String flashmobsDeletedReason) {
        this.flashmobsDeletedReason = flashmobsDeletedReason;
    }

    public String getReportsDeletedReason() {
        return reportsDeletedReason;
    }

    public void setReportsDeletedReason(String reportsDeletedReason) {
        this.reportsDeletedReason = reportsDeletedReason;
    }
}
