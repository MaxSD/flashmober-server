package com.wds.entity;

import javax.persistence.*;
import java.io.Serializable;

@SuppressWarnings("UnusedDeclaration")
@Entity
@Table(name = "fm_actions")
public class FmActions implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "action_for_user_id")
    private Long actionForUserId;

    @Column(name = "action_from_user_id")
    private Long actionFromUserId;

    @Column(name = "user_flashmob_id")
    private Long userFlashmobId;

    @Column(name = "type")
    private int type;

    @Column(name = "date")
    private Long date;

    @Column(name = "comment")
    private String comment;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getActionForUserId() {
        return actionForUserId;
    }

    public void setActionForUserId(Long actionForUserId) {
        this.actionForUserId = actionForUserId;
    }

    public Long getActionFromUserId() {
        return actionFromUserId;
    }

    public void setActionFromUserId(Long actionFromUserId) {
        this.actionFromUserId = actionFromUserId;
    }

    public Long getUserFlashmobId() {
        return userFlashmobId;
    }

    public void setUserFlashmobId(Long userFlashmobId) {
        this.userFlashmobId = userFlashmobId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
