package com.wds.entity;

import javax.persistence.*;
import java.io.Serializable;

@SuppressWarnings("UnusedDeclaration")
@Entity
@Table(name = "fm_flashmobs")
public class FmFlashmobs implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "owner_id")
    private Long owner_id;

    @Column(name = "photo")
    private String photo;

    @Column(name = "video")
    private String video;

    @Column(name = "description")
    private String description;

    @Column(name = "date_create")
    private Long date_create;

    @Column(name = "date_publication")
    private Long date_publication;

    @Column(name = "moderation")
    private boolean isModeration;

    @Column(name = "is_accepted_by_admin")
    private boolean isIsAcceptedBbyAdmin;

    @Column(name = "comment")
    private String comment;

    @Column(name = "is_default")
    private boolean isIsDefault;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(Long owner_id) {
        this.owner_id = owner_id;
    }

    public String getPhoto() {
        return photo;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getDate_create() {
        return date_create;
    }

    public void setDate_create(Long date_create) {
        this.date_create = date_create;
    }

    public Long getDate_publication() {
        return date_publication;
    }

    public void setDate_publication(Long date_publication) {
        this.date_publication = date_publication;
    }

    public boolean isModeration() {
        return isModeration;
    }

    public void setModeration(boolean isModeration) {
        this.isModeration = isModeration;
    }

    public boolean isIsAcceptedBbyAdmin() {
        return isIsAcceptedBbyAdmin;
    }

    public void setIsAcceptedBbyAdmin(boolean isIsAcceptedBbyAdmin) {
        this.isIsAcceptedBbyAdmin = isIsAcceptedBbyAdmin;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isIsDefault() {
        return isIsDefault;
    }

    public void setIsDefault(boolean isIsDefault) {
        this.isIsDefault = isIsDefault;
    }
}
