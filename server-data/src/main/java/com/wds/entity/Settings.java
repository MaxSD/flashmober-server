package com.wds.entity;

import javax.persistence.*;

@SuppressWarnings("UnusedDeclaration")
@Entity
@Table(name = "srv_settings")
public class Settings implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "param_name", nullable = false)
    private String paramName = "";

    @Column(name = "param_value", nullable = false)
    private String paramValue = "";

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getParamName() {
        return paramName;
    }
    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getParamValue() {
        return paramValue;
    }
    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }
}
