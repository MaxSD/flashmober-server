package com.wds.entity;

import javax.persistence.*;
import java.io.Serializable;

@SuppressWarnings("UnusedDeclaration")
@Entity
@Table(name = "fm_settings")
public class FmSettings implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "user_id")
    private long userId;

    @Column(name = "is_get_flashmobs")
    private boolean isIsGetFlashmobs;

    @Column(name = "is_get_flashmobs_from_friends_only")
    private boolean isIsGetFlashmobsFromFriendsOnly;

    @Column(name = "is_close_profile")
    private boolean isIsCloseProfile;

    @Column(name = "is_default_task")
    private boolean isIsDefaultTask;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public boolean isIsGetFlashmobs() {
        return isIsGetFlashmobs;
    }

    public void setIsGetFlashmobs(boolean isIsGetFlashmobs) {
        this.isIsGetFlashmobs = isIsGetFlashmobs;
    }

    public boolean isIsGetFlashmobsFromFriendsOnly() {
        return isIsGetFlashmobsFromFriendsOnly;
    }

    public void setIsGetFlashmobsFromFriendsOnly(boolean isIsGetFlashmobsFromFriendsOnly) {
        this.isIsGetFlashmobsFromFriendsOnly = isIsGetFlashmobsFromFriendsOnly;
    }

    public boolean isIsCloseProfile() {
        return isIsCloseProfile;
    }

    public void setIsCloseProfile(boolean isIsCloseProfile) {
        this.isIsCloseProfile = isIsCloseProfile;
    }

    public boolean isIsDefaultTask() {
        return isIsDefaultTask;
    }

    public void setIsDefaultTask(boolean isIsDefaultTask) {
        this.isIsDefaultTask = isIsDefaultTask;
    }
}
