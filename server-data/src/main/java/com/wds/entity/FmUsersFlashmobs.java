package com.wds.entity;

import javax.persistence.*;
import java.io.Serializable;

@SuppressWarnings("UnusedDeclaration")
@Entity
@Table(name = "fm_users_flashmobs")
public class FmUsersFlashmobs implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "flashmob_id")
    private Long flashmob_Id;

    @Column(name = "status")
    private int status;

    @Column(name = "date_assign")
    private Long date_assign;

    @Column(name = "date_start")
    private Long date_start;

    @Column(name = "date_end")
    private Long date_end;

    @Column(name = "photo_report")
    private String photo_report;

    @Column(name = "video_report")
    private String video_report;

    @Column(name = "friendship_with_uid")
    private Long friendshipWithUid;

    @Column(name = "is_default_report")
    private boolean isIsDefaultReport;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getFlashmob_Id() {
        return flashmob_Id;
    }

    public void setFlashmob_Id(Long flashmob_Id) {
        this.flashmob_Id = flashmob_Id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Long getDate_assign() {
        return date_assign;
    }

    public void setDate_assign(Long date_assign) {
        this.date_assign = date_assign;
    }

    public Long getDate_start() {
        return date_start;
    }

    public void setDate_start(Long date_start) {
        this.date_start = date_start;
    }

    public Long getDate_end() {
        return date_end;
    }

    public void setDate_end(Long date_end) {
        this.date_end = date_end;
    }

    public String getPhoto_report() {
        return photo_report;
    }

    public void setPhoto_report(String photo_report) {
        this.photo_report = photo_report;
    }

    public String getVideo_report() {
        return video_report;
    }

    public void setVideo_report(String video_report) {
        this.video_report = video_report;
    }

    public Long getFriendshipWithUid() {
        return friendshipWithUid;
    }

    public void setFriendshipWithUid(Long friendshipWithUid) {
        this.friendshipWithUid = friendshipWithUid;
    }

    public boolean isIsDefaultReport() {
        return isIsDefaultReport;
    }

    public void setIsIsDefaultReport(boolean isIsDefaultReport) {
        this.isIsDefaultReport = isIsDefaultReport;
    }
}
