package com.wds.entity;

import com.wds.database.HstoreUserType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

@SuppressWarnings("UnusedDeclaration")
@Entity
@TypeDef(name = "hstore", typeClass = HstoreUserType.class)
@Table(name = "srv_permissions")
public class Permission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column
    private String name;

    @Type(type = "hstore")
    @Column(columnDefinition = "hstore")
    private Map<String, String> text;

    @Type(type = "hstore")
    @Column(columnDefinition = "hstore")
    private Map<String, String> description;

    @ManyToMany(cascade = {CascadeType.REFRESH, CascadeType.DETACH, CascadeType.MERGE},
            fetch = FetchType.EAGER, mappedBy = "permissionsList")
    private List<Role> roleList;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Map<String, String> getText() { return text; }
    public void setText(Map<String, String> text) {
        this.text = text;
    }

    public Map<String, String> getDescription() {
        return description;
    }
    public void setDescription(Map<String, String> description) {
        this.description = description;
    }

    public List<Role> getRoleList() { return roleList; }
    public void setRoleList(List<Role> roleList) { this.roleList = roleList; }
}
