package com.wds.dataObjects;

public class FlashmobsUsersAssignedOn {
    private Long id;
    private Long flashmob_id;
    private Long user_flashmob_id;
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFlashmob_id() {
        return flashmob_id;
    }

    public void setFlashmob_id(Long flashmob_id) {
        this.flashmob_id = flashmob_id;
    }

    public Long getUser_flashmob_id() {
        return user_flashmob_id;
    }

    public void setUser_flashmob_id(Long user_flashmob_id) {
        this.user_flashmob_id = user_flashmob_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
