package com.wds.dao;

import com.wds.database.HibernateUtil;
import com.wds.database.Defines;
import com.wds.entity.FmSettings;
import com.wds.entity.FmUser;
import com.wds.entity.FmUsersCategories;
import com.wds.entity.FmUsersLanguages;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.CrudDatabaseException;
import com.wds.exception.v1.SelectDatabaseException;
import com.wds.exception.v1.UserAlreadyExistsException;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.*;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;

@Service("FmUsersDao")
public class FmUsersDao {

    public FmUser findById(long id) throws SelectDatabaseException {
        FmUser user;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUser.class).
                    add(Restrictions.eq("id", id));
            user = (FmUser) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return user;
    }

    public FmUser findByEmailAndPassword(String email, String password) throws SelectDatabaseException {
        FmUser user;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUser.class).
                    add(Restrictions.eq("email", email)).add(Restrictions.eq("password", password));
            user = (FmUser) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return user;
    }

    public FmUser findByEmail(String email) throws SelectDatabaseException {
        FmUser user;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUser.class).
                    add(Restrictions.or(Restrictions.eq("email", email).ignoreCase()));
            user = (FmUser) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return user;
    }

    public FmUser findByName(String name) throws SelectDatabaseException {
        FmUser user;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUser.class).
                    add(Restrictions.or(Restrictions.eq("name", name).ignoreCase()));
            user = (FmUser) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return user;
    }

    public FmUser findByVkId(String vkId) throws SelectDatabaseException {
        FmUser user;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUser.class).
                    add(Restrictions.eq("vkId", vkId));
            user = (FmUser) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return user;
    }

    public FmUser findByFbId(String fbId) throws SelectDatabaseException {
        FmUser user;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUser.class).
                    add(Restrictions.eq("fbId", fbId));
            user = (FmUser) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return user;
    }

    public FmUser findByTwId(String twId) throws SelectDatabaseException {
        FmUser user;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUser.class).
                    add(Restrictions.eq("twId", twId));
            user = (FmUser) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return user;
    }

    @SuppressWarnings("unchecked")
    public List<Object> searchByNickname(String nickName, Object entityObject) throws SelectDatabaseException {
        List<Object> resultList;
        Transaction transaction = null;
        try {

            StringBuilder sb = new StringBuilder("SELECT u.id AS uid, u.name, u.photo, u.about " +
                    "FROM fm_users u " +
                    "WHERE LOWER(name) LIKE ('%" + StringUtils.trim(nickName).toLowerCase() + "%') ");

            Session session = HibernateUtil.getSession();
            transaction = session.beginTransaction();

            resultList = session.createSQLQuery(sb.toString()).
                    addScalar("uid", new LongType()).
                    addScalar("name").
                    addScalar("photo").
                    addScalar("about").
                    setResultTransformer(Transformers.aliasToBean(entityObject.getClass())).list();

            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return resultList;
    }

    @SuppressWarnings("unchecked")
    public List<FmUser> getAll() throws SelectDatabaseException {
        Transaction transaction = null;
        List<FmUser> userList;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUser.class).
                    addOrder(Order.asc("id"));
            userList = criteria.list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return userList;
    }

    @SuppressWarnings("unchecked")
    public List<String> getUsersCountries() throws SelectDatabaseException {
        Transaction transaction = null;
        List<String> userCountries;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUser.class).
                    addOrder(Order.asc("country")).
                    setProjection(Projections.property("country")).
                    setProjection(Projections.distinct(Projections.property("country")));
            userCountries = criteria.list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return userCountries;
    }

    @SuppressWarnings("unchecked")
    public List<String> getUsersCities() throws SelectDatabaseException {
        Transaction transaction = null;
        List<String> userCities;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUser.class).
                    addOrder(Order.asc("city")).
                    setProjection(Projections.property("city")).
                    setProjection(Projections.distinct(Projections.property("city")));
            userCities = criteria.list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return userCities;
    }

    public List<FmUser> getAll(int offset, int limit, String search,
                               String nameFilter, String emailFilter, String countryFilter,
                               String cityFilter, String genderFilter,
                               String ageFromFilter, String ageToFilter) throws SelectDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();

            StringBuilder sbSql = new StringBuilder("SELECT u.* FROM ");
            sbSql.append("(SELECT DISTINCT * FROM fm_users ");

            if (!StringUtils.isBlank(nameFilter) || !StringUtils.isBlank(emailFilter) ||
                    !StringUtils.isBlank(countryFilter) || !StringUtils.isBlank(cityFilter) ||
                    !StringUtils.isBlank(genderFilter) || !StringUtils.isBlank(ageFromFilter) ||
                    !StringUtils.isBlank(ageToFilter)) {
                sbSql.append("WHERE 1=1 ");

                if (!StringUtils.isBlank(nameFilter)) {
                    sbSql.append(" AND LOWER(name) LIKE ('%" + StringUtils.trim(nameFilter).toLowerCase() + "%') ");
                }

                if (!StringUtils.isBlank(emailFilter)) {
                    sbSql.append(" AND LOWER(email) LIKE ('%" + StringUtils.trim(emailFilter).toLowerCase() + "%') ");
                }

                if (!StringUtils.isBlank(countryFilter)) {
                    sbSql.append(" AND country = '" + StringUtils.trim(countryFilter) + "' ");
                }

                if (!StringUtils.isBlank(cityFilter)) {
                    sbSql.append(" AND city = '" + StringUtils.trim(cityFilter) + "' ");
                }

                if (!StringUtils.isBlank(genderFilter)) {
                    sbSql.append(" AND gender = " + StringUtils.trim(genderFilter));
                }

                if (!StringUtils.isBlank(ageFromFilter)) {
                    sbSql.append(" AND age >= " + StringUtils.trim(ageFromFilter));
                }

                if (!StringUtils.isBlank(ageToFilter)) {
                    sbSql.append(" AND age <= " + StringUtils.trim(ageToFilter));
                }
            }

            sbSql.append(" ORDER BY id ASC) AS u ");
            sbSql.append(" OFFSET " + offset);

            if (limit > 0) {
                sbSql.append(" LIMIT " + limit);
            }

            @SuppressWarnings("unchecked")
            List<FmUser> userList = (List<FmUser>) session.createSQLQuery(sbSql.toString()).addEntity(FmUser.class).list();
            transaction.commit();

            return userList;
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
    }

    public List<FmUser> getAll(int offset, int limit, String nameFilter, String emailFilter) throws SelectDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();

            StringBuilder sbSql = new StringBuilder("SELECT u.* FROM ");
            sbSql.append("(SELECT DISTINCT * FROM fm_users ");

            if (!StringUtils.isBlank(nameFilter) || !StringUtils.isBlank(emailFilter)) {
                sbSql.append("WHERE 1=1 ");

                if (!StringUtils.isBlank(nameFilter)) {
                    sbSql.append(" AND LOWER(name) LIKE ('%" + StringUtils.trim(nameFilter).toLowerCase() + "%') ");
                }

                if (!StringUtils.isBlank(emailFilter)) {
                    sbSql.append(" AND LOWER(email) LIKE ('%" + StringUtils.trim(emailFilter).toLowerCase() + "%') ");
                }
            }

            sbSql.append(" ORDER BY id ASC) AS u ");
            sbSql.append(" OFFSET " + offset);

            if (limit > 0) {
                sbSql.append(" LIMIT " + limit);
            }

            @SuppressWarnings("unchecked")
            List<FmUser> userList = (List<FmUser>) session.createSQLQuery(sbSql.toString()).addEntity(FmUser.class).list();
            transaction.commit();

            return userList;
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
    }

    public List<FmUser> getBusinessUsers(int offset, int limit, String search,
                                         String nameFilter, String emailFilter, String countryFilter,
                                         String cityFilter, String genderFilter,
                                         String ageFromFilter, String ageToFilter) throws SelectDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();

            StringBuilder sbSql = new StringBuilder("SELECT u.* FROM ");
            sbSql.append("(SELECT DISTINCT * FROM fm_users ");
            sbSql.append("WHERE is_business_user = TRUE ");

            if (!StringUtils.isBlank(nameFilter) || !StringUtils.isBlank(emailFilter) ||
                    !StringUtils.isBlank(countryFilter) || !StringUtils.isBlank(cityFilter) ||
                    !StringUtils.isBlank(genderFilter) || !StringUtils.isBlank(ageFromFilter) ||
                    !StringUtils.isBlank(ageToFilter)) {

                if (!StringUtils.isBlank(nameFilter)) {
                    sbSql.append(" AND LOWER(name) LIKE ('%" + StringUtils.trim(nameFilter).toLowerCase() + "%') ");
                }

                if (!StringUtils.isBlank(emailFilter)) {
                    sbSql.append(" AND LOWER(email) LIKE ('%" + StringUtils.trim(emailFilter).toLowerCase() + "%') ");
                }

                if (!StringUtils.isBlank(countryFilter)) {
                    sbSql.append(" AND country = '" + StringUtils.trim(countryFilter) + "' ");
                }

                if (!StringUtils.isBlank(cityFilter)) {
                    sbSql.append(" AND city = '" + StringUtils.trim(cityFilter) + "' ");
                }

                if (!StringUtils.isBlank(genderFilter)) {
                    sbSql.append(" AND gender = " + StringUtils.trim(genderFilter));
                }

                if (!StringUtils.isBlank(ageFromFilter)) {
                    sbSql.append(" AND age >= " + StringUtils.trim(ageFromFilter));
                }

                if (!StringUtils.isBlank(ageToFilter)) {
                    sbSql.append(" AND age <= " + StringUtils.trim(ageToFilter));
                }
            }

            sbSql.append(" ORDER BY id ASC) AS u ");
            sbSql.append(" OFFSET " + offset);

            if (limit > 0) {
                sbSql.append(" LIMIT " + limit);
            }

            @SuppressWarnings("unchecked")
            List<FmUser> userList = (List<FmUser>) session.createSQLQuery(sbSql.toString()).addEntity(FmUser.class).list();
            transaction.commit();

            return userList;
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
    }

    public List<FmUser> getModerators(int offset, int limit, String search,
                                      String nameFilter, String emailFilter, String countryFilter,
                                      String cityFilter, String genderFilter,
                                      String ageFromFilter, String ageToFilter) throws SelectDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();

            StringBuilder sbSql = new StringBuilder("SELECT u.* FROM ");
            sbSql.append("(SELECT DISTINCT * FROM fm_users ");
            sbSql.append("WHERE id_role = " + Defines.USER_TYPE_MODERATOR);

            if (!StringUtils.isBlank(nameFilter) || !StringUtils.isBlank(emailFilter) ||
                    !StringUtils.isBlank(countryFilter) || !StringUtils.isBlank(cityFilter) ||
                    !StringUtils.isBlank(genderFilter) || !StringUtils.isBlank(ageFromFilter) ||
                    !StringUtils.isBlank(ageToFilter)) {

                if (!StringUtils.isBlank(nameFilter)) {
                    sbSql.append(" AND LOWER(name) LIKE ('%" + StringUtils.trim(nameFilter).toLowerCase() + "%') ");
                }

                if (!StringUtils.isBlank(emailFilter)) {
                    sbSql.append(" AND LOWER(email) LIKE ('%" + StringUtils.trim(emailFilter).toLowerCase() + "%') ");
                }

                if (!StringUtils.isBlank(countryFilter)) {
                    sbSql.append(" AND country = '" + StringUtils.trim(countryFilter) + "' ");
                }

                if (!StringUtils.isBlank(cityFilter)) {
                    sbSql.append(" AND city = '" + StringUtils.trim(cityFilter) + "' ");
                }

                if (!StringUtils.isBlank(genderFilter)) {
                    sbSql.append(" AND gender = " + StringUtils.trim(genderFilter));
                }

                if (!StringUtils.isBlank(ageFromFilter)) {
                    sbSql.append(" AND age >= " + StringUtils.trim(ageFromFilter));
                }

                if (!StringUtils.isBlank(ageToFilter)) {
                    sbSql.append(" AND age <= " + StringUtils.trim(ageToFilter));
                }
            }

            sbSql.append(" ORDER BY id ASC) AS u ");
            sbSql.append(" OFFSET " + offset);

            if (limit > 0) {
                sbSql.append(" LIMIT " + limit);
            }

            @SuppressWarnings("unchecked")
            List<FmUser> userList = (List<FmUser>) session.createSQLQuery(sbSql.toString()).addEntity(FmUser.class).list();
            transaction.commit();

            return userList;
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
    }

    public long getUsersCount() throws SelectDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();

            Criteria criteria = session.createCriteria(FmUser.class);
            criteria.setProjection(Projections.rowCount());

            @SuppressWarnings("unchecked")
            long usersCount = (Long) criteria.uniqueResult();
            transaction.commit();

            return usersCount;
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
    }

    public BigInteger getUsersCount(String nameFilter, String emailFilter, String countryFilter,
                              String cityFilter, String genderFilter,
                              String ageFromFilter, String ageToFilter) throws SelectDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();

            StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM fm_users WHERE 1=1 ");

            if (!StringUtils.isBlank(nameFilter)) {
                sbSql.append(" AND LOWER(name) LIKE ('%" + StringUtils.trim(nameFilter).toLowerCase() + "%') ");
            }

            if (!StringUtils.isBlank(emailFilter)) {
                sbSql.append(" AND LOWER(email) LIKE ('%" + StringUtils.trim(emailFilter).toLowerCase() + "%') ");
            }

            if (!StringUtils.isBlank(countryFilter)) {
                sbSql.append(" AND country = '" + StringUtils.trim(countryFilter) + "' ");
            }

            if (!StringUtils.isBlank(cityFilter)) {
                sbSql.append(" AND city = '" + StringUtils.trim(cityFilter) + "' ");
            }

            if (!StringUtils.isBlank(genderFilter)) {
                sbSql.append(" AND gender = " + StringUtils.trim(genderFilter));
            }

            if (!StringUtils.isBlank(ageFromFilter)) {
                sbSql.append(" AND age >= " + StringUtils.trim(ageFromFilter));
            }

            if (!StringUtils.isBlank(ageToFilter)) {
                sbSql.append(" AND age <= " + StringUtils.trim(ageToFilter));
            }

            @SuppressWarnings("unchecked")
            BigInteger usersCount = (BigInteger) session.createSQLQuery(sbSql.toString()).uniqueResult();
            transaction.commit();

            return usersCount;
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
    }

    public BigInteger getUsersCount(String nameFilter, String emailFilter) throws SelectDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();

            StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM fm_users WHERE 1=1 ");

            if (!StringUtils.isBlank(nameFilter)) {
                sbSql.append(" AND LOWER(name) LIKE ('%" + StringUtils.trim(nameFilter).toLowerCase() + "%') ");
            }

            if (!StringUtils.isBlank(emailFilter)) {
                sbSql.append(" AND LOWER(email) LIKE ('%" + StringUtils.trim(emailFilter).toLowerCase() + "%') ");
            }

            @SuppressWarnings("unchecked")
            BigInteger usersCount = (BigInteger) session.createSQLQuery(sbSql.toString()).uniqueResult();
            transaction.commit();

            return usersCount;
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
    }

    public long getBusinessUsersCount() throws SelectDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();

            Criteria criteria = session.createCriteria(FmUser.class);
            criteria.add(Restrictions.eq("isIsBusinessUser", true));
            criteria.setProjection(Projections.rowCount());

            @SuppressWarnings("unchecked")
            long usersCount = (Long) criteria.uniqueResult();
            transaction.commit();

            return usersCount;
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
    }

    public BigInteger getBusinessUsersCount(String nameFilter, String emailFilter, String countryFilter,
                                            String cityFilter, String genderFilter,
                                            String ageFromFilter, String ageToFilter) throws SelectDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();

            StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM fm_users WHERE is_business_user = TRUE ");

            if (!StringUtils.isBlank(nameFilter)) {
                sbSql.append(" AND LOWER(name) LIKE ('%" + StringUtils.trim(nameFilter).toLowerCase() + "%') ");
            }

            if (!StringUtils.isBlank(emailFilter)) {
                sbSql.append(" AND LOWER(email) LIKE ('%" + StringUtils.trim(emailFilter).toLowerCase() + "%') ");
            }

            if (!StringUtils.isBlank(countryFilter)) {
                sbSql.append(" AND country = '" + StringUtils.trim(countryFilter) + "' ");
            }

            if (!StringUtils.isBlank(cityFilter)) {
                sbSql.append(" AND city = '" + StringUtils.trim(cityFilter) + "' ");
            }

            if (!StringUtils.isBlank(genderFilter)) {
                sbSql.append(" AND gender = " + StringUtils.trim(genderFilter));
            }

            if (!StringUtils.isBlank(ageFromFilter)) {
                sbSql.append(" AND age >= " + StringUtils.trim(ageFromFilter));
            }

            if (!StringUtils.isBlank(ageToFilter)) {
                sbSql.append(" AND age <= " + StringUtils.trim(ageToFilter));
            }


            @SuppressWarnings("unchecked")
            BigInteger usersCount = (BigInteger) session.createSQLQuery(sbSql.toString()).uniqueResult();
            transaction.commit();

            return usersCount;
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
    }

    public long getModeratorsCount() throws SelectDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();

            Criteria criteria = session.createCriteria(FmUser.class);
            criteria.add(Restrictions.eq("role.id", Defines.USER_TYPE_MODERATOR));
            criteria.setProjection(Projections.rowCount());

            @SuppressWarnings("unchecked")
            long usersCount = (Long) criteria.uniqueResult();
            transaction.commit();

            return usersCount;
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
    }

    public BigInteger getModeratorsCount(String nameFilter, String emailFilter, String countryFilter,
                                         String cityFilter, String genderFilter,
                                         String ageFromFilter, String ageToFilter) throws SelectDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();

            StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM fm_users WHERE id_role = " + Defines.USER_TYPE_MODERATOR);

            if (!StringUtils.isBlank(nameFilter)) {
                sbSql.append(" AND LOWER(name) LIKE ('%" + StringUtils.trim(nameFilter).toLowerCase() + "%') ");
            }

            if (!StringUtils.isBlank(emailFilter)) {
                sbSql.append(" AND LOWER(email) LIKE ('%" + StringUtils.trim(emailFilter).toLowerCase() + "%') ");
            }

            if (!StringUtils.isBlank(countryFilter)) {
                sbSql.append(" AND country = '" + StringUtils.trim(countryFilter) + "' ");
            }

            if (!StringUtils.isBlank(cityFilter)) {
                sbSql.append(" AND city = '" + StringUtils.trim(cityFilter) + "' ");
            }

            if (!StringUtils.isBlank(genderFilter)) {
                sbSql.append(" AND gender = " + StringUtils.trim(genderFilter));
            }

            if (!StringUtils.isBlank(ageFromFilter)) {
                sbSql.append(" AND age >= " + StringUtils.trim(ageFromFilter));
            }

            if (!StringUtils.isBlank(ageToFilter)) {
                sbSql.append(" AND age <= " + StringUtils.trim(ageToFilter));
            }


            @SuppressWarnings("unchecked")
            BigInteger usersCount = (BigInteger) session.createSQLQuery(sbSql.toString()).uniqueResult();
            transaction.commit();

            return usersCount;
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
    }

    public void create(FmUser record) throws BaseException {
        Transaction transaction = null;
        try {
            Session session = HibernateUtil.getSession();
            transaction = session.beginTransaction();

            session.persist(record);

            FmSettingsDao settingsDao = new FmSettingsDao();
            FmSettings settings = settingsDao.findByUserId(record.getId(), session);

            if (settings == null) {
                settings = new FmSettings();
                settings.setUserId(record.getId());
                settings.setIsGetFlashmobs(true);
                settings.setIsGetFlashmobsFromFriendsOnly(true);
                settings.setIsCloseProfile(false);
                settings.setIsDefaultTask(false);
                settingsDao.create(settings, session);
            }

            // Create all categories for a new user
            FmUsersCategoriesDao usersCategoriesDao = new FmUsersCategoriesDao();
            FmUsersCategories usersCategories;
            for (long catId = 1; catId <= 14; catId ++ ) {
                usersCategories = new FmUsersCategories();
                usersCategories.setUserId(record.getId());
                usersCategories.setCategoryId(catId);
                usersCategoriesDao.create(usersCategories, session);
            }

            transaction.commit();
        } catch (ConstraintViolationException ce) {
            if (StringUtils.equals(ce.getConstraintName(), "fm_users_email_key"))
                throw new UserAlreadyExistsException();
            if (StringUtils.equals(ce.getConstraintName(), "unique_name"))
                throw new UserAlreadyExistsException();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void updateSettings(FmUser record, List<Long> categories_ids, List<String> languages) throws BaseException {
        Transaction transaction = null;
        try {
            Session session = HibernateUtil.getSession();
            transaction = session.beginTransaction();

            // Delete user's categories
            String strSql = "DELETE FROM fm_users_categories WHERE user_id = " + record.getId();
            session.createSQLQuery(strSql).executeUpdate();

            // Create user's categories
            if (categories_ids != null) {
                FmUsersCategoriesDao usersCategoriesDao = new FmUsersCategoriesDao();
                FmUsersCategories usersCategories;
                for (Long category_id : categories_ids) {
                    usersCategories = new FmUsersCategories();
                    usersCategories.setUserId(record.getId());
                    usersCategories.setCategoryId(category_id);
                    usersCategoriesDao.create(usersCategories, session);
                }
            }

            // Delete user's languages
            strSql = "DELETE FROM fm_users_languages WHERE user_id = " + record.getId();
            session.createSQLQuery(strSql).executeUpdate();

            // Create user's languages
            if (languages != null) {
                FmUsersLanguagesDao usersLanguagesDao = new FmUsersLanguagesDao();
                FmUsersLanguages usersLanguages;
                for (String language : languages) {
                    usersLanguages = new FmUsersLanguages();
                    usersLanguages.setUserId(record.getId());
                    usersLanguages.setLanguage(language);
                    usersLanguagesDao.create(usersLanguages, session);
                }
            }

            session.saveOrUpdate(record);
            transaction.commit();
        } catch (ConstraintViolationException ce) {
            HibernateUtil.rollbackTransaction(transaction);
            if (StringUtils.equals(ce.getConstraintName(), "fm_users_email_key"))
                throw new UserAlreadyExistsException();
            if (StringUtils.equals(ce.getConstraintName(), "unique_name"))
                throw new UserAlreadyExistsException();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    @SuppressWarnings("unchecked")
    public List<Long> getUsersIdsForAbyss(List<Long> usersIds, List<Long> catIds) throws SelectDatabaseException {
        return getUsersIdsForAbyss(usersIds, catIds, null);
    }

    @SuppressWarnings("unchecked")
    public List<Long> getUsersIdsForAbyss(List<Long> usersIds, List<Long> catIds, Session session) throws SelectDatabaseException {
        Transaction transaction = null;
        List<Long> resultList;
        try {
            if (session != null) {
                Criteria criteria = session.createCriteria(FmUsersCategories.class).
                        add(Restrictions.and(
                                Restrictions.in("categoryId", catIds),
                                Restrictions.in("userId", usersIds))).
                        setProjection(Projections.property("userId"));
                resultList = criteria.list();
            } else {
                transaction = HibernateUtil.beginTransaction();
                Criteria criteria = HibernateUtil.getSession().createCriteria(FmUsersCategories.class).
                        add(Restrictions.and(
                                Restrictions.in("categoryId", catIds),
                                Restrictions.in("userId", usersIds))).
                        setProjection(Projections.property("userId"));
                resultList = criteria.list();
                transaction.commit();
            }
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return resultList;
    }

    public void update(FmUser record) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().saveOrUpdate(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void merge(FmUser record) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().merge(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void delete(FmUser record) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().delete(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }
}
