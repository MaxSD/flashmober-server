package com.wds.dao;

import com.wds.database.HibernateUtil;
import com.wds.entity.FmUserToken;
import com.wds.exception.v1.*;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("FmUserTokenDao")
public class FmUserTokenDao {

    public FmUserToken findById(long id) throws SelectDatabaseException {
        FmUserToken userToken;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUserToken.class).
                    add(Restrictions.eq("id", id));
            userToken = (FmUserToken) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return userToken;
    }

    @SuppressWarnings("unchecked")
    public FmUserToken findByUserId(long userId) throws SelectDatabaseException {
        Transaction transaction = null;
        FmUserToken userToken;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUserToken.class).
                    add(Restrictions.eq("userId", userId));

            userToken = (FmUserToken) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return userToken;
    }

    @SuppressWarnings("unchecked")
    public List<FmUserToken> findByUsersIds(List<Long> usersIds) throws SelectDatabaseException {
        Transaction transaction = null;
        List<FmUserToken> userTokenList;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUserToken.class).
                    add(Restrictions.eq("userId", usersIds));

            userTokenList = (List<FmUserToken>) criteria.list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return userTokenList;
    }

    @SuppressWarnings("unchecked")
    public List<String> getTokensByUsersNames(List<String> usersNames) throws SelectDatabaseException {
        Transaction transaction = null;
        List<String> tokenList;
        try {

            StringBuilder sb = new StringBuilder("SELECT ut.token FROM fm_users AS u ");
            sb.append("INNER JOIN fm_user_token AS ut ON (u.id = ut.user_id) ");
            sb.append("WHERE ");

            for (int i = 0; i < usersNames.size(); i ++) {
                if (i > 0)
                    sb.append(" OR ");

                sb.append("u.name = '" + StringUtils.trimToEmpty(usersNames.get(i)) + "'");
            }

            transaction = HibernateUtil.beginTransaction();
            tokenList = (List<String>) HibernateUtil.getSession().createSQLQuery(sb.toString()).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return tokenList;
    }

    @SuppressWarnings("unchecked")
    public FmUserToken findByToken(String token) throws SelectDatabaseException {
        Transaction transaction = null;
        FmUserToken userToken;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUserToken.class).
                    add(Restrictions.eq("token", token));

            userToken = (FmUserToken) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return userToken;
    }

    @SuppressWarnings("unchecked")
    public List<FmUserToken> getAllForApple() throws SelectDatabaseException {
        Transaction transaction = null;
        List<FmUserToken> userTokenList;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUserToken.class).
                    add(Restrictions.eq("isIsAndroid", false));

            userTokenList = (List<FmUserToken>) criteria.list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return userTokenList;
    }

    @SuppressWarnings("unchecked")
    public List<FmUserToken> getAllForAndroid() throws SelectDatabaseException {
        Transaction transaction = null;
        List<FmUserToken> userTokenList;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUserToken.class).
                    add(Restrictions.eq("isIsAndroid", true));

            userTokenList = (List<FmUserToken>) criteria.list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return userTokenList;
    }

    public void create(FmUserToken record) throws BaseException {
        create(record, null);
    }

    public void create(FmUserToken record, Transaction transaction) throws BaseException {
        try {
            if (transaction != null) {
                HibernateUtil.getSession().persist(record);
            } else {
                transaction = HibernateUtil.beginTransaction();
                HibernateUtil.getSession().persist(record);
                transaction.commit();
            }
        } catch (ConstraintViolationException ce) {
            HibernateUtil.rollbackTransaction(transaction);
            if (StringUtils.equals(ce.getConstraintName(), "UserTokenExists"))
                throw new UsersTokenAlreadyExistsException();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void update(FmUserToken record) throws BaseException {
        update(record, null);
    }

    public void update(FmUserToken record, Transaction transaction) throws BaseException {
        try {
            if (transaction != null) {
                HibernateUtil.getSession().saveOrUpdate(record);
            } else {
                transaction = HibernateUtil.beginTransaction();
                HibernateUtil.getSession().saveOrUpdate(record);
                transaction.commit();
            }
        } catch (ConstraintViolationException ce) {
            HibernateUtil.rollbackTransaction(transaction);
            if (StringUtils.equals(ce.getConstraintName(), "UserTokenExists"))
                throw new UsersTokenAlreadyExistsException();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void delete(FmUserToken record) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().delete(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void delete(long uid, String token) throws CrudDatabaseException {
        Transaction transaction = null;
        try {

            String strSql = "DELETE FROM fm_user_token WHERE user_id = " + uid + " AND token = '" + token + "'";

            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().createSQLQuery(strSql).executeUpdate();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }
}
