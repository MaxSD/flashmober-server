package com.wds.dao;

import com.wds.database.HibernateUtil;
import com.wds.entity.FmFlashmobCategories;
import com.wds.entity.FmFlashmobTags;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.CrudDatabaseException;
import com.wds.exception.v1.SelectDatabaseException;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("FmFlashmobsTagsDao")
public class FmFlashmobsTagsDao {

    public FmFlashmobTags findById(long id) throws SelectDatabaseException {
        FmFlashmobTags resultList;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmFlashmobTags.class).
                    add(Restrictions.eq("id", id));
            resultList = (FmFlashmobTags) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return resultList;
    }

    @SuppressWarnings("unchecked")
    public List<FmFlashmobTags> findByFlashmobId(long flashmobId) throws SelectDatabaseException {
        List<FmFlashmobTags> resultList;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmFlashmobTags.class).
                    add(Restrictions.eq("flashmobId", flashmobId));
            resultList = criteria.list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return resultList;
    }

    @SuppressWarnings("unchecked")
    public List<String> getTagsListByFlashmobId(long flashmobId) throws SelectDatabaseException {
        List<String> resultList;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmFlashmobTags.class).
                    add(Restrictions.eq("flashmobId", flashmobId)).setProjection(Projections.property("tag"));
            resultList = criteria.list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return resultList;
    }

    public void create(FmFlashmobTags record) throws BaseException {
        create(record, null);
    }

    public void create(FmFlashmobTags record, Session session) throws BaseException {
        Transaction transaction = null;
        try {
            if (session != null) {
                session.persist(record);
            } else {
                transaction = HibernateUtil.beginTransaction();
                HibernateUtil.getSession().persist(record);
                transaction.commit();
            }
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void update(FmFlashmobTags record) throws BaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().saveOrUpdate(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void delete(FmFlashmobTags record) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().delete(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void delete(long flashmobId) throws CrudDatabaseException {
        Transaction transaction = null;
        try {

            String strSql = "DELETE FROM fm_flashmob_tags WHERE flashmob_id = " + flashmobId;

            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().createSQLQuery(strSql).executeUpdate();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }
}
