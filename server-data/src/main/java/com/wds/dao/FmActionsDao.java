package com.wds.dao;

import com.wds.database.HibernateUtil;
import com.wds.entity.FmActions;
import com.wds.entity.FmLike;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.CrudDatabaseException;
import com.wds.exception.v1.SelectDatabaseException;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BigIntegerType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("FmActionsDao")
public class FmActionsDao {

    public FmActions findById(long id) throws SelectDatabaseException {
        FmActions action;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmActions.class).
                    add(Restrictions.eq("id", id));
            action = (FmActions) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return action;
    }

    @SuppressWarnings("unchecked")
    public List<Object> findActionsForUser(long uid, Object entityObject) throws SelectDatabaseException {
        List<Object> actionsList;
        Transaction transaction = null;
        try {

            String strSql = "SELECT a.user_flashmob_id, uf.flashmob_id AS id, a.action_from_user_id AS uid, u.name AS name, u.email AS email, u.photo AS user_photo, u.gender, uf.photo_report, uf.video_report, a.type, a.date, a.comment, " +
                    "(SELECT count(*) AS count FROM fm_likes WHERE fm_likes.is_unliked = FALSE AND fm_likes.user_flashmob_id = uf.id AND user_id != "+uid+") AS like_count, fr.status AS friendship_status " +
                    "FROM fm_actions AS a " +
                    "INNER JOIN fm_users AS u ON (a.action_from_user_id = u.id) " +
                    "LEFT JOIN fm_users_flashmobs AS uf ON (uf.id = a.user_flashmob_id) " +
                    "LEFT JOIN fm_friends AS fr ON ((fr.user1_id = a.action_from_user_id AND fr.user2_id = a.action_for_user_id) OR (fr.user2_id = a.action_from_user_id AND fr.user1_id = a.action_for_user_id)) " +
                    "WHERE a.action_for_user_id = "+uid+" AND a.action_from_user_id != "+uid ;

            transaction = HibernateUtil.beginTransaction();
            actionsList = HibernateUtil.getSession().createSQLQuery(strSql).
                    addScalar("id", new LongType()).
                    addScalar("uid", new LongType()).
                    addScalar("user_flashmob_id", new LongType()).
                    addScalar("name").
                    addScalar("email").
                    addScalar("user_photo").
                    addScalar("gender", new IntegerType()).
                    addScalar("like_count", new BigIntegerType()).
                    addScalar("photo_report").
                    addScalar("video_report").
                    addScalar("type", new IntegerType()).
                    addScalar("date", new LongType()).
                    addScalar("comment").
                    addScalar("friendship_status", new IntegerType()).
                    setResultTransformer(Transformers.aliasToBean(entityObject.getClass())).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return actionsList;
    }

    public void create(FmActions record) throws BaseException {
        create(record, null);
    }

    public void create(FmActions record, StatelessSession session) throws BaseException {
        Transaction transaction = null;
        try {
            if (session != null) {
                session.insert(record);
            } else {
                transaction = HibernateUtil.beginTransaction();
                HibernateUtil.getSession().persist(record);
                transaction.commit();
            }
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void update(FmActions record) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().saveOrUpdate(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void delete(FmActions record) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().delete(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }
}
