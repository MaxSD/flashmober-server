package com.wds.dao;

import com.wds.database.HibernateUtil;
import com.wds.entity.Settings;
import com.wds.exception.v1.CrudDatabaseException;
import com.wds.exception.v1.SelectDatabaseException;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

@Service("SettingsDao")
public class SettingsDao {

    public Settings findByParamName(String paramName) throws SelectDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        try {
            Settings appSetting;
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = session.createCriteria(Settings.class).
                    add(Restrictions.eq("paramName", paramName));
            appSetting = (Settings) criteria.uniqueResult();
            transaction.commit();
            return appSetting;
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
    }

    public void update(Settings record) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().saveOrUpdate(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }
}
