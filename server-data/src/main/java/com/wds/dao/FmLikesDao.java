package com.wds.dao;

import com.wds.database.HibernateUtil;
import com.wds.database.Defines;
import com.wds.entity.FmActions;
import com.wds.entity.FmLike;
import com.wds.entity.FmUsersFlashmobs;
import com.wds.exception.v1.*;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;

@Service("FmLikesDao")
public class FmLikesDao {

    public FmLike findById(long id) throws SelectDatabaseException {
        FmLike like;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmLike.class).
                    add(Restrictions.eq("id", id));
            like = (FmLike) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return like;
    }

    public FmLike find(long userId, long userFlashmobId) throws SelectDatabaseException {
        FmLike like;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmLike.class).
                    add(Restrictions.eq("userId", userId)).
                    add(Restrictions.eq("user_flashmob_id", userFlashmobId));
            like = (FmLike) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return like;
    }

    @SuppressWarnings("unchecked")
    public List<Object> getLikesList(long offset, long limit, long userFlashmobId, Object entityObject) throws SelectDatabaseException {
        Transaction transaction = null;
        List<Object> flashmobsList;
        try {

            String strSql = "SELECT u.id AS uid, u.photo AS user_photo, u.name, u.email, l.date " +
                    "FROM fm_likes AS l, fm_users AS u " +
                    "WHERE l.user_id = u.id AND l.user_flashmob_id = " + userFlashmobId + " OFFSET " + offset;

            if (limit > 0) {
                strSql += " LIMIT " + limit;
            }

            transaction = HibernateUtil.beginTransaction();
            flashmobsList = HibernateUtil.getSession().createSQLQuery(strSql).
                    addScalar("uid", new LongType()).
                    addScalar("user_photo").
                    addScalar("name").
                    addScalar("email").
                    addScalar("date", new LongType()).
                    setResultTransformer(Transformers.aliasToBean(entityObject.getClass())).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return flashmobsList;
    }

    @SuppressWarnings("unchecked")
    public BigInteger getReportLikesCount(long userFlashmobId) throws SelectDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();

            String strSql = "SELECT COUNT(*) " +
                    "FROM fm_likes l " +
                    "WHERE l.user_flashmob_id = "+userFlashmobId;

            BigInteger count = (BigInteger) session.createSQLQuery(strSql).uniqueResult();
            transaction.commit();

            return count;
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
    }

    public void create(FmLike record, FmUsersFlashmobs usersFlashmobs) throws BaseException {
        Transaction transaction = null;
        StatelessSession session = null;
        try {
            session = HibernateUtil.getSessionFactory().openStatelessSession();
            transaction = session.beginTransaction();
            session.insert(record);

            // Add action
            FmActionsDao actionsDao = new FmActionsDao();
            FmActions actions = new FmActions();
            actions.setActionForUserId(usersFlashmobs.getUserId());
            actions.setActionFromUserId(record.getUserId());
            actions.setUserFlashmobId(usersFlashmobs.getId());
            actions.setType(Defines.ACTION_TYPE_LIKE);
            actions.setDate(System.currentTimeMillis() / 1000);
            actionsDao.create(actions, session);

            transaction.commit();
        } catch (ConstraintViolationException ce) {
            HibernateUtil.rollbackTransaction(transaction);
            if (StringUtils.equals(ce.getConstraintName(), "UserFlashmobLike"))
                throw new LikeFromUserAlreadyExistsException();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        } finally {
            if (session != null) session.close();
        }
    }

    public void update(FmLike record) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().saveOrUpdate(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void delete(FmLike record) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().delete(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }
}
