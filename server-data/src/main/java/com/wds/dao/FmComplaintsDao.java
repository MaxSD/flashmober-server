package com.wds.dao;

import com.wds.database.HibernateUtil;
import com.wds.entity.FmComment;
import com.wds.entity.FmComplaint;
import com.wds.entity.FmSettings;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.CrudDatabaseException;
import com.wds.exception.v1.SelectDatabaseException;
import com.wds.exception.v1.UserNotFoundException;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Service;

@Service("FmComplaintsDao")
public class FmComplaintsDao {

    public FmComplaint findById(long id) throws SelectDatabaseException {
        FmComplaint complaint;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmComplaint.class).
                    add(Restrictions.eq("id", id));
            complaint = (FmComplaint) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return complaint;
    }

    @SuppressWarnings("unchecked")
    public FmComplaint findByUserId(long userId) throws SelectDatabaseException {
        Transaction transaction = null;
        FmComplaint complaint;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmComplaint.class).
                    add(Restrictions.eq("userId", userId));

            complaint = (FmComplaint) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return complaint;
    }

    public void create(FmComplaint record) throws BaseException {
        create(record, null);
    }

    public void create(FmComplaint record, Transaction transaction) throws BaseException {
        try {
            if (transaction != null) {
                HibernateUtil.getSession().persist(record);
            } else {
                transaction = HibernateUtil.beginTransaction();
                HibernateUtil.getSession().persist(record);
                transaction.commit();
            }
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void update(FmComplaint record) throws BaseException {
        update(record, null);
    }

    public void update(FmComplaint record, Transaction transaction) throws CrudDatabaseException {
        try {
            if (transaction != null) {
                HibernateUtil.getSession().saveOrUpdate(record);
            } else {
                transaction = HibernateUtil.beginTransaction();
                HibernateUtil.getSession().saveOrUpdate(record);
                transaction.commit();
            }
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void delete(FmComplaint record) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().delete(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }
}
