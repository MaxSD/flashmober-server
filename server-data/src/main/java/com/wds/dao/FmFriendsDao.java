package com.wds.dao;

import com.wds.database.HibernateUtil;
import com.wds.database.Defines;
import com.wds.entity.FmActions;
import com.wds.entity.FmFriends;
import com.wds.entity.FmUser;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.CrudDatabaseException;
import com.wds.exception.v1.SelectDatabaseException;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BooleanType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;

@Service("FmFriendsDao")
public class FmFriendsDao {

    public FmFriends findById(long id) throws SelectDatabaseException {
        FmFriends friend;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUser.class).
                    add(Restrictions.eq("id", id));
            friend = (FmFriends) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return friend;
    }

    public FmFriends findByUser1IdAndUser2Id(long user1Id, long user2Id) throws SelectDatabaseException {
        FmFriends friend;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmFriends.class).
                    add(Restrictions.or(
                            Restrictions.and(Restrictions.eq("user1Id", user1Id), Restrictions.eq("user2Id", user2Id)),
                            Restrictions.and(Restrictions.eq("user1Id", user2Id), Restrictions.eq("user2Id", user1Id))
                    ));
            friend = (FmFriends) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return friend;
    }

    public FmFriends findFriendsByUser1IdAndUser2Id(long user1Id, long user2Id) throws SelectDatabaseException {
        FmFriends friend;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmFriends.class).
                    add(Restrictions.or(
                            Restrictions.and(Restrictions.eq("user1Id", user1Id), Restrictions.eq("user2Id", user2Id)),
                            Restrictions.and(Restrictions.eq("user1Id", user2Id), Restrictions.eq("user2Id", user1Id))
                    )).add(Restrictions.eq("status", 1));
            friend = (FmFriends) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return friend;
    }

    public FmFriends findFollowUser(long user1Id, long user2Id) throws SelectDatabaseException {
        FmFriends friend;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmFriends.class).
                    add(Restrictions.and(Restrictions.eq("user1Id", user1Id), Restrictions.eq("user2Id", user2Id))).
                    add(Restrictions.eq("status", 0));
            friend = (FmFriends) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return friend;
    }

    public FmFriends findFollowingUser(long user1Id, long user2Id) throws SelectDatabaseException {
        FmFriends friend;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmFriends.class).
                    add(Restrictions.and(Restrictions.eq("user1Id", user2Id), Restrictions.eq("user2Id", user1Id))).
                    add(Restrictions.eq("status", 0));
            friend = (FmFriends) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return friend;
    }

    @SuppressWarnings("unchecked")
    public List<Object> getFriendsFollow(long uid, Object entityObject) throws SelectDatabaseException {
        Transaction transaction = null;
        List<Object> friendsList;
        try {

            String strSQL = "SELECT u.id, u.email, u.gender, u.about, u.age, u.country, u.city, u.site, u.photo, " +
                    "u.vk_id AS vkId, u.fb_id AS fbId, u.tw_id AS twId  FROM fm_users AS u " +
                    "INNER JOIN fm_friends fr ON (fr.user2_id = u.id) " +
                    "WHERE fr.status = 0 AND fr.user1_id = "+uid;

            transaction = HibernateUtil.beginTransaction();
            friendsList = HibernateUtil.getSession().createSQLQuery(strSQL).
                    addScalar("id", new LongType()).
                    addScalar("email").
                    addScalar("gender", new IntegerType()).
                    addScalar("about").
                    addScalar("age", new IntegerType()).
                    addScalar("country").
                    addScalar("city").
                    addScalar("site").
                    addScalar("photo").
                    addScalar("vkId").
                    addScalar("fbId").
                    addScalar("twId").
                    setResultTransformer(Transformers.aliasToBean(entityObject.getClass())).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return friendsList;
    }

    @SuppressWarnings("unchecked")
    public List<Object> getFriendsFollowing(long uid, Object entityObject) throws SelectDatabaseException {
        Transaction transaction = null;
        List<Object> friendsList;
        try {

            String strSQL = "SELECT u.id, u.email, u.gender, u.about, u.age, u.country, u.city, u.site, u.photo, " +
                    "u.vk_id AS vkId, u.fb_id AS fbId, u.tw_id AS twId  FROM fm_users AS u " +
                    "INNER JOIN fm_friends fr ON (fr.user1_id = u.id) " +
                    "WHERE fr.status = 0 AND fr.user2_id = "+uid;

            transaction = HibernateUtil.beginTransaction();
            friendsList = HibernateUtil.getSession().createSQLQuery(strSQL).
                    addScalar("id", new LongType()).
                    addScalar("email").
                    addScalar("gender", new IntegerType()).
                    addScalar("about").
                    addScalar("age", new IntegerType()).
                    addScalar("country").
                    addScalar("city").
                    addScalar("site").
                    addScalar("photo").
                    addScalar("vkId").
                    addScalar("fbId").
                    addScalar("twId").
                    setResultTransformer(Transformers.aliasToBean(entityObject.getClass())).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return friendsList;
    }

    @SuppressWarnings("unchecked")
    public List<Object> getFriendsByUid(long uid, Object entityObject) throws SelectDatabaseException {
        Transaction transaction = null;
        List<Object> friendsList;
        try {

            String strSQL = "SELECT u.id, u.email, u.gender, u.about, u.age, u.country, u.city, u.site, u.photo, u.name, " +
                    "u.vk_id AS vkId, u.fb_id AS fbId, u.tw_id AS twId FROM fm_users AS u " +
                    "INNER JOIN fm_friends fr ON ((fr.user1_id = u.id AND fr.user2_id = "+uid+") OR (fr.user2_id = u.id AND fr.user1_id = "+uid+")) " +
                    "WHERE fr.status = 1 AND (fr.user1_id = "+uid+" OR fr.user2_id = "+uid+") ";

            transaction = HibernateUtil.beginTransaction();
            friendsList = HibernateUtil.getSession().createSQLQuery(strSQL).
                    addScalar("id", new LongType()).
                    addScalar("email").
                    addScalar("gender", new IntegerType()).
                    addScalar("about").
                    addScalar("age", new IntegerType()).
                    addScalar("country").
                    addScalar("city").
                    addScalar("site").
                    addScalar("photo").
                    addScalar("name").
                    addScalar("vkId").
                    addScalar("fbId").
                    addScalar("twId").
                    setResultTransformer(Transformers.aliasToBean(entityObject.getClass())).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return friendsList;
    }

    @SuppressWarnings("unchecked")
    public List<FmUser> getFriendsByUid(long uid, int offset, int limit) throws SelectDatabaseException {
        Transaction transaction = null;
        List<FmUser> friendsList;
        try {

            String strSQL = "SELECT u.* FROM fm_users AS u " +
                    "INNER JOIN fm_friends fr ON ((fr.user1_id = u.id AND fr.user2_id = "+uid+") OR (fr.user2_id = u.id AND fr.user1_id = "+uid+")) " +
                    "WHERE fr.status = 1 AND (fr.user1_id = "+uid+" OR fr.user2_id = "+uid+") OFFSET " + offset;

            if (limit > 0) {
                strSQL += " LIMIT " + limit;
            }

            transaction = HibernateUtil.beginTransaction();
            friendsList = HibernateUtil.getSession().createSQLQuery(strSQL).addEntity(FmUser.class).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return friendsList;
    }

    @SuppressWarnings("unchecked")
    public List<FmUser> getFriendsByUid(long uid, int offset, int limit, String nameFilter, String emailFilter) throws SelectDatabaseException {
        Transaction transaction = null;
        List<FmUser> friendsList;
        try {

            String strSQL = "SELECT u.* FROM fm_users AS u " +
                    "INNER JOIN fm_friends fr ON ((fr.user1_id = u.id AND fr.user2_id = "+uid+") OR (fr.user2_id = u.id AND fr.user1_id = "+uid+")) " +
                    "WHERE fr.status = 1 AND (fr.user1_id = "+uid+" OR fr.user2_id = "+uid+") " +
                    "AND LOWER (u.name) LIKE ('%"+nameFilter.toLowerCase()+"%') " +
                    "AND LOWER (u.email) LIKE ('%"+emailFilter.toLowerCase()+"%') " +
                    "OFFSET " + offset;

            if (limit > 0) {
                strSQL += " LIMIT " + limit;
            }

            transaction = HibernateUtil.beginTransaction();
            friendsList = HibernateUtil.getSession().createSQLQuery(strSQL).addEntity(FmUser.class).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return friendsList;
    }

    @SuppressWarnings("unchecked")
    public BigInteger getFriendsCountByUid(long uid) throws SelectDatabaseException {
        Transaction transaction = null;
        BigInteger count;
        try {

            String strSQL = "SELECT COUNT(*) FROM fm_users AS u " +
                    "INNER JOIN fm_friends fr ON ((fr.user1_id = u.id AND fr.user2_id = "+uid+") OR (fr.user2_id = u.id AND fr.user1_id = "+uid+")) " +
                    "WHERE fr.status = 1 AND (fr.user1_id = "+uid+" OR fr.user2_id = "+uid+")";

            transaction = HibernateUtil.beginTransaction();
            count = (BigInteger) HibernateUtil.getSession().createSQLQuery(strSQL).uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return count;
    }

    @SuppressWarnings("unchecked")
    public BigInteger getFriendsCountByUid(long uid, String nameFilter, String emailFilter) throws SelectDatabaseException {
        Transaction transaction = null;
        BigInteger count;
        try {

            String strSQL = "SELECT COUNT(*) FROM fm_users AS u " +
                    "INNER JOIN fm_friends fr ON ((fr.user1_id = u.id AND fr.user2_id = "+uid+") OR (fr.user2_id = u.id AND fr.user1_id = "+uid+")) " +
                    "WHERE fr.status = 1 AND (fr.user1_id = "+uid+" OR fr.user2_id = "+uid+") " +
                    "AND LOWER (u.name) LIKE ('%"+nameFilter.toLowerCase()+"%') " +
                    "AND LOWER (u.email) LIKE ('%"+emailFilter.toLowerCase()+"%') ";

            transaction = HibernateUtil.beginTransaction();
            count = (BigInteger) HibernateUtil.getSession().createSQLQuery(strSQL).uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return count;
    }

    @SuppressWarnings("unchecked")
    public String getFriendsIdsByUid(long uid) throws SelectDatabaseException {
        Transaction transaction = null;
        List<BigInteger> friendsIdsList;
        try {

            String strSQL = "SELECT u.id FROM fm_users AS u " +
                    "INNER JOIN fm_friends fr ON ((fr.user1_id = u.id AND fr.user2_id = "+uid+") OR (fr.user2_id = u.id AND fr.user1_id = "+uid+")) " +
                    "WHERE fr.status = 1 AND (fr.user1_id = "+uid+" OR fr.user2_id = "+uid+") ";

            transaction = HibernateUtil.beginTransaction();
            friendsIdsList = HibernateUtil.getSession().createSQLQuery(strSQL).list();
            if (friendsIdsList.size() == 0)
                friendsIdsList.add(new BigInteger("0"));

            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return StringUtils.join(friendsIdsList, ",");
    }

    @SuppressWarnings("unchecked")
    public List<Object> getFlashmobCounters(long uid, Object entityObject) throws SelectDatabaseException {
        Transaction transaction = null;
        List<Object> counters;
        try {

            String strSql = "SELECT is_flashmobs_deleted_by_moderator AS flashmobs_deleted_by_moderator, " +
                    "flashmobs_deleted_reason, " +
                    "is_reports_deleted_by_moderator AS reports_deleted_by_moderator, " +
                    "reports_deleted_reason, " +
                    "(SELECT COUNT(*) FROM fm_friends INNER JOIN fm_users AS u ON ((user1_id = "+uid+" AND u.id = user2_id) OR (user2_id = "+uid+" AND u.id = user1_id)) WHERE status = 1) AS friends_count, " +
                    "(SELECT COUNT(*) FROM fm_friends WHERE user1_id = "+uid+" AND status = 1) AS follow_count, " +
                    "(SELECT COUNT(*) FROM fm_friends WHERE user2_id = "+uid+" AND status = 1) AS following_count, " +
                    "(SELECT COUNT(*) FROM fm_users_flashmobs WHERE user_id = "+uid+" AND status = 4 AND is_default_report = TRUE) AS created_flashmobs_count, " +
                    "(SELECT COUNT(*) FROM fm_users_flashmobs WHERE user_id = "+uid+" AND status = 1) AS new_flashmobs_count, " +
                    "(SELECT COUNT(*) FROM fm_users_flashmobs WHERE user_id = "+uid+" AND status = 2) AS accepted_flashmobs_count, " +
                    "(SELECT COUNT(*) FROM fm_users_flashmobs WHERE user_id = "+uid+" AND status = 4 AND is_default_report = FALSE) AS finished_flashmobs_count, " +
                    "(SELECT COUNT(*) FROM fm_actions WHERE action_for_user_id = "+uid+" AND action_from_user_id <> "+uid+") AS notoficatios_count " +
                    "FROM fm_users WHERE id = "+uid;

            transaction = HibernateUtil.beginTransaction();
            counters = HibernateUtil.getSession().createSQLQuery(strSql).
                    addScalar("friends_count", new IntegerType()).
                    addScalar("follow_count", new IntegerType()).
                    addScalar("following_count", new IntegerType()).
                    addScalar("created_flashmobs_count", new IntegerType()).
                    addScalar("new_flashmobs_count", new IntegerType()).
                    addScalar("accepted_flashmobs_count", new IntegerType()).
                    addScalar("finished_flashmobs_count", new IntegerType()).
                    addScalar("notoficatios_count", new IntegerType()).
                    addScalar("flashmobs_deleted_by_moderator", new BooleanType()).
                    addScalar("flashmobs_deleted_reason").
                    addScalar("reports_deleted_by_moderator", new BooleanType()).
                    addScalar("reports_deleted_reason").
                    setResultTransformer(Transformers.aliasToBean(entityObject.getClass())).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return counters;
    }

    @SuppressWarnings("unchecked")
    public List<FmFriends> getAll() throws SelectDatabaseException {
        Transaction transaction = null;
        List<FmFriends> friendsList;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmFriends.class).
                    addOrder(Order.asc("id"));
            friendsList = criteria.list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return friendsList;
    }

    public void create(FmFriends record) throws BaseException {
        Transaction transaction = null;
        StatelessSession session = null;
        try {
            session = HibernateUtil.getSessionFactory().openStatelessSession();
            transaction = session.beginTransaction();
            session.insert(record);

            // Add action
            FmActionsDao actionsDao = new FmActionsDao();
            FmActions actions = new FmActions();
            actions.setActionForUserId(record.getUser2Id());
            actions.setActionFromUserId(record.getUser1Id());
            actions.setUserFlashmobId(0l);
            actions.setType(Defines.ACTION_TYPE_FRIEND_ADD);
            actions.setDate(System.currentTimeMillis() / 1000);
            actionsDao.create(actions, session);

            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        } finally {
            if (session != null) session.close();
        }
    }

    public void update(FmFriends record) throws BaseException {
        Transaction transaction = null;
        StatelessSession session = null;
        try {
            session = HibernateUtil.getSessionFactory().openStatelessSession();
            transaction = session.beginTransaction();
            session.update(record);

            // Add action
            FmActionsDao actionsDao = new FmActionsDao();
            FmActions actions = new FmActions();
            actions.setActionForUserId(record.getUser2Id());
            actions.setActionFromUserId(record.getUser1Id());
            actions.setUserFlashmobId(0l);
            actions.setType(Defines.ACTION_TYPE_FRIEND_ACCEPT);
            actions.setDate(System.currentTimeMillis() / 1000);
            actionsDao.create(actions, session);

            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        } finally {
            if (session != null) session.close();
        }
    }

    public void acceptFriendship(FmFriends record) throws BaseException {
        Transaction transaction = null;
        StatelessSession session = null;
        try {
            session = HibernateUtil.getSessionFactory().openStatelessSession();
            transaction = session.beginTransaction();
            session.update(record);

            // Add action
            FmActionsDao actionsDao = new FmActionsDao();
            FmActions actions = new FmActions();
            actions.setActionForUserId(record.getUser1Id());
            actions.setActionFromUserId(record.getUser2Id());
            actions.setUserFlashmobId(0l);
            actions.setType(Defines.ACTION_TYPE_FRIEND_ACCEPT);
            actions.setDate(System.currentTimeMillis() / 1000);
            actionsDao.create(actions, session);

            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        } finally {
            if (session != null) session.close();
        }
    }

    public void deleteFriendship(FmFriends record) throws BaseException {
        Transaction transaction = null;
        StatelessSession session = null;
        try {
            session = HibernateUtil.getSessionFactory().openStatelessSession();
            transaction = session.beginTransaction();
            session.update(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        } finally {
            if (session != null) session.close();
        }
    }

    public void delete(FmFriends record) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().delete(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void delete(long uid, long friendId) throws CrudDatabaseException {
        Transaction transaction = null;
        try {

            String strSql = "DELETE FROM fm_friends " +
                    "WHERE (user1_id = "+uid+" AND user2_id = "+friendId+") OR (user2_id = "+uid+" AND user1_id = "+friendId+") ";

            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().createSQLQuery(strSql).executeUpdate();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }
}
