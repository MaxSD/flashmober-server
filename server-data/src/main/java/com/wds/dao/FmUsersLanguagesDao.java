package com.wds.dao;

import com.wds.database.HibernateUtil;
import com.wds.entity.FmUsersLanguages;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.CrudDatabaseException;
import com.wds.exception.v1.SelectDatabaseException;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("FmUsersLanguagesDao")
public class FmUsersLanguagesDao {

    public FmUsersLanguages findById(long id) throws SelectDatabaseException {
        FmUsersLanguages usersLanguages;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUsersLanguages.class).
                    add(Restrictions.eq("id", id));
            usersLanguages = (FmUsersLanguages) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return usersLanguages;
    }

    @SuppressWarnings("unchecked")
    public List<FmUsersLanguages> findByUserId(long uid) throws SelectDatabaseException {
        List<FmUsersLanguages> usersLanguagesList;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUsersLanguages.class).
                    add(Restrictions.eq("userId", uid));
            usersLanguagesList = criteria.list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return usersLanguagesList;
    }

    @SuppressWarnings("unchecked")
    public List<Long> getUsersIdsByLanguageWithoutFriends(String language, List<Long> friendsIds) throws SelectDatabaseException {
        List<Long> usersLanguagesList;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUsersLanguages.class).
                    add(Restrictions.eq("language", language)).
                    add(Restrictions.not(Restrictions.in("userId", friendsIds))).
                    setProjection(Projections.property("userId"));
            usersLanguagesList = criteria.list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return usersLanguagesList;
    }

    @SuppressWarnings("unchecked")
    public List<Long> getUsersIdsByLanguage(String language) throws SelectDatabaseException {
        List<Long> usersLanguagesList;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUsersLanguages.class).
                    add(Restrictions.eq("language", language)).
                    setProjection(Projections.property("userId"));
            usersLanguagesList = criteria.list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return usersLanguagesList;
    }

    @SuppressWarnings("unchecked")
    public List<String> getLanguagesList(long uid) throws SelectDatabaseException {
        return getLanguagesList(uid, null);
    }

    @SuppressWarnings("unchecked")
    public List<String> getLanguagesList(long uid, Session session) throws SelectDatabaseException {
        Transaction transaction = null;
        List<String> resultList;
        try {
            if (session != null) {
                Criteria criteria = session.createCriteria(FmUsersLanguages.class).
                        add(Restrictions.eq("userId", uid)).
                        setProjection(Projections.property("language"));
                resultList = criteria.list();
            } else {
                transaction = HibernateUtil.beginTransaction();
                Criteria criteria = HibernateUtil.getSession().createCriteria(FmUsersLanguages.class).
                        add(Restrictions.eq("userId", uid)).
                        setProjection(Projections.property("language"));
                resultList = criteria.list();
                transaction.commit();
            }
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return resultList;
    }

    @SuppressWarnings("unchecked")
    public List<Long> getUsersIdsByLanguagesList(List<String> langList) throws SelectDatabaseException {
        return getUsersIdsByLanguagesList(langList, null);
    }

    @SuppressWarnings("unchecked")
    public List<Long> getUsersIdsByLanguagesList(List<String> langList, Session session) throws SelectDatabaseException {
        Transaction transaction = null;
        List<Long> resultList;
        try {
            if (session != null) {
                Criteria criteria = session.createCriteria(FmUsersLanguages.class).
                        add(Restrictions.in("language", langList)).
                        setProjection(Projections.distinct(Projections.property("userId")));
                resultList = criteria.list();
            } else {
                transaction = HibernateUtil.beginTransaction();
                Criteria criteria = HibernateUtil.getSession().createCriteria(FmUsersLanguages.class).
                        add(Restrictions.in("language", langList)).
                        setProjection(Projections.distinct(Projections.property("userId")));
                resultList = criteria.list();
                transaction.commit();
            }
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return resultList;
    }

    public void create(FmUsersLanguages record) throws BaseException {
        create(record, null);
    }

    public void create(FmUsersLanguages record, Session session) throws BaseException {
        Transaction transaction = null;
        try {
            if (session != null) {
                session.persist(record);
            } else {
                transaction = HibernateUtil.beginTransaction();
                HibernateUtil.getSession().persist(record);
                transaction.commit();
            }
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void update(FmUsersLanguages record) throws BaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().saveOrUpdate(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void delete(FmUsersLanguages record) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().delete(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }
}
