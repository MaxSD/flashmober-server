package com.wds.dao;

import com.wds.database.HibernateUtil;
import com.wds.database.Defines;
import com.wds.entity.*;
import com.wds.exception.v1.*;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;

@Service("FmCommentsDao")
public class FmCommentsDao {

    public FmComment findById(long id) throws SelectDatabaseException {
        FmComment comment;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmComment.class).
                    add(Restrictions.eq("id", id));
            comment = (FmComment) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return comment;
    }

    @SuppressWarnings("unchecked")
    public List<Object> getCommentsList(long userFlashmobId, Object entityObject) throws SelectDatabaseException {
        Transaction transaction = null;
        List<Object> flashmobsList;
        try {

            String strSql = "SELECT c.id AS id, u.id AS uid, u.photo AS user_photo, u.name, u.email, c.comment, c.date " +
                    "FROM fm_comments AS c, fm_users AS u " +
                    "WHERE c.user_id = u.id AND c.user_flashmob_id = " + userFlashmobId;

            transaction = HibernateUtil.beginTransaction();
            flashmobsList = HibernateUtil.getSession().createSQLQuery(strSql).
                    addScalar("id", new LongType()).
                    addScalar("uid", new LongType()).
                    addScalar("user_photo").
                    addScalar("name").
                    addScalar("email").
                    addScalar("comment").
                    addScalar("date", new LongType()).
                    setResultTransformer(Transformers.aliasToBean(entityObject.getClass())).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return flashmobsList;
    }

    @SuppressWarnings("unchecked")
    public List<Object> getCommentsList(long offset, long limit, long userFlashmobId, Object entityObject) throws SelectDatabaseException {
        Transaction transaction = null;
        List<Object> flashmobsList;
        try {

            String strSql = "SELECT u.id AS uid, u.photo AS user_photo, u.name, u.email, c.comment, c.date " +
                    "FROM fm_comments AS c, fm_users AS u " +
                    "WHERE c.user_id = u.id AND c.user_flashmob_id = " + userFlashmobId + " OFFSET " + offset;

            if (limit > 0) {
                strSql += " LIMIT " + limit;
            }

            transaction = HibernateUtil.beginTransaction();
            flashmobsList = HibernateUtil.getSession().createSQLQuery(strSql).
                    addScalar("uid", new LongType()).
                    addScalar("user_photo").
                    addScalar("name").
                    addScalar("email").
                    addScalar("comment").
                    addScalar("date", new LongType()).
                    setResultTransformer(Transformers.aliasToBean(entityObject.getClass())).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return flashmobsList;
    }

    @SuppressWarnings("unchecked")
    public List<Object> getAllCommentsList(long offset, long limit,
                                           String userNameFilter,
                                           String flashmobDescriptionFilter, Object entityObject) throws SelectDatabaseException {
        Transaction transaction = null;
        List<Object> flashmobsList;
        try {

            String strSql = "SELECT c.id, u.id AS uid, u.photo AS user_photo, u.name, f.description, uf.id AS user_flashmob_id, uf.photo_report, c.comment, c.date " +
                    "FROM fm_comments AS c, fm_users AS u, fm_users_flashmobs uf, fm_flashmobs f " +
                    "WHERE c.user_id = u.id AND c.user_flashmob_id = uf.id AND uf.flashmob_id = f.id AND uf.status = " + Defines.FLASHMOB_STATUS_FINISHED;

            if (!StringUtils.isBlank(userNameFilter)) {
                strSql += " AND LOWER(u.name) LIKE ('%"+userNameFilter.toLowerCase()+"%') ";
            }

            if (!StringUtils.isBlank(flashmobDescriptionFilter)) {
                strSql += " AND LOWER(f.description) LIKE ('%"+flashmobDescriptionFilter.toLowerCase()+"%') ";
            }

            strSql += " ORDER BY c.date DESC ";

                    strSql += " OFFSET " + offset;

            if (limit > 0) {
                strSql += " LIMIT " + limit;
            }

            transaction = HibernateUtil.beginTransaction();
            flashmobsList = HibernateUtil.getSession().createSQLQuery(strSql).
                    addScalar("id", new LongType()).
                    addScalar("uid", new LongType()).
                    addScalar("user_photo").
                    addScalar("name").
                    addScalar("description").
                    addScalar("user_flashmob_id", new LongType()).
                    addScalar("photo_report").
                    addScalar("comment").
                    addScalar("date", new LongType()).
                    setResultTransformer(Transformers.aliasToBean(entityObject.getClass())).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return flashmobsList;
    }

    @SuppressWarnings("unchecked")
    public BigInteger getCommentsCount(String userNameFilter,
                                       String flashmobDescriptionFilter) throws SelectDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();

            String strSql = "SELECT COUNT(*) " +
                    "FROM fm_comments AS c, fm_users AS u, fm_users_flashmobs uf, fm_flashmobs f " +
                    "WHERE c.user_id = u.id AND c.user_flashmob_id = uf.id AND uf.flashmob_id = f.id AND uf.status = " + Defines.FLASHMOB_STATUS_FINISHED;

            if (!StringUtils.isBlank(userNameFilter)) {
                strSql += " AND LOWER(u.name) LIKE ('%"+userNameFilter.toLowerCase()+"%') ";
            }

            if (!StringUtils.isBlank(flashmobDescriptionFilter)) {
                strSql += " AND LOWER(f.description) LIKE ('%"+flashmobDescriptionFilter.toLowerCase()+"%') ";
            }

            BigInteger count = (BigInteger) session.createSQLQuery(strSql).uniqueResult();
            transaction.commit();

            return count;
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
    }

    @SuppressWarnings("unchecked")
    public BigInteger getReportCommentsCount(long userFlashmobId) throws SelectDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();

            String strSql = "SELECT COUNT(*) " +
                    "FROM fm_comments c " +
                    "WHERE c.user_flashmob_id = "+userFlashmobId;

            BigInteger count = (BigInteger) session.createSQLQuery(strSql).uniqueResult();
            transaction.commit();

            return count;
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
    }

    public void create(FmComment record, FmUsersFlashmobs usersFlashmobs) throws BaseException {
        Transaction transaction = null;
        StatelessSession session = null;
        try {
            session = HibernateUtil.getSessionFactory().openStatelessSession();
            transaction = session.beginTransaction();
            session.insert(record);

            // Add action
            FmActionsDao actionsDao = new FmActionsDao();
            FmActions actions = new FmActions();
            actions.setActionForUserId(usersFlashmobs.getUserId());
            actions.setActionFromUserId(record.getUserId());
            actions.setUserFlashmobId(usersFlashmobs.getId());
            actions.setType(Defines.ACTION_TYPE_COMMENT);
            actions.setDate(System.currentTimeMillis() / 1000);
            actions.setComment(record.getComment());
            actionsDao.create(actions, session);

            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        } finally {
            if (session != null) session.close();
        }
    }

    public void update(FmComment record) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().saveOrUpdate(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void delete(FmComment record) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().delete(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }
}
