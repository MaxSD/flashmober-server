package com.wds.dao;

import com.wds.database.HibernateUtil;
import com.wds.entity.FmFlashmobCategories;
import com.wds.entity.FmUsersCategories;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.CrudDatabaseException;
import com.wds.exception.v1.SelectDatabaseException;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("FmFlashmobsCategoriesDao")
public class FmFlashmobsCategoriesDao {

    public FmFlashmobCategories findById(long id) throws SelectDatabaseException {
        FmFlashmobCategories resultList;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmFlashmobCategories.class).
                    add(Restrictions.eq("id", id));
            resultList = (FmFlashmobCategories) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return resultList;
    }

    @SuppressWarnings("unchecked")
    public List<FmFlashmobCategories> findByFlashmobId(long flashmobId) throws SelectDatabaseException {
        List<FmFlashmobCategories> resultList;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmFlashmobCategories.class).
                    add(Restrictions.eq("flashmobId", flashmobId));
            resultList = criteria.list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return resultList;
    }

    @SuppressWarnings("unchecked")
    public List<Long> getCategoriesIdsListByFlashmobId(long flashmobId) throws SelectDatabaseException {
        List<Long> resultList;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmFlashmobCategories.class).
                    add(Restrictions.eq("flashmobId", flashmobId)).setProjection(Projections.property("categoryId"));
            resultList = criteria.list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return resultList;
    }

    public void create(FmFlashmobCategories record) throws BaseException {
        create(record, null);
    }

    public void create(FmFlashmobCategories record, Session session) throws BaseException {
        Transaction transaction = null;
        try {
            if (session != null) {
                session.persist(record);
            } else {
                transaction = HibernateUtil.beginTransaction();
                HibernateUtil.getSession().persist(record);
                transaction.commit();
            }
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void update(FmFlashmobCategories record) throws BaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().saveOrUpdate(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void delete(FmFlashmobCategories record) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().delete(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }
}
