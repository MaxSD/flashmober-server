package com.wds.dao;

import com.wds.database.HibernateUtil;
import com.wds.database.Defines;
import com.wds.entity.FmUsersFlashmobs;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.CrudDatabaseException;
import com.wds.exception.v1.SelectDatabaseException;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("FmUsersFlashmobsDao")
public class FmUsersFlashmobsDao {

    public FmUsersFlashmobs findById(long id) throws SelectDatabaseException {
        FmUsersFlashmobs usersFlashmobs;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUsersFlashmobs.class).
                    add(Restrictions.eq("id", id));
            usersFlashmobs = (FmUsersFlashmobs) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return usersFlashmobs;
    }

    @SuppressWarnings("unchecked")
    public List<FmUsersFlashmobs> find(long userId, long flashmobId) throws SelectDatabaseException {
        List<FmUsersFlashmobs> result;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUsersFlashmobs.class).
                    add(Restrictions.and(
                            Restrictions.eq("userId", userId),
                            Restrictions.eq("flashmob_Id", flashmobId)
                    ));
            result = criteria.list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return result;
    }

    public FmUsersFlashmobs find(long userId, long flashmobId, int status) throws SelectDatabaseException {
        FmUsersFlashmobs usersFlashmobs;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUsersFlashmobs.class).
                    add(Restrictions.and(
                            Restrictions.eq("userId", userId),
                            Restrictions.eq("flashmob_Id", flashmobId),
                            Restrictions.eq("status", status)
                    ));
            usersFlashmobs = (FmUsersFlashmobs) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return usersFlashmobs;
    }

    @SuppressWarnings("unchecked")
    public List<FmUsersFlashmobs> getAll() throws SelectDatabaseException {
        Transaction transaction = null;
        List<FmUsersFlashmobs> usersFlashmobsList;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUsersFlashmobs.class).
                    addOrder(Order.asc("id"));
            usersFlashmobsList = criteria.list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return usersFlashmobsList;
    }

    @SuppressWarnings("unchecked")
    public List<FmUsersFlashmobs> getAllByFlastmobId(long flashmobId) throws SelectDatabaseException {
        Transaction transaction = null;
        List<FmUsersFlashmobs> usersFlashmobsList;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUsersFlashmobs.class).
                    add(Restrictions.eq("flashmob_Id", flashmobId));
            usersFlashmobsList = criteria.list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return usersFlashmobsList;
    }

    public FmUsersFlashmobs getLastFinishedFlashmob(long uid) throws SelectDatabaseException {
        FmUsersFlashmobs usersFlashmobs;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUsersFlashmobs.class).
                    add(Restrictions.eq("userId", uid)).
                    add(Restrictions.eq("status", Defines.FLASHMOB_STATUS_FINISHED)).
                    addOrder(Order.desc("date_end")).
                    setMaxResults(1);
            usersFlashmobs = (FmUsersFlashmobs) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return usersFlashmobs;
    }

    public FmUsersFlashmobs getFinishedFlashmobForFriendShip(long uid, long friendshipWithUid) throws SelectDatabaseException {
        FmUsersFlashmobs usersFlashmobs;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUsersFlashmobs.class).
                    add(Restrictions.eq("userId", uid)).
                    add(Restrictions.eq("friendshipWithUid", friendshipWithUid)).
                    add(Restrictions.eq("status", Defines.FLASHMOB_STATUS_FINISHED));
            usersFlashmobs = (FmUsersFlashmobs) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return usersFlashmobs;
    }

    public void create(FmUsersFlashmobs record) throws BaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().persist(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void update(FmUsersFlashmobs record) throws BaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().saveOrUpdate(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void delete(FmUsersFlashmobs record) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().delete(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void delete(long userFlashmobId) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            Session session = HibernateUtil.getSession();
            transaction = session.beginTransaction();

            String strSql = "DELETE FROM fm_users_flashmobs WHERE id = " + userFlashmobId;
            session.createSQLQuery(strSql).executeUpdate();

            strSql = "DELETE FROM fm_likes WHERE user_flashmob_id = " + userFlashmobId;
            session.createSQLQuery(strSql).executeUpdate();

            strSql = "DELETE FROM fm_comments WHERE user_flashmob_id = " + userFlashmobId;
            session.createSQLQuery(strSql).executeUpdate();

            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }
}
