package com.wds.dao;

import com.wds.database.HibernateUtil;
import com.wds.entity.FmUsersCategories;
import com.wds.entity.FmUsersFlashmobs;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.CrudDatabaseException;
import com.wds.exception.v1.SelectDatabaseException;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("FmUsersCategoriesDao")
public class FmUsersCategoriesDao {

    public FmUsersCategories findById(long id) throws SelectDatabaseException {
        FmUsersCategories usersCategories;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUsersCategories.class).
                    add(Restrictions.eq("id", id));
            usersCategories = (FmUsersCategories) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return usersCategories;
    }

    @SuppressWarnings("unchecked")
    public List<FmUsersCategories> findByUserId(long uid) throws SelectDatabaseException {
        List<FmUsersCategories> usersCategoriesesList;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUsersCategories.class).
                    add(Restrictions.eq("userId", uid));
            usersCategoriesesList = criteria.list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return usersCategoriesesList;
    }

    @SuppressWarnings("unchecked")
    public List<Long> getCategoriesIdsListByUserId(long uid) throws SelectDatabaseException {
        List<Long> usersCategoriesesList;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUsersCategories.class).
                    add(Restrictions.eq("userId", uid)).setProjection(Projections.property("categoryId"));
            usersCategoriesesList = criteria.list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return usersCategoriesesList;
    }

    @SuppressWarnings("unchecked")
    public List<FmUsersCategories> getAllByUsersIdsAndCategoryId(List usersIds, long categoryId) throws SelectDatabaseException {
        List<FmUsersCategories> usersCategoriesesList;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmUsersCategories.class).
                    add(Restrictions.eq("categoryId", categoryId)).add(Restrictions.in("userId", usersIds));
            usersCategoriesesList = criteria.list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return usersCategoriesesList;
    }

    public void create(FmUsersCategories record) throws BaseException {
        create(record, null);
    }

    public void create(FmUsersCategories record, Session session) throws BaseException {
        Transaction transaction = null;
        try {
            if (session != null) {
                session.persist(record);
            } else {
                transaction = HibernateUtil.beginTransaction();
                HibernateUtil.getSession().persist(record);
                transaction.commit();
            }
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void update(FmUsersCategories record) throws BaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().saveOrUpdate(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void delete(FmUsersCategories record) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().delete(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }
}
