package com.wds.dao;

import com.wds.database.HibernateUtil;
import com.wds.entity.FmSettings;
import com.wds.entity.FmUserToken;
import com.wds.exception.v1.*;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("FmSettingsDao")
public class FmSettingsDao {

    public FmSettings findById(long id) throws SelectDatabaseException {
        FmSettings settings;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmSettings.class).
                    add(Restrictions.eq("id", id));
            settings = (FmSettings) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return settings;
    }

    @SuppressWarnings("unchecked")
    public FmSettings findByUserId(long userId) throws SelectDatabaseException {
        return findByUserId(userId, null);
    }

    @SuppressWarnings("unchecked")
    public FmSettings findByUserId(long userId, Session session) throws SelectDatabaseException {
        Transaction transaction = null;
        FmSettings settings;
        try {
            if (session != null) {
                Criteria criteria = session.createCriteria(FmSettings.class).
                        add(Restrictions.eq("userId", userId));

                settings = (FmSettings) criteria.uniqueResult();
            } else {
                transaction = HibernateUtil.beginTransaction();
                Criteria criteria = HibernateUtil.getSession().createCriteria(FmSettings.class).
                        add(Restrictions.eq("userId", userId));

                settings = (FmSettings) criteria.uniqueResult();
                transaction.commit();
            }
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return settings;
    }

    public List<Long> getUsersIdsWithSetting_GetFlashmobsFromFriendsOnly_ON(List<Long> usersIds) throws SelectDatabaseException {
        return getUsersIdsWithSetting_GetFlashmobsFromFriendsOnly_ON(usersIds, null);
    }

    @SuppressWarnings("unchecked")
    public List<Long> getUsersIdsWithSetting_GetFlashmobsFromFriendsOnly_ON(List<Long> usersIds, Session session) throws SelectDatabaseException {
        Transaction transaction = null;
        List<Long> resultList;
        try {
            if (session != null) {
                Criteria criteria = session.createCriteria(FmSettings.class).
                        add(Restrictions.and(
                                Restrictions.eq("isIsGetFlashmobsFromFriendsOnly", true),
                                Restrictions.in("userId", usersIds))).
                        setProjection(Projections.property("userId"));
                resultList = criteria.list();
            } else {
                transaction = HibernateUtil.beginTransaction();
                Criteria criteria = HibernateUtil.getSession().createCriteria(FmSettings.class).
                        add(Restrictions.and(
                                Restrictions.eq("isIsGetFlashmobsFromFriendsOnly", true),
                                Restrictions.in("userId", usersIds))).
                        setProjection(Projections.property("userId"));
                resultList = criteria.list();
                transaction.commit();
            }
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return resultList;
    }

    public List<Long> getUsersIdsWithSetting_GetFlashmobs_ON(List<Long> usersIds) throws SelectDatabaseException {
        return getUsersIdsWithSetting_GetFlashmobs_ON(usersIds, null);
    }

    @SuppressWarnings("unchecked")
    public List<Long> getUsersIdsWithSetting_GetFlashmobs_ON(List<Long> usersIds, Session session) throws SelectDatabaseException {
        Transaction transaction = null;
        List<Long> resultList;
        try {
            if (session != null) {
                Criteria criteria = session.createCriteria(FmSettings.class).
                        add(Restrictions.and(
                                Restrictions.or(
                                        Restrictions.eq("isIsGetFlashmobs", true),
                                        Restrictions.eq("isIsGetFlashmobsFromFriendsOnly", true)),
                                Restrictions.in("userId", usersIds))).
                        setProjection(Projections.property("userId"));
                resultList = criteria.list();
            } else {
                transaction = HibernateUtil.beginTransaction();
                Criteria criteria = HibernateUtil.getSession().createCriteria(FmSettings.class).
                        add(Restrictions.and(
                                Restrictions.or(
                                        Restrictions.eq("isIsGetFlashmobs", true),
                                        Restrictions.eq("isIsGetFlashmobsFromFriendsOnly", true)),
                                Restrictions.in("userId", usersIds))).
                        setProjection(Projections.property("userId"));
                resultList = criteria.list();
                transaction.commit();
            }
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return resultList;
    }

    public void create(FmSettings record) throws BaseException {
        create(record, null);
    }

    public void create(FmSettings record, Session session) throws BaseException {
        Transaction transaction = null;
        try {
            if (session != null) {
                session.persist(record);
            } else {
                transaction = HibernateUtil.beginTransaction();
                HibernateUtil.getSession().saveOrUpdate(record);
                transaction.commit();
            }
        } catch (ConstraintViolationException ce) {
            HibernateUtil.rollbackTransaction(transaction);
            if (StringUtils.equals(ce.getConstraintName(), "User"))
                throw new UserNotFoundException();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void update(FmSettings record) throws BaseException {
        update(record, null);
    }

    public void update(FmSettings record, Transaction transaction) throws CrudDatabaseException {
        try {
            if (transaction != null) {
                HibernateUtil.getSession().saveOrUpdate(record);
            } else {
                transaction = HibernateUtil.beginTransaction();
                HibernateUtil.getSession().saveOrUpdate(record);
                transaction.commit();
            }
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void delete(FmSettings record) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().delete(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }
}
