package com.wds.dao;

import com.wds.dataObjects.FlashmobsUsersAssignedOn;
import com.wds.database.HibernateUtil;
import com.wds.database.Defines;
import com.wds.entity.FmFlashmobCategories;
import com.wds.entity.FmFlashmobTags;
import com.wds.entity.FmFlashmobs;
import com.wds.entity.FmUsersFlashmobs;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.CrudDatabaseException;
import com.wds.exception.v1.SelectDatabaseException;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BigIntegerType;
import org.hibernate.type.LongType;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("FmFlashmobsDao")
public class FmFlashmobsDao {

    public FmFlashmobs findById(long id) throws SelectDatabaseException {
        FmFlashmobs flashmobs;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmFlashmobs.class).
                    add(Restrictions.eq("id", id));
            flashmobs = (FmFlashmobs) criteria.uniqueResult();

            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return flashmobs;
    }

    public Object find(long id, long uid, Object entityObject) throws SelectDatabaseException {
        Object returnObject;
        Transaction transaction = null;
        try {

            String strSQL = "SELECT uf.flashmob_id AS id, f.owner_id AS uid, f.photo AS photo, " +
                    "f.description AS description, uf.date_start AS date_start," +
                    "uf.photo_report AS photo_report, uf.video_report AS video_report " +
                    "FROM fm_users_flashmobs AS uf " +
                    "   INNER JOIN fm_flashmobs f ON (f.id = uf.flashmob_id) " +
                    "WHERE f.date_publication <= " + System.currentTimeMillis() / 1000 + " AND uf.flashmob_id = " + id + " AND uf.user_id = " + uid;

            transaction = HibernateUtil.beginTransaction();
            returnObject = HibernateUtil.getSession().createSQLQuery(strSQL).
                    addScalar("id", new LongType()).
                    addScalar("uid", new LongType()).
                    addScalar("photo").
                    addScalar("description").
                    addScalar("date_start", new LongType()).
                    addScalar("photo_report").
                    addScalar("video_report").
                    setResultTransformer(Transformers.aliasToBean(entityObject.getClass())).uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return returnObject;
    }

    @SuppressWarnings("unchecked")
    public List<FmFlashmobs> getAll() throws SelectDatabaseException {
        Transaction transaction = null;
        List<FmFlashmobs> flashmobsList;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmFlashmobs.class).
                    addOrder(Order.asc("id"));
            flashmobsList = criteria.list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return flashmobsList;
    }

    @SuppressWarnings("unchecked")
    public List<FmFlashmobs> getDefaultFlashmobs() throws SelectDatabaseException {
        Transaction transaction = null;
        List<FmFlashmobs> flashmobsList;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(FmFlashmobs.class).
                    add(Restrictions.eq("isIsDefault", true));
            flashmobsList = criteria.list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return flashmobsList;
    }

    @SuppressWarnings("unchecked")
    public List<Object> getAll(int offset, int limit, String descriptionFilter, Object entityObject) throws SelectDatabaseException {
        Transaction transaction = null;
        List<Object> flashmobsList;
        try {
            transaction = HibernateUtil.beginTransaction();

            StringBuilder sbSql = new StringBuilder("SELECT f.id, f.photo, f.date_create, f.description, f.owner_id, u.name AS owner_name FROM fm_flashmobs f, fm_users u WHERE f.owner_id = u.id ");
            if (!StringUtils.isBlank(descriptionFilter)) {
                sbSql.append(" AND LOWER(description) LIKE ('%" + StringUtils.trim(descriptionFilter).toLowerCase() + "%') ");
            }

            sbSql.append(" ORDER BY date_create DESC ");

            sbSql.append(" OFFSET " + offset);

            if (limit > 0) {
                sbSql.append(" LIMIT " + limit);
            }

            flashmobsList = HibernateUtil.getSession().createSQLQuery(sbSql.toString()).
                    addScalar("id", new LongType()).
                    addScalar("photo").
                    addScalar("date_create", new LongType()).
                    addScalar("description").
                    addScalar("owner_id", new LongType()).
                    addScalar("owner_name").
                    setResultTransformer(Transformers.aliasToBean(entityObject.getClass())).list();

            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return flashmobsList;
    }

    @SuppressWarnings("unchecked")
    public List<FmFlashmobs> getAllByOwnerId(long ownerId, int offset, int limit, String descriptionFilter) throws SelectDatabaseException {
        Transaction transaction = null;
        List<FmFlashmobs> flashmobsList;
        try {
            transaction = HibernateUtil.beginTransaction();

            StringBuilder sbSql = new StringBuilder("SELECT * FROM fm_flashmobs WHERE owner_id = " + ownerId + " ");
            if (!StringUtils.isBlank(descriptionFilter)) {
                sbSql.append(" AND LOWER(description) LIKE ('%" + StringUtils.trim(descriptionFilter).toLowerCase() + "%') ");
            }

            sbSql.append(" ORDER BY date_create DESC ");

            sbSql.append(" OFFSET " + offset);

            if (limit > 0) {
                sbSql.append(" LIMIT " + limit);
            }

            flashmobsList = HibernateUtil.getSession().createSQLQuery(sbSql.toString()).addEntity(FmFlashmobs.class).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return flashmobsList;
    }

    @SuppressWarnings("unchecked")
    public List<Object> getAllOnModeration(int offset, int limit, String descriptionFilter, Object entityObject) throws SelectDatabaseException {
        Transaction transaction = null;
        List<Object> flashmobsList;
        try {
            transaction = HibernateUtil.beginTransaction();

            StringBuilder sbSql = new StringBuilder("SELECT f.id, f.photo, f.date_create, f.description, f.owner_id, u.name AS owner_name FROM fm_flashmobs f, fm_users u WHERE f.owner_id = u.id AND moderation = TRUE AND is_accepted_by_admin = FALSE ");
            if (!StringUtils.isBlank(descriptionFilter)) {
                sbSql.append(" AND LOWER(description) LIKE ('%" + StringUtils.trim(descriptionFilter).toLowerCase() + "%') ");
            }

            sbSql.append(" ORDER BY date_create DESC ");

            sbSql.append(" OFFSET " + offset);

            if (limit > 0) {
                sbSql.append(" LIMIT " + limit);
            }

            flashmobsList = HibernateUtil.getSession().createSQLQuery(sbSql.toString()).
                    addScalar("id", new LongType()).
                    addScalar("photo").
                    addScalar("date_create", new LongType()).
                    addScalar("description").
                    addScalar("owner_id", new LongType()).
                    addScalar("owner_name").
                    setResultTransformer(Transformers.aliasToBean(entityObject.getClass())).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return flashmobsList;
    }

    @SuppressWarnings("unchecked")
    public BigInteger getFlashmobsCount(String descriptionFilter) throws SelectDatabaseException {
        Transaction transaction = null;
        BigInteger count;
        try {
            transaction = HibernateUtil.beginTransaction();

            StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM fm_flashmobs ");
            if (!StringUtils.isBlank(descriptionFilter)) {
                sbSql.append(" WHERE LOWER(description) LIKE ('%" + descriptionFilter.toLowerCase() + "%') ");
            }

            count = (BigInteger) HibernateUtil.getSession().createSQLQuery(sbSql.toString()).uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return count;
    }

    @SuppressWarnings("unchecked")
    public BigInteger getFlashmobsCountByOwnerId(long ownerId, String descriptionFilter) throws SelectDatabaseException {
        Transaction transaction = null;
        BigInteger count;
        try {
            transaction = HibernateUtil.beginTransaction();

            StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM fm_flashmobs WHERE owner_id = " + ownerId + " ");
            if (!StringUtils.isBlank(descriptionFilter)) {
                sbSql.append(" AND LOWER(description) LIKE ('%" + descriptionFilter.toLowerCase() + "%') ");
            }

            count = (BigInteger) HibernateUtil.getSession().createSQLQuery(sbSql.toString()).uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return count;
    }

    @SuppressWarnings("unchecked")
    public BigInteger getFlashmobsOnModerationCount(String descriptionFilter) throws SelectDatabaseException {
        Transaction transaction = null;
        BigInteger count;
        try {
            transaction = HibernateUtil.beginTransaction();

            StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM fm_flashmobs WHERE moderation = TRUE AND is_accepted_by_admin = FALSE ");
            if (!StringUtils.isBlank(descriptionFilter)) {
                sbSql.append(" AND LOWER(description) LIKE ('%" + descriptionFilter.toLowerCase() + "%') ");
            }

            count = (BigInteger) HibernateUtil.getSession().createSQLQuery(sbSql.toString()).uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return count;
    }

    @SuppressWarnings("unchecked")
    public List<Object> getFlashmobList(long uid, long current_uid, int status, Object entityObject) throws SelectDatabaseException {
        Transaction transaction = null;
        List<Object> flashmobsList;
        try {
            String strSql = "";

            if (status == Defines.FLASHMOB_STATUS_CREATED || status == Defines.FLASHMOB_STATUS_ACCEPTED) {
                strSql = "SELECT f.id, uf.user_id AS owner_id, uf.id AS user_flashmob_id, " +
                        "               u1.name, u1.email, u1.photo AS user_photo, " +
                        "               f.owner_id AS creater_id, f.video, f.photo, f.date_create, f.date_publication, f.description, uf.date_assign, uf.date_start, uf.date_end, uf.status, uf.photo_report, uf.video_report, ( SELECT count(*) AS count " +
                        "           FROM fm_users_flashmobs " +
                        "          WHERE fm_users_flashmobs.flashmob_id = f.id AND status = " + Defines.FLASHMOB_STATUS_FINISHED + ") AS people_count, ( SELECT count(*) AS count " +
                        "           FROM fm_likes " +
                        "          WHERE fm_likes.is_unliked = FALSE AND fm_likes.user_flashmob_id = uf.id) AS like_count, ( SELECT count(*) AS count " +
                        "           FROM fm_likes " +
                        "          WHERE fm_likes.is_unliked = FALSE AND fm_likes.user_flashmob_id = uf.id AND fm_likes.user_id = " + current_uid + ") AS is_me_liked, ( SELECT count(*) AS count " +
                        "           FROM fm_comments " +
                        "          WHERE fm_comments.user_flashmob_id = uf.id) AS comment_count " +
                        "   FROM fm_users u, fm_users_flashmobs uf, fm_flashmobs f " +
                        "   INNER JOIN fm_users u1 ON (u1.id = f.owner_id) " +
                        "  WHERE f.is_accepted_by_admin = TRUE AND f.date_publication <= " + System.currentTimeMillis() / 1000 + " AND u.id = uf.user_id AND uf.flashmob_id = f.id AND uf.user_id = " + uid + " AND status = " + status;
            } else if (status == Defines.FLASHMOB_STATUS_FINISHED) {
                strSql = "SELECT f.id, uf.user_id AS owner_id, uf.id AS user_flashmob_id, u.name, u.email, u.photo AS user_photo, f.owner_id AS creater_id, f.video, f.photo, f.date_create, f.date_publication, f.description, uf.date_assign, uf.date_start, uf.date_end, uf.status, uf.photo_report, uf.video_report, ( SELECT count(*) AS count " +
                        "           FROM fm_users_flashmobs " +
                        "          WHERE fm_users_flashmobs.flashmob_id = f.id AND status = " + Defines.FLASHMOB_STATUS_FINISHED + ") AS people_count, ( SELECT count(*) AS count " +
                        "           FROM fm_likes " +
                        "          WHERE fm_likes.is_unliked = FALSE AND fm_likes.user_flashmob_id = uf.id) AS like_count, ( SELECT count(*) AS count " +
                        "           FROM fm_likes " +
                        "          WHERE fm_likes.is_unliked = FALSE AND fm_likes.user_flashmob_id = uf.id AND fm_likes.user_id = " + current_uid + ") AS is_me_liked, ( SELECT count(*) AS count " +
                        "           FROM fm_comments " +
                        "          WHERE fm_comments.user_flashmob_id = uf.id) AS comment_count " +
                        "   FROM fm_users u, fm_users_flashmobs uf, fm_flashmobs f " +
                        "  WHERE f.is_accepted_by_admin = TRUE AND f.date_publication <= " + System.currentTimeMillis() / 1000 + " AND u.id = uf.user_id AND uf.flashmob_id = f.id AND uf.user_id = " + uid + " AND is_default_report = FALSE AND status = " + status;
            } else {
                strSql = "SELECT f.id, u.id AS owner_id, uf.id AS user_flashmob_id, u.name, u.email, u.photo AS user_photo, f.owner_id AS creater_id, f.video, f.photo, f.date_create, f.date_publication, f.description, uf.date_assign, uf.date_start, uf.date_end, uf.status, uf.photo_report, uf.video_report, ( SELECT count(*) AS count " +
                        "           FROM fm_users_flashmobs " +
                        "          WHERE fm_users_flashmobs.flashmob_id = f.id AND status = " + Defines.FLASHMOB_STATUS_FINISHED + ") AS people_count, ( SELECT count(*) AS count " +
                        "           FROM fm_likes " +
                        "          WHERE fm_likes.is_unliked = FALSE AND fm_likes.user_flashmob_id = uf.id) AS like_count, ( SELECT count(*) AS count " +
                        "           FROM fm_comments " +
                        "          WHERE fm_comments.user_flashmob_id = uf.id) AS comment_count " +
                        "   FROM fm_users_flashmobs uf, fm_flashmobs f, fm_users u " +
                        "  WHERE f.is_accepted_by_admin = TRUE AND f.date_publication <= " + System.currentTimeMillis() / 1000 + " AND uf.flashmob_id = f.id AND u.id = f.owner_id AND uf.user_id = " + uid + " AND status = " + status;
            }

            transaction = HibernateUtil.beginTransaction();

            if (status == Defines.FLASHMOB_STATUS_FINISHED || status == Defines.FLASHMOB_STATUS_ACCEPTED) {
                flashmobsList = HibernateUtil.getSession().createSQLQuery(strSql).
                        addScalar("id", new LongType()).
                        addScalar("owner_id", new LongType()).
                        addScalar("user_flashmob_id", new LongType()).
                        addScalar("name").
                        addScalar("email").
                        addScalar("user_photo").
                        addScalar("video").
                        addScalar("photo").
                        addScalar("date_create", new LongType()).
                        addScalar("date_publication", new LongType()).
                        addScalar("description").
                        addScalar("date_assign", new LongType()).
                        addScalar("date_start", new LongType()).
                        addScalar("date_end", new LongType()).
                        addScalar("photo_report").
                        addScalar("video_report").
                        addScalar("people_count", new BigIntegerType()).
                        addScalar("like_count", new BigIntegerType()).
                        addScalar("comment_count", new BigIntegerType()).
                        addScalar("is_me_liked", new BigIntegerType()).
                        setResultTransformer(Transformers.aliasToBean(entityObject.getClass())).list();
            } else {
                flashmobsList = HibernateUtil.getSession().createSQLQuery(strSql).
                        addScalar("id", new LongType()).
                        addScalar("owner_id", new LongType()).
                        addScalar("user_flashmob_id", new LongType()).
                        addScalar("name").
                        addScalar("email").
                        addScalar("user_photo").
                        addScalar("video").
                        addScalar("photo").
                        addScalar("date_create", new LongType()).
                        addScalar("date_publication", new LongType()).
                        addScalar("description").
                        addScalar("date_assign", new LongType()).
                        addScalar("date_start", new LongType()).
                        addScalar("date_end", new LongType()).
                        addScalar("photo_report").
                        addScalar("video_report").
                        addScalar("people_count", new BigIntegerType()).
                        addScalar("like_count", new BigIntegerType()).
                        addScalar("comment_count", new BigIntegerType()).
                        setResultTransformer(Transformers.aliasToBean(entityObject.getClass())).list();
            }

            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return flashmobsList;
    }

    @SuppressWarnings("unchecked")
    public Object getFlashmobByUserFlashmobId(long userFlashmobId, long current_uid, Object entityObject) throws SelectDatabaseException {
        Transaction transaction = null;
        Object flashmob;
        try {

            String strSql = "SELECT f.id, uf.user_id AS owner_id, uf.id AS user_flashmob_id, u.name, u.email, u.photo AS user_photo, f.owner_id AS creater_id, f.photo, f.date_create, f.description, uf.date_start, uf.date_end, uf.status, uf.photo_report, uf.video_report, ( SELECT count(*) AS count " +
                    "           FROM fm_users_flashmobs " +
                    "          WHERE fm_users_flashmobs.flashmob_id = f.id AND status = " + Defines.FLASHMOB_STATUS_FINISHED + ") AS people_count, ( SELECT count(*) AS count " +
                    "           FROM fm_likes " +
                    "          WHERE fm_likes.is_unliked = FALSE AND fm_likes.user_flashmob_id = uf.id) AS like_count, ( SELECT count(*) AS count " +
                    "           FROM fm_likes " +
                    "          WHERE fm_likes.is_unliked = FALSE AND fm_likes.user_flashmob_id = uf.id AND fm_likes.user_id = " + current_uid + ") AS is_me_liked, ( SELECT count(*) AS count " +
                    "           FROM fm_comments " +
                    "          WHERE fm_comments.user_flashmob_id = uf.id) AS comment_count " +
                    "   FROM fm_users_flashmobs uf, fm_users u, fm_flashmobs f " +
                    "  WHERE f.is_accepted_by_admin = TRUE AND f.date_publication <= " + System.currentTimeMillis() / 1000 + " AND uf.id = " + userFlashmobId + " AND u.id = uf.user_id AND uf.flashmob_id = f.id";

            transaction = HibernateUtil.beginTransaction();
            flashmob = HibernateUtil.getSession().createSQLQuery(strSql).
                    addScalar("id", new LongType()).
                    addScalar("owner_id", new LongType()).
                    addScalar("user_flashmob_id", new LongType()).
                    addScalar("name").
                    addScalar("email").
                    addScalar("user_photo").
                    addScalar("photo").
                    addScalar("date_create", new LongType()).
                    addScalar("description").
                    addScalar("date_end", new LongType()).
                    addScalar("photo_report").
                    addScalar("video_report").
                    addScalar("people_count", new BigIntegerType()).
                    addScalar("like_count", new BigIntegerType()).
                    addScalar("comment_count", new BigIntegerType()).
                    addScalar("is_me_liked", new BigIntegerType()).
                    setResultTransformer(Transformers.aliasToBean(entityObject.getClass())).uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return flashmob;
    }

    @SuppressWarnings("unchecked")
    public List<Object> getFlashmobReport(long uid, String categoriesIdsList, int offset, int limit,
                                          Object entityObject) throws SelectDatabaseException {

        Transaction transaction = null;
        List<Object> flashmobsList;
        try {

            FmFriendsDao friendsDao = new FmFriendsDao();
            String friendsIds = friendsDao.getFriendsIdsByUid(uid);

            String strSql = "SELECT DISTINCT * FROM (" +
                    " SELECT f.id, uf.user_id AS owner_id, uf.id AS user_flashmob_id, u.name, u.email, u.photo AS user_photo, f.owner_id AS creater_id, f.photo, f.date_create, f.description, uf.date_start, uf.date_end, uf.status, uf.photo_report, uf.video_report, ( SELECT count(*) AS count " +
                    "           FROM fm_users_flashmobs " +
                    "          WHERE fm_users_flashmobs.flashmob_id = f.id AND status = " + Defines.FLASHMOB_STATUS_FINISHED + ") AS people_count, ( SELECT count(*) AS count " +
                    "           FROM fm_likes " +
                    "          WHERE fm_likes.is_unliked = FALSE AND fm_likes.user_flashmob_id = uf.id) AS like_count, ( SELECT count(*) AS count " +
                    "           FROM fm_likes " +
                    "          WHERE fm_likes.is_unliked = FALSE AND fm_likes.user_flashmob_id = uf.id AND fm_likes.user_id = " + uid + ") AS is_me_liked, ( SELECT count(*) AS count " +
                    "           FROM fm_comments " +
                    "          WHERE fm_comments.user_flashmob_id = uf.id) AS comment_count " +
                    "   FROM fm_users u, fm_users_flashmobs uf, fm_flashmobs f " +
                    "  WHERE f.is_accepted_by_admin = TRUE AND f.date_publication <= " + System.currentTimeMillis() / 1000 + " AND u.id = uf.user_id AND uf.flashmob_id = f.id AND (uf.user_id IN (" + friendsIds + ") OR uf.user_id = " + uid + ") AND status = " + Defines.FLASHMOB_STATUS_FINISHED;

            if (!StringUtils.isBlank(categoriesIdsList)) {
                strSql += " UNION " +
                        " SELECT f.id, uf.user_id AS owner_id, uf.id AS user_flashmob_id, u.name, u.email, u.photo AS user_photo, f.owner_id AS creater_id, f.photo, f.date_create, f.description, uf.date_start, uf.date_end, uf.status, uf.photo_report, uf.video_report, ( SELECT count(*) AS count " +
                        "                               FROM fm_users_flashmobs " +
                        "                              WHERE fm_users_flashmobs.flashmob_id = f.id AND status = " + Defines.FLASHMOB_STATUS_FINISHED + ") AS people_count, ( SELECT count(*) AS count " +
                        "                               FROM fm_likes " +
                        "                              WHERE fm_likes.is_unliked = FALSE AND fm_likes.user_flashmob_id = uf.id) AS like_count, ( SELECT count(*) AS count " +
                        "                               FROM fm_likes " +
                        "                              WHERE fm_likes.is_unliked = FALSE AND fm_likes.user_flashmob_id = uf.id AND fm_likes.user_id = " + uid + ") AS is_me_liked, ( SELECT count(*) AS count " +
                        "                               FROM fm_comments " +
                        "                              WHERE fm_comments.user_flashmob_id = uf.id) AS comment_count " +
                        "                       FROM fm_flashmob_categories fc, fm_users u, fm_users_flashmobs uf, fm_flashmobs f " +
                        "                      WHERE f.is_accepted_by_admin = TRUE AND f.date_publication <= " + System.currentTimeMillis() / 1000 + " AND fc.category_id IN (" + categoriesIdsList + ") AND fc.flashmob_id = uf.flashmob_id AND uf.flashmob_id = f.id AND u.id = uf.user_id AND status = " + Defines.FLASHMOB_STATUS_FINISHED;
            }

            strSql += " ) AS result ORDER BY date_end DESC ";

            if (offset > 0)
                strSql += " OFFSET " + offset;

            if (limit > 0)
                strSql += " LIMIT " + limit;

            transaction = HibernateUtil.beginTransaction();
            flashmobsList = HibernateUtil.getSession().createSQLQuery(strSql).
                    addScalar("id", new LongType()).
                    addScalar("owner_id", new LongType()).
                    addScalar("user_flashmob_id", new LongType()).
                    addScalar("name").
                    addScalar("email").
                    addScalar("user_photo").
                    addScalar("photo").
                    addScalar("date_create", new LongType()).
                    addScalar("description").
                    addScalar("date_end", new LongType()).
                    addScalar("photo_report").
                    addScalar("video_report").
                    addScalar("people_count", new BigIntegerType()).
                    addScalar("like_count", new BigIntegerType()).
                    addScalar("is_me_liked", new BigIntegerType()).
                    addScalar("comment_count", new BigIntegerType()).
                    setResultTransformer(Transformers.aliasToBean(entityObject.getClass())).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return flashmobsList;
    }

    @SuppressWarnings("unchecked")
    public List<Object> getFlashmobReportByUserCategories(long uid, String categoriesIdsList, Object entityObject) throws SelectDatabaseException {
        Transaction transaction = null;
        List<Object> flashmobsList;
        try {

            FmFriendsDao friendsDao = new FmFriendsDao();
            String friendsIds = friendsDao.getFriendsIdsByUid(uid);

            String strSql = "SELECT DISTINCT f.id, uf.user_id AS owner_id, uf.id AS user_flashmob_id, u.name, u.email, u.photo AS user_photo, f.owner_id AS creater_id, f.photo, f.date_create, f.description, uf.date_start, uf.date_end, uf.status, uf.photo_report, uf.video_report, ( SELECT count(*) AS count " +
                    "           FROM fm_users_flashmobs " +
                    "          WHERE fm_users_flashmobs.flashmob_id = f.id AND status = " + Defines.FLASHMOB_STATUS_FINISHED + ") AS people_count, ( SELECT count(*) AS count " +
                    "           FROM fm_likes " +
                    "          WHERE fm_likes.is_unliked = FALSE AND fm_likes.user_flashmob_id = uf.id) AS like_count, ( SELECT count(*) AS count " +
                    "           FROM fm_likes " +
                    "          WHERE fm_likes.is_unliked = FALSE AND fm_likes.user_flashmob_id = uf.id AND fm_likes.user_id = " + uid + ") AS is_me_liked, ( SELECT count(*) AS count " +
                    "           FROM fm_comments " +
                    "          WHERE fm_comments.user_flashmob_id = uf.id) AS comment_count " +
                    "   FROM fm_flashmob_categories fc, fm_users u, fm_users_flashmobs uf, fm_flashmobs f " +
                    "  WHERE f.is_accepted_by_admin = TRUE AND f.date_publication <= " + System.currentTimeMillis() / 1000 + " AND fc.category_id IN (" + categoriesIdsList + ") AND fc.flashmob_id = uf.flashmob_id AND uf.flashmob_id = f.id AND u.id = uf.user_id AND status = 4";

            transaction = HibernateUtil.beginTransaction();
            flashmobsList = HibernateUtil.getSession().createSQLQuery(strSql).
                    addScalar("id", new LongType()).
                    addScalar("owner_id", new LongType()).
                    addScalar("user_flashmob_id", new LongType()).
                    addScalar("name").
                    addScalar("email").
                    addScalar("user_photo").
                    addScalar("photo").
                    addScalar("date_create", new LongType()).
                    addScalar("description").
                    addScalar("date_end", new LongType()).
                    addScalar("photo_report").
                    addScalar("video_report").
                    addScalar("people_count", new BigIntegerType()).
                    addScalar("like_count", new BigIntegerType()).
                    addScalar("is_me_liked", new BigIntegerType()).
                    addScalar("comment_count", new BigIntegerType()).
                    setResultTransformer(Transformers.aliasToBean(entityObject.getClass())).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return flashmobsList;
    }

    @SuppressWarnings("unchecked")
    public List<Object> getFlashmobReports(long flashmobId, Object entityObject) throws SelectDatabaseException {
        Transaction transaction = null;
        List<Object> flashmobsList;
        try {

            String strSql = "SELECT f.id, uf.user_id AS owner_id, uf.id AS user_flashmob_id, u.name, u.email, u.photo AS user_photo, u.about AS user_about, f.photo, f.date_create, f.description, uf.date_start, uf.date_end, uf.status, uf.photo_report, uf.video_report, ( SELECT count(*) AS count " +
                    "           FROM fm_likes " +
                    "          WHERE fm_likes.is_unliked = FALSE AND fm_likes.user_flashmob_id = uf.id) AS like_count " +
                    "   FROM fm_users u, fm_users_flashmobs uf, fm_flashmobs f " +
                    "  WHERE f.is_accepted_by_admin = TRUE AND f.date_publication <= " + System.currentTimeMillis() / 1000 + " AND uf.flashmob_id = " + flashmobId + " AND u.id = uf.user_id AND f.id = uf.flashmob_id AND status = 4";

            transaction = HibernateUtil.beginTransaction();
            flashmobsList = HibernateUtil.getSession().createSQLQuery(strSql).
                    addScalar("id", new LongType()).
                    addScalar("owner_id", new LongType()).
                    addScalar("user_flashmob_id", new LongType()).
                    addScalar("name").
                    addScalar("email").
                    addScalar("user_photo").
                    addScalar("user_about").
                    addScalar("photo").
                    addScalar("date_create", new LongType()).
                    addScalar("description").
                    addScalar("date_end", new LongType()).
                    addScalar("photo_report").
                    addScalar("video_report").
                    addScalar("like_count", new BigIntegerType()).
                    setResultTransformer(Transformers.aliasToBean(entityObject.getClass())).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return flashmobsList;
    }

    @SuppressWarnings("unchecked")
    public List<Object> getAllUsersReports(long offset, long limit,
                                           String userNameFilter,
                                           String flashmobDescriptionFilter, Object entityObject) throws SelectDatabaseException {
        Transaction transaction = null;
        List<Object> flashmobsList;
        try {

            String strSql = "SELECT uf.id, f.id AS flashmob_id, uf.user_id AS owner_id, uf.id AS user_flashmob_id, u.name, u.email, u.photo AS user_photo, u.about AS user_about, f.photo, f.date_create, f.description, uf.date_start, uf.date_end, uf.status, uf.photo_report, uf.video_report, " +
                    " (SELECT COUNT(*) FROM fm_likes WHERE is_unliked = FALSE AND user_flashmob_id = uf.id) AS like_count " +
                    " FROM fm_users_flashmobs uf, fm_users u, fm_flashmobs f " +
                    " WHERE f.is_accepted_by_admin = TRUE AND f.date_publication <= " + System.currentTimeMillis() / 1000 + " AND u.id = uf.user_id AND f.id = uf.flashmob_id AND status = 4 ";

            if (!StringUtils.isBlank(userNameFilter)) {
                strSql += " AND LOWER(u.name) LIKE('%" + userNameFilter.toLowerCase() + "%') ";
            }

            if (!StringUtils.isBlank(userNameFilter)) {
                strSql += " AND LOWER(f.description) LIKE('%" + flashmobDescriptionFilter.toLowerCase() + "%') ";
            }

            strSql += " ORDER BY uf.date_end DESC ";

            strSql += " OFFSET " + offset + " ";

            if (limit > 0) {
                strSql += " LIMIT " + limit + " ";
            }

            transaction = HibernateUtil.beginTransaction();
            flashmobsList = HibernateUtil.getSession().createSQLQuery(strSql).
                    addScalar("id", new LongType()).
                    addScalar("flashmob_id", new LongType()).
                    addScalar("owner_id", new LongType()).
                    addScalar("user_flashmob_id", new LongType()).
                    addScalar("name").
                    addScalar("email").
                    addScalar("user_photo").
                    addScalar("user_about").
                    addScalar("photo").
                    addScalar("date_create", new LongType()).
                    addScalar("description").
                    addScalar("date_end", new LongType()).
                    addScalar("photo_report").
                    addScalar("video_report").
                    addScalar("like_count", new BigIntegerType()).
                    setResultTransformer(Transformers.aliasToBean(entityObject.getClass())).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return flashmobsList;
    }

    @SuppressWarnings("unchecked")
    public List<Object> getBusinessUsersReports(long offset, long limit,
                                                String userNameFilter,
                                                String flashmobDescriptionFilter, Object entityObject) throws SelectDatabaseException {
        Transaction transaction = null;
        List<Object> flashmobsList;
        try {

            String strSql = "SELECT uf.id, f.id AS flashmob_id, uf.user_id AS owner_id, uf.id AS user_flashmob_id, u.name, u.email, u.photo AS user_photo, u.about AS user_about, f.photo, f.date_create, f.description, uf.date_start, uf.date_end, uf.status, uf.photo_report, uf.video_report, " +
                    " (SELECT COUNT(*) FROM fm_likes WHERE is_unliked = FALSE AND user_flashmob_id = uf.id) AS like_count " +
                    " FROM fm_users_flashmobs uf, fm_users u, fm_flashmobs f " +
                    " WHERE u.is_business_user = TRUE AND f.is_accepted_by_admin = TRUE AND f.date_publication <= " + System.currentTimeMillis() / 1000 + " AND u.id = uf.user_id AND f.id = uf.flashmob_id AND status = 4 ";

            if (!StringUtils.isBlank(userNameFilter)) {
                strSql += " AND LOWER(u.name) LIKE('%" + userNameFilter.toLowerCase() + "%') ";
            }

            if (!StringUtils.isBlank(userNameFilter)) {
                strSql += " AND LOWER(f.description) LIKE('%" + flashmobDescriptionFilter.toLowerCase() + "%') ";
            }

            strSql += " ORDER BY uf.date_end DESC ";

            strSql += " OFFSET " + offset + " ";

            if (limit > 0) {
                strSql += " LIMIT " + limit + " ";
            }

            transaction = HibernateUtil.beginTransaction();
            flashmobsList = HibernateUtil.getSession().createSQLQuery(strSql).
                    addScalar("id", new LongType()).
                    addScalar("flashmob_id", new LongType()).
                    addScalar("owner_id", new LongType()).
                    addScalar("user_flashmob_id", new LongType()).
                    addScalar("name").
                    addScalar("email").
                    addScalar("user_photo").
                    addScalar("user_about").
                    addScalar("photo").
                    addScalar("date_create", new LongType()).
                    addScalar("description").
                    addScalar("date_end", new LongType()).
                    addScalar("photo_report").
                    addScalar("video_report").
                    addScalar("like_count", new BigIntegerType()).
                    setResultTransformer(Transformers.aliasToBean(entityObject.getClass())).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return flashmobsList;
    }

    @SuppressWarnings("unchecked")
    public List<Object> getMyTasksReports(long uid, long offset, long limit,
                                          String userNameFilter,
                                          String flashmobDescriptionFilter, Object entityObject) throws SelectDatabaseException {
        Transaction transaction = null;
        List<Object> flashmobsList;
        try {

            String strSql = "SELECT uf.id, f.id AS flashmob_id, uf.user_id AS owner_id, uf.id AS user_flashmob_id, u.name, u.email, u.photo AS user_photo, u.about AS user_about, f.photo, f.date_create, f.description, uf.date_start, uf.date_end, uf.status, uf.photo_report, uf.video_report, " +
                    " (SELECT COUNT(*) FROM fm_likes WHERE is_unliked = FALSE AND user_flashmob_id = uf.id) AS like_count " +
                    " FROM fm_users_flashmobs uf, fm_users u, fm_flashmobs f " +
                    " WHERE f.owner_id = " + uid + " AND f.is_accepted_by_admin = TRUE AND f.date_publication <= " + System.currentTimeMillis() / 1000 + " AND u.id = uf.user_id AND f.id = uf.flashmob_id AND status = 4 ";

            if (!StringUtils.isBlank(userNameFilter)) {
                strSql += " AND LOWER(u.name) LIKE('%" + userNameFilter.toLowerCase() + "%') ";
            }

            if (!StringUtils.isBlank(userNameFilter)) {
                strSql += " AND LOWER(f.description) LIKE('%" + flashmobDescriptionFilter.toLowerCase() + "%') ";
            }

            strSql += " ORDER BY uf.date_end DESC ";

            strSql += " OFFSET " + offset + " ";

            if (limit > 0) {
                strSql += " LIMIT " + limit + " ";
            }

            transaction = HibernateUtil.beginTransaction();
            flashmobsList = HibernateUtil.getSession().createSQLQuery(strSql).
                    addScalar("id", new LongType()).
                    addScalar("flashmob_id", new LongType()).
                    addScalar("owner_id", new LongType()).
                    addScalar("user_flashmob_id", new LongType()).
                    addScalar("name").
                    addScalar("email").
                    addScalar("user_photo").
                    addScalar("user_about").
                    addScalar("photo").
                    addScalar("date_create", new LongType()).
                    addScalar("description").
                    addScalar("date_end", new LongType()).
                    addScalar("photo_report").
                    addScalar("video_report").
                    addScalar("like_count", new BigIntegerType()).
                    setResultTransformer(Transformers.aliasToBean(entityObject.getClass())).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return flashmobsList;
    }

    @SuppressWarnings("unchecked")
    public BigInteger getAllUsersReportsCount() throws SelectDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();

            String strSql = "SELECT COUNT(*) " +
                    "FROM fm_users_flashmobs uf, fm_users u " +
                    "WHERE u.id = uf.user_id AND status = " + Defines.FLASHMOB_STATUS_FINISHED;

            BigInteger count = (BigInteger) session.createSQLQuery(strSql).uniqueResult();
            transaction.commit();

            return count;
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
    }

    @SuppressWarnings("unchecked")
    public BigInteger getAllUsersReportsCount(String userNameFilter,
                                              String flashmobDescriptionFilter) throws SelectDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();

            String strSql = "SELECT COUNT(*) " +
                    " FROM fm_users_flashmobs uf, fm_users u, fm_flashmobs f " +
                    " WHERE u.id = uf.user_id AND f.id = uf.flashmob_id AND status = 4 ";

            if (!StringUtils.isBlank(userNameFilter)) {
                strSql += " AND LOWER(u.name) LIKE('%" + userNameFilter.toLowerCase() + "%') ";
            }

            if (!StringUtils.isBlank(userNameFilter)) {
                strSql += " AND LOWER(f.description) LIKE('%" + flashmobDescriptionFilter.toLowerCase() + "%') ";
            }

            BigInteger count = (BigInteger) session.createSQLQuery(strSql).uniqueResult();
            transaction.commit();

            return count;
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
    }

    /*======================================================================================================*/
    @SuppressWarnings("unchecked")
    public List<Object> getFlashmobReports(long offset, long limit,
                                           long flashmobId,
                                           String userNameFilter,
                                           String flashmobDescriptionFilter, Object entityObject) throws SelectDatabaseException {
        Transaction transaction = null;
        List<Object> flashmobsList;
        try {

            String strSql = "SELECT uf.id, f.id AS flashmob_id, uf.user_id AS owner_id, uf.id AS user_flashmob_id, u.name, u.email, u.photo AS user_photo, u.about AS user_about, f.photo, f.date_create, f.description, uf.date_start, uf.date_end, uf.status, uf.photo_report, uf.video_report " +
                    " FROM fm_users_flashmobs uf, fm_users u, fm_flashmobs f " +
                    " WHERE f.id = " + flashmobId + " AND f.is_accepted_by_admin = TRUE AND f.date_publication <= " + System.currentTimeMillis() / 1000 + " AND u.id = uf.user_id AND f.id = uf.flashmob_id AND status = 4 ";

            if (!StringUtils.isBlank(userNameFilter)) {
                strSql += " AND LOWER(u.name) LIKE('%" + userNameFilter.toLowerCase() + "%') ";
            }

            if (!StringUtils.isBlank(userNameFilter)) {
                strSql += " AND LOWER(f.description) LIKE('%" + flashmobDescriptionFilter.toLowerCase() + "%') ";
            }

            strSql += " OFFSET " + offset + " ";

            if (limit > 0) {
                strSql += " LIMIT " + limit + " ";
            }

            transaction = HibernateUtil.beginTransaction();
            flashmobsList = HibernateUtil.getSession().createSQLQuery(strSql).
                    addScalar("id", new LongType()).
                    addScalar("flashmob_id", new LongType()).
                    addScalar("owner_id", new LongType()).
                    addScalar("user_flashmob_id", new LongType()).
                    addScalar("name").
                    addScalar("email").
                    addScalar("user_photo").
                    addScalar("user_about").
                    addScalar("photo").
                    addScalar("date_create", new LongType()).
                    addScalar("description").
                    addScalar("date_end", new LongType()).
                    addScalar("photo_report").
                    addScalar("video_report").
                    setResultTransformer(Transformers.aliasToBean(entityObject.getClass())).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return flashmobsList;
    }

    @SuppressWarnings("unchecked")
    public BigInteger getFlashmobReportsCount(long flashmobId) throws SelectDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();

            String strSql = "SELECT COUNT(*) " +
                    "FROM fm_users_flashmobs uf, fm_users u " +
                    "WHERE uf.flashmob_id = " + flashmobId + " AND u.id = uf.user_id AND status = 4";

            BigInteger count = (BigInteger) session.createSQLQuery(strSql).uniqueResult();
            transaction.commit();

            return count;
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
    }

    @SuppressWarnings("unchecked")
    public BigInteger getFlashmobReportsCount(long flashmobId,
                                              String userNameFilter,
                                              String flashmobDescriptionFilter) throws SelectDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();

            String strSql = "SELECT COUNT(*) " +
                    " FROM fm_users_flashmobs uf, fm_users u, fm_flashmobs f " +
                    " WHERE uf.flashmob_id = " + flashmobId + " AND  u.id = uf.user_id AND f.id = uf.flashmob_id AND status = 4 ";

            if (!StringUtils.isBlank(userNameFilter)) {
                strSql += " AND LOWER(u.name) LIKE('%" + userNameFilter.toLowerCase() + "%') ";
            }

            if (!StringUtils.isBlank(userNameFilter)) {
                strSql += " AND LOWER(f.description) LIKE('%" + flashmobDescriptionFilter.toLowerCase() + "%') ";
            }

            BigInteger count = (BigInteger) session.createSQLQuery(strSql).uniqueResult();
            transaction.commit();

            return count;
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
    }
    /*======================================================================================================*/

    /*======================================================================================================*/
    @SuppressWarnings("unchecked")
    public List<Object> getFlashmobUsersAccepted(long offset, long limit,
                                                 long flashmobId,
                                                 String userNameFilter,
                                                 String flashmobDescriptionFilter, Object entityObject) throws SelectDatabaseException {
        Transaction transaction = null;
        List<Object> flashmobsList;
        try {

            String strSql = "SELECT uf.id, f.id AS flashmob_id, uf.user_id AS owner_id, uf.id AS user_flashmob_id, u.name, u.email, u.photo AS user_photo, u.about AS user_about, f.photo, f.date_create, f.description, uf.date_start, uf.date_end, uf.status, uf.photo_report, uf.video_report " +
                    " FROM fm_users_flashmobs uf, fm_users u, fm_flashmobs f " +
                    " WHERE f.id = " + flashmobId + " AND f.is_accepted_by_admin = TRUE AND f.date_publication <= " + System.currentTimeMillis() / 1000 + " AND u.id = uf.user_id AND f.id = uf.flashmob_id AND status = " + Defines.FLASHMOB_STATUS_ACCEPTED;

            if (!StringUtils.isBlank(userNameFilter)) {
                strSql += " AND LOWER(u.name) LIKE('%" + userNameFilter.toLowerCase() + "%') ";
            }

            if (!StringUtils.isBlank(userNameFilter)) {
                strSql += " AND LOWER(f.description) LIKE('%" + flashmobDescriptionFilter.toLowerCase() + "%') ";
            }

            strSql += " OFFSET " + offset + " ";

            if (limit > 0) {
                strSql += " LIMIT " + limit + " ";
            }

            transaction = HibernateUtil.beginTransaction();
            flashmobsList = HibernateUtil.getSession().createSQLQuery(strSql).
                    addScalar("id", new LongType()).
                    addScalar("flashmob_id", new LongType()).
                    addScalar("owner_id", new LongType()).
                    addScalar("user_flashmob_id", new LongType()).
                    addScalar("name").
                    addScalar("email").
                    addScalar("user_photo").
                    addScalar("user_about").
                    addScalar("photo").
                    addScalar("date_create", new LongType()).
                    addScalar("description").
                    addScalar("date_start", new LongType()).
                    addScalar("date_end", new LongType()).
                    addScalar("photo_report").
                    addScalar("video_report").
                    setResultTransformer(Transformers.aliasToBean(entityObject.getClass())).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return flashmobsList;
    }

    @SuppressWarnings("unchecked")
    public BigInteger getFlashmobAcceptedUsersCount(long flashmobId) throws SelectDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();

            String strSql = "SELECT COUNT(*) " +
                    "FROM fm_users_flashmobs uf, fm_users u " +
                    "WHERE uf.flashmob_id = " + flashmobId + " AND u.id = uf.user_id AND status = " + Defines.FLASHMOB_STATUS_ACCEPTED;

            BigInteger count = (BigInteger) session.createSQLQuery(strSql).uniqueResult();
            transaction.commit();

            return count;
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
    }

    @SuppressWarnings("unchecked")
    public BigInteger getFlashmobAcceptedUsersCount(long flashmobId,
                                                    String userNameFilter,
                                                    String flashmobDescriptionFilter) throws SelectDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();

            String strSql = "SELECT COUNT(*) " +
                    " FROM fm_users_flashmobs uf, fm_users u, fm_flashmobs f " +
                    " WHERE uf.flashmob_id = " + flashmobId + " AND  u.id = uf.user_id AND f.id = uf.flashmob_id AND status = " + Defines.FLASHMOB_STATUS_ACCEPTED;

            if (!StringUtils.isBlank(userNameFilter)) {
                strSql += " AND LOWER(u.name) LIKE('%" + userNameFilter.toLowerCase() + "%') ";
            }

            if (!StringUtils.isBlank(userNameFilter)) {
                strSql += " AND LOWER(f.description) LIKE('%" + flashmobDescriptionFilter.toLowerCase() + "%') ";
            }

            BigInteger count = (BigInteger) session.createSQLQuery(strSql).uniqueResult();
            transaction.commit();

            return count;
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
    }
    /*======================================================================================================*/

    /*======================================================================================================*/
    @SuppressWarnings("unchecked")
    public List<Object> getFlashmobUsersRejected(long offset, long limit,
                                                 long flashmobId,
                                                 String userNameFilter,
                                                 String flashmobDescriptionFilter, Object entityObject) throws SelectDatabaseException {
        Transaction transaction = null;
        List<Object> flashmobsList;
        try {

            String strSql = "SELECT uf.id, f.id AS flashmob_id, uf.user_id AS owner_id, uf.id AS user_flashmob_id, u.name, u.email, u.photo AS user_photo, u.about AS user_about, f.photo, f.date_create, f.description, uf.date_start, uf.date_end, uf.status, uf.photo_report, uf.video_report " +
                    " FROM fm_users_flashmobs uf, fm_users u, fm_flashmobs f " +
                    " WHERE f.id = " + flashmobId + " AND f.is_accepted_by_admin = TRUE AND f.date_publication <= " + System.currentTimeMillis() / 1000 + " AND u.id = uf.user_id AND f.id = uf.flashmob_id AND status = " + Defines.FLASHMOB_STATUS_REJECTED;

            if (!StringUtils.isBlank(userNameFilter)) {
                strSql += " AND LOWER(u.name) LIKE('%" + userNameFilter.toLowerCase() + "%') ";
            }

            if (!StringUtils.isBlank(userNameFilter)) {
                strSql += " AND LOWER(f.description) LIKE('%" + flashmobDescriptionFilter.toLowerCase() + "%') ";
            }

            strSql += " OFFSET " + offset + " ";

            if (limit > 0) {
                strSql += " LIMIT " + limit + " ";
            }

            transaction = HibernateUtil.beginTransaction();
            flashmobsList = HibernateUtil.getSession().createSQLQuery(strSql).
                    addScalar("id", new LongType()).
                    addScalar("flashmob_id", new LongType()).
                    addScalar("owner_id", new LongType()).
                    addScalar("user_flashmob_id", new LongType()).
                    addScalar("name").
                    addScalar("email").
                    addScalar("user_photo").
                    addScalar("user_about").
                    addScalar("photo").
                    addScalar("date_create", new LongType()).
                    addScalar("description").
                    addScalar("date_end", new LongType()).
                    addScalar("photo_report").
                    addScalar("video_report").
                    setResultTransformer(Transformers.aliasToBean(entityObject.getClass())).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return flashmobsList;
    }

    @SuppressWarnings("unchecked")
    public BigInteger getFlashmobRejectedUsersCount(long flashmobId) throws SelectDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();

            String strSql = "SELECT COUNT(*) " +
                    "FROM fm_users_flashmobs uf, fm_users u " +
                    "WHERE uf.flashmob_id = " + flashmobId + " AND u.id = uf.user_id AND status = " + Defines.FLASHMOB_STATUS_REJECTED;

            BigInteger count = (BigInteger) session.createSQLQuery(strSql).uniqueResult();
            transaction.commit();

            return count;
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
    }

    @SuppressWarnings("unchecked")
    public BigInteger getFlashmobRejectedUsersCount(long flashmobId,
                                                    String userNameFilter,
                                                    String flashmobDescriptionFilter) throws SelectDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();

            String strSql = "SELECT COUNT(*) " +
                    " FROM fm_users_flashmobs uf, fm_users u, fm_flashmobs f " +
                    " WHERE uf.flashmob_id = " + flashmobId + " AND  u.id = uf.user_id AND f.id = uf.flashmob_id AND status = " + Defines.FLASHMOB_STATUS_REJECTED;

            if (!StringUtils.isBlank(userNameFilter)) {
                strSql += " AND LOWER(u.name) LIKE('%" + userNameFilter.toLowerCase() + "%') ";
            }

            if (!StringUtils.isBlank(userNameFilter)) {
                strSql += " AND LOWER(f.description) LIKE('%" + flashmobDescriptionFilter.toLowerCase() + "%') ";
            }

            BigInteger count = (BigInteger) session.createSQLQuery(strSql).uniqueResult();
            transaction.commit();

            return count;
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
    }
    /*======================================================================================================*/

    @SuppressWarnings("unchecked")
    public BigInteger getBusinessUsersReportsCount() throws SelectDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();

            String strSql = "SELECT COUNT(*) " +
                    "FROM fm_users_flashmobs uf, fm_users u " +
                    "WHERE u.is_business_user = TRUE AND u.id = uf.user_id AND status = 4";

            BigInteger count = (BigInteger) session.createSQLQuery(strSql).uniqueResult();
            transaction.commit();

            return count;
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
    }

    @SuppressWarnings("unchecked")
    public BigInteger getBusinessUsersReportsCount(String userNameFilter,
                                                   String flashmobDescriptionFilter) throws SelectDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();

            String strSql = "SELECT COUNT(*) " +
                    " FROM fm_users_flashmobs uf, fm_users u, fm_flashmobs f " +
                    "WHERE u.is_business_user = TRUE AND u.id = uf.user_id AND f.id = uf.flashmob_id AND status = 4";

            if (!StringUtils.isBlank(userNameFilter)) {
                strSql += " AND LOWER(u.name) LIKE('%" + userNameFilter.toLowerCase() + "%') ";
            }

            if (!StringUtils.isBlank(userNameFilter)) {
                strSql += " AND LOWER(f.description) LIKE('%" + flashmobDescriptionFilter.toLowerCase() + "%') ";
            }

            BigInteger count = (BigInteger) session.createSQLQuery(strSql).uniqueResult();
            transaction.commit();

            return count;
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
    }

    @SuppressWarnings("unchecked")
    public List<Object> getFlashmobListByOwnerId(long uid, Object entityObject) throws SelectDatabaseException {
        Transaction transaction = null;
        List<Object> flashmobsList;
        try {

            String strSQL = "SELECT f.id, uf.user_id AS owner_id, uf.id AS user_flashmob_id, u.name, u.email, u.photo AS user_photo, f.owner_id AS creater_id, f.photo, f.date_create, f.description, uf.date_start, uf.date_end, uf.status, uf.photo_report, uf.video_report, ( SELECT count(*) AS count " +
                    "           FROM fm_users_flashmobs " +
                    "          WHERE fm_users_flashmobs.flashmob_id = f.id AND status = " + Defines.FLASHMOB_STATUS_FINISHED + ") AS people_count, ( SELECT count(*) AS count " +
                    "           FROM fm_likes " +
                    "          WHERE fm_likes.is_unliked = FALSE AND fm_likes.user_flashmob_id = uf.id) AS like_count, ( SELECT count(*) AS count " +
                    "           FROM fm_likes " +
                    "          WHERE fm_likes.is_unliked = FALSE AND fm_likes.user_flashmob_id = uf.id AND fm_likes.user_id = " + uid + ") AS is_me_liked, ( SELECT count(*) AS count " +
                    "           FROM fm_comments " +
                    "          WHERE fm_comments.user_flashmob_id = uf.id) AS comment_count " +
                    "   FROM fm_users u, fm_users_flashmobs uf, fm_flashmobs f " +
                    "  WHERE f.is_accepted_by_admin = TRUE AND f.date_publication <= " + System.currentTimeMillis() / 1000 + " AND f.owner_id = " + uid + " AND u.id = uf.user_id AND uf.flashmob_id = f.id AND uf.is_default_report = TRUE AND status = " + Defines.FLASHMOB_STATUS_FINISHED;

            transaction = HibernateUtil.beginTransaction();
            flashmobsList = HibernateUtil.getSession().createSQLQuery(strSQL).
                    addScalar("id", new LongType()).
                    addScalar("owner_id", new LongType()).
                    addScalar("user_flashmob_id", new LongType()).
                    addScalar("name").
                    addScalar("email").
                    addScalar("user_photo").
                    addScalar("photo").
                    addScalar("date_create", new LongType()).
                    addScalar("description").
                    addScalar("date_end", new LongType()).
                    addScalar("photo_report").
                    addScalar("video_report").
                    addScalar("people_count", new BigIntegerType()).
                    addScalar("like_count", new BigIntegerType()).
                    addScalar("is_me_liked", new BigIntegerType()).
                    addScalar("comment_count", new BigIntegerType()).
                    setResultTransformer(Transformers.aliasToBean(entityObject.getClass())).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return flashmobsList;
    }

    @SuppressWarnings("unchecked")
    public List<Object> getFinishedFlashmobsListByOwnerIdWithPaging(
            long uid, int offset, int limit,
            Object entityObject) throws SelectDatabaseException {
        Transaction transaction = null;
        List<Object> flashmobsList;
        try {

            String strSQL = "SELECT f.id AS id, uf.id AS user_flashmob_id, f.photo AS photo, f.date_create AS date_create, f.description AS description, " +
                    "   (SELECT COUNT(*) FROM fm_users_flashmobs WHERE flashmob_id = f.id AND status = " + Defines.FLASHMOB_STATUS_FINISHED + ") AS people_count " +
                    "FROM fm_flashmobs f " +
                    "INNER JOIN fm_users_flashmobs uf ON (uf.flashmob_id = f.id) " +
                    "WHERE f.is_accepted_by_admin = TRUE AND f.date_publication <= " + System.currentTimeMillis() / 1000 + " AND f.owner_id = " + uid + " AND uf.user_id = f.owner_id AND uf.status = " + Defines.FLASHMOB_STATUS_FINISHED + " " +
                    "OFFSET " + offset;

            if (limit > 0) {
                strSQL += " LIMIT " + limit;
            }

            transaction = HibernateUtil.beginTransaction();
            flashmobsList = HibernateUtil.getSession().createSQLQuery(strSQL).
                    addScalar("id", new LongType()).
                    addScalar("user_flashmob_id", new LongType()).
                    addScalar("photo").
                    addScalar("date_create", new LongType()).
                    addScalar("description").
                    addScalar("people_count", new BigIntegerType()).
                    setResultTransformer(Transformers.aliasToBean(entityObject.getClass())).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return flashmobsList;
    }

    @SuppressWarnings("unchecked")
    public BigInteger getFinishedFlashmobsCountByOwnerId(long uid) throws SelectDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();

            String sbSql = "SELECT COUNT(*) " +
                    "FROM fm_flashmobs f " +
                    "INNER JOIN fm_users_flashmobs uf ON (uf.flashmob_id = f.id) " +
                    "WHERE f.is_accepted_by_admin = TRUE AND f.date_publication <= " + System.currentTimeMillis() / 1000 + " AND f.owner_id = " + uid + " AND uf.user_id = f.owner_id AND uf.status = " + Defines.FLASHMOB_STATUS_FINISHED;

            @SuppressWarnings("unchecked")
            BigInteger count = (BigInteger) session.createSQLQuery(sbSql).uniqueResult();
            transaction.commit();

            return count;
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
    }

    @SuppressWarnings("unchecked")
    public List<Object> getInteresting(Object entityObject, long startDate, long endDate) throws SelectDatabaseException {
        Transaction transaction = null;
        List<Object> flashmobsList;
        try {

            String strSQL = "SELECT DISTINCT uf.user_id AS uid, l.user_flashmob_id, uf.photo_report, uf.video_report " +
                    "FROM fm_likes AS l, fm_users_flashmobs AS uf " +
                    "WHERE l.is_unliked = FALSE AND uf.id = l.user_flashmob_id AND uf.status = " + Defines.FLASHMOB_STATUS_FINISHED + " AND l.date >= " + startDate + " AND l.date <= " + endDate;

            transaction = HibernateUtil.beginTransaction();
            flashmobsList = HibernateUtil.getSession().createSQLQuery(strSQL).
                    addScalar("uid", new LongType()).
                    addScalar("photo_report").
                    addScalar("video_report").
                    addScalar("user_flashmob_id", new LongType()).
                    setResultTransformer(Transformers.aliasToBean(entityObject.getClass())).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return flashmobsList;
    }

    @SuppressWarnings("unchecked")
    public List<Object> searchByTags(String[] tags, String description,
                                     Object entityObject) throws SelectDatabaseException {
        List<Object> resultList;
        Transaction transaction = null;
        try {

            StringBuilder sb = new StringBuilder("SELECT DISTINCT uf.user_id AS uid, uf.id AS user_flashmob_id, uf.photo_report " +
                    "FROM fm_flashmob_tags ft, fm_users_flashmobs uf, fm_flashmobs f " +
                    "WHERE ");

            if (tags != null) {
                sb.append(" (");
                int i = 0;
                for (String tag : tags) {
                    if (i > 0)
                        sb.append(" OR ");

                    sb.append("ft.tag = '").append(tag.toLowerCase()).append("'");
                    i++;
                }
                sb.append(") AND ");
            }

            sb.append(" ft.flashmob_id = uf.flashmob_id AND f.id = uf.flashmob_id AND uf.status = " + Defines.FLASHMOB_STATUS_FINISHED);

            if (!StringUtils.isBlank(description))
                sb.append(" AND LOWER(f.description) LIKE('%").append(description.toLowerCase()).append("%') ");

            Session session = HibernateUtil.getSession();
            transaction = session.beginTransaction();

            resultList = session.createSQLQuery(sb.toString()).
                    addScalar("uid", new LongType()).
                    addScalar("photo_report").
                    addScalar("user_flashmob_id", new LongType()).
                    setResultTransformer(Transformers.aliasToBean(entityObject.getClass())).list();

            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return resultList;
    }

    @SuppressWarnings("unchecked")
    public List<FlashmobsUsersAssignedOn> getFlashmobUsersAssignedOn(long flashmobId) throws SelectDatabaseException {
        Transaction transaction = null;
        List<FlashmobsUsersAssignedOn> flashmobsUsersAssignedOnList;
        try {

            String strSql = "SELECT uf.id, f.id AS flashmob_id, uf.id AS user_flashmob_id, u.name " +
                    " FROM fm_users_flashmobs uf, fm_users u, fm_flashmobs f " +
                    " WHERE f.id = " + flashmobId + " AND f.is_accepted_by_admin = TRUE AND u.id = uf.user_id AND f.id = uf.flashmob_id AND status = " + Defines.FLASHMOB_STATUS_CREATED;

            transaction = HibernateUtil.beginTransaction();
            flashmobsUsersAssignedOnList = HibernateUtil.getSession().createSQLQuery(strSql).
                    addScalar("id", new LongType()).
                    addScalar("flashmob_id", new LongType()).
                    addScalar("user_flashmob_id", new LongType()).
                    addScalar("name").
                    setResultTransformer(Transformers.aliasToBean((new FlashmobsUsersAssignedOn()).getClass())).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return flashmobsUsersAssignedOnList;
    }

    @SuppressWarnings("unchecked")
    public List<FlashmobsUsersAssignedOn> getFlashmobUsersAssignedOnDate(long datePublication) throws SelectDatabaseException {
        Transaction transaction = null;
        List<FlashmobsUsersAssignedOn> flashmobsUsersAssignedOnList;
        try {

            String strSql = "SELECT uf.id, f.id AS flashmob_id, uf.id AS user_flashmob_id, u.name " +
                    " FROM fm_users_flashmobs uf, fm_users u, fm_flashmobs f " +
                    " WHERE f.date_publication = " + datePublication + " AND f.is_accepted_by_admin = TRUE AND u.id = uf.user_id AND f.id = uf.flashmob_id AND status = " + Defines.FLASHMOB_STATUS_CREATED;

            transaction = HibernateUtil.beginTransaction();
            flashmobsUsersAssignedOnList = HibernateUtil.getSession().createSQLQuery(strSql).
                    addScalar("id", new LongType()).
                    addScalar("flashmob_id", new LongType()).
                    addScalar("user_flashmob_id", new LongType()).
                    addScalar("name").
                    setResultTransformer(Transformers.aliasToBean((new FlashmobsUsersAssignedOn()).getClass())).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return flashmobsUsersAssignedOnList;
    }

    public List<Map<String, Long>> create(FmFlashmobs record, List<Long> categoriesIds, List<String> tags,
                                          List<Long> friends_ids, List<String> languagesList,
                                          boolean send_into_abyss, boolean isBusinessUser, boolean isDefault,
                                          int flashmobUsersCount, long dateAssign) throws BaseException {
        Transaction transaction = null;
        try {
            Session session = HibernateUtil.getSession();
            transaction = session.beginTransaction();
            session.persist(record);

            List<Map<String, Long>> returnObject = new ArrayList<>();

            if (!isDefault) {
                // Create flashmob's categories
                if (categoriesIds != null && categoriesIds.size() > 0) {
                    for (Long catId : categoriesIds) {
                        FmFlashmobCategories flashmobCategories = new FmFlashmobCategories();
                        flashmobCategories.setFlashmobId(record.getId());
                        flashmobCategories.setCategoryId(catId);
                        session.persist(flashmobCategories);
                    }
                }

                // Create flashmob's tags
                if (tags != null && tags.size() > 0) {
                    for (String tag : tags) {
                        FmFlashmobTags flashmobTags = new FmFlashmobTags();
                        flashmobTags.setFlashmobId(record.getId());
                        flashmobTags.setTag(StringUtils.trimToEmpty(tag).toLowerCase());
                        session.persist(flashmobTags);
                    }
                }

                if (!send_into_abyss) {
                    if (friends_ids != null && friends_ids.size() > 0) {

                        // Get users ids with setting 'Get flashmobs from friends only = TRUE'
                        FmSettingsDao settingsDao = new FmSettingsDao();
                        friends_ids = settingsDao.getUsersIdsWithSetting_GetFlashmobsFromFriendsOnly_ON(friends_ids, session);

                        if (friends_ids != null && friends_ids.size() > 0) {
                            int i = 0;
                            for (Long friendId : friends_ids) {
                                i++;
                                FmUsersFlashmobs usersFlashmobs = new FmUsersFlashmobs();
                                usersFlashmobs.setUserId(friendId);
                                usersFlashmobs.setFlashmob_Id(record.getId());
                                usersFlashmobs.setStatus(1);
                                usersFlashmobs.setDate_assign(dateAssign);
                                usersFlashmobs.setDate_end(0l);
                                usersFlashmobs.setFriendshipWithUid(0l);
                                session.persist(usersFlashmobs);

                                if (i % 25 == 0) { //25, same as the JDBC batch size
                                    //flush a batch of inserts and release memory:
                                    session.flush();
                                    session.clear();
                                }

                                Map<String, Long> returnEl = new HashMap<>();
                                returnEl.put("uid", friendId);
                                returnEl.put("userFlashmobId", usersFlashmobs.getId());
                                returnObject.add(returnEl);
                            }
                        }
                    }

                    // Assign the flashmob to me
                    FmUsersFlashmobs usersFlashmobs = new FmUsersFlashmobs();
                    usersFlashmobs.setUserId(record.getOwner_id());
                    usersFlashmobs.setFlashmob_Id(record.getId());
                    usersFlashmobs.setStatus(4);
                    usersFlashmobs.setDate_end(System.currentTimeMillis() / 1000);
                    usersFlashmobs.setPhoto_report(record.getPhoto());
                    usersFlashmobs.setVideo_report(record.getVideo());
                    usersFlashmobs.setFriendshipWithUid(0l);
                    usersFlashmobs.setIsIsDefaultReport(true);
                    session.persist(usersFlashmobs);
                } else {
                    // Assign the flashmob to me
                    FmUsersFlashmobs usersFlashmobs = new FmUsersFlashmobs();
                    usersFlashmobs.setUserId(record.getOwner_id());
                    usersFlashmobs.setFlashmob_Id(record.getId());
                    usersFlashmobs.setStatus(4);
                    usersFlashmobs.setDate_end(System.currentTimeMillis() / 1000);
                    usersFlashmobs.setPhoto_report(record.getPhoto());
                    usersFlashmobs.setVideo_report(record.getVideo());
                    usersFlashmobs.setFriendshipWithUid(0l);
                    usersFlashmobs.setIsIsDefaultReport(true);
                    session.persist(usersFlashmobs);

                    // Get my languages list (by my uid)
                    FmUsersLanguagesDao usersLanguagesDao = new FmUsersLanguagesDao();
                    if (languagesList == null || languagesList.size() == 0) {
                        languagesList = usersLanguagesDao.getLanguagesList(record.getOwner_id(), session);
                        if (languagesList == null || languagesList.size() == 0) {
                            languagesList = new ArrayList<>(1);
                            languagesList.add("ru");
                        }
                    }

                    // Get users ids list by languages list
                    List<Long> usersIds = usersLanguagesDao.getUsersIdsByLanguagesList(languagesList, session);

                    // Get users ids list by categories ids list
                    FmUsersDao usersDao = new FmUsersDao();
                    //usersIds = usersDao.getUsersIdsForAbyss(usersIds, categoriesIds, session);

                    // Assign flashmob to users
                    if (usersIds != null && usersIds.size() > 0) {

                        // Get users ids with setting 'Get flashmobs = TRUE'
                        FmSettingsDao settingsDao = new FmSettingsDao();
                        usersIds = settingsDao.getUsersIdsWithSetting_GetFlashmobs_ON(usersIds, session);

                        if (usersIds != null && usersIds.size() > 0) {
                            int i = 0;
                            for (Long userId : usersIds) {

                                // check max count of the recipients
                                if (isBusinessUser && i == flashmobUsersCount) {
                                    session.flush();
                                    session.clear();
                                    break;
                                }

                                usersFlashmobs = new FmUsersFlashmobs();
                                if (userId != record.getOwner_id()) {
                                    usersFlashmobs.setUserId(userId);
                                    usersFlashmobs.setFlashmob_Id(record.getId());
                                    usersFlashmobs.setStatus(1);
                                    usersFlashmobs.setDate_assign(dateAssign);
                                    usersFlashmobs.setDate_end(0l);
                                    usersFlashmobs.setFriendshipWithUid(0l);
                                    usersFlashmobs.setPhoto_report(record.getPhoto());
                                    session.persist(usersFlashmobs);
                                }

                                if (i % 25 == 0) { //25, same as the JDBC batch size
                                    //flush a batch of inserts and release memory:
                                    session.flush();
                                    session.clear();
                                }

                                Map<String, Long> returnEl = new HashMap<>();
                                returnEl.put("uid", userId);
                                returnEl.put("userFlashmobId", usersFlashmobs.getId());
                                returnObject.add(returnEl);

                                i++;
                            }
                        }
                    }
                }
            }

            session.flush();
            session.clear();

            transaction.commit();

            return returnObject;
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void update(FmFlashmobs record) throws BaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().saveOrUpdate(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void delete(FmFlashmobs record) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().delete(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }
}
