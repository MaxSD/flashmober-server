package com.wds.dao;

import com.wds.database.HibernateUtil;
import com.wds.entity.PasswordRecovery;
import com.wds.exception.v1.CrudDatabaseException;
import com.wds.exception.v1.SelectDatabaseException;
import org.hibernate.Criteria;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

@Service("PasswordRecoveryDao")
public class PasswordRecoveryDao {

    public PasswordRecovery findBySessionId(String sessionId) throws SelectDatabaseException {
        PasswordRecovery passwordRecovery;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(PasswordRecovery.class).
                    add(Restrictions.eq("sessionId", sessionId));
            passwordRecovery = (PasswordRecovery) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return passwordRecovery;
    }

    public void create(PasswordRecovery passwordRecovery) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().saveOrUpdate(passwordRecovery);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void delete(String sessionId) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            String hql = "delete from PasswordRecovery where sessionId = :sessionId";
            HibernateUtil.getSession().createQuery(hql).setString("sessionId", sessionId).executeUpdate();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }
}
