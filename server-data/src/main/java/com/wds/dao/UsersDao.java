package com.wds.dao;

import com.wds.database.HibernateUtil;
import com.wds.entity.User;
import com.wds.exception.v1.CrudDatabaseException;
import com.wds.exception.v1.SelectDatabaseException;
import org.hibernate.Criteria;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("UsersDao")
public class UsersDao {

    public User findById(long id) throws SelectDatabaseException {
        User user;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(User.class).
                    add(Restrictions.eq("id", id));
            user = (User) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return user;
    }

    public User findByLogin(String login) throws SelectDatabaseException {
        User user;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(User.class).
                    add(Restrictions.eq("login", login));
            user = (User) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return user;
    }

    public User findByEmail(String email) throws SelectDatabaseException {
        User user;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(User.class).
                    add(Restrictions.eq("email", email));
            user = (User) criteria.uniqueResult();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return user;
    }

    @SuppressWarnings("unchecked")
    public List<User> getAll() throws SelectDatabaseException {
        Transaction transaction = null;
        List<User> userList;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(User.class).
                    addOrder(Order.asc("id")).
                    setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            userList = criteria.list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new SelectDatabaseException();
        }
        return userList;
    }

    public void create(User record) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().saveOrUpdate(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void update(User record) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().saveOrUpdate(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }

    public void delete(User record) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().delete(record);
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            throw new CrudDatabaseException();
        }
    }
}
