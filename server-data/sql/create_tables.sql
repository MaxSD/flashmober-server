-- ***********************************************************************************
-- Table: fm_users
DROP TABLE IF EXISTS fm_users CASCADE;
DROP SEQUENCE IF EXISTS fm_users_id_seq;

-- Sequence: fm_users_id_seq
CREATE SEQUENCE fm_users_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE fm_users_id_seq
OWNER TO flashmober;

CREATE TABLE fm_users
(
  id                                BIGINT  NOT NULL DEFAULT nextval('fm_users_id_seq' :: REGCLASS),
  email                             TEXT             DEFAULT '' :: TEXT,
  password                          TEXT             DEFAULT '' :: TEXT,
  name                              TEXT             DEFAULT '' :: TEXT,
  vk_id                             TEXT             DEFAULT '' :: TEXT,
  fb_id                             TEXT             DEFAULT '' :: TEXT,
  tw_id                             TEXT             DEFAULT '' :: TEXT,
  photo                             TEXT             DEFAULT '' :: TEXT,
  gender                            INTEGER          DEFAULT 0,
  age                               INTEGER          DEFAULT 0,
  about                             TEXT             DEFAULT '' :: TEXT,
  country                           TEXT             DEFAULT '' :: TEXT,
  city                              TEXT             DEFAULT '' :: TEXT,
  site                              TEXT             DEFAULT '' :: TEXT,
  is_business_user                  BOOLEAN NOT NULL DEFAULT FALSE,
  is_locked                         BOOLEAN NOT NULL DEFAULT FALSE,
  lock_comment                      TEXT    NOT NULL DEFAULT '' :: TEXT,
  lock_expire_date                  BIGINT  NOT NULL DEFAULT 0,
  id_role                           BIGINT           DEFAULT 3,
  flashmob_users_count              INTEGER          DEFAULT 0,
  is_flashmobs_deleted_by_moderator BOOLEAN          DEFAULT FALSE,
  flashmobs_deleted_reason          TEXT             DEFAULT '' :: TEXT,
  is_reports_deleted_by_moderator   BOOLEAN          DEFAULT FALSE,
  reports_deleted_reason            TEXT             DEFAULT '' :: TEXT,
  CONSTRAINT fm_users_pkey PRIMARY KEY (id),
  CONSTRAINT "User_Role" FOREIGN KEY (id_role)
  REFERENCES srv_roles (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fm_users_email_key UNIQUE (email),
  CONSTRAINT unique_name UNIQUE (name)
)
WITH (
OIDS =FALSE
);
ALTER TABLE fm_users
OWNER TO flashmober;

-- Default: users
INSERT INTO fm_users (email, password, name, vk_id, fb_id, tw_id, photo, gender, age, about, country, city, site, is_business_user, is_locked, lock_comment, lock_expire_date, id_role,
                      flashmob_users_count, is_flashmobs_deleted_by_moderator, is_reports_deleted_by_moderator)
VALUES
  ('admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', '', '', '', '', 0, 0, '', '', '', '', FALSE, FALSE, '', 0, 1,
   0, FALSE, FALSE);

INSERT INTO fm_users (email, password, name, vk_id, fb_id, tw_id, photo, gender, age, about, country, city, site, is_business_user, is_locked, lock_comment, lock_expire_date, id_role,
                      flashmob_users_count, is_flashmobs_deleted_by_moderator, is_reports_deleted_by_moderator)
VALUES
  ('FlashMober', '21232f297a57a5a743894a0e4a801fc3', 'FlashMober', '', '', '', '', 0, 0, '', '', '', '', FALSE, FALSE,
   '', 0, 1, 0, FALSE, FALSE);

-- ***********************************************************************************
-- Table: fm_users_categories
DROP TABLE IF EXISTS fm_users_categories CASCADE;
DROP SEQUENCE IF EXISTS fm_users_categories_id_seq;

-- Sequence: fm_users_categories_id_seq
CREATE SEQUENCE fm_users_categories_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE fm_users_categories_id_seq
OWNER TO flashmober;

CREATE TABLE fm_users_categories
(
  id          BIGINT NOT NULL DEFAULT nextval('fm_users_categories_id_seq' :: REGCLASS),
  user_id     BIGINT,
  category_id BIGINT,
  CONSTRAINT fm_users_categories_pkey PRIMARY KEY (id),
  CONSTRAINT "User" FOREIGN KEY (user_id)
  REFERENCES fm_users (id) MATCH SIMPLE
  ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
OIDS =FALSE
);
ALTER TABLE fm_users_categories
OWNER TO flashmober;

-- Index: "fki_UC_User"
DROP INDEX IF EXISTS "fki_UC_User";
CREATE INDEX "fki_UC_User"
ON fm_users_categories
USING BTREE
(user_id);

-- ***********************************************************************************
-- Table: fm_users_categories
DROP TABLE IF EXISTS fm_users_categories CASCADE;
DROP SEQUENCE IF EXISTS fm_users_categories_id_seq;

-- Sequence: fm_users_categories_id_seq
CREATE SEQUENCE fm_users_categories_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE fm_users_categories_id_seq
OWNER TO flashmober;

CREATE TABLE fm_users_categories
(
  id          BIGINT NOT NULL DEFAULT nextval('fm_users_categories_id_seq' :: REGCLASS),
  user_id     BIGINT,
  category_id BIGINT,
  CONSTRAINT fm_users_categories_pkey PRIMARY KEY (id),
  CONSTRAINT "User" FOREIGN KEY (user_id)
  REFERENCES fm_users (id) MATCH SIMPLE
  ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
OIDS =FALSE
);
ALTER TABLE fm_users_categories
OWNER TO flashmober;

-- Index: "fki_UC_User"
DROP INDEX IF EXISTS "fki_UC_User";
CREATE INDEX "fki_UC_User"
ON fm_users_categories
USING BTREE
(user_id);

-- ***********************************************************************************
-- Table: fm_friends
DROP TABLE IF EXISTS fm_friends CASCADE;
DROP SEQUENCE IF EXISTS fm_friends_id_seq;

-- Sequence: fm_friends_id_seq
CREATE SEQUENCE fm_friends_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE fm_friends_id_seq
OWNER TO flashmober;

CREATE TABLE fm_friends
(
  id       BIGINT NOT NULL DEFAULT nextval('fm_friends_id_seq' :: REGCLASS),
  user1_id BIGINT,
  user2_id BIGINT,
  status   INTEGER         DEFAULT 1,
  CONSTRAINT fm_friends_pkey PRIMARY KEY (id)
)
WITH (
OIDS =FALSE
);
ALTER TABLE fm_friends
OWNER TO flashmober;

-- ***********************************************************************************
-- Table: fm_flashmobs
DROP TABLE IF EXISTS fm_flashmobs CASCADE;
DROP SEQUENCE IF EXISTS fm_flashmobs_id_seq;

-- Sequence: fm_flashmobs_id_seq
CREATE SEQUENCE fm_flashmobs_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE fm_flashmobs_id_seq
OWNER TO flashmober;

CREATE TABLE fm_flashmobs
(
  id                   BIGINT  NOT NULL DEFAULT nextval('fm_flashmobs_id_seq' :: REGCLASS),
  owner_id             BIGINT,
  photo                TEXT,
  description          TEXT,
  send_into_abyss      BOOLEAN NOT NULL DEFAULT FALSE,
  date_create          BIGINT,
  date_publication     BIGINT           DEFAULT 0,
  moderation           BOOLEAN          DEFAULT FALSE,
  is_accepted_by_admin BOOLEAN          DEFAULT TRUE,
  comment              TEXT    NOT NULL DEFAULT '' :: TEXT,
  video                TEXT             DEFAULT '' :: TEXT,
  is_default           BOOLEAN          DEFAULT FALSE,
  CONSTRAINT fm_flashmobs_pkey PRIMARY KEY (id)
)
WITH (
OIDS =FALSE
);
ALTER TABLE fm_flashmobs
OWNER TO flashmober;

-- ***********************************************************************************
-- Table: fm_flashmob_categories
DROP TABLE IF EXISTS fm_flashmob_categories CASCADE;
DROP SEQUENCE IF EXISTS fm_flashmob_categories_id_seq;

-- Sequence: fm_flashmob_categories_id_seq
CREATE SEQUENCE fm_flashmob_categories_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE fm_flashmob_categories_id_seq
OWNER TO flashmober;

CREATE TABLE fm_flashmob_categories
(
  id          BIGINT NOT NULL DEFAULT nextval('fm_flashmob_categories_id_seq' :: REGCLASS),
  flashmob_id BIGINT,
  category_id BIGINT,
  CONSTRAINT fm_flashmob_categories_pkey PRIMARY KEY (id),
  CONSTRAINT "Flashmob_Categories_Flashmob_fk" FOREIGN KEY (flashmob_id)
  REFERENCES fm_flashmobs (id) MATCH SIMPLE
  ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
OIDS =FALSE
);
ALTER TABLE fm_flashmob_categories
OWNER TO flashmober;

-- ***********************************************************************************
-- Table: fm_flashmob_tags
DROP TABLE IF EXISTS fm_flashmob_tags CASCADE;
DROP SEQUENCE IF EXISTS fm_flashmob_tags_id_seq;

-- Sequence: fm_flashmob_tags_id_seq
CREATE SEQUENCE fm_flashmob_tags_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE fm_flashmob_tags_id_seq
OWNER TO flashmober;

CREATE TABLE fm_flashmob_tags
(
  id          BIGINT NOT NULL DEFAULT nextval('fm_flashmob_tags_id_seq' :: REGCLASS),
  flashmob_id BIGINT,
  tag         TEXT,
  CONSTRAINT fm_flashmob_tags_pkey PRIMARY KEY (id)
)
WITH (
OIDS =FALSE
);
ALTER TABLE fm_flashmob_tags
OWNER TO flashmober;

-- ***********************************************************************************
-- Table: fm_users_flashmobs
DROP TABLE IF EXISTS fm_users_flashmobs;
DROP SEQUENCE IF EXISTS fm_users_flashmobs_id_seq;

-- Sequence: fm_users_flashmobs_id_seq
CREATE SEQUENCE fm_users_flashmobs_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE fm_users_flashmobs_id_seq
OWNER TO flashmober;

CREATE TABLE fm_users_flashmobs
(
  id                  BIGINT NOT NULL DEFAULT nextval('fm_users_flashmobs_id_seq' :: REGCLASS),
  user_id             BIGINT,
  flashmob_id         BIGINT,
  status              INTEGER         DEFAULT 0,
  date_assign         BIGINT          DEFAULT 0,
  date_start          BIGINT,
  date_end            BIGINT,
  photo_report        TEXT            DEFAULT '' :: TEXT,
  video_report        TEXT            DEFAULT '' :: TEXT,
  friendship_with_uid BIGINT NOT NULL DEFAULT 0,
  is_default_report   BOOLEAN         DEFAULT FALSE,
  CONSTRAINT fm_users_flashmobs_pkey PRIMARY KEY (id),
  CONSTRAINT "Flashmob" FOREIGN KEY (flashmob_id)
  REFERENCES fm_flashmobs (id) MATCH SIMPLE
  ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT "User" FOREIGN KEY (user_id)
  REFERENCES fm_users (id) MATCH SIMPLE
  ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
OIDS =FALSE
);
ALTER TABLE fm_users_flashmobs
OWNER TO flashmober;

-- ***********************************************************************************
-- Table: fm_users_languages
DROP TABLE IF EXISTS fm_users_languages CASCADE;
DROP SEQUENCE IF EXISTS fm_users_languages_id_seq;

-- Sequence: fm_users_languages_id_seq
CREATE SEQUENCE fm_users_languages_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE fm_users_languages_id_seq
OWNER TO flashmober;

CREATE TABLE fm_users_languages
(
  id       BIGINT NOT NULL DEFAULT nextval('fm_users_languages_id_seq' :: REGCLASS),
  user_id  BIGINT,
  language TEXT,
  CONSTRAINT fm_users_languages_pkey PRIMARY KEY (id),
  CONSTRAINT "User" FOREIGN KEY (user_id)
  REFERENCES fm_users (id) MATCH SIMPLE
  ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT "User_Language" UNIQUE (user_id, language)
)
WITH (
OIDS =FALSE
);
ALTER TABLE fm_users_languages
OWNER TO flashmober;

-- ***********************************************************************************
-- Table: fm_comments
DROP TABLE IF EXISTS fm_comments CASCADE;
DROP SEQUENCE IF EXISTS fm_comments_id_seq;

-- Sequence: fm_comments_id_seq
CREATE SEQUENCE fm_comments_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE fm_comments_id_seq
OWNER TO flashmober;

CREATE TABLE fm_comments
(
  id               BIGINT NOT NULL DEFAULT nextval('fm_comments_id_seq' :: REGCLASS),
  user_id          BIGINT,
  user_flashmob_id BIGINT,
  comment          TEXT,
  date             BIGINT,
  CONSTRAINT fm_comments_pkey PRIMARY KEY (id)
)
WITH (
OIDS =FALSE
);
ALTER TABLE fm_comments
OWNER TO flashmober;

-- ***********************************************************************************
-- Table: fm_likes
DROP TABLE IF EXISTS fm_likes CASCADE;
DROP SEQUENCE IF EXISTS fm_likes_id_seq;

-- Sequence: fm_likes_id_seq
CREATE SEQUENCE fm_likes_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE fm_likes_id_seq
OWNER TO flashmober;

CREATE TABLE fm_likes
(
  id               BIGINT NOT NULL DEFAULT nextval('fm_likes_id_seq' :: REGCLASS),
  user_id          BIGINT,
  user_flashmob_id BIGINT,
  date             BIGINT,
  is_unliked       BOOLEAN         DEFAULT FALSE,
  CONSTRAINT fm_likes_pkey PRIMARY KEY (id),
  CONSTRAINT "UserFlashmobLike" UNIQUE (user_id, user_flashmob_id)
)
WITH (
OIDS =FALSE
);
ALTER TABLE fm_likes
OWNER TO flashmober;

-- ***********************************************************************************
-- View: view_users_flashmobs
DROP VIEW IF EXISTS view_users_flashmobs;
CREATE VIEW view_users_flashmobs AS
  SELECT
    f.id,
    uf.user_id                                                            AS owner_id,
    u.name,
    u.email,
    u.photo                                                               AS user_photo,
    f.owner_id                                                            AS creater_id,
    f.photo,
    f.date_create,
    f.description,
    uf.date_start,
    uf.date_end,
    uf.status,
    uf.photo_report,
    uf.video_report,
    (SELECT count(*) AS count
     FROM fm_users_flashmobs
     WHERE fm_users_flashmobs.flashmob_id = f.id)                         AS people_count,
    (SELECT count(*) AS count
     FROM fm_likes
     WHERE fm_likes.user_flashmob_id = uf.id)                             AS like_count,
    (SELECT count(*) AS count
     FROM fm_likes
     WHERE fm_likes.user_flashmob_id = uf.id AND fm_likes.user_id = u.id) AS is_me_liked,
    (SELECT count(*) AS count
     FROM fm_comments
     WHERE fm_comments.user_flashmob_id = uf.id)                          AS comment_count
  FROM fm_users u, fm_users_flashmobs uf, fm_flashmobs f
  WHERE u.id = uf.user_id AND uf.flashmob_id = f.id;

ALTER TABLE view_users_flashmobs
OWNER TO flashmober;

-- ***********************************************************************************
-- Table: fm_user_token

DROP TABLE IF EXISTS fm_user_token CASCADE;
DROP SEQUENCE IF EXISTS fm_user_token_id_seq;

-- Sequence: fm_user_token_id_seq
CREATE SEQUENCE fm_user_token_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE fm_user_token_id_seq
OWNER TO flashmober;

CREATE TABLE fm_user_token
(
  id         BIGINT  NOT NULL DEFAULT nextval('fm_user_token_id_seq' :: REGCLASS),
  user_id    BIGINT  NOT NULL,
  is_android BOOLEAN NOT NULL DEFAULT FALSE,
  token      TEXT,
  CONSTRAINT fm_user_token_pkey PRIMARY KEY (id),
  CONSTRAINT "User" FOREIGN KEY (user_id)
  REFERENCES fm_users (id) MATCH SIMPLE
  ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT "UserTokenExists" UNIQUE (user_id)
)
WITH (
OIDS =FALSE
);
ALTER TABLE fm_user_token
OWNER TO flashmober;

-- Index: "fki_User"

DROP INDEX IF EXISTS "fki_User";
CREATE INDEX "fki_User"
ON fm_user_token
USING BTREE
(user_id);

-- ***********************************************************************************
-- Table: fm_actions
DROP TABLE IF EXISTS fm_actions CASCADE;
DROP SEQUENCE IF EXISTS fm_actions_id_seq;

-- Sequence: fm_actions_id_seq
CREATE SEQUENCE fm_actions_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE fm_actions_id_seq
OWNER TO flashmober;

CREATE TABLE fm_actions
(
  id                  BIGINT NOT NULL DEFAULT nextval('fm_actions_id_seq' :: REGCLASS),
  action_for_user_id  BIGINT,
  action_from_user_id BIGINT,
  user_flashmob_id    BIGINT,
  type                INTEGER,
  date                BIGINT,
  comment             TEXT            DEFAULT '' :: TEXT,
  CONSTRAINT fm_actions_pkey PRIMARY KEY (id)
)
WITH (
OIDS =FALSE
);
ALTER TABLE fm_actions
OWNER TO flashmober;

-- ***********************************************************************************
-- Table: fm_change_email
DROP TABLE IF EXISTS fm_change_email CASCADE;
DROP SEQUENCE IF EXISTS fm_change_email_id_seq;

-- Sequence: fm_change_email_id_seq
CREATE SEQUENCE fm_change_email_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE fm_change_email_id_seq
OWNER TO flashmober;

CREATE TABLE fm_change_email
(
  id      BIGINT NOT NULL DEFAULT nextval('fm_change_email_id_seq' :: REGCLASS),
  user_id BIGINT,
  email   TEXT,
  code    INTEGER,
  status  INTEGER         DEFAULT 0,
  CONSTRAINT fm_change_email_pkey PRIMARY KEY (id)
)
WITH (
OIDS =FALSE
);
ALTER TABLE fm_change_email
OWNER TO flashmober;

-- ***********************************************************************************
-- Table: fm_settings
DROP TABLE IF EXISTS fm_settings CASCADE;
DROP SEQUENCE IF EXISTS fm_settings_id_seq;

-- Sequence: fm_settings_id_seq
CREATE SEQUENCE fm_settings_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE fm_settings_id_seq
OWNER TO flashmober;

CREATE TABLE fm_settings
(
  id                                 BIGINT  NOT NULL DEFAULT nextval('fm_settings_id_seq' :: REGCLASS),
  user_id                            BIGINT,
  is_get_flashmobs                   BOOLEAN NOT NULL DEFAULT FALSE,
  is_get_flashmobs_from_friends_only BOOLEAN NOT NULL DEFAULT FALSE,
  is_close_profile                   BOOLEAN NOT NULL DEFAULT FALSE,
  is_default_task                    BOOLEAN NOT NULL DEFAULT FALSE,
  CONSTRAINT fm_settings_pkey PRIMARY KEY (id)
)
WITH (
OIDS =FALSE
);
ALTER TABLE fm_settings
OWNER TO flashmober;

-- ***********************************************************************************
-- Table: fm_complaints
DROP TABLE IF EXISTS fm_complaints CASCADE;
DROP SEQUENCE IF EXISTS fm_complaints_id_seq;

-- Sequence: fm_complaints_id_seq
CREATE SEQUENCE fm_complaints_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE fm_complaints_id_seq
OWNER TO flashmober;

CREATE TABLE fm_complaints
(
  id          BIGINT NOT NULL DEFAULT nextval('fm_complaints_id_seq' :: REGCLASS),
  user_id     BIGINT,
  text        TEXT,
  date_create BIGINT,
  CONSTRAINT fm_complaints_pkey PRIMARY KEY (id)
)
WITH (
OIDS =FALSE
);
ALTER TABLE fm_complaints
OWNER TO flashmober;