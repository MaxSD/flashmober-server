-- ***********************************************************************************

-- Table: srv_roles
DROP TABLE IF EXISTS srv_roles CASCADE;
DROP SEQUENCE IF EXISTS srv_roles_id_seq;

-- Sequence: srv_roles_id_seq
CREATE SEQUENCE srv_roles_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE srv_roles_id_seq
OWNER TO flashmober;

-- noinspection SqlResolve
CREATE TABLE srv_roles
(
  id bigint NOT NULL DEFAULT nextval('srv_roles_id_seq'::regclass),
  name text NOT NULL,
  text hstore NOT NULL,
  description hstore NOT NULL,
  CONSTRAINT srv_roles_pkey PRIMARY KEY (id),
  CONSTRAINT srv_roles_name_key UNIQUE (name)
)
WITH (
OIDS=FALSE
);
ALTER TABLE srv_roles
OWNER TO flashmober;

-- Default: srv_roles
INSERT INTO srv_roles (name, text, description) VALUES ('ROLE_ADMIN', '"en"=>"Administrator", "ru"=>"Администратор"', '"en"=>"Administrator", "ru"=>"Администратор"');
INSERT INTO srv_roles (name, text, description) VALUES ('ROLE_MODERATOR', '"en"=>"Moderator", "ru"=>"Моделатор"', '"en"=>"Moderator", "ru"=>"Моделатор"');
INSERT INTO srv_roles (name, text, description) VALUES ('ROLE_USER', '"en"=>"User", "ru"=>"Пользователь"', '"en"=>"User", "ru"=>"Пользователь"');
INSERT INTO srv_roles (name, text, description) VALUES ('ROLE_BUSINESS_USER', '"en"=>"Business user", "ru"=>"Бизнес пользователь"', '"en"=>"Business user", "ru"=>"Бизнес пользователь"');

-- ***********************************************************************************

-- Table: srv_permissions
DROP TABLE IF EXISTS srv_permissions CASCADE;
DROP SEQUENCE IF EXISTS srv_permissions_id_seq;

CREATE SEQUENCE srv_permissions_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE srv_permissions_id_seq
OWNER TO flashmober;

-- noinspection SqlResolve
CREATE TABLE srv_permissions
(
  id bigint NOT NULL DEFAULT nextval('srv_permissions_id_seq'::regclass),
  name text NOT NULL,
  text hstore NOT NULL,
  description hstore NOT NULL,
  CONSTRAINT srv_permissions_pkey PRIMARY KEY (id),
  CONSTRAINT srv_permissions_name_key UNIQUE (name)
)
WITH (
OIDS=FALSE
);
ALTER TABLE srv_permissions
OWNER TO flashmober;

-- Default: srv_permissions
INSERT INTO srv_permissions (name, text, description) VALUES ('PERMISSION_ROOT', '"en"=>"Full rights", "ru"=>"Полные права"', '"en"=>"Full rights", "ru"=>"Полные права"');
INSERT INTO srv_permissions (name, text, description) VALUES ('PERMISSION_CREATE_ROLE', '"en"=>"Create a roles", "ru"=>"Создание ролей"', '"en"=>"Create a roles", "ru"=>"Создание ролей"');
INSERT INTO srv_permissions (name, text, description) VALUES ('PERMISSION_EDIT_ROLE', '"en"=>"Edit a roles", "ru"=>"Редактирование ролей"', '"en"=>"Edit a roles", "ru"=>"Редактирование ролей"');
INSERT INTO srv_permissions (name, text, description) VALUES ('PERMISSION_DELETE_ROLE', '"en"=>"Delete a roles", "ru"=>"Удаление ролей"', '"en"=>"Delete a roles", "ru"=>"Удаление ролей"');

INSERT INTO srv_permissions (name, text, description) VALUES ('PERMISSION_CREATE_USERS', '"en"=>"Create users", "ru"=>"Создание пользователей"', '"en"=>"Create users", "ru"=>"Создание учетных записей пользователей"');
INSERT INTO srv_permissions (name, text, description) VALUES ('PERMISSION_EDIT_USERS', '"en"=>"Edit users", "ru"=>"Редактирование пользователей"', '"en"=>"Edit users", "ru"=>"Редактирование учетный записей пользователей"');
INSERT INTO srv_permissions (name, text, description) VALUES ('PERMISSION_VIEW_USERS', '"en"=>"Vew users", "ru"=>"Просмотр пользователей"', '"en"=>"View users", "ru"=>"Просмотр пользователей"');
INSERT INTO srv_permissions (name, text, description) VALUES ('PERMISSION_DELETE_USERS', '"en"=>"Delete users", "ru"=>"Удаление пользователей"', '"en"=>"Delete users", "ru"=>"Удаление пользователей"');

INSERT INTO srv_permissions (name, text, description) VALUES ('PERMISSION_CREATE_TASKS', '"en"=>"Create tasks", "ru"=>"Создание заданий"', '"en"=>"Create tasks", "ru"=>"Создание заданий"');
INSERT INTO srv_permissions (name, text, description) VALUES ('PERMISSION_EDIT_TASKS', '"en"=>"Edit tasks", "ru"=>"Редактирование заданий"', '"en"=>"Edit tasks", "ru"=>"Редактирование заданий"');
INSERT INTO srv_permissions (name, text, description) VALUES ('PERMISSION_VIEW_TASKS', '"en"=>"Vew tasks", "ru"=>"Просмотр заданий"', '"en"=>"View tasks", "ru"=>"Просмотр заданий"');
INSERT INTO srv_permissions (name, text, description) VALUES ('PERMISSION_DELETE_TASKS', '"en"=>"Delete tasks", "ru"=>"Удаление заданий"', '"en"=>"Delete tasks", "ru"=>"Удаление заданий"');

INSERT INTO srv_permissions (name, text, description) VALUES ('PERMISSION_CREATE_REPORTS', '"en"=>"Create reports", "ru"=>"Создание отчётов"', '"en"=>"Create reports", "ru"=>"Создание отчётов"');
INSERT INTO srv_permissions (name, text, description) VALUES ('PERMISSION_EDIT_REPORTS', '"en"=>"Edit reports", "ru"=>"Редактирование отчётов"', '"en"=>"Edit reports", "ru"=>"Редактирование отчётов"');
INSERT INTO srv_permissions (name, text, description) VALUES ('PERMISSION_VIEW_REPORTS', '"en"=>"Vew reports", "ru"=>"Просмотр отчётов"', '"en"=>"View reports", "ru"=>"Просмотр отчётов"');
INSERT INTO srv_permissions (name, text, description) VALUES ('PERMISSION_DELETE_REPORTS', '"en"=>"Delete reports", "ru"=>"Удаление отчётов"', '"en"=>"Delete reports", "ru"=>"Удаление отчётов"');

INSERT INTO srv_permissions (name, text, description) VALUES ('PERMISSION_CREATE_COMMENTS', '"en"=>"Create comments", "ru"=>"Создание комментариев"', '"en"=>"Create comments", "ru"=>"Создание комментариев"');
INSERT INTO srv_permissions (name, text, description) VALUES ('PERMISSION_EDIT_COMMENTS', '"en"=>"Edit comments", "ru"=>"Редактирование комментариев"', '"en"=>"Edit comments", "ru"=>"Редактирование комментариев"');
INSERT INTO srv_permissions (name, text, description) VALUES ('PERMISSION_VIEW_COMMENTS', '"en"=>"Vew comments", "ru"=>"Просмотр комментариев"', '"en"=>"View comments", "ru"=>"Просмотр комментариев"');
INSERT INTO srv_permissions (name, text, description) VALUES ('PERMISSION_DELETE_COMMENTS', '"en"=>"Delete comments", "ru"=>"Удаление комментариев"', '"en"=>"Delete comments", "ru"=>"Удаление комментариев"');
-- ***********************************************************************************

-- Table: srv_roles_permissions
DROP TABLE IF EXISTS srv_roles_permissions;
DROP SEQUENCE IF EXISTS srv_roles_permissions_id_seq;

CREATE SEQUENCE srv_roles_permissions_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE srv_roles_permissions_id_seq
OWNER TO flashmober;

CREATE TABLE srv_roles_permissions
(
  id bigint NOT NULL DEFAULT nextval('srv_roles_permissions_id_seq'::regclass),
  id_role bigint NOT NULL,
  id_permission bigint NOT NULL,
  CONSTRAINT srv_roles_permissions_pkey PRIMARY KEY (id),
  CONSTRAINT "Permission" FOREIGN KEY (id_permission)
  REFERENCES srv_permissions (id) MATCH SIMPLE
  ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT "Role" FOREIGN KEY (id_role)
  REFERENCES srv_roles (id) MATCH SIMPLE
  ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT "Role_Permission" UNIQUE (id_role, id_permission)
)
WITH (
OIDS=FALSE
);
ALTER TABLE srv_roles_permissions
OWNER TO flashmober;
DROP INDEX IF EXISTS "fki_Permission";
CREATE INDEX "fki_Permission"
ON srv_roles_permissions
USING btree
(id_permission);

DROP INDEX IF EXISTS "fki_Role";
CREATE INDEX "fki_Role"
ON srv_roles_permissions
USING btree
(id_role);

-- Default: srv_roles_permissions
INSERT INTO srv_roles_permissions (id_role, id_permission) VALUES (1, 1);

-- ***********************************************************************************

-- Table: srv_users
DROP TABLE IF EXISTS srv_users;
DROP SEQUENCE IF EXISTS srv_users_id_seq;

CREATE SEQUENCE srv_users_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE srv_users_id_seq
  OWNER TO flashmober;

CREATE TABLE srv_users
(
  id bigint NOT NULL DEFAULT nextval('srv_users_id_seq'::regclass),
  institution text NOT NULL DEFAULT '',
  ed_inst_id bigint DEFAULT 0,
  login text NOT NULL,
  password text NOT NULL,
  email text,
  id_role bigint,
  CONSTRAINT srv_admins_pkey PRIMARY KEY (id),
  CONSTRAINT "User_Role" FOREIGN KEY (id_role)
      REFERENCES srv_roles (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT srv_admins_email_key UNIQUE (email),
  CONSTRAINT srv_admins_login_key UNIQUE (login)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE srv_users
  OWNER TO flashmober;
DROP INDEX IF EXISTS "fki_User_Role";
CREATE INDEX "fki_User_Role"
  ON srv_users
  USING btree
  (id_role);

-- Default: srv_users
INSERT INTO srv_users (login, password, email, id_role) VALUES('admin', '$2a$10$L9ieHP45Y4TQQhovMuXwY.oI2E/o.tox5sw7GqKMNe3RJVcHj.eH2', '', 1);

-- ***********************************************************************************

-- Table: srv_settings
DROP TABLE IF EXISTS srv_settings;
DROP SEQUENCE IF EXISTS srv_settings_id_seq;
CREATE SEQUENCE srv_settings_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE srv_settings_id_seq
OWNER TO flashmober;

-- Table: srv_settings
CREATE TABLE srv_settings
(
  id bigint NOT NULL DEFAULT nextval('srv_settings_id_seq'::regclass),
  param_name text NOT NULL,
  param_value text NOT NULL,
  CONSTRAINT srv_settings_pkey PRIMARY KEY (id),
  CONSTRAINT srv_settings_param_name_key UNIQUE (param_name)
)
WITH (
OIDS=FALSE
);
ALTER TABLE srv_settings
OWNER TO flashmober;

-- Default: srv_settings (admin, admin)
INSERT INTO srv_settings (param_name, param_value) VALUES('rest_auth_username', 'FlashMober-server-api');
INSERT INTO srv_settings (param_name, param_value) VALUES('rest_auth_password', 'K7rASH8G3NXlg97BpAmBWAp72JESVXHjD3wDj4');

-- ***********************************************************************************

-- Table: srv_password_recovery
DROP TABLE IF EXISTS srv_password_recovery;
-- Sequence: srv_password_recovery_id_seq
DROP SEQUENCE IF EXISTS srv_password_recovery_id_seq;
CREATE SEQUENCE srv_password_recovery_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE srv_password_recovery_id_seq
OWNER TO flashmober;

-- Table: srv_password_recovery
CREATE TABLE srv_password_recovery
(
  id bigint NOT NULL DEFAULT nextval('srv_password_recovery_id_seq'::regclass),
  date_time timestamp without time zone,
  session_id text,
  user_email text,
  CONSTRAINT srv_password_recovery_pkey PRIMARY KEY (id)
)
WITH (
OIDS=FALSE
);
ALTER TABLE srv_password_recovery
OWNER TO flashmober;
-- ***********************************************************************************