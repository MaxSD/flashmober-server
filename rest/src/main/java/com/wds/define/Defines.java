package com.wds.define;

public class Defines {

    public static int        SETTING_IS_GET_FLASHMOBS = 1;
    public static int        SETTING_IS_GET_FLASHMOBS_FROM_FRIENDS_ONLY = 2;
    public static int        SETTING_IS_CLOSE_PROFILE = 3;
    public static int        SETTING_DEFAULT_TASK = 4;

    public static int        MAX_FRINEDS_RECIPIENTS_OF_FLASHMOBES_FOR_NOT_BUSINESS_USER = 5;
    public static int        MAX_RECIPIENTS_OF_FLASHMOBES_FOR_NOT_BUSINESS_USER = 1000;

    public static boolean       DEBUG_MODE = true;

    public final static String  REST_AUTH_USERNAME =        "rest_auth_username";
    public final static String  REST_AUTH_PASSWORD =        "rest_auth_password";

    public static String getDirectoryImages() { return DEBUG_MODE ? DebugDefines.DIRECTORY_TO_IMAGES : ReleaseDefines.DIRECTORY_TO_IMAGES; }
    public static String getHostImages() { return DEBUG_MODE ? DebugDefines.HOST_TO_IMAGES : ReleaseDefines.HOST_TO_IMAGES; }
}
