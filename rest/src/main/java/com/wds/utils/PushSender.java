package com.wds.utils;

import com.wds.dao.FmUserTokenDao;
import com.wds.entity.FmUserToken;
import com.wds.exception.v1.BaseException;
import com.wds.utils.push.apple.ApplePushNotificationThread;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class PushSender {

    public static void sendPush(long uid, int typeNotification, long userFlashmobId) throws BaseException {
        /**
         * Send push notification to user for whom the flashmob is assigned
         */
        FmUserTokenDao userTokenDao = new FmUserTokenDao();
        FmUserToken userToken = userTokenDao.findByUserId(uid);

        if (userToken != null) {
            List<String> appleTokenList = new ArrayList<>();
            appleTokenList.add(StringUtils.trimToEmpty(userToken.getToken()));

            ApplePushNotificationThread applePushNotificationThread =
                    new ApplePushNotificationThread(appleTokenList, "Новая заявка на добавление в друзья",
                            typeNotification,
                            userFlashmobId);
            applePushNotificationThread.start();
        }
    }
}
