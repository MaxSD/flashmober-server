package com.wds.exception.v1;

public class UserIsLockedException extends BaseException {

    public UserIsLockedException(String lockReason) {
        super();
        this.code = 403;
        this.localCode = 2;
        this.message = lockReason;
    }
}
