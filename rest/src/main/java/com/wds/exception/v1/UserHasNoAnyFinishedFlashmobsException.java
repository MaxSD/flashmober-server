package com.wds.exception.v1;

public class UserHasNoAnyFinishedFlashmobsException extends BaseException {

    public UserHasNoAnyFinishedFlashmobsException() {
        super();
        this.code = 404;
        this.localCode = 5;
        this.message = "Failed. User has no any finished flashmobs!";
    }
}
