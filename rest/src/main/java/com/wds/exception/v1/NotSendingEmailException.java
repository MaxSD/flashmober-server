package com.wds.exception.v1;

public class NotSendingEmailException extends BaseException {

    public NotSendingEmailException() {
        super();
        this.code = 403;
        this.localCode = 1;
        this.message = "Failed. Email sending failed!";
    }
}
