package com.wds.exception.v1;

public class UserHasNoAnyFinishedFlashmobForFriendshipException extends BaseException {

    public UserHasNoAnyFinishedFlashmobForFriendshipException() {
        super();
        this.code = 404;
        this.localCode = 6;
        this.message = "Failed. User has no any finished flashmob for friendship!";
    }
}
