package com.wds.exception.v1;

public class NotSendingParameterException extends BaseException {

    public NotSendingParameterException() {
        super();
        this.code = 400;
        this.localCode = 1;
        this.message = "Failed. Parameters were not sent!";
    }
}
