package com.wds.exception.v1;

public class WrongValueCountException extends BaseException {

    public WrongValueCountException() {
        super();
        this.code = 400;
        this.localCode = 8;
        this.message = "Failed. Wrong value of parameter <count>!";
    }
}
