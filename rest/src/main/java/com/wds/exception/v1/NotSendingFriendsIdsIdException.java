package com.wds.exception.v1;

public class NotSendingFriendsIdsIdException extends BaseException {

    public NotSendingFriendsIdsIdException() {
        super();
        this.code = 400;
        this.localCode = 12;
        this.message = "Failed. Parameter <friends_ids> not sending!";
    }
}
