package com.wds.exception.v1;

public class NotSendingStatusException extends BaseException {

    public NotSendingStatusException() {
        super();
        this.code = 400;
        this.localCode = 11;
        this.message = "Failed. Parameter <status> not sending!";
    }
}
