package com.wds.exception.v1;

public class WrongSettingsValueException extends BaseException {

    public WrongSettingsValueException() {
        super();
        this.code = 400;
        this.localCode = 16;
        this.message = "Failed. Wrong value of parameter <value>!";
    }

}
