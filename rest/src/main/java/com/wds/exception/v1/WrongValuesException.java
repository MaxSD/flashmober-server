package com.wds.exception.v1;

public class WrongValuesException extends BaseException {

    public WrongValuesException() {
        super();
        this.code = 400;
        this.localCode = 4;
        this.message = "Failed. Wrong value of parameter(s)!";
    }
}
