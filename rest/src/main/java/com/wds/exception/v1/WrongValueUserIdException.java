package com.wds.exception.v1;

public class WrongValueUserIdException extends BaseException {

    public WrongValueUserIdException() {
        super();
        this.code = 400;
        this.localCode = 9;
        this.message = "Failed. Wrong value of parameter <userId>!";
    }

}
