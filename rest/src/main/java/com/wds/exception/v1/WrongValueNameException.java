package com.wds.exception.v1;

public class WrongValueNameException extends BaseException {

    public WrongValueNameException() {
        super();
        this.code = 400;
        this.localCode = 18;
        this.message = "Failed. Wrong value of parameter <name>!";
    }
}
