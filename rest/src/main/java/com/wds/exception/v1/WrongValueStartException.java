package com.wds.exception.v1;

public class WrongValueStartException extends BaseException {

    public WrongValueStartException() {
        super();
        this.code = 400;
        this.localCode = 7;
        this.message = "Failed. Wrong value of parameter <start>!";
    }
}
