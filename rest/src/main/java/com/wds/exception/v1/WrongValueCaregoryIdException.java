package com.wds.exception.v1;

public class WrongValueCaregoryIdException extends BaseException {

    public WrongValueCaregoryIdException() {
        super();
        this.code = 400;
        this.localCode = 10;
        this.message = "Failed. Wrong value of parameter <categoryId>!";
    }

}
