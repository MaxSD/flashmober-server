package com.wds.exception.v1;

public class DateParseException extends BaseException {

    public DateParseException() {
        super();
        this.code = 500;
        this.localCode = 8;
        this.message = "Failed.Date parse exception!";
    }
}
