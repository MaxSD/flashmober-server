package com.wds.exception.v1;

public class WrongValueNewEmailException extends BaseException {

    public WrongValueNewEmailException() {
        super();
        this.code = 400;
        this.localCode = 14;
        this.message = "Failed. Parameter <newEmail> not sending!";
    }

}
