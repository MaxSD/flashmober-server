package com.wds.exception.v1;

public class NotSendingIdException extends BaseException {

    public NotSendingIdException() {
        super();
        this.code = 400;
        this.localCode = 2;
        this.message = "Failed. Parameter <id> not sending!";
    }
}
