package com.wds.exception.v1;

public class NotSendingPasswordException extends BaseException {

    public NotSendingPasswordException() {
        super();
        this.code = 400;
        this.localCode = 3;
        this.message = "Failed. Parameter <password> not sending!";
    }
}
