package com.wds.exception.v1;

public class FlashmobNotFoundException extends BaseException {

    public FlashmobNotFoundException() {
        super();
        this.code = 404;
        this.localCode = 3;
        this.message = "Failed. Flashmob not found!";
    }
}
