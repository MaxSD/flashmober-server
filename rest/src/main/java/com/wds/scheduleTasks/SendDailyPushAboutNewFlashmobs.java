package com.wds.scheduleTasks;

import com.wds.dao.FmFlashmobsDao;
import com.wds.dao.FmUserTokenDao;
import com.wds.dataObjects.FlashmobsUsersAssignedOn;
import com.wds.entity.FmUserToken;
import com.wds.utils.ApplicationUtility;
import com.wds.utils.push.apple.ApplePushNotificationThread;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimerTask;

public class SendDailyPushAboutNewFlashmobs extends TimerTask {

    @Override
    public void run() {
        System.out.println("Timer task 'SendDailyPushAboutNewFlashmobs' started at:" + new Date());

        try {
            /**
             * Send push notification, if publication date is now.
             */
            int sentNotificationsCount = 0;

            FmFlashmobsDao flashmobsDao = new FmFlashmobsDao();
            List<FlashmobsUsersAssignedOn> flashmobsUsersAssignedOnList =
                    flashmobsDao.getFlashmobUsersAssignedOnDate(ApplicationUtility.getDateInSeconds());

            if (flashmobsUsersAssignedOnList != null && flashmobsUsersAssignedOnList.size() > 0) {
                FmUserTokenDao userTokenDao = new FmUserTokenDao();
                for (FlashmobsUsersAssignedOn flashmobsUsersAssignedOn : flashmobsUsersAssignedOnList) {
                    FmUserToken userToken = userTokenDao.findByUserId(flashmobsUsersAssignedOn.getId());

                    if (userToken != null) {
                        List<String> appleTokenList = new ArrayList<>();
                        appleTokenList.add(StringUtils.trimToEmpty(userToken.getToken()));

                        ApplePushNotificationThread applePushNotificationThread =
                                new ApplePushNotificationThread(appleTokenList, "Пользователь " + flashmobsUsersAssignedOn.getName() + " предлагает Вам флешмоб",
                                        com.wds.utils.define.Defines.NOTIFICATION_TYPE_NEW_FLASHMOB,
                                        flashmobsUsersAssignedOn.getUser_flashmob_id());
                        applePushNotificationThread.start();
                        sentNotificationsCount ++;
                    }
                }
            }

            System.out.println("Timer task 'SendDailyPushAboutNewFlashmobs' finished at:" + new Date() + " " + sentNotificationsCount + " has been sent!");
        } catch (Throwable tr) {
            System.out.println("Timer task 'SendDailyPushAboutNewFlashmobs' ERROR!");
            tr.printStackTrace();
        }
    }
}
