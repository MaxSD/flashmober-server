package com.wds.json.v1.flashmobs.report;

import java.math.BigInteger;

public class GetFlashmobReportResponse {
    private Long id;
    private Long owner_id;
    private Long user_flashmob_id;
    private String name;
    private String email;
    private String user_photo;
    private String photo;
    private Long date_create;
    private String description;
    private Long date_end;
    private String photo_report;
    private String video_report;
    private BigInteger people_count;
    private BigInteger like_count;
    private BigInteger is_me_liked;
    private BigInteger comment_count;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(Long owner_id) {
        this.owner_id = owner_id;
    }

    public Long getUser_flashmob_id() {
        return user_flashmob_id;
    }

    public void setUser_flashmob_id(Long user_flashmob_id) {
        this.user_flashmob_id = user_flashmob_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUser_photo() {
        return user_photo;
    }

    public void setUser_photo(String user_photo) {
        this.user_photo = user_photo;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Long getDate_create() {
        return date_create;
    }

    public void setDate_create(Long date_create) {
        this.date_create = date_create;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getDate_end() {
        return date_end;
    }

    public void setDate_end(Long date_end) {
        this.date_end = date_end;
    }

    public String getPhoto_report() {
        return photo_report;
    }

    public void setPhoto_report(String photo_report) {
        this.photo_report = photo_report;
    }

    public String getVideo_report() {
        return video_report;
    }

    public void setVideo_report(String video_report) {
        this.video_report = video_report;
    }

    public BigInteger getPeople_count() {
        return people_count;
    }

    public void setPeople_count(BigInteger people_count) {
        this.people_count = people_count;
    }

    public BigInteger getLike_count() {
        return like_count;
    }

    public void setLike_count(BigInteger like_count) {
        this.like_count = like_count;
    }

    public BigInteger getIs_me_liked() {
        return is_me_liked;
    }

    public void setIs_me_liked(BigInteger is_me_liked) {
        this.is_me_liked = is_me_liked;
    }

    public BigInteger getComment_count() {
        return comment_count;
    }

    public void setComment_count(BigInteger comment_count) {
        this.comment_count = comment_count;
    }
}
