package com.wds.json.v1;

@SuppressWarnings("UnusedDeclaration")
public class ErrorReturn {

    private int code;
    private String error;
    private String method;

    public ErrorReturn(String error) {
        this.error = error;
    }

    public ErrorReturn(int code, String error, String method) {
        this.code = code;
        this.error = error;
        this.method = method;
    }

    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }

    public String getError() { return error; }
    public void setError(String error) { this.error = error; }

    public String getMethod() { return method; }
    public void setMethod(String method) { this.method = method; }
}
