package com.wds.json.v1.user.registration;

import java.util.List;

public class GetUserSettingsResponse {
    private long uid;
    private String email;
    private String name;
    private String photo;
    private int gender;
    private int age;
    private String about;
    private String country;
    private String city;
    private String site;
    private List<Long> categories_ids;
    private List<String> languages;
    private boolean isIsBusinessUser;
    private String vkId;
    private String fdId;
    private String twId;

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public List<Long> getCategories_ids() {
        return categories_ids;
    }

    public void setCategories_ids(List<Long> categories_ids) {
        this.categories_ids = categories_ids;
    }

    public List<String> getLanguages() {
        return languages;
    }

    public void setLanguages(List<String> languages) {
        this.languages = languages;
    }

    public boolean isIsBusinessUser() {
        return isIsBusinessUser;
    }

    public void setIsBusinessUser(boolean isIsBusinessUser) {
        this.isIsBusinessUser = isIsBusinessUser;
    }

    public String getVkId() {
        return vkId;
    }

    public void setVkId(String vkId) {
        this.vkId = vkId;
    }

    public String getFdId() {
        return fdId;
    }

    public void setFdId(String fdId) {
        this.fdId = fdId;
    }

    public String getTwId() {
        return twId;
    }

    public void setTwId(String twId) {
        this.twId = twId;
    }
}
