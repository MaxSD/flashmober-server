package com.wds.json.v1.friends.counters;

public class GetFriendsCountersResponse {
    private Integer friends_count;
    private Integer follow_count;
    private Integer following_count;
    private Integer created_flashmobs_count;
    private Integer new_flashmobs_count;
    private Integer accepted_flashmobs_count;
    private Integer finished_flashmobs_count;
    private Integer notoficatios_count;
    private boolean flashmobs_deleted_by_moderator;
    private boolean reports_deleted_by_moderator;
    private String flashmobs_deleted_reason;
    private String reports_deleted_reason;

    public Integer getFriends_count() {
        return friends_count;
    }

    public void setFriends_count(Integer friends_count) {
        this.friends_count = friends_count;
    }

    public Integer getFollow_count() {
        return follow_count;
    }

    public void setFollow_count(Integer follow_count) {
        this.follow_count = follow_count;
    }

    public Integer getFollowing_count() {
        return following_count;
    }

    public void setFollowing_count(Integer following_count) {
        this.following_count = following_count;
    }

    public Integer getCreated_flashmobs_count() {
        return created_flashmobs_count;
    }

    public void setCreated_flashmobs_count(Integer created_flashmobs_count) {
        this.created_flashmobs_count = created_flashmobs_count;
    }

    public Integer getNew_flashmobs_count() {
        return new_flashmobs_count;
    }

    public void setNew_flashmobs_count(Integer new_flashmobs_count) {
        this.new_flashmobs_count = new_flashmobs_count;
    }

    public Integer getAccepted_flashmobs_count() {
        return accepted_flashmobs_count;
    }

    public void setAccepted_flashmobs_count(Integer accepted_flashmobs_count) {
        this.accepted_flashmobs_count = accepted_flashmobs_count;
    }

    public Integer getFinished_flashmobs_count() {
        return finished_flashmobs_count;
    }

    public void setFinished_flashmobs_count(Integer finished_flashmobs_count) {
        this.finished_flashmobs_count = finished_flashmobs_count;
    }

    public Integer getNotoficatios_count() {
        return notoficatios_count;
    }

    public void setNotoficatios_count(Integer notoficatios_count) {
        this.notoficatios_count = notoficatios_count;
    }

    public boolean isFlashmobs_deleted_by_moderator() {
        return flashmobs_deleted_by_moderator;
    }

    public void setFlashmobs_deleted_by_moderator(boolean flashmobs_deleted_by_moderator) {
        this.flashmobs_deleted_by_moderator = flashmobs_deleted_by_moderator;
    }

    public boolean isReports_deleted_by_moderator() {
        return reports_deleted_by_moderator;
    }

    public void setReports_deleted_by_moderator(boolean reports_deleted_by_moderator) {
        this.reports_deleted_by_moderator = reports_deleted_by_moderator;
    }

    public String getFlashmobs_deleted_reason() {
        return flashmobs_deleted_reason;
    }

    public void setFlashmobs_deleted_reason(String flashmobs_deleted_reason) {
        this.flashmobs_deleted_reason = flashmobs_deleted_reason;
    }

    public String getReports_deleted_reason() {
        return reports_deleted_reason;
    }

    public void setReports_deleted_reason(String reports_deleted_reason) {
        this.reports_deleted_reason = reports_deleted_reason;
    }
}
