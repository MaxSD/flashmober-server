package com.wds.json.v1.friends.add;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@SuppressWarnings("UnusedDeclaration")
@JsonIgnoreProperties(ignoreUnknown = true)
public class FriendsAddResponse {
    private boolean default_task;

    public boolean isDefault_task() {
        return default_task;
    }

    public void setDefault_task(boolean default_task) {
        this.default_task = default_task;
    }
}
