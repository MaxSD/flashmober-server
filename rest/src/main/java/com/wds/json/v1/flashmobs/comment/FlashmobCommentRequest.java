package com.wds.json.v1.flashmobs.comment;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@SuppressWarnings("UnusedDeclaration")
@JsonIgnoreProperties(ignoreUnknown = true)
public class FlashmobCommentRequest {
    private Long user_flashmob_id;
    private Long uid;
    private String comment;
    private List<String> users_names_for_push;

    public Long getUser_flashmob_id() {
        return user_flashmob_id;
    }

    public void setUser_flashmob_id(Long user_flashmob_id) {
        this.user_flashmob_id = user_flashmob_id;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<String> getUsers_names_for_push() {
        return users_names_for_push;
    }

    public void setUsers_names_for_push(List<String> users_names_for_push) {
        this.users_names_for_push = users_names_for_push;
    }
}
