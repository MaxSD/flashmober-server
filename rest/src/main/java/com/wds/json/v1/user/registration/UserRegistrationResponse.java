package com.wds.json.v1.user.registration;

public class UserRegistrationResponse {
    private long uid;

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }
}
