package com.wds.json.v1.friends.upload;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@SuppressWarnings("UnusedDeclaration")
@JsonIgnoreProperties(ignoreUnknown = true)
public class FriendsUploadRequest {
    private Long uid;
    private List<FriendsUploadSocialIdsObject> friends;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public List<FriendsUploadSocialIdsObject> getFriends() {
        return friends;
    }

    public void setFriends(List<FriendsUploadSocialIdsObject> friends) {
        this.friends = friends;
    }
}
