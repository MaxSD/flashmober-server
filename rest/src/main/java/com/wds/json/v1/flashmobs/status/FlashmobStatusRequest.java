package com.wds.json.v1.flashmobs.status;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@SuppressWarnings("UnusedDeclaration")
@JsonIgnoreProperties(ignoreUnknown = true)
public class FlashmobStatusRequest {
    private Long id;
    private Long uid;
    private Integer status;
    private String photo_report;
    private String video_report;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getPhoto_report() {
        return photo_report;
    }

    public void setPhoto_report(String photo_report) {
        this.photo_report = photo_report;
    }

    public String getVideo_report() {
        return video_report;
    }

    public void setVideo_report(String video_report) {
        this.video_report = video_report;
    }
}
