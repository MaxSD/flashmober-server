package com.wds.json.v1.actions;

import java.math.BigInteger;

public class GetActionsResponse {
    private Long id;
    private Long uid;
    private Long user_flashmob_id;
    private String name;
    private String email;
    private String user_photo;
    private Integer gender;
    private BigInteger like_count;
    private String photo_report;
    private String video_report;
    private Integer type;
    private Long date;
    private String comment;
    private Integer friendship_status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Long getUser_flashmob_id() {
        return user_flashmob_id;
    }

    public void setUser_flashmob_id(Long user_flashmob_id) {
        this.user_flashmob_id = user_flashmob_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUser_photo() {
        return user_photo;
    }

    public void setUser_photo(String user_photo) {
        this.user_photo = user_photo;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public BigInteger getLike_count() {
        return like_count;
    }

    public void setLike_count(BigInteger like_count) {
        this.like_count = like_count;
    }

    public String getPhoto_report() {
        return photo_report;
    }

    public void setPhoto_report(String photo_report) {
        this.photo_report = photo_report;
    }

    public String getVideo_report() {
        return video_report;
    }

    public void setVideo_report(String video_report) {
        this.video_report = video_report;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getFriendship_status() {
        return friendship_status;
    }

    public void setFriendship_status(Integer friendship_status) {
        this.friendship_status = friendship_status;
    }
}
