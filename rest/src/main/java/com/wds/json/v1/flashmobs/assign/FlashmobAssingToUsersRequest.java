package com.wds.json.v1.flashmobs.assign;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@SuppressWarnings("UnusedDeclaration")
@JsonIgnoreProperties(ignoreUnknown = true)
public class FlashmobAssingToUsersRequest {
    private Long uid;
    private Long user_flashmob_id;
    private List<Long> users_ids;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Long getUser_flashmob_id() {
        return user_flashmob_id;
    }

    public void setUser_flashmob_id(Long user_flashmob_id) {
        this.user_flashmob_id = user_flashmob_id;
    }

    public List<Long> getUsers_ids() {
        return users_ids;
    }

    public void setUsers_ids(List<Long> users_ids) {
        this.users_ids = users_ids;
    }
}
