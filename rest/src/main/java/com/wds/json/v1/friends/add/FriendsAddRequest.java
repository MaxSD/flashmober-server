package com.wds.json.v1.friends.add;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.wds.json.v1.friends.upload.FriendsUploadSocialIdsObject;

import java.util.List;

@SuppressWarnings("UnusedDeclaration")
@JsonIgnoreProperties(ignoreUnknown = true)
public class FriendsAddRequest {
    private Long uid;
    private Long friendId;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Long getFriendId() {
        return friendId;
    }

    public void setFriendId(Long friendId) {
        this.friendId = friendId;
    }
}
