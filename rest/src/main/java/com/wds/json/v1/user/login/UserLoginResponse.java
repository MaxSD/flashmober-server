package com.wds.json.v1.user.login;

public class UserLoginResponse {
    private long uid;
    private boolean first_login;

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public boolean isFirst_login() {
        return first_login;
    }

    public void setFirst_login(boolean first_login) {
        this.first_login = first_login;
    }
}
