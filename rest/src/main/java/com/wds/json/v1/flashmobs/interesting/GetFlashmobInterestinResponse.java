package com.wds.json.v1.flashmobs.interesting;

public class GetFlashmobInterestinResponse {
    private Long uid;
    private Long user_flashmob_id;
    private String photo_report;
    private String video_report;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Long getUser_flashmob_id() {
        return user_flashmob_id;
    }

    public void setUser_flashmob_id(Long user_flashmob_id) {
        this.user_flashmob_id = user_flashmob_id;
    }

    public String getPhoto_report() {
        return photo_report;
    }

    public void setPhoto_report(String photo_report) {
        this.photo_report = photo_report;
    }

    public String getVideo_report() {
        return video_report;
    }

    public void setVideo_report(String video_report) {
        this.video_report = video_report;
    }
}
