package com.wds.json.v1.settings;

public class GetSettingsResponse {
    private boolean get_notifications;
    private boolean get_flashmobs;
    private boolean get_flashmobs_from_friends_only;
    private boolean close_profile;
    private boolean default_task;

    public boolean isGet_notifications() {
        return get_notifications;
    }

    public void setGet_notifications(boolean get_notifications) {
        this.get_notifications = get_notifications;
    }

    public boolean isGet_flashmobs() {
        return get_flashmobs;
    }

    public void setGet_flashmobs(boolean get_flashmobs) {
        this.get_flashmobs = get_flashmobs;
    }

    public boolean isGet_flashmobs_from_friends_only() {
        return get_flashmobs_from_friends_only;
    }

    public void setGet_flashmobs_from_friends_only(boolean get_flashmobs_from_friends_only) {
        this.get_flashmobs_from_friends_only = get_flashmobs_from_friends_only;
    }

    public boolean isClose_profile() {
        return close_profile;
    }

    public void setClose_profile(boolean close_profile) {
        this.close_profile = close_profile;
    }

    public boolean isDefault_task() {
        return default_task;
    }

    public void setDefault_task(boolean default_task) {
        this.default_task = default_task;
    }
}
