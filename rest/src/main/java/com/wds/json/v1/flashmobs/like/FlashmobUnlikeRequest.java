package com.wds.json.v1.flashmobs.like;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@SuppressWarnings("UnusedDeclaration")
@JsonIgnoreProperties(ignoreUnknown = true)
public class FlashmobUnlikeRequest {
    private Long user_flashmob_id;
    private Long uid;

    public Long getUser_flashmob_id() {
        return user_flashmob_id;
    }

    public void setUser_flashmob_id(Long user_flashmob_id) {
        this.user_flashmob_id = user_flashmob_id;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }
}
