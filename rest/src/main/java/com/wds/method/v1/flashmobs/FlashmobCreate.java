package com.wds.method.v1.flashmobs;

import com.wds.dao.FmFlashmobsDao;
import com.wds.dao.FmUserTokenDao;
import com.wds.dao.FmUsersDao;
import com.wds.entity.FmFlashmobs;
import com.wds.entity.FmUser;
import com.wds.entity.FmUserToken;
import com.wds.exception.v1.*;
import com.wds.json.v1.flashmobs.create.FlashmobCreateResponse;
import com.wds.utils.ApplicationUtility;
import com.wds.utils.push.apple.ApplePushNotificationThread;
import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.DataFormatException;

public class FlashmobCreate {

    private static void validateRequest(Long uid, String description,
                                        boolean send_into_abyss, String photoURL) throws BaseException {

        if (uid == null || uid == 0)
            throw new WrongValueUserIdException();
    }

    public static List<FlashmobCreateResponse> performRequest(Long uid, String description,
                                                              List<Long> categoriesIds,
                                                              List<String> tags,
                                                              List<Long> friends_ids,
                                                              List<String> languagesList,
                                                              Boolean send_into_abyss,
                                                              String videoURL,
                                                              String photoURL,
                                                              String datePublication,
                                                              Boolean isDefault) throws BaseException {

        validateRequest(uid, description, send_into_abyss, photoURL);

        FmUsersDao usersDao = new FmUsersDao();
        FmUser user = usersDao.findById(uid);

        if (user == null)
            throw new UserNotFoundException();

        /*
        // if is not business user and friends list size > 5
        List<Long> randomFriends_ids;

        if (user.getRole().getId() > 2) {
            if (!user.isIsBusinessUser() && !send_into_abyss &&
                    friends_ids.size() > Defines.MAX_FRINEDS_RECIPIENTS_OF_FLASHMOBES_FOR_NOT_BUSINESS_USER) {
                randomFriends_ids = new ArrayList<>();

                Random randomGenerator = new Random();
                for (int i = 0; i < Defines.MAX_FRINEDS_RECIPIENTS_OF_FLASHMOBES_FOR_NOT_BUSINESS_USER; i++) {
                    randomFriends_ids.add(friends_ids.get(randomGenerator.nextInt(friends_ids.size())));
                }

                friends_ids = randomFriends_ids;
            }
        }
        */

        FmFlashmobs fmFlashmobs = new FmFlashmobs();
        fmFlashmobs.setOwner_id(uid);
        fmFlashmobs.setPhoto(photoURL);
        fmFlashmobs.setVideo(StringUtils.trimToEmpty(videoURL));
        fmFlashmobs.setDescription(description);

        if (user.isIsBusinessUser()) {
            fmFlashmobs.setModeration(true);
            fmFlashmobs.setIsAcceptedBbyAdmin(false);

            TimeZone timeZone = TimeZone.getTimeZone("GMT+0");
            Calendar calendar = Calendar.getInstance(timeZone);
            fmFlashmobs.setDate_create(calendar.getTimeInMillis() / 1000);
        } else {
            fmFlashmobs.setModeration(false);
            fmFlashmobs.setIsAcceptedBbyAdmin(true);

            TimeZone timeZone = TimeZone.getTimeZone("GMT+0");
            Calendar calendar = Calendar.getInstance(timeZone);
            fmFlashmobs.setDate_create(calendar.getTimeInMillis() / 1000);
        }

        try {
            if (!StringUtils.isBlank(datePublication)) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                fmFlashmobs.setDate_publication(ApplicationUtility.getDateInSeconds(sdf.parse(datePublication)));
            } else {
                fmFlashmobs.setDate_publication(ApplicationUtility.getDateInSeconds());
            }
        } catch (ParseException pe) {
            throw new DateParseException();
        }

        fmFlashmobs.setComment("");
        fmFlashmobs.setIsDefault(isDefault);

        FmFlashmobsDao fmFlashmobsDao = new FmFlashmobsDao();
        List<Map<String, Long>> flashmobCreateResult = fmFlashmobsDao.create(
                fmFlashmobs, categoriesIds, tags, friends_ids, languagesList, send_into_abyss, user.isIsBusinessUser(), isDefault, user.getFlashmobUsersCount(), ApplicationUtility.getCurrentTimeStampGMT_0());

        FlashmobCreateResponse responseObject = new FlashmobCreateResponse();
        responseObject.setId(fmFlashmobs.getId());

        List<FlashmobCreateResponse> response = new ArrayList<>();
        response.add(responseObject);

        /**
         * Send push
         */
        if (!user.isIsBusinessUser()) {
            /**
             * Notification will be sent if task has been created by not business users.
             * If task created by business user - notification will be sent after moderation.
             */
            if (StringUtils.isBlank(datePublication) ||
                    fmFlashmobs.getDate_publication() == ApplicationUtility.getDateInSeconds()) {

                if (flashmobCreateResult != null && flashmobCreateResult.size() > 0) {
                    for (Map<String, Long> resultEl : flashmobCreateResult) {
                        if (resultEl != null) {
                            if (resultEl.get("uid") != null && resultEl.get("userFlashmobId") != null) {
                                FmUserTokenDao userTokenDao = new FmUserTokenDao();
                                FmUserToken userToken = userTokenDao.findByUserId(resultEl.get("uid"));

                                if (userToken != null) {
                                    List<String> appleTokenList = new ArrayList<>();
                                    appleTokenList.add(StringUtils.trimToEmpty(userToken.getToken()));

                                    ApplePushNotificationThread applePushNotificationThread =
                                            new ApplePushNotificationThread(appleTokenList, "Пользователь " + user.getName() + " предлагает Вам флешмоб",
                                                    com.wds.utils.define.Defines.NOTIFICATION_TYPE_NEW_FLASHMOB,
                                                    resultEl.get("userFlashmobId"));
                                    applePushNotificationThread.start();
                                }
                            }
                        }
                    }
                }
            }
        }

        return response;
    }
}
