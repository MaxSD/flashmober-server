package com.wds.method.v1.user;

import com.wds.dao.FmUsersCategoriesDao;
import com.wds.dao.FmUsersDao;
import com.wds.dao.FmUsersLanguagesDao;
import com.wds.entity.FmUser;
import com.wds.entity.FmUsersCategories;
import com.wds.entity.FmUsersLanguages;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingIdException;
import com.wds.exception.v1.UserNotFoundException;
import com.wds.json.v1.user.registration.GetUserSettingsResponse;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.core.MultivaluedMap;
import java.util.ArrayList;
import java.util.List;

public class GetUserSettings {

    private static void validateRequest(MultivaluedMap<String, String> requestParameters) throws BaseException {
        if (StringUtils.isBlank(requestParameters.getFirst("uid")))
            throw new NotSendingIdException();
    }

    public static List<GetUserSettingsResponse> performRequest(MultivaluedMap<String, String> requestParameters) throws BaseException {

        validateRequest(requestParameters);

        long uid = Long.valueOf(requestParameters.getFirst("uid"));

        FmUsersDao usersDao = new FmUsersDao();
        FmUser user = usersDao.findById(uid);

        if (user == null)
            throw new UserNotFoundException();

        // User's categories
        FmUsersCategoriesDao usersCategoriesDao = new FmUsersCategoriesDao();
        List<FmUsersCategories> usersCategoriesList = usersCategoriesDao.findByUserId(uid);
        List<Long> categories_ids = new ArrayList<>();
        for (FmUsersCategories category : usersCategoriesList) {
            categories_ids.add(category.getCategoryId());
        }

        // User's languages
        FmUsersLanguagesDao usersLanguagesDao = new FmUsersLanguagesDao();
        List<FmUsersLanguages> usersLanguagesList = usersLanguagesDao.findByUserId(uid);
        List<String> languages = new ArrayList<>();
        for (FmUsersLanguages language : usersLanguagesList) {
            languages.add(language.getLanguage());
        }

        List<GetUserSettingsResponse> result = new ArrayList<>();
        GetUserSettingsResponse returnObject = new GetUserSettingsResponse();

        returnObject.setUid(user.getId());
        returnObject.setEmail(user.getEmail());
        returnObject.setName(user.getName());
        returnObject.setPhoto(user.getPhoto());
        returnObject.setGender(user.getGender());
        returnObject.setAbout(user.getAbout());
        returnObject.setAge(user.getAge());
        returnObject.setCountry(user.getCountry());
        returnObject.setCity(user.getCity());
        returnObject.setSite(user.getSite());

        returnObject.setCategories_ids(categories_ids);
        returnObject.setLanguages(languages);
        returnObject.setIsBusinessUser(user.isIsBusinessUser());

        returnObject.setVkId(StringUtils.trimToEmpty(user.getVkId()));
        returnObject.setFdId(StringUtils.trimToEmpty(user.getFbId()));
        returnObject.setTwId(StringUtils.trimToEmpty(user.getTwId()));

        result.add(returnObject);

        return result;
    }
}
