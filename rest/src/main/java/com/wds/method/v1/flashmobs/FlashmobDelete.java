package com.wds.method.v1.flashmobs;

import com.wds.dao.FmUsersFlashmobsDao;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingIdException;
import com.wds.json.v1.flashmobs.delete.FlashmobDeleteRequest;

public class FlashmobDelete {

    private static void validateRequest(FlashmobDeleteRequest request) throws BaseException {
        if (request.getUser_flashmob_id() == null || request.getUser_flashmob_id() == 0)
            throw new NotSendingIdException();
    }

    public static void performRequest(FlashmobDeleteRequest request) throws BaseException {

        validateRequest(request);

        FmUsersFlashmobsDao usersFlashmobsDao = new FmUsersFlashmobsDao();
        usersFlashmobsDao.delete(request.getUser_flashmob_id());
    }
}
