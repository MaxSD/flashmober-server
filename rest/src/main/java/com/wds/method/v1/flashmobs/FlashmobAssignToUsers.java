package com.wds.method.v1.flashmobs;

import com.wds.dao.FmUsersFlashmobsDao;
import com.wds.database.Defines;
import com.wds.entity.FmUsersFlashmobs;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.FlashmobNotFoundException;
import com.wds.exception.v1.WrongValueUserIdException;
import com.wds.json.v1.flashmobs.assign.FlashmobAssingToUsersRequest;
import com.wds.utils.ApplicationUtility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FlashmobAssignToUsers {

    private static void validateRequest(FlashmobAssingToUsersRequest request) throws BaseException {

        if (request.getUid() == null || request.getUid() == 0)
            throw new WrongValueUserIdException();
    }

    public static List<Map<String, Object>> performRequest(FlashmobAssingToUsersRequest request) throws BaseException {

        validateRequest(request);

        List<Map<String, Object>> resultObject = new ArrayList<>();

        FmUsersFlashmobsDao usersFlashmobsDao = new FmUsersFlashmobsDao();
        FmUsersFlashmobs usersFlashmob = usersFlashmobsDao.findById(request.getUser_flashmob_id());

        if (usersFlashmob == null)
            throw new FlashmobNotFoundException();

        if (request.getUsers_ids() != null) {
            for (Long userId : request.getUsers_ids()) {
                List<FmUsersFlashmobs> usersFlashmobsList = usersFlashmobsDao.find(userId, usersFlashmob.getFlashmob_Id());
                if (usersFlashmobsList == null || usersFlashmobsList.size() == 0 ||
                        request.getUid() == userId) {

                    FmUsersFlashmobs newUsersFlashmob = new FmUsersFlashmobs();
                    newUsersFlashmob.setUserId(userId);
                    newUsersFlashmob.setFlashmob_Id(usersFlashmob.getFlashmob_Id());
                    newUsersFlashmob.setStatus(1);
                    newUsersFlashmob.setDate_assign(ApplicationUtility.getCurrentTimeStampGMT_0());
                    newUsersFlashmob.setDate_end(0l);
                    newUsersFlashmob.setFriendshipWithUid(0l);
                    usersFlashmobsDao.create(newUsersFlashmob);

                    Map<String, Object> returnObject = new HashMap<>();
                    returnObject.put("uid", userId);
                    returnObject.put("user_flashmob_id", newUsersFlashmob.getId());
                    resultObject.add(returnObject);
                }
            }
        }

        return resultObject;
    }
}
