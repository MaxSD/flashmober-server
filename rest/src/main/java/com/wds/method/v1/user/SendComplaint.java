package com.wds.method.v1.user;

import com.wds.dao.FmComplaintsDao;
import com.wds.entity.FmComplaint;
import com.wds.exception.v1.*;
import com.wds.json.v1.user.complaint.SendComplaintRequest;
import com.wds.utils.ApplicationUtility;

public class SendComplaint {

    private static void validateRequest(SendComplaintRequest request) throws BaseException {
        if (request.getUid() == null || request.getUid() == 0)
            throw new WrongValueUserIdException();
    }

    public static void performRequest(SendComplaintRequest request) throws BaseException {

        validateRequest(request);

        FmComplaintsDao complaintsDao = new FmComplaintsDao();
        FmComplaint complaint = new FmComplaint();

        complaint.setUserId(request.getUid());
        complaint.setText(request.getText());
        complaint.setDateCreate(ApplicationUtility.getCurrentTimeStamp());

        complaintsDao.create(complaint);
    }
}
