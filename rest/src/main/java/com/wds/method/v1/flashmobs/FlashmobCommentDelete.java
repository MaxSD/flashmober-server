package com.wds.method.v1.flashmobs;

import com.wds.dao.FmCommentsDao;
import com.wds.entity.FmComment;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingIdException;
import com.wds.json.v1.flashmobs.comment.FlashmobCommentDeleteRequest;

public class FlashmobCommentDelete {

    private static void validateRequest(FlashmobCommentDeleteRequest request) throws BaseException {
        if (request.getId() == null || request.getId() == 0)
            throw new NotSendingIdException();
    }

    public static void performRequest(FlashmobCommentDeleteRequest request) throws BaseException {

        validateRequest(request);

        FmCommentsDao commentsDao = new FmCommentsDao();
        FmComment comment = commentsDao.findById(request.getId());

        if (comment != null) {
            commentsDao.delete(comment);
        }
    }
}
