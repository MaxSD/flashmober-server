package com.wds.method.v1.user;

import com.wds.dao.FmUsersDao;
import com.wds.entity.FmUser;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.IncorrectPasswordException;
import com.wds.exception.v1.UserNotFoundException;
import com.wds.json.v1.user.changePassword.UserChangePasswordRequest;
import com.wds.utils.ApplicationUtility;
import org.apache.commons.lang3.StringUtils;

public class UserChangePassword {

    public static void performRequest(UserChangePasswordRequest request) throws BaseException {

        FmUsersDao usersDao = new FmUsersDao();
        FmUser user = usersDao.findById(request.getUid());

        if (user == null)
            throw new UserNotFoundException();

        if (!StringUtils.trimToEmpty(user.getPassword()).equals(StringUtils.trimToEmpty(ApplicationUtility.md5crypt(request.getOldPassword()))))
            throw new IncorrectPasswordException();

        user.setPassword(StringUtils.trimToEmpty(ApplicationUtility.md5crypt(request.getNewPassword())));
        usersDao.update(user);
    }
}
