package com.wds.method.v1.user;

import com.wds.dao.*;
import com.wds.database.Defines;
import com.wds.entity.FmFlashmobs;
import com.wds.entity.FmUser;
import com.wds.entity.FmUsersCategories;
import com.wds.entity.FmUsersFlashmobs;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingParameterException;
import com.wds.exception.v1.UserAlreadyExistsException;
import com.wds.exception.v1.UserIsLockedException;
import com.wds.json.v1.user.registration.UserRegistrationRequest;
import com.wds.json.v1.user.registration.UserRegistrationResponse;
import com.wds.utils.ApplicationUtility;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class UserRegistration {

    public static List<UserRegistrationResponse> performRequest(UserRegistrationRequest request) throws BaseException {

        List<UserRegistrationResponse> response = new ArrayList<>(0);

        FmUsersDao usersDao = new FmUsersDao();
        FmUser user = null;
        if (!StringUtils.isBlank(request.getEmail()) && !StringUtils.isBlank(request.getPassword())) {
            // Search user by email and password
            user = usersDao.findByEmail(request.getEmail().trim().toLowerCase());

            if (user != null) {
                throw new UserAlreadyExistsException();
            } else {
                user = new FmUser();
                user.setEmail(StringUtils.trimToEmpty(request.getEmail()));
                user.setPassword(StringUtils.trimToEmpty(ApplicationUtility.md5crypt(request.getPassword())));
                user.setName("[" + String.valueOf(System.currentTimeMillis()) + "]");
                user.setPhoto("");
                user.setSite("");
                user.setCountry("");
                user.setCity("");
                user.setAge(0);
                user.setVkId("");
                user.setFbId("");
                user.setTwId("");
                user.setAbout("");
                user.setGender(0);
                user.setIsBusinessUser(false);
                user.setIsLocked(false);
                user.setLockComment("");
                user.setLockExpireDate(0l);

                RolesDao rolesDao = new RolesDao();
                user.setRole(rolesDao.findById(Defines.USER_TYPE_USER));
                usersDao.create(user);

                // Assign default flashmobs
                assignDefaultFlashmobs(user.getId());
            }
        } else if (!StringUtils.isBlank(request.getEmail()) &&
                (!StringUtils.isBlank(request.getFbId()) ||
                        !StringUtils.isBlank(request.getTwId()) ||
                        !StringUtils.isBlank(request.getVkId()))) {

            // Search user by social networks ids
            if (!StringUtils.isBlank(request.getVkId()))
                user = usersDao.findByVkId(request.getVkId());

            if (!StringUtils.isBlank(request.getFbId()))
                user = usersDao.findByFbId(request.getFbId());

            if (!StringUtils.isBlank(request.getTwId()))
                user = usersDao.findByTwId(request.getTwId());

            if (user == null) {
                user = usersDao.findByEmail(request.getEmail().trim().toLowerCase());

                if (user != null) {
                    /**
                     * Check if user is locked
                     */
                    if (user.isIsLocked())
                        throw new UserIsLockedException("Ваш аккаунт заблокирован. Причина: " + user.getLockComment());

                    if (!StringUtils.isBlank(request.getVkId()) && StringUtils.isBlank(user.getVkId())) {
                        user.setVkId(StringUtils.trimToEmpty(request.getVkId()));
                    }
                    if (!StringUtils.isBlank(request.getFbId()) && StringUtils.isBlank(user.getFbId())) {
                        user.setFbId(StringUtils.trimToEmpty(request.getFbId()));
                    }
                    if (!StringUtils.isBlank(request.getTwId()) && StringUtils.isBlank(user.getTwId())) {
                        user.setTwId(StringUtils.trimToEmpty(request.getTwId()));
                    }
                    usersDao.update(user);
                } else {
                    user = new FmUser();
                    user.setEmail(request.getEmail().trim().toLowerCase());
                    if (!StringUtils.isBlank(request.getVkId()) && StringUtils.isBlank(user.getVkId())) {
                        user.setVkId(StringUtils.trimToEmpty(request.getVkId()));
                    }
                    if (!StringUtils.isBlank(request.getFbId()) && StringUtils.isBlank(user.getFbId())) {
                        user.setFbId(StringUtils.trimToEmpty(request.getFbId()));
                    }
                    if (!StringUtils.isBlank(request.getTwId()) && StringUtils.isBlank(user.getTwId())) {
                        user.setTwId(StringUtils.trimToEmpty(request.getTwId()));
                    }
                    user.setName("[" + String.valueOf(System.currentTimeMillis()) + "]");
                    user.setPhoto("");
                    user.setSite("");
                    user.setCountry("");
                    user.setCity("");
                    user.setAge(0);
                    user.setVkId("");
                    user.setFbId("");
                    user.setTwId("");
                    user.setAbout("");
                    user.setGender(0);
                    user.setIsBusinessUser(false);
                    user.setIsLocked(false);
                    user.setLockComment("");
                    user.setLockExpireDate(0l);

                    RolesDao rolesDao = new RolesDao();
                    user.setRole(rolesDao.findById(Defines.USER_TYPE_USER));
                    usersDao.create(user);

                    // Assign default flashmobs
                    assignDefaultFlashmobs(user.getId());
                }
            }
        } else {
            throw new NotSendingParameterException();
        }

        UserRegistrationResponse responseObject = new UserRegistrationResponse();
        responseObject.setUid(user.getId());

        response.add(responseObject);

        return response;
    }

    private static void assignDefaultFlashmobs(long uid) throws BaseException {
        FmFlashmobsDao flashmobsDao = new FmFlashmobsDao();
        List<FmFlashmobs> defaultFlashmobsList = flashmobsDao.getDefaultFlashmobs();

        if (defaultFlashmobsList != null && defaultFlashmobsList.size() > 0) {
            FmUsersFlashmobsDao usersFlashmobsDao = new FmUsersFlashmobsDao();

            for (FmFlashmobs flashmob : defaultFlashmobsList) {
                FmUsersFlashmobs usersFlashmobs = new FmUsersFlashmobs();
                usersFlashmobs.setUserId(uid);
                usersFlashmobs.setFlashmob_Id(flashmob.getId());
                usersFlashmobs.setStatus(1);
                usersFlashmobs.setDate_assign(ApplicationUtility.getCurrentTimeStampGMT_0());
                usersFlashmobs.setDate_end(0l);
                usersFlashmobs.setFriendshipWithUid(0l);
                usersFlashmobsDao.create(usersFlashmobs);
            }
        }
    }
}
