package com.wds.method.v1.user;

import com.wds.dao.FmFriendsDao;
import com.wds.entity.FmFriends;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingIdException;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.core.MultivaluedMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetIsUsersFriends {

    private static void validateRequest(MultivaluedMap<String, String> requestParameters) throws BaseException {
        if (StringUtils.isBlank(requestParameters.getFirst("user1Id")))
            throw new NotSendingIdException();
        if (StringUtils.isBlank(requestParameters.getFirst("user2Id")))
            throw new NotSendingIdException();
    }

    public static List<Map<String, Object>> performRequest(MultivaluedMap<String, String> requestParameters) throws BaseException {

        validateRequest(requestParameters);

        List<Map<String, Object>> resultObject = new ArrayList<>();

        FmFriendsDao friendsDao = new FmFriendsDao();
        FmFriends friends = friendsDao.findFriendsByUser1IdAndUser2Id(Long.valueOf(requestParameters.getFirst("user1Id")),
                Long.valueOf(requestParameters.getFirst("user2Id")));

        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("is_friends", friends != null);

        friends = friendsDao.findFollowUser(Long.valueOf(requestParameters.getFirst("user1Id")),
                Long.valueOf(requestParameters.getFirst("user2Id")));
        returnObject.put("is_follow", friends != null);

        friends = friendsDao.findFollowingUser(Long.valueOf(requestParameters.getFirst("user1Id")),
                Long.valueOf(requestParameters.getFirst("user2Id")));
        returnObject.put("is_following", friends != null);

        resultObject.add(returnObject);

        return resultObject;
    }
}
