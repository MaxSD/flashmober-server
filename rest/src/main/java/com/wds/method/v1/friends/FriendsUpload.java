package com.wds.method.v1.friends;

import com.wds.dao.FmFriendsDao;
import com.wds.dao.FmUsersDao;
import com.wds.entity.FmFriends;
import com.wds.entity.FmUser;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingIdException;
import com.wds.json.v1.friends.upload.FriendsUploadRequest;
import com.wds.json.v1.friends.upload.FriendsUploadSocialIdsObject;
import org.apache.commons.lang3.StringUtils;

public class FriendsUpload {

    private static void validateRequest(FriendsUploadRequest request) throws BaseException {
        if (request.getUid() == null || request.getUid() == 0)
            throw new NotSendingIdException();
    }

    public static void performRequest(FriendsUploadRequest request) throws BaseException {

        validateRequest(request);

        FmUsersDao usersDao = new FmUsersDao();
        FmFriendsDao fmFriendsDao = new FmFriendsDao();

        for (FriendsUploadSocialIdsObject friendSocialIds : request.getFriends()) {
            FmUser user = null;

            if (!StringUtils.isBlank(friendSocialIds.getVkId())) {
                user = usersDao.findByVkId(friendSocialIds.getVkId());
            } else if (!StringUtils.isBlank(friendSocialIds.getFbId())) {
                user = usersDao.findByFbId(friendSocialIds.getFbId());
            } else if (!StringUtils.isBlank(friendSocialIds.getTwId())) {
                user = usersDao.findByTwId(friendSocialIds.getTwId());
            }

            if (user != null) {

                FmFriends friend = fmFriendsDao.findByUser1IdAndUser2Id(request.getUid(), user.getId());

                if (friend == null) {
                    friend = new FmFriends();
                    friend.setUser1Id(request.getUid());
                    friend.setUser2Id(user.getId());
                    friend.setStatus(1);
                    fmFriendsDao.create(friend);
                }
            }
        }
    }
}
