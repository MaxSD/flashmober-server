package com.wds.method.v1.user;

import com.wds.dao.FmUsersDao;
import com.wds.entity.FmUser;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.WrongValueNameException;
import com.wds.json.v1.user.checkUserName.CheckUserNameRequest;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CheckUserName {

    private static void validateRequest(CheckUserNameRequest request) throws BaseException {
        if (StringUtils.isBlank(request.getName()))
            throw new WrongValueNameException();
    }

    public static List<Map<String, Object>> performRequest(CheckUserNameRequest request) throws BaseException {

        validateRequest(request);

        List<Map<String, Object>> resultObject = new ArrayList<>();

        FmUsersDao usersDao = new FmUsersDao();
        FmUser user = usersDao.findByName(request.getName());

        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("is_exists", user != null);
        resultObject.add(returnObject);

        return resultObject;
    }
}
