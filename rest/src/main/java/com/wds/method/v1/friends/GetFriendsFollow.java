package com.wds.method.v1.friends;

import com.wds.dao.FmFriendsDao;
import com.wds.entity.FmUser;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingIdException;
import com.wds.json.v1.friends.follow.GetFollowResponse;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.core.MultivaluedMap;
import java.util.List;

public class GetFriendsFollow {

    private static void validateRequest(MultivaluedMap<String, String> requestParameters) throws BaseException {
        if (StringUtils.isBlank(requestParameters.getFirst("uid")))
            throw new NotSendingIdException();
    }

    public static List<Object> performRequest(MultivaluedMap<String, String> requestParameters) throws BaseException {

        validateRequest(requestParameters);

        FmFriendsDao fmFriendsDao = new FmFriendsDao();
        return fmFriendsDao.getFriendsFollow(Long.valueOf(requestParameters.getFirst("uid")),
                new GetFollowResponse());
    }
}
