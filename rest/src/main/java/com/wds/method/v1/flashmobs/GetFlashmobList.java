package com.wds.method.v1.flashmobs;

import com.wds.dao.FmFlashmobsDao;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingIdException;
import com.wds.exception.v1.NotSendingStatusException;
import com.wds.json.v1.flashmobs.list.GetFlashmobListResponse;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.core.MultivaluedMap;
import java.util.List;

public class GetFlashmobList {

    private static void validateRequest(MultivaluedMap<String, String> requestParameters) throws BaseException {
        if (StringUtils.isBlank(requestParameters.getFirst("uid")))
            throw new NotSendingIdException();
        if (StringUtils.isBlank(requestParameters.getFirst("current_uid")))
            throw new NotSendingIdException();
        if (StringUtils.isBlank(requestParameters.getFirst("status")))
            throw new NotSendingStatusException();
    }

    public static List<Object> performRequest(MultivaluedMap<String, String> requestParameters) throws BaseException {

        validateRequest(requestParameters);

        FmFlashmobsDao flashmobsDao = new FmFlashmobsDao();

        return flashmobsDao.getFlashmobList(Long.valueOf(requestParameters.getFirst("uid")),
                Long.valueOf(requestParameters.getFirst("current_uid")),
                Integer.valueOf(requestParameters.getFirst("status")), new GetFlashmobListResponse());
    }
}
