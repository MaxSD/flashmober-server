package com.wds.method.v1.user;

import com.wds.dao.FmUsersDao;
import com.wds.entity.FmUser;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.UserNotFoundException;
import com.wds.exception.v1.WrongValueUserIdException;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class UserSettings {

    private static void validateRequest(Long uid, String email, String name, String photoURL,
                                        int gender, int age, String about,
                                        String country, String city, String site,
                                        Long[] categories_ids) throws BaseException {

        if (uid == null || uid == 0)
            throw new WrongValueUserIdException();
    }

    public static void performRequest(Long uid, String email, String name, String photoURL,
                                                              int gender, int age, String about,
                                                              String country, String city, String site,
                                                              List<Long> categories_ids,
                                                              List<String> languages) throws BaseException {

        FmUsersDao usersDao = new FmUsersDao();
        FmUser user = usersDao.findById(uid);

        if (user == null)
            throw new UserNotFoundException();

        //user.setEmail(StringUtils.trimToEmpty(email));
        user.setName(StringUtils.trimToEmpty(name));

        if (!StringUtils.isBlank(photoURL))
            user.setPhoto(StringUtils.trimToEmpty(photoURL));

        user.setGender(gender);
        user.setAge(age);
        user.setAbout(StringUtils.trimToEmpty(about));
        user.setCountry(StringUtils.trimToEmpty(country));
        user.setCity(StringUtils.trimToEmpty(city));
        user.setSite(StringUtils.trimToEmpty(site));

        usersDao.updateSettings(user, categories_ids, languages);
    }
}
