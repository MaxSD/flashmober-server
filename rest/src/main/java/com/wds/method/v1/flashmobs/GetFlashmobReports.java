package com.wds.method.v1.flashmobs;

import com.wds.dao.FmFlashmobsDao;
import com.wds.dao.FmUsersFlashmobsDao;
import com.wds.entity.FmUsersFlashmobs;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.FlashmobNotFoundException;
import com.wds.exception.v1.NotSendingIdException;
import com.wds.json.v1.flashmobs.report.GetFlashmobReportResponse;
import com.wds.json.v1.flashmobs.report.GetFlashmobReportsResponse;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.core.MultivaluedMap;
import java.util.List;

public class GetFlashmobReports {

    private static void validateRequest(MultivaluedMap<String, String> requestParameters) throws BaseException {
        if (StringUtils.isBlank(requestParameters.getFirst("user_flashmob_id")))
            throw new NotSendingIdException();
    }

    public static List<Object> performRequest(MultivaluedMap<String, String> requestParameters) throws BaseException {

        validateRequest(requestParameters);

        long userFlashmobId = Long.valueOf(requestParameters.getFirst("user_flashmob_id"));

        FmUsersFlashmobsDao usersFlashmobsDao = new FmUsersFlashmobsDao();
        FmUsersFlashmobs usersFlashmob = usersFlashmobsDao.findById(userFlashmobId);

        if (usersFlashmob == null)
            throw new FlashmobNotFoundException();

        FmFlashmobsDao flashmobsDao = new FmFlashmobsDao();

        return flashmobsDao.getFlashmobReports(usersFlashmob.getFlashmob_Id(), new GetFlashmobReportsResponse());
    }
}
