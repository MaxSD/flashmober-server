package com.wds.method.v1.friends;

import com.wds.dao.FmFriendsDao;
import com.wds.dao.FmUsersDao;
import com.wds.entity.FmUser;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingIdException;
import com.wds.exception.v1.UserIsLockedException;
import com.wds.exception.v1.UserNotFoundException;
import com.wds.json.v1.friends.counters.GetFriendsCountersResponse;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.core.MultivaluedMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetFriendsCounters {

    private static void validateRequest(MultivaluedMap<String, String> requestParameters) throws BaseException {
        if (StringUtils.isBlank(requestParameters.getFirst("uid")))
            throw new NotSendingIdException();
    }

    public static List<Map<String, Object>> performRequest(MultivaluedMap<String, String> requestParameters) throws BaseException {

        validateRequest(requestParameters);

        FmUsersDao usersDao = new FmUsersDao();
        FmUser user = usersDao.findById(Long.valueOf(requestParameters.getFirst("uid")));

        if (user == null)
            throw new UserNotFoundException();

        /**
         * Check if user is locked
         */
        if (user.isIsLocked())
            throw new UserIsLockedException("Ваш аккаунт заблокирован. Причина: " + user.getLockComment());

        List<Map<String, Object>> result = new ArrayList<>(1);
        FmFriendsDao fmFriendsDao = new FmFriendsDao();

        List<Object> resultList = fmFriendsDao.getFlashmobCounters(Long.valueOf(requestParameters.getFirst("uid")),
                new GetFriendsCountersResponse());

        if (resultList != null && resultList.size() > 0) {
            GetFriendsCountersResponse resultObj = (GetFriendsCountersResponse) resultList.get(0);
            if (resultObj != null) {
                boolean isFlashmobsDeletedByModerator = user.isIsFlashmobsDeletedByModerator();
                boolean isReportsDeletedByModerator = user.isIsReportsDeletedByModerator();

                Map<String, Object> resultRow = new HashMap<>(0);
                resultRow.put("friends_count", resultObj.getFriends_count());
                resultRow.put("follow_count", resultObj.getFollow_count());
                resultRow.put("following_count", resultObj.getFollowing_count());
                resultRow.put("created_flashmobs_count", resultObj.getCreated_flashmobs_count());
                resultRow.put("new_flashmobs_count", resultObj.getNew_flashmobs_count());
                resultRow.put("accepted_flashmobs_count", resultObj.getAccepted_flashmobs_count());
                resultRow.put("finished_flashmobs_count", resultObj.getFinished_flashmobs_count());
                resultRow.put("notoficatios_count", resultObj.getNotoficatios_count());
                resultRow.put("flashmobs_deleted_by_moderator", isFlashmobsDeletedByModerator);
                resultRow.put("reports_deleted_by_moderator", isReportsDeletedByModerator);
                resultRow.put("flashmobs_deleted_reason", resultObj.getFlashmobs_deleted_reason());
                resultRow.put("reports_deleted_reason", resultObj.getReports_deleted_reason());
                result.add(resultRow);
            }
        }

        //user.setIsIsFlashmobsDeletedByModerator(false);
        //user.setIsIsReportsDeletedByModerator(false);
        //usersDao.update(user);

        return result;
    }
}
