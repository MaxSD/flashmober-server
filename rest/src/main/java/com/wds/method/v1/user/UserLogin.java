package com.wds.method.v1.user;

import com.wds.dao.FmUsersDao;
import com.wds.dao.RolesDao;
import com.wds.database.Defines;
import com.wds.entity.FmUser;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingParameterException;
import com.wds.exception.v1.UserIsLockedException;
import com.wds.exception.v1.UserNotFoundException;
import com.wds.json.v1.user.login.UserLoginRequest;
import com.wds.json.v1.user.login.UserLoginResponse;
import com.wds.utils.ApplicationUtility;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class UserLogin {

    public static List<UserLoginResponse> performRequest(UserLoginRequest request) throws BaseException {

        boolean is_first_login_with_social = false;
        List<UserLoginResponse> response = new ArrayList<>(0);

        FmUsersDao usersDao = new FmUsersDao();
        FmUser user = null;
        if (!StringUtils.isBlank(request.getEmail()) && !StringUtils.isBlank(request.getPassword())) {
            // Search user by email and password
            user = usersDao.findByEmailAndPassword(StringUtils.trimToEmpty(request.getEmail()),
                    ApplicationUtility.md5crypt(request.getPassword()));

            if (user == null) {
                throw new UserNotFoundException();
            }

            /**
             * Check if user is locked
             */
            if (user.isIsLocked())
                throw new UserIsLockedException("Ваш аккаунт заблокирован. Причина: " + user.getLockComment());

        } else if (!StringUtils.isBlank(request.getEmail()) &&
                (!StringUtils.isBlank(request.getFbId()) ||
                        !StringUtils.isBlank(request.getTwId()) ||
                        !StringUtils.isBlank(request.getVkId()))) {

            // Search user by social networks ids
            if (!StringUtils.isBlank(request.getVkId()))
                user = usersDao.findByVkId(request.getVkId());

            if (!StringUtils.isBlank(request.getFbId()))
                user = usersDao.findByFbId(request.getFbId());

            if (!StringUtils.isBlank(request.getTwId()))
                user = usersDao.findByTwId(request.getTwId());

            if (user == null) {
                is_first_login_with_social = true;

                user = usersDao.findByEmail(request.getEmail());

                if (user != null) {
                    /**
                     * Check if user is locked
                     */
                    if (user.isIsLocked())
                        throw new UserIsLockedException("Ваш аккаунт заблокирован. Причина: " + user.getLockComment());

                    if (!StringUtils.isBlank(request.getVkId()) && StringUtils.isBlank(user.getVkId())) {
                        user.setVkId(StringUtils.trimToEmpty(request.getVkId()));
                    }
                    if (!StringUtils.isBlank(request.getFbId()) && StringUtils.isBlank(user.getFbId())) {
                        user.setFbId(StringUtils.trimToEmpty(request.getFbId()));
                    }
                    if (!StringUtils.isBlank(request.getTwId()) && StringUtils.isBlank(user.getTwId())) {
                        user.setTwId(StringUtils.trimToEmpty(request.getTwId()));
                    }
                }
            }
        } else {
            throw new NotSendingParameterException();
        }

        if (user == null) {
            user = new FmUser();
            user.setEmail(request.getEmail().trim().toLowerCase());
            user.setVkId(StringUtils.trimToEmpty(request.getVkId()));
            user.setFbId(StringUtils.trimToEmpty(request.getFbId()));
            user.setTwId(StringUtils.trimToEmpty(request.getTwId()));

            user.setName("[" + String.valueOf(System.currentTimeMillis()) + "]");
            user.setPhoto("");
            user.setSite("");
            user.setCountry("");
            user.setCity("");
            user.setAge(0);
            user.setAbout("");
            user.setGender(0);
            user.setIsBusinessUser(false);
            user.setIsLocked(false);
            user.setLockComment("");
            user.setLockExpireDate(0l);

            RolesDao rolesDao = new RolesDao();
            user.setRole(rolesDao.findById(Defines.USER_TYPE_USER));
            usersDao.create(user);
        } else {
            /**
             * Check if user is locked
             */
            if (user.isIsLocked())
                throw new UserIsLockedException("Ваш аккаунт заблокирован. Причина: " + user.getLockComment());

            if (!StringUtils.isBlank(request.getVkId()) && StringUtils.isBlank(user.getVkId())) {
                user.setVkId(StringUtils.trimToEmpty(request.getVkId()));
            }
            if (!StringUtils.isBlank(request.getFbId()) && StringUtils.isBlank(user.getFbId())) {
                user.setFbId(StringUtils.trimToEmpty(request.getFbId()));
            }
            if (!StringUtils.isBlank(request.getTwId()) && StringUtils.isBlank(user.getTwId())) {
                user.setTwId(StringUtils.trimToEmpty(request.getTwId()));
            }

            usersDao.update(user);
        }

        UserLoginResponse responseObject = new UserLoginResponse();
        responseObject.setUid(user.getId());
        responseObject.setFirst_login(is_first_login_with_social);

        response.add(responseObject);

        return response;
    }
}
