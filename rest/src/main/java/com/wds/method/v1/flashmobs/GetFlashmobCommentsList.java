package com.wds.method.v1.flashmobs;

import com.wds.dao.FmCommentsDao;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingIdException;
import com.wds.json.v1.flashmobs.comment.GetFlashmobCommentsListResponse;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.core.MultivaluedMap;
import java.util.List;

public class GetFlashmobCommentsList {

    private static void validateRequest(MultivaluedMap<String, String> requestParameters) throws BaseException {
        if (StringUtils.isBlank(requestParameters.getFirst("user_flashmob_id")))
            throw new NotSendingIdException();
    }

    public static List<Object> performRequest(MultivaluedMap<String, String> requestParameters) throws BaseException {

        validateRequest(requestParameters);

        FmCommentsDao commentsDao = new FmCommentsDao();

        return commentsDao.getCommentsList(Long.valueOf(requestParameters.getFirst("user_flashmob_id")),
                new GetFlashmobCommentsListResponse());
    }
}
