package com.wds.method.v1.user;

import com.wds.dao.FmUsersDao;
import com.wds.entity.FmUser;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.UserNotFoundException;

public class DeletedReportMessageIsViewed {

    public static void performRequest(long uid) throws BaseException {

        FmUsersDao usersDao = new FmUsersDao();
        FmUser user = usersDao.findById(uid);

        if (user == null)
            throw new UserNotFoundException();

        user.setIsIsReportsDeletedByModerator(false);
        usersDao.update(user);
    }
}
