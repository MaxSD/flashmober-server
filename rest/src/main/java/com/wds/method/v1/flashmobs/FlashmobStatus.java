package com.wds.method.v1.flashmobs;

import com.wds.dao.FmUsersFlashmobsDao;
import com.wds.entity.FmUsersFlashmobs;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.FlashmobNotFoundException;
import com.wds.exception.v1.NotSendingIdException;
import com.wds.utils.ApplicationUtility;
import org.apache.commons.lang3.StringUtils;

public class FlashmobStatus {

    private static void validateRequest(Long userFlashmobId, Integer status) throws BaseException {
        if (userFlashmobId == null || userFlashmobId == 0)
            throw new NotSendingIdException();
        if (status == null || status == 0)
            throw new NotSendingIdException();
    }

    public static void performRequest(Long userFlashmobId, Integer status,
                                      String videoReportURL, String photoReportURL) throws BaseException {

        validateRequest(userFlashmobId, status);

        FmUsersFlashmobsDao usersFlashmobsDao = new FmUsersFlashmobsDao();
        FmUsersFlashmobs usersFlashmobs =
                usersFlashmobsDao.findById(userFlashmobId);

        if (usersFlashmobs == null)
            throw new FlashmobNotFoundException();

        usersFlashmobs.setStatus(status);

        if (!StringUtils.isBlank(photoReportURL))
            usersFlashmobs.setPhoto_report(photoReportURL);

        if (!StringUtils.isBlank(videoReportURL))
            usersFlashmobs.setVideo_report(videoReportURL);

        /**
         * Accepted
         */
        if (status == 2) {
            usersFlashmobs.setDate_start(ApplicationUtility.getCurrentTimeStamp());
        }

        /**
         * Rejected || Finished
         */
        if (status == 3 || status == 4) {
            usersFlashmobs.setDate_end(ApplicationUtility.getCurrentTimeStamp());
        }

        usersFlashmobsDao.update(usersFlashmobs);
    }
}
