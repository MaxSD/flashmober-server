package com.wds.method.v1.flashmobs;

import com.wds.dao.FmFlashmobsDao;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingIdException;
import com.wds.json.v1.flashmobs.list.GetFleshmobListByOwnerIdResponse;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.core.MultivaluedMap;
import java.util.List;

public class GetFlashmobListByOwnerId {

    private static void validateRequest(MultivaluedMap<String, String> requestParameters) throws BaseException {
        if (StringUtils.isBlank(requestParameters.getFirst("uid")))
            throw new NotSendingIdException();
    }

    public static List<Object> performRequest(MultivaluedMap<String, String> requestParameters) throws BaseException {

        validateRequest(requestParameters);

        FmFlashmobsDao flashmobsDao = new FmFlashmobsDao();

        return flashmobsDao.getFlashmobListByOwnerId(Long.valueOf(requestParameters.getFirst("uid")),
                new GetFleshmobListByOwnerIdResponse());
    }
}
