package com.wds.method.v1.user;

import com.wds.dao.FmUsersDao;
import com.wds.entity.FmUser;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.UserNotFoundException;
import com.wds.exception.v1.WrongValueIdException;
import com.wds.exception.v1.WrongValueNameException;
import com.wds.json.v1.user.setUserName.SetUserNameRequest;
import org.apache.commons.lang3.StringUtils;

public class SetUserName {

    private static void validateRequest(SetUserNameRequest request) throws BaseException {
        if (request.getUid() == null || request.getUid() <= 0)
            throw new WrongValueIdException();
        if (StringUtils.isBlank(request.getName()))
            throw new WrongValueNameException();
    }

    public static void performRequest(SetUserNameRequest request) throws BaseException {

        validateRequest(request);

        FmUsersDao usersDao = new FmUsersDao();
        FmUser user = usersDao.findById(request.getUid());

        if (user == null)
            throw new UserNotFoundException();

        user.setName(request.getName());
        usersDao.update(user);
    }
}
