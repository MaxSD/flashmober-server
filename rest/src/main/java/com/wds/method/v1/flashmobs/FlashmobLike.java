package com.wds.method.v1.flashmobs;

import com.wds.dao.FmLikesDao;
import com.wds.dao.FmUserTokenDao;
import com.wds.dao.FmUsersDao;
import com.wds.dao.FmUsersFlashmobsDao;
import com.wds.entity.FmLike;
import com.wds.entity.FmUser;
import com.wds.entity.FmUserToken;
import com.wds.entity.FmUsersFlashmobs;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.FlashmobNotFoundException;
import com.wds.exception.v1.NotSendingIdException;
import com.wds.exception.v1.UserNotFoundException;
import com.wds.json.v1.flashmobs.like.FlashmobLikeRequest;
import com.wds.utils.ApplicationUtility;
import com.wds.utils.define.Defines;
import com.wds.utils.push.apple.ApplePushNotificationThread;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class FlashmobLike {

    private static void validateRequest(FlashmobLikeRequest request) throws BaseException {
        if (request.getUser_flashmob_id() == null || request.getUser_flashmob_id() == 0)
            throw new NotSendingIdException();
        if (request.getUid() == null || request.getUid() == 0)
            throw new NotSendingIdException();
    }

    public static void performRequest(FlashmobLikeRequest request) throws BaseException {

        validateRequest(request);

        FmUsersFlashmobsDao usersFlashmobsDao = new FmUsersFlashmobsDao();
        FmUsersFlashmobs usersFlashmobs = usersFlashmobsDao.findById(request.getUser_flashmob_id());

        if (usersFlashmobs == null)
            throw new FlashmobNotFoundException();

        FmUsersDao usersDao = new FmUsersDao();
        FmUser user = usersDao.findById(request.getUid());

        if (user == null)
            throw new UserNotFoundException();

        FmLikesDao likesDao = new FmLikesDao();
        FmLike like = likesDao.find(request.getUid(), request.getUser_flashmob_id());
        if (like != null) {
            like.setIsIsUnliked(false);
            likesDao.update(like);
        } else {
            like = new FmLike();
            like.setUserId(request.getUid());
            like.setUser_flashmob_id(usersFlashmobs.getId());
            like.setDate(ApplicationUtility.getCurrentTimeStamp());
            likesDao.create(like, usersFlashmobs);

            if (request.getUid() != usersFlashmobs.getUserId()) {
                /**
                 * Send push notification to user for whom the flashmob is assigned
                 */
                FmUserTokenDao userTokenDao = new FmUserTokenDao();
                FmUserToken userToken = userTokenDao.findByUserId(usersFlashmobs.getUserId());

                if (userToken != null) {
                    List<String> appleTokenList = new ArrayList<>();
                    appleTokenList.add(StringUtils.trimToEmpty(userToken.getToken()));

                    ApplePushNotificationThread applePushNotificationThread =
                            new ApplePushNotificationThread(appleTokenList, "Пользователю " + user.getName() + " нравится Ваш флешмоб",
                                    com.wds.utils.define.Defines.NOTIFICATION_TYPE_LIKE,
                                    usersFlashmobs.getId());
                    applePushNotificationThread.start();
                }
            }
        }
    }
}
