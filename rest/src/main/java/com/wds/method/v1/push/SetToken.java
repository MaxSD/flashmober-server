package com.wds.method.v1.push;

import com.wds.dao.FmUserTokenDao;
import com.wds.entity.FmUserToken;
import com.wds.exception.v1.*;
import com.wds.json.v1.push.SetTokenRequest;
import org.apache.commons.lang3.StringUtils;

public class SetToken {

    private static void validateRequest(SetTokenRequest request) throws BaseException {
        if (request.getUid() == null)
            throw new NotSendingIdException();
        if (StringUtils.isBlank(request.getToken()))
            throw new NotSendingTokenException();
    }

    public static void performRequest(SetTokenRequest request) throws BaseException {
        validateRequest(request);

        FmUserTokenDao userTokenDao = new FmUserTokenDao();

        FmUserToken userToken = userTokenDao.findByToken(request.getToken());
        if (userToken != null) {
            if (userToken.getUserId() != request.getUid()) {
                // delete the same token for another user
                userTokenDao.delete(userToken);
            }
        }

        userToken = userTokenDao.findByUserId(request.getUid());
        if (userToken == null)
            userToken = new FmUserToken();

        userToken.setUserId(request.getUid());
        userToken.setIsAndroid(request.isIsAndroid());
        userToken.setToken(request.getToken());

        userTokenDao.update(userToken);
    }
}
