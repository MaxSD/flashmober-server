package com.wds.method.v1.settings;

import com.wds.dao.FmSettingsDao;
import com.wds.dao.FmUserTokenDao;
import com.wds.entity.FmSettings;
import com.wds.entity.FmUserToken;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingIdException;
import com.wds.json.v1.settings.GetSettingsResponse;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.core.MultivaluedMap;
import java.util.ArrayList;
import java.util.List;

public class GetSettings {

    private static void validateRequest(MultivaluedMap<String, String> requestParameters) throws BaseException {
        if (StringUtils.isBlank(requestParameters.getFirst("uid")))
            throw new NotSendingIdException();
    }

    public static List<GetSettingsResponse> performRequest(MultivaluedMap<String, String> requestParameters) throws BaseException {

        validateRequest(requestParameters);

        long uid = Long.valueOf(requestParameters.getFirst("uid"));

        List<GetSettingsResponse> result = new ArrayList<>();
        GetSettingsResponse returnObject = new GetSettingsResponse();

        FmUserTokenDao userTokenDao = new FmUserTokenDao();
        FmUserToken userToken = userTokenDao.findByUserId(uid);
        returnObject.setGet_notifications(userToken != null);

        FmSettingsDao settingsDao = new FmSettingsDao();
        FmSettings settings = settingsDao.findByUserId(uid);

        if (settings == null) {
            returnObject.setGet_flashmobs(false);
            returnObject.setGet_flashmobs_from_friends_only(false);
            returnObject.setClose_profile(false);
            returnObject.setDefault_task(false);
        } else {
            returnObject.setGet_flashmobs(settings.isIsGetFlashmobs());
            returnObject.setGet_flashmobs_from_friends_only(settings.isIsGetFlashmobsFromFriendsOnly());
            returnObject.setClose_profile(settings.isIsCloseProfile());
            returnObject.setDefault_task(settings.isIsDefaultTask());
        }

        result.add(returnObject);

        return result;
    }
}
