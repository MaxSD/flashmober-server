package com.wds.method.v1.friends;

import com.wds.dao.*;
import com.wds.entity.*;
import com.wds.exception.v1.*;
import com.wds.json.v1.friends.add.FriendsAddRequest;
import com.wds.json.v1.friends.add.FriendsAddResponse;
import com.wds.utils.ApplicationUtility;
import com.wds.utils.push.apple.ApplePushNotificationThread;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class FriendsAdd {

    private static void validateRequest(FriendsAddRequest request) throws BaseException {
        if (request.getUid() == null || request.getUid() == 0)
            throw new NotSendingIdException();
        if (request.getFriendId() == null || request.getFriendId() == 0)
            throw new NotSendingIdException();
    }

    public static List<FriendsAddResponse> performRequest(FriendsAddRequest request) throws BaseException {

        validateRequest(request);

        List<FriendsAddResponse> result = new ArrayList<>();
        FriendsAddResponse returnObject = new FriendsAddResponse();

        FmFriendsDao fmFriendsDao = new FmFriendsDao();
        FmFriends friend = fmFriendsDao.findByUser1IdAndUser2Id(request.getUid(), request.getFriendId());

        if (friend != null)
            throw new RequestToAddFriendAlreadyExistsException();

        FmUsersDao usersDao = new FmUsersDao();
        FmUser user = usersDao.findById(request.getUid());

        if (user == null)
            throw new UserNotFoundException();

        FmSettingsDao settingsDao = new FmSettingsDao();
        FmSettings settings = settingsDao.findByUserId(request.getFriendId());

        if (settings.isIsDefaultTask()) {
            FmUsersFlashmobsDao usersFlashmobsDao = new FmUsersFlashmobsDao();

            /**
             * Get last finished flashmob by user
             */
            FmUsersFlashmobs usersFlashmobs = usersFlashmobsDao.getLastFinishedFlashmob(request.getFriendId());
            if (usersFlashmobs == null)
                throw new UserHasNoAnyFinishedFlashmobsException();

            /**
             * Assign the last finished flashmob to the new user (friend in the future)
             */
            FmUsersFlashmobs newUsersFlashmobs = new FmUsersFlashmobs();
            newUsersFlashmobs.setDate_start(ApplicationUtility.getCurrentTimeStampGMT_0());
            newUsersFlashmobs.setUserId(request.getUid());
            newUsersFlashmobs.setFlashmob_Id(usersFlashmobs.getFlashmob_Id());
            newUsersFlashmobs.setStatus(1);
            newUsersFlashmobs.setFriendshipWithUid(request.getFriendId());
            usersFlashmobsDao.create(newUsersFlashmobs);

            returnObject.setDefault_task(true);
        } else {
            returnObject.setDefault_task(false);
        }

        friend = new FmFriends();
        friend.setUser1Id(request.getUid());
        friend.setUser2Id(request.getFriendId());
        friend.setStatus(0);
        fmFriendsDao.create(friend);

        /**
         * Send push notification to user for whom the flashmob is assigned
         */
        FmUserTokenDao userTokenDao = new FmUserTokenDao();
        FmUserToken userToken = userTokenDao.findByUserId(request.getFriendId());

        if (userToken != null) {
            List<String> appleTokenList = new ArrayList<>();
            appleTokenList.add(StringUtils.trimToEmpty(userToken.getToken()));

            ApplePushNotificationThread applePushNotificationThread =
                    new ApplePushNotificationThread(appleTokenList, "Пользователь "+user.getName()+" хочет добавить Вас в друзья",
                            com.wds.utils.define.Defines.NOTIFICATION_TYPE_FRIENDSHIP,
                            0l);
            applePushNotificationThread.start();
        }

        result.add(returnObject);
        return result;
    }
}
