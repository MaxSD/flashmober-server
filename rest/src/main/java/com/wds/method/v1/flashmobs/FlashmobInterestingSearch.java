package com.wds.method.v1.flashmobs;

import com.wds.dao.FmFlashmobsDao;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingParameterException;
import com.wds.json.v1.flashmobs.interesting.FlashmobInterestingSearchRequest;
import com.wds.json.v1.flashmobs.interesting.FlashmobInterestingSearchResponse;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class FlashmobInterestingSearch {

    private static void validateRequest(FlashmobInterestingSearchRequest request) throws BaseException {
        if ((request.getTags() == null || request.getTags().length == 0) &&
                StringUtils.isBlank(request.getDescription())) {

            throw new NotSendingParameterException();
        }
    }

    public static List<Object> performRequest(FlashmobInterestingSearchRequest request) throws BaseException {

        validateRequest(request);

        FmFlashmobsDao flashmobsDao = new FmFlashmobsDao();

        return flashmobsDao.searchByTags(request.getTags(), request.getDescription(),
                new FlashmobInterestingSearchResponse());
    }
}
