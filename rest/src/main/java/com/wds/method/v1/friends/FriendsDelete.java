package com.wds.method.v1.friends;

import com.wds.dao.FmFriendsDao;
import com.wds.entity.FmFriends;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingIdException;
import com.wds.json.v1.friends.delete.FriendsDeleteRequest;

public class FriendsDelete {

    private static void validateRequest(FriendsDeleteRequest request) throws BaseException {
        if (request.getUid() == null || request.getUid() == 0)
            throw new NotSendingIdException();
        if (request.getFriendId() == null || request.getFriendId() == 0)
            throw new NotSendingIdException();
    }

    public static void performRequest(FriendsDeleteRequest request) throws BaseException {

        validateRequest(request);

        FmFriendsDao fmFriendsDao = new FmFriendsDao();
        FmFriends friend = fmFriendsDao.findByUser1IdAndUser2Id(request.getFriendId(), request.getUid());

        if (friend != null) {
            if (friend.getStatus() == 0) {
                fmFriendsDao.delete(request.getUid(), request.getFriendId());
            } else {
                friend.setUser1Id(request.getFriendId());
                friend.setUser2Id(request.getUid());
                friend.setStatus(0);
                fmFriendsDao.deleteFriendship(friend);
            }
        }
    }
}
