package com.wds.method.v1.friends;

import com.wds.dao.FmFriendsDao;
import com.wds.dao.FmSettingsDao;
import com.wds.dao.FmUsersFlashmobsDao;
import com.wds.entity.FmFriends;
import com.wds.entity.FmSettings;
import com.wds.entity.FmUsersFlashmobs;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingIdException;
import com.wds.exception.v1.RequestToAddFriendNotFoundException;
import com.wds.exception.v1.UserHasNoAnyFinishedFlashmobForFriendshipException;
import com.wds.json.v1.friends.accept.FriendsAcceptRequest;

public class FriendsAccept {

    private static void validateRequest(FriendsAcceptRequest request) throws BaseException {
        if (request.getUid() == null || request.getUid() == 0)
            throw new NotSendingIdException();
        if (request.getFriendId() == null || request.getFriendId() == 0)
            throw new NotSendingIdException();
    }

    public static void performRequest(FriendsAcceptRequest request) throws BaseException {

        validateRequest(request);

        FmFriendsDao fmFriendsDao = new FmFriendsDao();
        FmFriends friend = fmFriendsDao.findByUser1IdAndUser2Id(request.getFriendId(), request.getUid());

        if (friend == null)
            throw new RequestToAddFriendNotFoundException();

        FmSettingsDao settingsDao = new FmSettingsDao();
        FmSettings settings = settingsDao.findByUserId(request.getUid());

        if (settings.isIsDefaultTask()) {
            FmUsersFlashmobsDao usersFlashmobsDao = new FmUsersFlashmobsDao();

            /**
             * Get default task for friendship with me
             */
            FmUsersFlashmobs usersFlashmobs =
                    usersFlashmobsDao.getFinishedFlashmobForFriendShip(request.getFriendId(), request.getUid());

            if (usersFlashmobs == null)
                throw new UserHasNoAnyFinishedFlashmobForFriendshipException();
        }

        friend.setStatus(1);
        fmFriendsDao.acceptFriendship(friend);
    }
}
