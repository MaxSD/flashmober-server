package com.wds.method.v1.settings;

import com.wds.dao.FmSettingsDao;
import com.wds.dao.FmUsersFlashmobsDao;
import com.wds.database.Defines;
import com.wds.entity.FmSettings;
import com.wds.entity.FmUsersFlashmobs;
import com.wds.exception.v1.*;
import com.wds.json.v1.settings.SettingsRequest;

public class SetSetting {

    private static void validateRequest(SettingsRequest request) throws BaseException {
        if (request.getUid() == null || request.getUid() == 0)
            throw new WrongValueUserIdException();
        if (request.getValue() == null)
            throw new WrongSettingsValueException();
    }

    public static void performRequest(SettingsRequest request, int settingType) throws BaseException {

        validateRequest(request);

        FmSettingsDao settingsDao = new FmSettingsDao();
        FmSettings settings = settingsDao.findByUserId(request.getUid());

        if (settings == null) {
            settings = new FmSettings();
            settings.setUserId(request.getUid());
        }

        if (settingType == com.wds.define.Defines.SETTING_IS_GET_FLASHMOBS)
            settings.setIsGetFlashmobs(request.getValue());
        if (settingType == com.wds.define.Defines.SETTING_IS_GET_FLASHMOBS_FROM_FRIENDS_ONLY)
            settings.setIsGetFlashmobsFromFriendsOnly(request.getValue());
        if (settingType == com.wds.define.Defines.SETTING_IS_CLOSE_PROFILE)
            settings.setIsCloseProfile(request.getValue());

        if (settingType == com.wds.define.Defines.SETTING_DEFAULT_TASK) {
            if (request.getValue()) {
                FmUsersFlashmobsDao usersFlashmobsDao = new FmUsersFlashmobsDao();

                /**
                 * Get last finished flashmob by user
                 */
                FmUsersFlashmobs usersFlashmobs = usersFlashmobsDao.getLastFinishedFlashmob(request.getUid());
                if (usersFlashmobs == null)
                    throw new UserHasNoAnyFinishedFlashmobsException();
            }

            settings.setIsDefaultTask(request.getValue());
        }

        settingsDao.update(settings);
    }
}
