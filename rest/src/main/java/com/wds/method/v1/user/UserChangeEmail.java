package com.wds.method.v1.user;

import com.wds.dao.FmChangeEmailDao;
import com.wds.dao.FmUsersDao;
import com.wds.entity.FmChangeEmail;
import com.wds.entity.FmUser;
import com.wds.exception.v1.*;
import com.wds.json.v1.user.changeEmail.UserChangeEmailRequest;
import com.wds.utils.ApplicationUtility;
import com.wds.utils.SenderEmail;
import org.apache.commons.lang3.StringUtils;

public class UserChangeEmail {

    private static void validateRequest(UserChangeEmailRequest request) throws BaseException {
        if (request.getUid() == null || request.getUid() == 0)
            throw new WrongValueUserIdException();
        if (StringUtils.isBlank(request.getNewEmail()))
            throw new WrongValueNewEmailException();
    }

    public static void performRequest(UserChangeEmailRequest request) throws BaseException {

        validateRequest(request);

        FmUsersDao usersDao = new FmUsersDao();
        FmUser user = usersDao.findById(request.getUid());

        if (user == null)
            throw new UserNotFoundException();

        FmChangeEmailDao changeEmailDao = new FmChangeEmailDao();
        changeEmailDao.delete(request.getUid());

        FmChangeEmail changeEmail = new FmChangeEmail();
        changeEmail.setUserId(request.getUid());
        changeEmail.setEmail(request.getNewEmail());

        int code = ApplicationUtility.generateRandomInt(1000, 9999);
        changeEmail.setCode(code);
        changeEmail.setStatus(0);
        changeEmailDao.create(changeEmail);

        try {
            SenderEmail senderEmail = new SenderEmail();
            senderEmail.sendEmail(user.getEmail(), "Change user's email", "Secret code for changing user's email: " + code);
        } catch (Exception ex) {
            throw new NotSendingEmailException();
        }
    }
}
