package com.wds.method.v1.flashmobs;

import com.wds.dao.FmLikesDao;
import com.wds.dao.FmUsersFlashmobsDao;
import com.wds.entity.FmLike;
import com.wds.entity.FmUsersFlashmobs;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.FlashmobNotFoundException;
import com.wds.exception.v1.NotSendingIdException;
import com.wds.json.v1.flashmobs.like.FlashmobUnlikeRequest;

public class FlashmobUnlike {

    private static void validateRequest(FlashmobUnlikeRequest request) throws BaseException {
        if (request.getUser_flashmob_id() == null || request.getUser_flashmob_id() == 0)
            throw new NotSendingIdException();
        if (request.getUid() == null || request.getUid() == 0)
            throw new NotSendingIdException();
    }

    public static void performRequest(FlashmobUnlikeRequest request) throws BaseException {

        validateRequest(request);

        FmUsersFlashmobsDao usersFlashmobsDao = new FmUsersFlashmobsDao();
        FmUsersFlashmobs usersFlashmobs = usersFlashmobsDao.findById(request.getUser_flashmob_id());

        if (usersFlashmobs == null)
            throw new FlashmobNotFoundException();

        FmLikesDao likesDao = new FmLikesDao();
        FmLike like = likesDao.find(request.getUid(), usersFlashmobs.getId());

        if (like != null) {
            like.setIsIsUnliked(true);
            likesDao.update(like);
        }
    }
}
