package com.wds.method.v1.flashmobs;

import com.wds.dao.FmFlashmobsDao;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingIdException;
import com.wds.json.v1.flashmobs.get.GetFlashmobResponse;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.core.MultivaluedMap;
import java.util.ArrayList;
import java.util.List;

public class GetFlashmob {

    private static void validateRequest(MultivaluedMap<String, String> requestParameters) throws BaseException {
        if (StringUtils.isBlank(requestParameters.getFirst("user_flashmob_id")))
            throw new NotSendingIdException();
        if (StringUtils.isBlank(requestParameters.getFirst("current_uid")))
            throw new NotSendingIdException();
    }

    public static List<GetFlashmobResponse> performRequest(MultivaluedMap<String, String> requestParameters) throws BaseException {

        validateRequest(requestParameters);

        FmFlashmobsDao flashmobsDao = new FmFlashmobsDao();

        List<GetFlashmobResponse> response = new ArrayList<>();

        GetFlashmobResponse returnObject =
                (GetFlashmobResponse) flashmobsDao.getFlashmobByUserFlashmobId(
                        Long.valueOf(requestParameters.getFirst("user_flashmob_id")),
                        Long.valueOf(requestParameters.getFirst("current_uid")),
                        new GetFlashmobResponse());

        if (returnObject != null)
            response.add(returnObject);

        return response;
    }
}
