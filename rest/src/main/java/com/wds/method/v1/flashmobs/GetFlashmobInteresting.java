package com.wds.method.v1.flashmobs;

import com.wds.dao.FmFlashmobsDao;
import com.wds.exception.v1.BaseException;
import com.wds.json.v1.flashmobs.interesting.GetFlashmobInterestinResponse;
import com.wds.utils.ApplicationUtility;

import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

public class GetFlashmobInteresting {

    public static List<Object> performRequest() throws BaseException {
        FmFlashmobsDao flashmobsDao = new FmFlashmobsDao();

        TimeZone timeZone = TimeZone.getTimeZone("GMT+0");
        Calendar calendar = Calendar.getInstance(timeZone);
        calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) - 7);

        return flashmobsDao.getInteresting(new GetFlashmobInterestinResponse() ,
                calendar.getTimeInMillis() / 1000, ApplicationUtility.getCurrentTimeStamp());
    }
}
