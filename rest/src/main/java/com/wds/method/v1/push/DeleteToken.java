package com.wds.method.v1.push;

import com.wds.dao.FmUserTokenDao;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingIdException;
import com.wds.exception.v1.NotSendingTokenException;
import com.wds.json.v1.push.DeleteTokenRequest;
import org.apache.commons.lang3.StringUtils;

public class DeleteToken {

    private static void validateRequest(DeleteTokenRequest request) throws BaseException {
        if (request.getUid() == null)
            throw new NotSendingIdException();
        if (StringUtils.isBlank(request.getToken()))
            throw new NotSendingTokenException();
    }

    public static void performRequest(DeleteTokenRequest request) throws BaseException {
        validateRequest(request);

        FmUserTokenDao userTokenDao = new FmUserTokenDao();
        userTokenDao.delete(request.getUid(), request.getToken());
    }
}
