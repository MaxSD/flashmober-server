package com.wds.method.v1.user;

import com.wds.dao.FmChangeEmailDao;
import com.wds.dao.FmUsersDao;
import com.wds.entity.FmChangeEmail;
import com.wds.entity.FmUser;
import com.wds.exception.v1.*;
import com.wds.json.v1.user.changeEmail.UserChangeEmailAcceptRequest;
import com.wds.json.v1.user.changeEmail.UserChangeEmailRequest;
import com.wds.utils.ApplicationUtility;
import org.apache.commons.lang3.StringUtils;

public class UserChangeEmailAccept {

    private static void validateRequest(UserChangeEmailAcceptRequest request) throws BaseException {
        if (request.getUid() == null || request.getUid() == 0)
            throw new WrongValueUserIdException();
        if (request.getCode() == null || request.getCode() == 0)
            throw new WrongValueNewEmailException();
    }

    public static void performRequest(UserChangeEmailAcceptRequest request) throws BaseException {

        validateRequest(request);

        FmUsersDao usersDao = new FmUsersDao();
        FmUser user = usersDao.findById(request.getUid());

        if (user == null)
            throw new UserNotFoundException();

        FmChangeEmailDao changeEmailDao = new FmChangeEmailDao();
        FmChangeEmail changeEmail = changeEmailDao.findByUid(request.getUid());

        // Check if request to change user's email exists
        if (changeEmail == null)
            throw new RequestToChangeEmailIsNotFoundException();

        // Check code for changing user's email
        if (changeEmail.getCode() != request.getCode())
            throw new IncorrectCodeException();

        // Set new user's email
        user.setEmail(changeEmail.getEmail());
        usersDao.update(user);

        // Delete request to change user's email
        changeEmailDao.delete(changeEmail);
    }
}
