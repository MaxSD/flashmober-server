package com.wds.method.v1.flashmobs;

import com.wds.dao.FmCommentsDao;
import com.wds.dao.FmUserTokenDao;
import com.wds.dao.FmUsersDao;
import com.wds.dao.FmUsersFlashmobsDao;
import com.wds.entity.FmComment;
import com.wds.entity.FmUser;
import com.wds.entity.FmUserToken;
import com.wds.entity.FmUsersFlashmobs;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.FlashmobNotFoundException;
import com.wds.exception.v1.NotSendingIdException;
import com.wds.exception.v1.UserNotFoundException;
import com.wds.json.v1.flashmobs.comment.FlashmobCommentRequest;
import com.wds.utils.ApplicationUtility;
import com.wds.utils.define.Defines;
import com.wds.utils.push.apple.ApplePushNotificationThread;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class FlashmobComment {

    private static void validateRequest(FlashmobCommentRequest request) throws BaseException {
        if (request.getUser_flashmob_id() == null || request.getUser_flashmob_id() == 0)
            throw new NotSendingIdException();
        if (request.getUid() == null || request.getUid() == 0)
            throw new NotSendingIdException();
    }

    public static void performRequest(FlashmobCommentRequest request) throws BaseException {

        validateRequest(request);

        FmUsersFlashmobsDao usersFlashmobsDao = new FmUsersFlashmobsDao();
        FmUsersFlashmobs usersFlashmobs = usersFlashmobsDao.findById(request.getUser_flashmob_id());

        if (usersFlashmobs == null)
            throw new FlashmobNotFoundException();

        FmUsersDao usersDao = new FmUsersDao();
        FmUser user = usersDao.findById(request.getUid());

        if (user == null)
            throw new UserNotFoundException();

        FmCommentsDao commentsDao = new FmCommentsDao();
        FmComment comment = new FmComment();

        comment.setUserId(request.getUid());
        comment.setUser_flashmob_id(usersFlashmobs.getId());
        comment.setComment(request.getComment());
        comment.setDate(ApplicationUtility.getCurrentTimeStamp());

        commentsDao.create(comment, usersFlashmobs);

        if (request.getUid() != usersFlashmobs.getUserId()) {
            /**
             * Send push notification to user for whom the flashmob is assigned
             */
            ApplePushNotificationThread applePushNotificationThread;
            FmUserTokenDao userTokenDao = new FmUserTokenDao();
            FmUserToken userToken = userTokenDao.findByUserId(usersFlashmobs.getUserId());

            if (userToken != null) {
                List<String> appleTokenList = new ArrayList<>();
                appleTokenList.add(StringUtils.trimToEmpty(userToken.getToken()));

                applePushNotificationThread =
                        new ApplePushNotificationThread(appleTokenList, "Пользователь " + user.getName() + " прокоментировал Ваш флешмоб",
                                com.wds.utils.define.Defines.NOTIFICATION_TYPE_COMMENT,
                                usersFlashmobs.getId());
                applePushNotificationThread.start();
            }

            /**
             * Send push notifications to the users in the Comment
             */
            if (request.getUsers_names_for_push() != null && request.getUsers_names_for_push().size() > 0) {
                userTokenDao = new FmUserTokenDao();
                List<String> tokensList = userTokenDao.getTokensByUsersNames(request.getUsers_names_for_push());

                if (tokensList.size() > 0) {
                    applePushNotificationThread =
                            new ApplePushNotificationThread(tokensList, "Пользователь " + user.getName() + " оставил Вам комментарий",
                                    com.wds.utils.define.Defines.NOTIFICATION_TYPE_COMMENT_USERS,
                                    usersFlashmobs.getId());
                    applePushNotificationThread.start();
                }
            }
        }
    }
}
