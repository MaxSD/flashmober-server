package com.wds.method.v1.user;

import com.wds.dao.FmUsersDao;
import com.wds.entity.FmUser;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingEmailException;
import com.wds.exception.v1.UserNotFoundException;
import com.wds.json.v1.user.forgotPassword.UserForgotPasswordRequest;
import com.wds.utils.ApplicationUtility;
import com.wds.utils.SenderEmail;

public class UserForgotPassword {

    public static void performRequest(UserForgotPasswordRequest request) throws BaseException {

        try {
            String newPassword = ApplicationUtility.generateRandomString(8);

            FmUsersDao usersDao = new FmUsersDao();
            FmUser user = usersDao.findByEmail(request.getEmail());

            if (user == null)
                throw new UserNotFoundException();

            user.setPassword(ApplicationUtility.md5crypt(newPassword));
            usersDao.update(user);

            SenderEmail senderEmail = new SenderEmail();
            senderEmail.sendEmail(request.getEmail(), "New password", "Your password has been changed.\n Your new password is: " + newPassword);
        } catch (Exception ex) {
            throw new NotSendingEmailException();
        }
    }
}
