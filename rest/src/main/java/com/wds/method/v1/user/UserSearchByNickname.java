package com.wds.method.v1.user;

import com.wds.dao.FmUsersDao;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingParameterException;
import com.wds.json.v1.user.search.SearchByNicknameRequest;
import com.wds.json.v1.user.search.SearchByNicknameResponse;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class UserSearchByNickname {

    private static void validateRequest(SearchByNicknameRequest request) throws BaseException {
        if (StringUtils.isBlank(request.getNickname()))
            throw new NotSendingParameterException();
    }

    public static List<Object> performRequest(SearchByNicknameRequest request) throws BaseException {

        validateRequest(request);

        FmUsersDao usersDao = new FmUsersDao();
        return usersDao.searchByNickname(request.getNickname(), new SearchByNicknameResponse());
    }
}
