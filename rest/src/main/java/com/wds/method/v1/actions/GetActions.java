package com.wds.method.v1.actions;

import com.wds.dao.FmActionsDao;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingIdException;
import com.wds.json.v1.actions.GetActionsResponse;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.core.MultivaluedMap;
import java.util.ArrayList;
import java.util.List;

public class GetActions {

    private static void validateRequest(MultivaluedMap<String, String> requestParameters) throws BaseException {
        if (StringUtils.isBlank(requestParameters.getFirst("uid")))
            throw new NotSendingIdException();
    }

    public static List<Object> performRequest(MultivaluedMap<String, String> requestParameters) throws BaseException {

        validateRequest(requestParameters);
        long uid = Long.valueOf(requestParameters.getFirst("uid"));

        FmActionsDao actionsDao = new FmActionsDao();

        return actionsDao.findActionsForUser(uid, new GetActionsResponse());
    }
}
