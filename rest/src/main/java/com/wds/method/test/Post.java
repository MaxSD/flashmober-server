package com.wds.method.test;

import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingIdException;
import com.wds.exception.v1.WrongValueIdException;
import com.wds.json.test.post.PostRequest;

public class Post {

    private static void validateRequest(PostRequest request) throws BaseException {
        if (request.getId() == null)
            throw new NotSendingIdException();
        if (request.getId() <= 0)
            throw new WrongValueIdException();
    }

    public static void performRequest(PostRequest request) throws BaseException {
        validateRequest(request);
    }
}
