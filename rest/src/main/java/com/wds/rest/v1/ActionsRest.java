package com.wds.rest.v1;

import com.wds.exception.v1.BaseException;
import com.wds.json.v1.ErrorReturn;
import com.wds.method.v1.actions.GetActions;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

@Path("v1/actions")
@Produces(MediaType.APPLICATION_JSON)
public class ActionsRest {

    @GET
    @Path("/list/{uid}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getActionsList(@Context UriInfo uriInfo) {
        String methodName = "/actions/list/{uid}";

        MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();
        try {
            return Response.ok(GetActions.performRequest(pathParams)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }
}
