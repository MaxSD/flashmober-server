package com.wds.rest.v1;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.FormDataParam;
import com.wds.define.Defines;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingParameterException;
import com.wds.json.v1.ErrorReturn;
import com.wds.json.v1.flashmobs.assign.FlashmobAssingToUsersRequest;
import com.wds.json.v1.flashmobs.comment.FlashmobCommentDeleteRequest;
import com.wds.json.v1.flashmobs.comment.FlashmobCommentRequest;
import com.wds.json.v1.flashmobs.delete.FlashmobDeleteRequest;
import com.wds.json.v1.flashmobs.interesting.FlashmobInterestingSearchRequest;
import com.wds.json.v1.flashmobs.like.FlashmobLikeRequest;
import com.wds.json.v1.flashmobs.like.FlashmobUnlikeRequest;
import com.wds.method.v1.flashmobs.*;
import com.wds.utils.ApplicationUtility;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Path("v1/flashmob")
@Produces(MediaType.APPLICATION_JSON)
public class FlashmobRest {

    @POST
    @Path("/create")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response flashmobCreate(
            FormDataMultiPart multiPart,
            @FormDataParam("is_from_admin_area") Boolean is_from_admin_area,
            @FormDataParam("uid") Long uid,
            @FormDataParam("description") String description,
            @FormDataParam("send_into_abyss") Boolean send_into_abyss,
            @FormDataParam("moderation") Boolean moderation,
            @FormDataParam("is_accept_by_admin") Boolean is_accept_by_admin,
            @FormDataParam("is_default") Boolean is_default,
            @FormDataParam("comment") String comment,
            @FormDataParam("video") String video,
            @FormDataParam("datePublication") String datePublication,
            @FormDataParam("photo") InputStream photo,
            @FormDataParam("photo") FormDataContentDisposition photoDetail,
            @FormDataParam("photoFileName") String photoFileName,
            @FormDataParam("photoFileNameForUpload") String photoFileNameForUpload) {

        if (is_from_admin_area == null) is_from_admin_area = false;
        if (send_into_abyss == null) send_into_abyss = false;
        if (is_default == null) is_default = false;
        List<FormDataBodyPart> categoriesList = multiPart.getFields("categories_ids");
        List<FormDataBodyPart> tagsList = multiPart.getFields("tags");
        List<FormDataBodyPart> friends_idsList = multiPart.getFields("friends_ids");
        List<FormDataBodyPart> languages = multiPart.getFields("languages");

        String methodName = "/flashmob/create";
        try {

            List<Long> categoriesIds = new ArrayList<>();
            if (categoriesList != null) {
                for (FormDataBodyPart field : categoriesList) {
                    categoriesIds.add(Long.valueOf(field.getValue()));
                }
            }

            List<String> tags = new ArrayList<>();
            if (tagsList != null) {
                if (is_from_admin_area) {
                    for (String tag : StringUtils.split(tagsList.get(0).getValue(), ",")) {
                        tags.add(StringUtils.trim(tag));
                    }
                } else {
                    for (FormDataBodyPart field : tagsList) {
                        tags.add(field.getValue());
                    }
                }
            }

            List<Long> friends_ids = new ArrayList<>();
            if (friends_idsList != null) {
                for (FormDataBodyPart field : friends_idsList) {
                    friends_ids.add(Long.valueOf(field.getValue()));
                }
            }

            List<String> languagesList = new ArrayList<>();
            if (languages != null) {
                for (FormDataBodyPart field : languages) {
                    languagesList.add(field.getValue());
                }
            }

            String photoURL = "";

            if (StringUtils.isBlank(photoFileName)) {
                if (photo != null) {
                    photoFileName = String.valueOf(System.currentTimeMillis()) + String.valueOf(uid) + "." + StringUtils.substringAfterLast(photoFileName, ".");

                    ApplicationUtility.writeToFile(photo, com.wds.utils.define.Defines.getUsersPhotoPathStorage() + photoFileName);
                    photoURL = Defines.getHostImages() + photoFileName;
                }
            } else {
                photoURL = Defines.getHostImages() + photoFileNameForUpload + "." + StringUtils.substringAfterLast(photoFileName, ".");
            }

            return Response.ok(FlashmobCreate.performRequest(uid, description, categoriesIds, tags, friends_ids, languagesList,
                    send_into_abyss, video, photoURL, datePublication, is_default)).build();
            //return Response.ok().build();

        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/assign_to_users")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response assignToUsers(FlashmobAssingToUsersRequest request) {

        String methodName = "/flashmob/assign_to_users";
        try {
            return Response.ok(FlashmobAssignToUsers.performRequest(request)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @GET
    @Path("/list/{uid}/{current_uid}/{status}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getFlashmobsList(@Context UriInfo uriInfo) {
        String methodName = "/flashmob/list/{uid}/{current_uid}/{status}";

        MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();
        try {
            return Response.ok(GetFlashmobList.performRequest(pathParams)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @GET
    @Path("/list_by_ownerId/{uid}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getFlashmobsListByOwnerId(@Context UriInfo uriInfo) {
        String methodName = "/flashmob/list_by_ownerId/{uid}";

        MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();
        try {
            return Response.ok(GetFlashmobListByOwnerId.performRequest(pathParams)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @GET
    @Path("/reports_by_flashmob/{user_flashmob_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getFlashmobsReports(@Context UriInfo uriInfo) {
        String methodName = "/flashmob/reports_by_flashmob/{user_flashmob_id}";

        MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();
        try {
            return Response.ok(GetFlashmobReports.performRequest(pathParams)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @GET
    @Path("/report/{uid}/{offset}/{limit}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getFlashmobsReport(@Context UriInfo uriInfo) {
        String methodName = "/flashmob/report/{uid}/{offset}/{limit}";

        MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();
        try {
            return Response.ok(GetFlashmobReport.performRequest(pathParams)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @GET
    @Path("/report_by_user_groups/{uid}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getFlashmobsReportByUserGroups(@Context UriInfo uriInfo) {
        String methodName = "/flashmob/report_by_user_groups/{uid}";

        MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();
        try {
            return Response.ok(GetFlashmobReportByUserGroups.performRequest(pathParams)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/status")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response changeStatus(
            @FormDataParam("user_flashmob_id") Long userFlashmobId,
            @FormDataParam("status") Integer status,
            @FormDataParam("video_report") String videoReportURL,
            @FormDataParam("photo_report") InputStream photoReport,
            @FormDataParam("photo_report") FormDataContentDisposition photoReportDetail
    ) {

        String methodName = "/flashmob/status";
        try {
            String photoReportURL = "";

            if (photoReport != null) {
                ApplicationUtility.writeToFile(photoReport, com.wds.utils.define.Defines.getUsersPhotoPathStorage() + photoReportDetail.getFileName());
                photoReportURL = Defines.getHostImages() + photoReportDetail.getFileName();
            }

            FlashmobStatus.performRequest(userFlashmobId, status, videoReportURL, photoReportURL);
            return Response.ok(new ArrayList<>(0)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @GET
    @Path("/{user_flashmob_id}/{current_uid}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getFlashmob(@Context UriInfo uriInfo) {
        String methodName = "/flashmob/{user_flashmob_id}/{current_uid}";

        MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();
        try {
            return Response.ok(GetFlashmob.performRequest(pathParams)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/like")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response like(FlashmobLikeRequest request) {
        String methodName = "/flashmob/like";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            FlashmobLike.performRequest(request);
            return Response.ok(new ArrayList<>(0)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/unlike")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response unlike(FlashmobUnlikeRequest request) {
        String methodName = "/flashmob/unlike";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            FlashmobUnlike.performRequest(request);
            return Response.ok(new ArrayList<>(0)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/comment/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addComment(FlashmobCommentRequest request) {
        String methodName = "/flashmob/comment/add";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            FlashmobComment.performRequest(request);
            return Response.ok(new ArrayList<>(0)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @GET
    @Path("/comment/list/{user_flashmob_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getCommentsList(@Context UriInfo uriInfo) {
        String methodName = "/comment/list/{user_flashmob_id}";

        MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();
        try {
            return Response.ok(GetFlashmobCommentsList.performRequest(pathParams)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/comment/delete")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteComment(FlashmobCommentDeleteRequest request) {
        String methodName = "/flashmob/comment/add";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            FlashmobCommentDelete.performRequest(request);
            return Response.ok(new ArrayList<>(0)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @GET
    @Path("/interesting")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getInteresting(@Context UriInfo uriInfo) {
        String methodName = "/interesting";

        try {
            return Response.ok(GetFlashmobInteresting.performRequest()).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/interesting/search")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response interestingSearch(FlashmobInterestingSearchRequest request) {
        String methodName = "/interesting/search";

        try {
            return Response.ok(FlashmobInterestingSearch.performRequest(request)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteFlashmob(FlashmobDeleteRequest request) {
        String methodName = "/delete";

        try {
            FlashmobDelete.performRequest(request);
            return Response.ok(new ArrayList<>(0)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }
}
