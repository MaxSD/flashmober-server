package com.wds.rest.v1;

import com.wds.database.Defines;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingParameterException;
import com.wds.json.v1.ErrorReturn;
import com.wds.json.v1.settings.SettingsRequest;
import com.wds.method.v1.settings.GetSettings;
import com.wds.method.v1.settings.SetSetting;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.ArrayList;

@Path("v1/settings")
@Produces(MediaType.APPLICATION_JSON)
public class SettingsRest {

    @POST
    @Path("/get_flashmobs")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getFlashmobs(SettingsRequest request) {
        String methodName = "/settings/get_flashmobs";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            SetSetting.performRequest(request, com.wds.define.Defines.SETTING_IS_GET_FLASHMOBS);
            return Response.ok(new ArrayList<>(0)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/get_flashmobs_from_friends_only")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getFlashmobsFromFriendsOnly(SettingsRequest request) {
        String methodName = "/settings/get_flashmobs_from_friends_only";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            SetSetting.performRequest(request, com.wds.define.Defines.SETTING_IS_GET_FLASHMOBS_FROM_FRIENDS_ONLY);
            return Response.ok(new ArrayList<>(0)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/close_profile")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response closeProfile(SettingsRequest request) {
        String methodName = "/settings/close_profile";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            SetSetting.performRequest(request, com.wds.define.Defines.SETTING_IS_CLOSE_PROFILE);
            return Response.ok(new ArrayList<>(0)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/default_task")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response defaultTask(SettingsRequest request) {
        String methodName = "/settings/default_task";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            SetSetting.performRequest(request, com.wds.define.Defines.SETTING_DEFAULT_TASK);
            return Response.ok(new ArrayList<>(0)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @GET
    @Path("/{uid}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response isUsersFriends(@Context UriInfo uriInfo) {
        String methodName = "/{uid}";

        MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();
        try {
            return Response.ok(GetSettings.performRequest(pathParams)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }
}
