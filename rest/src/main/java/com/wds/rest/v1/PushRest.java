package com.wds.rest.v1;

import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingParameterException;
import com.wds.json.v1.ErrorReturn;
import com.wds.json.v1.push.DeleteTokenRequest;
import com.wds.json.v1.push.SetTokenRequest;
import com.wds.method.v1.push.DeleteToken;
import com.wds.method.v1.push.SetToken;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.ArrayList;

@Path("v1/push")
@Produces(MediaType.APPLICATION_JSON)
public class PushRest {

    @POST
    @Path("/set_token")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response consumeJSON(SetTokenRequest request) {
        String methodName = "/push/set_token";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            SetToken.performRequest(request);
            return Response.ok(new ArrayList<>(0)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/delete_token")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response consumeJSON(DeleteTokenRequest request) {
        String methodName = "/push/set_token";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            DeleteToken.performRequest(request);
            return Response.ok(new ArrayList<>(0)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }
}
