package com.wds.rest.v1;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.FormDataParam;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingParameterException;
import com.wds.json.v1.ErrorReturn;
import com.wds.json.v1.flashmobs.interesting.FlashmobInterestingSearchRequest;
import com.wds.json.v1.user.changeEmail.UserChangeEmailAcceptRequest;
import com.wds.json.v1.user.changeEmail.UserChangeEmailRequest;
import com.wds.json.v1.user.changePassword.UserChangePasswordRequest;
import com.wds.json.v1.user.checkUserName.CheckUserNameRequest;
import com.wds.json.v1.user.complaint.SendComplaintRequest;
import com.wds.json.v1.user.forgotPassword.UserForgotPasswordRequest;
import com.wds.json.v1.user.login.UserLoginRequest;
import com.wds.json.v1.user.registration.UserRegistrationRequest;
import com.wds.json.v1.user.search.SearchByNicknameRequest;
import com.wds.json.v1.user.setUserName.SetUserNameRequest;
import com.wds.method.v1.flashmobs.FlashmobInterestingSearch;
import com.wds.method.v1.user.*;
import com.wds.utils.ApplicationUtility;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Path("v1/user")
@Produces(MediaType.APPLICATION_JSON)
public class UserRest {

    @POST
    @Path("/register")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response userRegister(UserRegistrationRequest request) {
        String methodName = "/user/register";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            return Response.ok(UserRegistration.performRequest(request)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response userLogin(UserLoginRequest request) {
        String methodName = "/user/login";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            return Response.ok(UserLogin.performRequest(request)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/forgot_password")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response userForgotPassword(UserForgotPasswordRequest request) {
        String methodName = "/user/forgot_password";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            UserForgotPassword.performRequest(request);
            return Response.ok(new ArrayList<>(0)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/change_password")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response userChangePassword(UserChangePasswordRequest request) {
        String methodName = "/user/change_password";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            UserChangePassword.performRequest(request);
            return Response.ok(new ArrayList<>(0)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/change_email")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response userChangeEmail(UserChangeEmailRequest request) {
        String methodName = "/user/change_email";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            UserChangeEmail.performRequest(request);
            return Response.ok(new ArrayList<>(0)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/change_email_accept")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response userChangeEmailAccept(UserChangeEmailAcceptRequest request) {
        String methodName = "/user/change_email_accept";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            UserChangeEmailAccept.performRequest(request);
            return Response.ok(new ArrayList<>(0)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @GET
    @Path("/settings/{uid}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getUserSettings(@Context UriInfo uriInfo) {
        String methodName = "/user/settings/{uid}";

        MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();
        try {
            return Response.ok(GetUserSettings.performRequest(pathParams)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/settings")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response userSettings(
            FormDataMultiPart multiPart,
            @FormDataParam("uid") Long uid,
            @FormDataParam("email") String email,
            @FormDataParam("name") String name,
            @FormDataParam("photo") InputStream photo,
            @FormDataParam("photo") FormDataContentDisposition photoDetail,
            @FormDataParam("gender") int gender,
            @FormDataParam("age") int age,
            @FormDataParam("about") String about,
            @FormDataParam("country") String country,
            @FormDataParam("city") String city,
            @FormDataParam("site") String site) {

        String methodName = "/user/settings";
        try {

            // Categories
            List<FormDataBodyPart> categories_idsList = multiPart.getFields("categories_ids");
            List<Long> categories_ids = new ArrayList<>();
            if (categories_idsList != null) {
                for (FormDataBodyPart field : categories_idsList) {
                    categories_ids.add(Long.valueOf(field.getValue()));
                }
            }

            // Languages
            List<FormDataBodyPart> languagesList = multiPart.getFields("languages");
            List<String> languages = new ArrayList<>();
            if (languagesList != null) {
                for (FormDataBodyPart field : languagesList) {
                    languages.add(field.getValue());
                }
            }

            String photoURL = "";

            if (photo != null) {
                ApplicationUtility.writeToFile(photo, com.wds.utils.define.Defines.getUsersPhotoPathStorage() + photoDetail.getFileName());
                photoURL = com.wds.define.Defines.getHostImages() + photoDetail.getFileName();
            }

            UserSettings.performRequest(uid, email, name, photoURL, gender, age, about,
                    country, city, site, categories_ids, languages);

            Map<String, String> returnObject = new HashMap<>();
            returnObject.put("photo", photoURL);

            List<Map> result = new ArrayList<>();
            result.add(returnObject);

            return Response.ok(result).build();

        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @GET
    @Path("/is_users_friends/{user1Id}/{user2Id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response isUsersFriends(@Context UriInfo uriInfo) {
        String methodName = "/is_users_friends/{user1Id}/{user2Id}";

        MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();
        try {
            return Response.ok(GetIsUsersFriends.performRequest(pathParams)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/send_complaint")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response sendComplaint(SendComplaintRequest request) {
        String methodName = "/user/send_complaint";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            SendComplaint.performRequest(request);
            return Response.ok(new ArrayList<>(0)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/check_user_name")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response checkUserName(CheckUserNameRequest request) {
        String methodName = "/user/check_user_name";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            return Response.ok(CheckUserName.performRequest(request)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/set_user_name")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response setUserName(SetUserNameRequest request) {
        String methodName = "/user/set_user_name";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            SetUserName.performRequest(request);
            return Response.ok(new ArrayList<>(0)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/deleted_flashmob_message_is_viewed/{uid}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deletedFlashmobMessageIsViewd(
            @PathParam("uid") long uid
    ) {
        String methodName = "v1/user/deleted_flashmob_message_is_viewed/{uid}";

        try {
            DeletedFlashmobMessageIsViewed.performRequest(uid);
            return Response.ok(new ArrayList<>(0)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/deleted_report_message_is_viewed/{uid}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deletedReportMessageIsViewd(
            @PathParam("uid") long uid
    ) {
        String methodName = "v1/user/deleted_report_message_is_viewed/{uid}";

        try {
            DeletedReportMessageIsViewed.performRequest(uid);
            return Response.ok(new ArrayList<>(0)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/searchByNickname")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response interestingSearch(SearchByNicknameRequest request) {
        String methodName = "/searchByNickname";

        try {
            return Response.ok(UserSearchByNickname.performRequest(request)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

}
