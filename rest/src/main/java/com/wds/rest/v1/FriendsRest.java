package com.wds.rest.v1;

import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingParameterException;
import com.wds.json.v1.ErrorReturn;
import com.wds.json.v1.friends.accept.FriendsAcceptRequest;
import com.wds.json.v1.friends.add.FriendsAddRequest;
import com.wds.json.v1.friends.delete.FriendsDeleteRequest;
import com.wds.json.v1.friends.upload.FriendsUploadRequest;
import com.wds.method.v1.friends.*;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.ArrayList;

@Path("v1/friends")
@Produces(MediaType.APPLICATION_JSON)
public class FriendsRest {

    @POST
    @Path("/upload")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response friendsUpload(FriendsUploadRequest request) {
        String methodName = "/friends/upload";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            FriendsUpload.performRequest(request);
            return Response.ok(new ArrayList<>(0)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @GET
    @Path("/{uid}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getFriends(@Context UriInfo uriInfo) {
        String methodName = "/friends/{uid}";

        MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();
        try {
            return Response.ok(GetFriends.performRequest(pathParams)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response friendsAdd(FriendsAddRequest request) {
        String methodName = "/friends/add";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            return Response.ok(FriendsAdd.performRequest(request)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/accept")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response friendsAccept(FriendsAcceptRequest request) {
        String methodName = "/friends/accept";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            FriendsAccept.performRequest(request);
            return Response.ok(new ArrayList<>(0)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @GET
    @Path("/follow/{uid}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getFriendsFollow(@Context UriInfo uriInfo) {
        String methodName = "/friends/follow/{uid}";

        MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();
        try {
            return Response.ok(GetFriendsFollow.performRequest(pathParams)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @GET
    @Path("/following/{uid}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getFriendsFollowing(@Context UriInfo uriInfo) {
        String methodName = "/friends/following/{uid}";

        MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();
        try {
            return Response.ok(GetFriendsFollowing.performRequest(pathParams)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response friendsDelete(FriendsDeleteRequest request) {
        String methodName = "/friends/delete";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            FriendsDelete.performRequest(request);
            return Response.ok(new ArrayList<>(0)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @GET
    @Path("/counters/{uid}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getCounters(@Context UriInfo uriInfo) {
        String methodName = "/counters/{uid}";

        MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();
        try {
            return Response.ok(GetFriendsCounters.performRequest(pathParams)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }
}
